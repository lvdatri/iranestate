<?php
	/**
	 * updates property_search table full text index
	 */

	ini_set('display_errors',1);
	error_reporting(E_ALL);
//	$file_path = dirname(__FILE__).'/';
//	$file_path = '/home/iranesta/public_html/';

	$file_path = '/home/iranestate/public_html/';
		
//	echo "\n".$file_path."\n";
//	exit;
	require_once $file_path.'AlProject/Application/Bl/Application.php';
	
	$application = new Bl_Application();
	$application->initialise();
	
	$si = new Bl_SearchIndex();
	$si->update();			
	$si->cleanUpPropertySearch();	
	
	$log = new Bl_Record_Log();
	$log->setType(Bl_Record_Log::$PROPERTY_SEARCH_MAINTENANCE);
	$log->save();