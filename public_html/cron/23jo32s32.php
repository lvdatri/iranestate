<?php
	/**
	 * set properties to sold that are over 1 year old
	 */

	ini_set('display_errors',1);
	error_reporting(E_ALL);
// 	$file_path = dirname(__FILE__).'/../';
	$file_path = '/home/iranestate/public_html/';
		

	require_once $file_path.'AlProject/Application/Bl/Application.php';
	
	$application = new Bl_Application();
	$application->initialise();
	
	$db = Al_Db::get_instance();
	$db->query("
		UPDATE properties SET
			property_status_id = '" . intval(Bl_Data_Statuses::BUY_SOLD) . "'
		WHERE expiry_date < NOW()
		AND temp = 0
		AND property_status_id = 2
		AND property_transaction_id <> '" . intval(Bl_Data_Transactions::FOR_BOOKING) . "'			
	");
	
	$log = new Bl_Record_Log();
	$log->setType(Bl_Record_Log::$EXPIRED_PROPERTY_SET_TO_SOLD);
	$log->save();