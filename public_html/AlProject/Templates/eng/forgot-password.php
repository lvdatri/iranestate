<script type="text/javascript">
	$(".greyBox").corner();

	$(document).ready(function(){
		$('#form-forgot-password').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
	});
</script>

<?php $this->message->show(); ?>
<div class="left-col">
<div class="greyBox" style=" margin-bottom:100px;" >    

<h1><?php echo $this->getStr(Al_Language::FORGOT_PWD_TITLE); ?></h1>
<?php if($this->emailSent) { ?>
<div>
	<?php echo $this->getStr(Al_Language::FORGOT_PWD_EMAIL_SENT_MESSAGE); ?>
</div>
<?php } else { ?>
<p><?php echo $this->getStr(Al_Language::FORGOT_PWD_DESCRIPTION); ?></p>
<table width="100%" border="0" style="margin-top:15px;">
  <tr>
    <td valign="top">
		<form action='<?php echo $this->pathModule; ?>forgot-password' method='post' id="form-forgot-password">
    	<table border="0" cellpadding="0" cellspacing="0">
        	<tr>
	            <td class="label" style="text-align:center; padding-top:10px;"><strong><?php echo $this->getStr(Al_Language::FORGOT_PWD_EMAIL); ?>:</strong></td>
	            <td class="field"><input name="email" type="text" id="email" value="<?php echo Al_Utilities::ePost('email'); ?>" size="30"></td>
            </tr>
        	<tr>
	            <td >&nbsp;</td>
	            <td ><input type="submit" name="login" value="<?php echo $this->getStr(Al_Language::FORGOT_PWD_BTN); ?>" /></td>
            </tr>
        </table>
		</form>
    </td>
  </tr>
</table>
<?php } ?>


</div>
</div>

