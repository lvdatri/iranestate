<div>
<table width="100%" border="0">
  <tr>
    <td><h6><?php echo $this->getStr(Al_Language::REGISTER_TITLE); ?></h6></td>
    <td><h6><?php echo $this->getStr(Al_Language::LOGIN_TITLE); ?></h6></td>
  </tr>
  <tr>
    <td>
		<form action='<?php echo $this->pathModule; ?>register' method='post' id="tmlt-frm-register">
    	<table border="0">
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_NAME); ?>: 
          <input name="popup" type="hidden" id="popup" value="1" /></td>
	            <td class="field"><input style="width:150px;" name="name" type="text" value="<?php echo Al_Utilities::ePost('name'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_EMAIL); ?>:</td>
	            <td class="field"><input style="width:150px;" name="email" type="text" id="email" value="<?php echo Al_Utilities::ePost('email'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_PASSWORD); ?>:</td>
	            <td class="field"><input style="width:150px;" name="password" type="password" value="<?php echo Al_Utilities::ePost('password'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_REPEAT_PASSWORD); ?>:</td>
	            <td class="field"><input style="width:150px;" name="password_confirm" type="password" id="password_confirm" value="<?php echo Al_Utilities::ePost('password_confirm'); ?>" ></td>
            </tr>
        	<tr>
	            <td  class="label" valign="middle"><?php echo $this->getStr(Al_Language::REGISTER_QUESTION); ?> <img src="<?php echo $this->pathModule.'register/captcha-popup'; ?>" border="0" /></td>
	            <td class="field"><input style="width:150px;"  name="captcha" type="text" id="captcha"  /></td>
            </tr>
        	<tr>
	            <td></td>
	            <td><input  type="submit" name="register" value="<?php echo $this->getStr(Al_Language::REGISTER_BTN_REGISTER); ?>"></td>
            </tr>
        </table>    
		</form>      
    </td>
    <td valign="top">
		<form action='<?php echo $this->pathModule; ?>login' method='post' id="tmlt-frm-login">
    	<table border="0">
        	<tr>
	            <td class="label" height="25"><?php echo $this->getStr(Al_Language::LOGIN_EMAIL); ?>:</td>
	            <td class="field"><input  style="width:150px;"  name="lemail" type="text" id="lemail" value="<?php echo Al_Utilities::ePost('lemail'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::LOGIN_PASSWORD); ?>:</td>
	            <td class="field"><input  style="width:150px;"  name="password" type="password" value="<?php echo Al_Utilities::ePost('password'); ?>" ></td>
            </tr>
        	<tr>
	            <td><input type="submit" name="login" value="<?php echo $this->getStr(Al_Language::LOGIN_BTN_LOGIN); ?>"></td>
	            <td><a href="<?php echo $this->pathModule; ?>forgot-password" style="color:#000;"><?php echo $this->getStr(Al_Language::LOGIN_FORGOT_PASSWORD); ?></a></td>
            </tr>
        </table>
		</form>
    </td>
  </tr>
</table>
</div>