<script type="text/javascript">
    $("#intro-box").corner();
    $(document).ready(function () {
            $("#mySale").tablesorter();
            $("#myRent").tablesorter();
            $("#myBooking").tablesorter();

        }
    );
</script>
<div class="single-column">
    <h1><?php echo $this->getStr(Al_Language::MY_ACCOUNT_HEADING); ?></h1>
    <?php $this->message->show(); ?>
    <?php $this->tabs->displayAccount(); ?>
    <div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;">
        <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_FOR_SALE); ?></h3>


        <?php if (count($this->properties[Bl_Data_Transactions::FOR_SALE]) > 0) { ?>
            <table id="mySale" class="tablesorter">
                <thead align="center">
                <tr height="28px" align="center" valign="middle">
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_REFERENCE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CREATED); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_EXPIRES); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_STATUS); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_TYPE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CITY); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_ZONE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_PRICE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_VIEWS); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CONTACT_US); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_EDIT); ?></th>
                </tr>
                </thead>
                <tbody style="font-size:11px;">
                <?php foreach ($this->properties[Bl_Data_Transactions::FOR_SALE] as $row) {
                    $this->property->loadFromArray($row); ?>
                    <tr>
                        <td><?php echo $this->property->getPropertyId(); ?></td>
                        <td><?php echo $this->property->getCreationDate(true); ?></td>
                        <td><?php echo $this->property->getExpiryDate(true); ?></td>
                        <td><?php echo $this->property->getStatus(); ?></td>
                        <td><?php echo $this->property->getPropertyType(); ?></td>
                        <td><?php echo $this->property->getCity(); ?></td>
                        <td><?php echo $this->property->getZone(); ?></td>
                        <td><?php echo $this->property->getPrice(true); ?></td>
                        <td><?php echo $this->property->getViewCount(); ?></td>
                        <td><?php echo $this->property->getContactCount(); ?></td>
                        <td nowrap>
                            <a href="<?php echo $this->property->getUrl(); ?>"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PREVIEW); ?></a> |
                            <a href="<?php echo $this->pathModule . "property/intro/id/" . $this->property->getId(); ?>"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_EDIT); ?></a> |
                            <a onclick="$.al.confirm('<?php echo $this->getStr(Al_Language::MY_ACCOUNT_MSG_DELETE_PROPERTY); ?>','<?php echo $this->pathModule . "property/delete/pid/" . $this->property->getId(); ?>');" href="#"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_DELETE); ?></a>
                        </td>
                    </tr>
                <?php } // end foreach ?>
                </tbody>
            </table>
        <?php } // end if ?>
        <input onclick="location='<?php echo $this->pathModule; ?>property/intro/new/<?php echo Bl_Data_Transactions::FOR_SALE; ?>';" name="addNew" type="button" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_ADD_NEW_PROPERTY); ?>">

        <div class="imgpad"></div>
        <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_FOR_RENT); ?></h3>

        <?php if (count($this->properties[Bl_Data_Transactions::FOR_RENT]) > 0) { ?>
            <table id="myRent" class="tablesorter">
                <thead align="center">
                <tr height="28px" align="center" valign="middle">
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_REFERENCE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CREATED); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_EXPIRES); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_STATUS); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_TYPE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CITY); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_ZONE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_PRICE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_VIEWS); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CONTACT_US); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_EDIT); ?></th>
                </tr>
                </thead>
                <tbody style="font-size:11px;">
                <?php foreach ($this->properties[Bl_Data_Transactions::FOR_RENT] as $row) {
                    $this->property->loadFromArray($row); ?>
                    <tr>
                        <td><?php echo $this->property->getPropertyId(); ?></td>
                        <td><?php echo $this->property->getCreationDate(true); ?></td>
                        <td><?php echo $this->property->getExpiryDate(true); ?></td>
                        <td><?php echo $this->property->getStatus(); ?></td>
                        <td><?php echo $this->property->getPropertyType(); ?></td>
                        <td><?php echo $this->property->getCity(); ?></td>
                        <td><?php echo $this->property->getZone(); ?></td>
                        <td><?php echo $this->property->getPrice(true); ?></td>
                        <td><?php echo $this->property->getViewCount(); ?></td>
                        <td><?php echo $this->property->getContactCount(); ?></td>
                        <td nowrap>
                            <a href="<?php echo $this->property->getUrl(); ?>"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PREVIEW); ?></a> |
                            <a href="<?php echo $this->pathModule . "property/intro/id/" . $this->property->getId(); ?>"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_EDIT); ?></a> |
                            <a onclick="$.al.confirm('<?php echo $this->getStr(Al_Language::MY_ACCOUNT_MSG_DELETE_PROPERTY); ?>','<?php echo $this->pathModule . "property/delete/pid/" . $this->property->getId(); ?>');" href="#"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_DELETE); ?></a>
                        </td>
                    </tr>
                <?php } // end foreach ?>
                </tbody>
            </table>
        <?php } // end if ?>
        <input onclick="location='<?php echo $this->pathModule; ?>property/intro/new/<?php echo Bl_Data_Transactions::FOR_RENT; ?>';" name="addNew" type="button" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_ADD_NEW_PROPERTY); ?>">

        <div class="imgpad"></div>
        <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_FOR_BOOKING); ?></h3>

        <?php if (count($this->properties[Bl_Data_Transactions::FOR_BOOKING]) > 0) { ?>
            <table id="myBooking" class="tablesorter">
                <thead align="center">
                <tr height="28px" align="center" valign="middle">
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_REFERENCE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CREATED); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_EXPIRES); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_STATUS); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_TYPE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CITY); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_ZONE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_PRICE); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_VIEWS); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_CONTACT_US); ?></th>
                    <th><?php echo $this->getStr(Al_Language::MY_ACCOUNT_TABLE_EDIT); ?></th>
                </tr>
                </thead>
                <tbody style="font-size:11px;">
                <?php foreach ($this->properties[Bl_Data_Transactions::FOR_BOOKING] as $row) {
                    $this->property->loadFromArray($row); ?>
                    <tr>
                        <td><?php echo $this->property->getPropertyId(); ?></td>
                        <td><?php echo $this->property->getCreationDate(true); ?></td>
                        <td><?php echo $this->property->getExpiryDate(true); ?></td>
                        <td><?php echo $this->property->getStatus(); ?></td>
                        <td><?php echo $this->property->getPropertyType(); ?></td>
                        <td><?php echo $this->property->getCity(); ?></td>
                        <td><?php echo $this->property->getZone(); ?></td>
                        <td><?php echo $this->property->getPrice(true); ?></td>
                        <td><?php echo $this->property->getViewCount(); ?></td>
                        <td><?php echo $this->property->getContactCount(); ?></td>
                        <td nowrap>
                            <a href="<?php echo $this->property->getUrl(); ?>"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PREVIEW); ?></a> |
                            <a href="<?php echo $this->pathModule . "property/intro/id/" . $this->property->getId(); ?>"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_EDIT); ?></a> |
                            <a onclick="$.al.confirm('<?php echo $this->getStr(Al_Language::MY_ACCOUNT_MSG_DELETE_PROPERTY); ?>','<?php echo $this->pathModule . "property/delete/pid/" . $this->property->getId(); ?>');" href="#"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_DELETE); ?></a>
                        </td>
                    </tr>
                <?php } // end foreach ?>
                </tbody>
            </table>
        <?php } // end if ?>
        <input onclick="location='<?php echo $this->pathModule; ?>property/intro/new/<?php echo Bl_Data_Transactions::FOR_BOOKING; ?>';" name="addNew" type="button" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_ADD_NEW_PROPERTY); ?>">
    </div>
    <?php $this->tabs->displayEnd(); ?>
</div>