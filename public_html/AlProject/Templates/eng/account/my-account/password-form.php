<script type="text/javascript" >

$("#intro-box").corner();

$(document).ready(function() {
	$('#frmR').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
});
</script>
<div class="single-column" >
<h1><?php echo $this->getStr(Al_Language::MY_ACCOUNT_HEADING); ?></h1> 
<?php $this->message->show(); ?>
<?php $this->tabs->displayAccount(); ?>
<div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;" >
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_UPDATE_ACCOUNT_PASSWORD); ?></h3>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="label"  width="13%" nowrap><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_CURRENT_PASSWORD); ?>:</td>
                <td class="field" width="87%"><input class="frmF" name="current_password" type="password" id="current_password" size="30" /></td>
            </tr>
            <tr>
                <td class="label"   width="13%" nowrap><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_NEW_PASSWORD); ?>:</td>
                <td class="field"  width="87%"><input class="frmF" name="password" type="password" id="password" size="30" /></td>
            </tr>
            <tr>
                <td class="label"   width="13%" nowrap><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_NEW_PASSWORD_CONFIRM); ?>:</td>
                <td class="field"  width="87%"><input class="frmF" name="password_confirm" type="password" id="password_confirm" size="30" /></td>
            </tr>
            <tr>
                <td colspan="2">
                
                      <input type="submit" name="button" id="button" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_SAVE); ?>" />
                      <input onclick="location='<?php echo $this->form->getCancelAction(); ?>'"  type="button" name="button2" id="button2" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_CANCEL); ?>" />
                </td>
            </tr>
        </table>
    </div>
</form>
<?php $this->tabs->displayEnd(); ?>
</div>