<script type="text/javascript">
    $("#intro-box").corner();

</script>
<div class="single-column">
    <h1><?php echo $this->getStr(Al_Language::MY_ACCOUNT_HEADING); ?></h1>
    <?php $this->message->show(); ?>
    <?php $this->tabs->displayAccount(); ?>
    <div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;">

        <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_ACCOUNT_DETAILS); ?></h3>

        <table class="account-view" cellpadding="5px;">
            <tbody>
            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_EMAIL); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getEmail(); ?></td>
            </tr>




            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_RECEIVE_EMAILS); ?>:</strong></td>
                <td><?php echo $this->member->getNewsletterSubscription(true); ?></td>
            </tr>





            </tbody>
        </table>
        <div class="imgpad"></div>
        <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_SUB_PROFILE_DETAILS); ?></h3>

        <table class="account-view" cellpadding="5px;">
            <tbody>
            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_SHOW_PROFILE); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getShowProfile(true); ?></td>
            </tr>


            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_ACCOUNT_NAME); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getName(); ?></td>
            </tr>
            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_ADDRESS); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getAddress(); ?></td>
            </tr>
            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_CITY); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getCity(); ?></td>
            </tr>
            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_POSTCODE); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getPostcode(); ?></td>
            </tr>
            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PHONE); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getPhone(); ?></td>
            </tr>

            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_COMPANY_NAME); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getCompanyName(); ?></td>
            </tr>

            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PROFILE_EMAIL); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getProfileEmail(); ?></td>
            </tr>


            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_WEBSITE); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getWebsite(); ?></td>
            </tr>

            <tr>
                <td>
                    <strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PROFILE_DESCRIPTION_ENG); ?>:&nbsp;</strong>
                </td>
                <td><?php echo $this->member->getProfileDescriptionEng(); ?></td>
            </tr>
            <tr>
                <td>
                    <strong><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PROFILE_DESCRIPTION_PER); ?>:&nbsp;</strong>
                </td>
                <td><?php echo $this->member->getProfileDescriptionPer(); ?></td>
            </tr>


            </tbody>
        </table>


        <div class="imgpad"></div>
        <table class="account-view" cellpadding="5px;">
            <tbody>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input onClick="location='<?php echo $this->pathController; ?>update-details';" name="Button" type="button" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_UPDATE); ?>">
                    <input name="Button" type="button" onClick="location='<?php echo $this->pathController; ?>change-password';" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_CHANGE_PASSWORD); ?>">
                </td>
            </tr>
            </tbody>
        </table>


    </div>
    <?php $this->tabs->displayEnd(); ?>
</div>