<script type="text/javascript">

    $("#intro-box").corner();

    $(document).ready(function () {
        $('#frmR').jqTransform({imgPath: '<?php echo $this->_settings->path_images; ?>form/'});
    });
</script>
<div class="single-column">
    <h1><?php echo $this->getStr(Al_Language::MY_ACCOUNT_HEADING); ?></h1>
    <?php $this->tabs->displayAccount(); ?>

    <?php $this->message->show(); ?>
    <div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;">


        <form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm">
            <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_UPDATE_ACCOUNT_DETAILS); ?></h3>


            <table class="frm" width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_EMAIL); ?>:</td>
                    <td class="field" width="87%">
                        <input name="email" type="text" id="email" value="<?php echo $this->form->getEmail(); ?>" size="30"/>
                    </td>
                </tr>

                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_RECEIVE_EMAILS); ?>:</td>
                    <td class="field" width="87%">
                        <input name="newsletter_subscription" type="checkbox" id="newsletter_subscription" value="1" <?php echo $this->form->cbNewsletterSubscription(); ?> />
                    </td>
                </tr>

            </table>

            <div class="imgpad"></div>
            <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_SUB_PROFILE_DETAILS); ?></h3>
            <table class="frm" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_SHOW_PROFILE); ?>:</td>
                    <td class="field" width="87%"><input name="show_profile" type="checkbox" id="show_profile" value="1" <?php echo $this->form->cbShowProfile(); ?> /></td>
                </tr>

                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_ACCOUNT_NAME); ?>:</td>
                    <td class="field" width="87%">
                        <input name="name" type="text" id="name" value="<?php echo $this->form->getName(); ?>" size="30"/>
                    </td>
                </tr>

                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_COMPANY_NAME); ?>:</td>
                    <td class="field" width="87%"><input class="frmF" name="company_name" type="text" id="company_name" value="<?php echo $this->form->getCompanyName(); ?>" size="30" /></td>
                </tr>

                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_ADDRESS); ?>:</td>
                    <td class="field" width="87%">
                        <input name="address" type="text" id="address" value="<?php echo $this->form->getAddress(); ?>" size="30"/>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_CITY); ?>:</td>
                    <td class="field" width="87%">
                        <input name="city" type="text" id="city" value="<?php echo $this->form->getCity(); ?>" size="30"/>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_POSTCODE); ?>:</td>
                    <td class="field" width="87%">
                        <input name="postcode" type="text" id="postcode" value="<?php echo $this->form->getPostcode(); ?>" size="30"/>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PHONE); ?>:</td>
                    <td class="field" width="87%">
                        <input name="phone" type="text" id="phone" value="<?php echo $this->form->getPhone(); ?>" size="30"/>
                    </td>
                </tr>





                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PROFILE_EMAIL); ?>:</td>
                    <td class="field" width="87%"><input class="frmF" name="profile_email" type="text" id="profile_email" value="<?php echo $this->form->getProfileEmail(); ?>" size="30" /></td>
                </tr>



                <tr>
                    <td class="label" width="13%"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_WEBSITE); ?>:</td>
                    <td class="field" width="87%"><input class="frmF" name="website" type="text" id="website" value="<?php echo $this->form->getWebsite(); ?>" size="30" /></td>
                </tr>


            </table>



            <div class="imgpad"></div>
            <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::MY_ACCOUNT_SUB_PROFILE_DESCRIPTION); ?></h3>

            <table width="200" border="0">
                <tr>
                    <td class="label"><label><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PROFILE_DESCRIPTION_ENG); ?>:</label></td>
                    <td class="label"><label><?php echo $this->getStr(Al_Language::MY_ACCOUNT_LBL_PROFILE_DESCRIPTION_PER); ?>:</label></td>
                </tr>
                <tr>
                    <td class="field" style="padding:3px 0px;"><textarea   name="profile_description_eng" cols="45" rows="6" id="profile_description_eng"><?php echo $this->form->getProfileDescriptionEng(); ?></textarea></td>
                    <td class="field" style="padding:3px 6px;"><textarea   name="profile_description_per" cols="45" rows="6" id="profile_description_per"><?php echo $this->form->getProfileDescriptionPer(); ?></textarea></td>
                </tr>

            </table>

            <div class="imgpad"></div>
            <table class="frm" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">
                        <input type="submit" name="button" id="button" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_SAVE); ?>"/>
                        <input onclick="location='<?php echo $this->form->getCancelAction(); ?>'" type="button" name="button2" id="button2" value="<?php echo $this->getStr(Al_Language::MY_ACCOUNT_BTN_CANCEL); ?>"/>
                    </td>
                </tr>
            </table>


        </form>
    </div>
    <?php $this->tabs->displayEnd(); ?>
</div>