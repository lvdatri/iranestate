	<script type="text/javascript">
	
		
	$(".detail-left-col").corner();
	$(".detail-info-block").corner();
	$(".detail-icon-block").corner();
	$(".propertyContact").corner();
	$(".friendDetails").corner();	
	$(".page-nav").corner();	

	$(document).ready(function(){
		
		$('#slick-toggle').click(function() {
			var path = '<?php echo $this->pathController.'contact-count/id/'.$this->property->getId(); ?>';
			$.get(path);
		});	 
		
		<?php if(!isset($_POST['contact'])) { ?>
			$('#slickbox').hide();
		<?php } ?>
		$('a#slick-show').click(function() {
			$('#slickbox').show('slow');
			return false;
		});
		$('a#slick-hide').click(function() {
			$('#slickbox').hide('fast');
			return false;
		});
		$('a#slick-toggle').click(function() {
			$('#slickbox').toggle(400);
			return false;
		});		
		
		
		<?php if(!isset($_POST['tofriend'])) { ?>
			$('#slickbox2').hide();
		<?php } ?>
		$('a#slick-show').click(function() {
			$('#slickbox2').show('slow');
			return false;
		});
	
		$('a#slick-hide').click(function() {
			$('#slickbox2').hide('fast');
			return false;
		});
		$('a#slick-toggle2').click(function() {
			$('#slickbox2').toggle(400);
			return false;
		});			
		
		
		$('#contactForm').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
		$('#friendForm').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});

		$('#sent-to-friend').click(function(event) {
			event.preventDefault();
			$('#friendForm').submit();
		});

		$('#detail-update').click(function(event) {
			event.preventDefault();
			$('#contactForm').submit();
		});

	});
	
	</script>
  <div class="page-nav">
    <div class="left-link"><a href="<?php echo $this->pathController.$this->transactionPath.'/'.$this->propertySearch->getSearchParams(); ?>"><?php echo $this->getStr(Al_Language::LISTING_BACK_TO_SEARCH_RESULTS); ?></a></div>
    <div class="right-link">
    <a href="<?php echo $this->pathController.$this->transactionPath.'/search/1/';?>"><?php echo $this->property->getTransactionType(); ?></a> | <a href="<?php echo $this->pathController.$this->transactionPath.'/search/1/type/'.$this->property->getPropertyTypeId().'/';?>"><?php echo $this->property->getPropertyType(); ?></a> | <a href="<?php echo $this->pathController.$this->transactionPath.'/search/1/location/'.$this->property->getCity(); ?>"><?php echo $this->property->getCity(); ?></a>
    </div>
  </div>
<?php $this->message->show(); ?>  
  
  
    <div class="detail-left-col" style="width:504px; margin-bottom:15px;">
	<div class="propertyGalleryContainer">
	<!-- Gallery -->    
        <div id="gallery">
          <div id="imagearea">
            <div id="image">
              <a href="javascript:slideShow.nav(-1)" class="imgnav " id="previmg"></a>
              <a href="javascript:slideShow.nav(1)" class="imgnav " id="nextimg"></a>
            </div>
          </div>
          
          
          
          <div id="thumbwrapper">
            <div id="thumbarea">
              <ul id="thumbs">
				<?php $i = 1; foreach($this->images as $image) { ?>
                <li title="<?php echo $image['path_med']; ?>" value="<?php echo 'bb'.$image['id']; ?>"><img src="<?php echo $image['path_th']; ?>" /></li>
                <?php $i++; } ?>
              </ul>
            </div>
          </div>
        </div>
      <script type="text/javascript">
			var imgid = 'image';
			var imgdir = 'fullsize';
			var imgext = '.jpg';
			var thumbid = 'thumbs';
			var auto = true;
			var autodelay = 5;
        </script>
	  <script type='text/javascript' src='<?php echo $this->_settings->path_web_ssl; ?>js/jquery/slide.js'></script>
	<!-- Gallery -->    
    </div>      
      
      
      
      

            <div class="detail-icon-block">
                <img src="<?php echo $this->_settings->path_images; ?>ico-map.png" width="32" height="32" alt="map" />
                <p><?php echo $this->getStr(Al_Language::LISTING_AREA_MAP); ?></p>	
            </div>           
            
  <?php if($this->property->getLat() != '') { ?>
	<div class="propertyMapContainer">
	<!-- map -->
	  <script type="text/javascript"> 

			$(document).ready(function() { 
					var gmap_map = jQuery.al.gmap_set(
						'map_canvas',
						<?php echo $this->property->getLat(); ?>,
						<?php echo $this->property->getLon(); ?>,
						<?php echo $this->property->getDepth(); ?>
					);
					
				  	var gmap_marker = jQuery.al.gmap_setMarker (
						gmap_map,
						<?php echo $this->property->getLat(); ?>,
						<?php echo $this->property->getLon(); ?>,
						'<?php echo $this->_settings->path_images; ?>marker.png',
						'<?php echo $this->_settings->path_images; ?>shadow.png',
						false
					);			
			}); 

        </script> 
        <div id="map_canvas" style="width: 505px; height: 300px"></div> 
    <!-- map -->    
    </div>
    <?php } ?>
            
      
      </div>
      
      <div class="detail-right-col" style="width:374px;">
            <div class="detail-title" style="margin-bottom:10px; height:30px;">
                <p><?php echo $this->property->getPropertyType(); ?>&nbsp;
        <?php echo $this->property->getTransactionType(); ?> </p>
            </div>
            
            <div class="detail-icons" >
                <ul>
                    <?php if($this->property->getBedrooms() > 0) { ?>
                    <li><img src="<?php echo $this->_settings->path_images; ?>ico-bed-big.png" width="28" height="29" alt="room size" />
                        <p><?php echo $this->property->getBedrooms(); ?> </p>
                    </li>
                    <?php } ?>
					<?php if($this->property->getBathrooms() > 0) { ?>
                    <li><img src="<?php echo $this->_settings->path_images; ?>ico-bath-big.png" width="28" height="29" alt="room size" />
                        <p><?php echo $this->property->getBathrooms(); ?> </p>
                    </li>
                    <?php } ?>
                    <?php if($this->property->getParking() > 0) { ?>
                    <li><img src="<?php echo $this->_settings->path_images; ?>ico-garage-big.png" width="28" height="29" alt="room size" />
                        <p><?php echo $this->property->getParking(); ?> </p>
                    </li>
                    <?php } ?>
                    
                    <?php if($this->property->getLandArea() > 0) { ?>
                    <li><img src="<?php echo $this->_settings->path_images; ?>ico-roomsize-big.png" width="28" height="29" alt="room size" />
                        <p><?php echo $this->property->getLandArea(); ?>m<span>2</span></p>
                    </li>
                    <?php } ?>

                </ul>
            </div>
            
            <div class="detail-info-block" style="clear:both;">
<table border="0" cellpadding="5" cellspacing="0" width="100%">
	<tr>
      <td>
      
	<?php if($this->property->getPropertyStatusId()==Bl_Data_Statuses::BUY_SOLD) { ?> 
        <p class="propertyPrice"><strong><?php echo $this->getStr(Al_Language::FLD_PROP_PRICE); ?>: </strong><br/>
            <div style="padding-left:36px; "> <?php echo $this->property->getPrice(true); ?></div>
        </p>
	<?php } else { ?>
        <p class="propertyPrice"><strong><?php echo $this->getStr(Al_Language::FLD_PROP_PRICE); ?>: </strong><br/>
            <div style="padding-left:36px; "> <?php echo $this->property->getPrice(true); ?> <?php if($this->property->getPrice()>0) { echo 'Toman'; } ?> </div>
            <?php if($this->property->getPrice()>0) { ?>  
            <div style="padding-left:36px;"><?php echo $this->property->getPriceUsd(true); ?> $USD</div>
            <div style="padding-left:36px;"><?php echo $this->property->getPriceGbp(true); ?> &#163;GBP</div>
            <div style="padding-left:36px;"><?php echo $this->property->getPriceAud(true); ?> $AUD</div>
            <?php } ?>
        </p>
        
        <?php if($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_RENT && $this->property->getDeposit()>0) { ?>         
            <p class="propertyPrice" style="padding-top:5px;" ><strong><?php echo $this->getStr(Al_Language::FLD_PROP_DEPOSIT); ?>: </strong><br/>
                <div style="padding-left:36px;">
                <?php echo $this->property->getDeposit(true); ?> Toman
                </div>
                <div style="padding-left:36px;"><?php echo $this->property->getDepositUsd(true); ?> $USD</div>
                <div style="padding-left:36px;"><?php echo $this->property->getDepositGbp(true); ?> &#163;GBP</div>
                <div style="padding-left:36px;"><?php echo $this->property->getDepositAud(true); ?> $AUD</div>
            </p>        
        <?php } ?>
	<?php } ?>
        
        
      </td>
    </tr>
	<tr>
      <td><p class="propertyIntroduction"><strong><?php echo $this->getStr(Al_Language::FLD_PROP_INTRODUCTION); ?>: </strong><?php echo $this->property->getIntroduction(true); ?></p></td>
    </tr>
	<tr>
      <td><p class="propertyFeatures"><strong><?php echo $this->getStr(Al_Language::FLD_PROP_FEATURES); ?>: </strong><?php echo $this->property->getFeatures(true); ?></p></td>
    </tr>

    	<?php if($this->property->getLandArea()>0) { ?>
		    <tr>
		      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_LAND_AREA); ?>: </strong><?php echo $this->property->getLandArea(); ?> <?php echo $this->getStr(Al_Language::FLD_PROP_METERS); ?></td></tr>   
        <?php } ?>  
    	<?php if($this->property->getBuildArea()>0) { ?>
		    <tr>
		      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_BUILD_AREA); ?>: </strong><?php echo $this->property->getBuildArea(); ?> <?php echo $this->getStr(Al_Language::FLD_PROP_METERS); ?></td></tr>   
        <?php } ?>  

    	<?php if($this->property->getTerraceM2()>0) { ?>
		    <tr>
		      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_TERRACE); ?>: </strong><?php echo $this->property->getTerraceM2(); ?> <?php echo $this->getStr(Al_Language::FLD_PROP_METERS); ?></td></tr>   
        <?php } ?>
    	<?php if($this->property->getLandWidth()>0) { ?>
		    <tr>
		      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_LAND_WIDTH); ?>: </strong><?php echo $this->property->getLandWidth(); ?> <?php echo $this->getStr(Al_Language::FLD_PROP_METERS); ?></td></tr>   
        <?php } ?>
    	<?php if($this->property->getLandHeight()>0) { ?>
		    <tr>
		      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_LAND_HEIGHT); ?>: </strong><?php echo $this->property->getLandHeight(); ?> <?php echo $this->getStr(Al_Language::FLD_PROP_METERS); ?></td></tr>   
        <?php } ?>
    	<?php if($this->property->getAverageCeilingHeight()>0) { ?>
		    <tr>
		      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_AVERAGE_CEILING_HEIGHT); ?>: </strong><?php echo $this->property->getAverageCeilingHeight(); ?> <?php echo $this->getStr(Al_Language::FLD_PROP_METERS); ?></td></tr>   
        <?php } ?>
    	<?php if($this->property->getLocatedOnLevel()>0) { ?>
		    <tr><td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_LOCATED_ON_LEVEL); ?>: </strong><?php echo $this->property->getLocatedOnLevel(); ?></td></tr>   
        <?php } ?>
    	<?php if($this->property->getNumberOfLevels()>0) { ?>
		    <tr><td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_NUM_OF_LEVELS); ?>: </strong><?php echo $this->property->getNumberOfLevels(); ?></td></tr>   
        <?php } ?>
    	<?php if($this->property->getCeilingHeight()>0) { ?>
		    <tr>
		      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_CEILING_HEIGHT); ?>: </strong><?php echo $this->property->getCeilingHeight(); ?> <?php echo $this->getStr(Al_Language::FLD_PROP_METERS); ?></td></tr>   
        <?php } ?>

	<?php if($this->property->getComments() != '') { ?>    
	<tr>
      <td>
       <p class="propertyComments"><strong><?php echo $this->getStr(Al_Language::FLD_PROP_COMMENTS); ?>: </strong><?php echo $this->property->getComments(); ?></p>
      </td>
    </tr>
    <?php } ?>    
	<?php if($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_SALE) { ?>

                                       
    <?php } ?>
     
	<tr>
      <td>
		<p id="address"><strong><?php echo $this->getStr(Al_Language::FLD_PROP_ADDRESS); ?>: </strong><?php echo $this->property->getAddress() .', ' . $this->property->getZone() . ' '. $this->property->getCountry() . ' '.$this->property->getPostalCode(); ?></p>      
      </td>
    </tr>
	<tr>
      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_VIEWED); ?>:</strong> <?php echo $this->property->getViewCount(); ?></td>
    </tr>
	<tr>
      <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_LISTED_DATE); ?>:</strong> <?php echo $this->property->getCreationDate(true); ?></td>
    </tr>
	<tr>
	  <td><strong><?php echo $this->getStr(Al_Language::FLD_PROP_PROPERTY_ID); ?>:</strong> <?php echo $this->property->getPropertyId(); ?></td>
    </tr>
    <?php if($this->member->getShowProfile()=='1') { ?>
    <tr>
        <td>
            <strong><a style="color: #093;" href="<?php echo $this->member->getMemberUrl(); ?>">
                    <?php echo $this->getStr(Al_Language::LISTING_BTN_VIEW_AGENT_PROFILE); ?>
                </a></strong>
        </td>
    </tr>
    <?php } ?>
</table>
                <div class="detail-buttons">
                  <a href="#" class="contact-agent-butt"  id="slick-toggle" ><?php echo $this->getStr(Al_Language::LISTING_CONTACT_AGENT); ?></a>
                  <a href="#" class="send-to-butt" id="slick-toggle2"><?php echo $this->getStr(Al_Language::LISTING_SEND_TO_FRIEND); ?></a>
                  <a href="#" class="print-butt" onclick="window.print();return false;"><?php echo $this->getStr(Al_Language::LISTING_PRINT_PAGE); ?></a>                            
                </div>

    </div>
<div class="propertyContact" id="slickbox">
        <h6><?php echo $this->getStr(Al_Language::LISTING_CONTACT_AGENT); ?></h6>
    	<form id="contactForm" name="contactForm" action="<?php echo $this->property->getUrl(); ?>" method="post"><input name="contact" type="hidden" value="1" />
        <table width="100%" border="0">
          <tr>
            <td valign="top"><strong><?php echo $this->getStr(Al_Language::LISTING_CA_NAME); ?></strong>:</td>
            <td class="field"><?php echo $this->property->getContactName(); ?></td>
          </tr>
          <tr>
            <td valign="top" ><strong><?php echo $this->getStr(Al_Language::LISTING_CA_EMAIL); ?></strong>: </td>
            <td class="field"><?php echo $this->property->getContactEmail(); ?></td>
          </tr>
          <tr>
            <td valign="top" ><strong><?php echo $this->getStr(Al_Language::LISTING_CA_PHONE); ?></strong>: </td>
            <td class="field"><?php echo $this->property->getContactPhone(); ?></td>
          </tr>
          <tr>
            <td width="35%" valign="top" class="label"><?php echo $this->getStr(Al_Language::LISTING_CA_YOUR_NAME); ?>:</td>
            <td width="65%" class="field"><input name="contact_name" type="text" id="contact_name" value="<?php echo Al_Utilities::ePost('contact_name'); ?>" /></td>
          </tr>
          <tr>
            <td valign="top" class="label"><?php echo $this->getStr(Al_Language::LISTING_CA_YOUR_EMAIL); ?>:</td>
            <td  class="field" ><input name="contact_email" type="text" id="contact_email" value="<?php echo Al_Utilities::ePost('contact_email'); ?>"/></td>
          </tr>
          <tr>
            <td valign="top" class="label"><?php echo $this->getStr(Al_Language::LISTING_CA_YOUR_PHONE_NUMBER); ?>:</td>
            <td class="field"><input name="contact_phone" type="text" id="contact_phone" value="<?php echo Al_Utilities::ePost('contact_phone'); ?>" /></td>
          </tr>
          <tr>
            <td valign="top" class="label"><?php echo $this->getStr(Al_Language::LISTING_CA_SUBJECT); ?>:</td>
            <td class="field"><input name="contact_subject" type="text" id="contact_subject" value="<?php echo Al_Utilities::ePost('contact_subject','Property - '.$this->property->getCity()); ?>"/></td>
          </tr>
          <tr>
            <td valign="top" class="label"><?php echo $this->getStr(Al_Language::LISTING_CA_DESCRIPTION); ?>:</td>
            <td class="field"><textarea name="contact_description" id="contact_description" ><?php echo Al_Utilities::ePost('contact_description'); ?></textarea></td>
          </tr>
          <tr>
            <td valign="top" nowrap="nowrap" class="label"><?php echo $this->getStr(Al_Language::LISTING_CA_QUESTION); ?> <img src="<?php echo $this->pathController.'contact-captcha'; ?>" border="0" /></td>
            <td class="field"><input name="contact_captcha" type="text" id="contact_captcha" /></td>
          </tr>
          <tr>
            <td></td>
            <td>
	            <input name="" type="submit" value="<?php echo $this->getStr(Al_Language::LISTING_CA_SEND); ?>" />
            </td>
          </tr>
        </table>
        </form>
              </div>
              
              
              
<div class="friendDetails" id="slickbox2">
    	<form action="<?php echo $this->property->getUrl(); ?>" name="friendForm" id="friendForm" method="post"><input name="tofriend" type="hidden" value="1" />
        <h6 class="headings"><?php echo $this->getStr(Al_Language::LISTING_SEND_TO_FRIEND); ?></h6>
        <table width="100%" border="0">
          <tr>
            <td class="label"><?php echo $this->getStr(Al_Language::LISTING_SF_YOUR_NAME); ?>:</td>
            <td class="field"><input name="friend_your_name" type="text" id="friend_your_name" value="<?php echo Al_Utilities::ePost('friend_your_name'); ?>" /></td>
          </tr>
          <tr>
            <td class="label"><?php echo $this->getStr(Al_Language::LISTING_SF_YOUR_EMAIL); ?>:</td>
            <td class="field"><input name="friend_your_email" type="text" id="friend_your_email" value="<?php echo Al_Utilities::ePost('friend_your_email'); ?>" /></td>
          </tr>
          <tr>
            <td class="label"><?php echo $this->getStr(Al_Language::LISTING_SF_FRIENDS_NAME); ?>:</td>
            <td class="field"><input name="friend_friends_name" type="text" id="friend_friends_name" value="<?php echo Al_Utilities::ePost('friend_friends_name'); ?>" /></td>
          </tr>
          <tr>
            <td class="label"><?php echo $this->getStr(Al_Language::LISTING_SF_FRIENDS_EMAIL); ?>:</td>
            <td class="field"><input name="friend_friends_email" type="text" id="friend_friends_email" value="<?php echo Al_Utilities::ePost('friend_friends_email'); ?>" /></td>
          </tr>
          <tr>
            <td class="label"><?php echo $this->getStr(Al_Language::LISTING_SF_SUBJECT); ?>:</td>
            <td class="field"><input name="friend_friends_subject" type="text" id="friend_friends_subject" value="<?php echo Al_Utilities::ePost('friend_friends_subject','Property - '.$this->property->getCity()); ?>"/></td>
          </tr>
          <tr>
            <td class="label"><?php echo $this->getStr(Al_Language::LISTING_SF_DESCRIPTION); ?>:</td>
            <td class="field"><textarea name="friend_description" id="friend_description"><?php echo Al_Utilities::ePost('friend_description'); ?></textarea></td>
          </tr>
          <tr>
            <td class="label"><?php echo $this->getStr(Al_Language::LISTING_SF_QUESTION); ?> <img src="<?php echo $this->pathController.'friend-captcha'; ?>" border="0" /></td>
            <td class="field"><input name="friend_captcha" type="text" id="friend_captcha" value="" /></td>
          </tr>
          <tr>
            <td></td>
            <td>
            <input name="" type="submit" value="<?php echo $this->getStr(Al_Language::LISTING_SF_SEND); ?>" />
            </td>
          </tr>
        </table>
        </form>                     
            
      </div>

<!--      
<div id="adsense" style="width:100%; text-align:center;">
	<script type="text/javascript"><!--
    google_ad_client = "ca-pub-7939109411571734";
    /* iranestate */
    google_ad_slot = "2626385086";
    google_ad_width = 336;
    google_ad_height = 280;
    //-- >
    </script>
    <script type="text/javascript"
    src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
</div>
-->

<?php if($this->_settings->languagePrefix == Bl_Settings::LANG_PERSIAN_PREFIX) { ?>
<div style="width:100%; height:280px; margin:2px; text-align:center;">
<script src="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=rsb&c=28&pli=8834957&PluID=0&w=335&h=280&ord=[timestamp]&ucm=true"></script>
<noscript>
<a href="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=brd&FlightID=8834957&Page=&PluID=0&Pos=1902294517" target="_blank"><img src="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=bsr&FlightID=8834957&Page=&PluID=0&Pos=1902294517" border=0 width=335 height=280></a>
</noscript>
</div>
<!--
<div style="width:100%; height:280px; margin:2px; text-align:center;">
	<a href="http://iransavar.com/fa/type/car" title="&#1580;&#1587;&#1578;&#1580;&#1608; &#1601;&#1585;&#1608;&#1588; &#1608; &#1582;&#1585;&#1740;&#1583; &#1575;&#1578;&#1608;&#1605;&#1576;&#1740;&#1604; &#1607;&#1575;&#1740; &#1580;&#1583;&#1740;&#1583; &#1608; &#1605;&#1608;&#1585;&#1583; &#1575;&#1587;&#1578;&#1601;&#1575;&#1583;&#1607; &#1583;&#1585; &#1605;&#1588;&#1607;&#1583; &#1575;&#1589;&#1601;&#1607;&#1575;&#1606; &#1588;&#1740;&#1585;&#1575;&#1586; &#1578;&#1607;&#1585;&#1575;&#1606;"><img src="http://iranestate.com/images/iransavar-fa.png" title="&#1580;&#1587;&#1578;&#1580;&#1608; &#1601;&#1585;&#1608;&#1588; &#1608; &#1582;&#1585;&#1740;&#1583; &#1575;&#1578;&#1608;&#1605;&#1576;&#1740;&#1604; &#1607;&#1575;&#1740; &#1580;&#1583;&#1740;&#1583; &#1608; &#1605;&#1608;&#1585;&#1583; &#1575;&#1587;&#1578;&#1601;&#1575;&#1583;&#1607; &#1583;&#1585; &#1605;&#1588;&#1607;&#1583; &#1575;&#1589;&#1601;&#1607;&#1575;&#1606; &#1588;&#1740;&#1585;&#1575;&#1586; &#1578;&#1607;&#1585;&#1575;&#1606;" /></a>
</div>
-->

<?php } else { ?>
<div style="width:100%; height:280px; margin:2px; text-align:center;">
<script src="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=rsb&c=28&pli=8834958&PluID=0&w=335&h=280&ord=[timestamp]&ucm=true"></script>
<noscript>
<a href="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=brd&FlightID=8834958&Page=&PluID=0&Pos=173966907" target="_blank"><img src="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=bsr&FlightID=8834958&Page=&PluID=0&Pos=173966907" border=0 width=335 height=280></a>
</noscript>
</div>
<!--
<div style="width:100%; height:280px; margin:2px; text-align:center;">
	<a href="http://iransavar.com/en/type/car" title="Search Sell Buy New and Used Cars For Sale in Tehran Shiraz Mashhad Esfahan"><img src="http://iranestate.com/images/iransavar-en.png" title="Search Sell Buy New and Used Cars For Sale in Tehran Shiraz Mashhad Esfahan" /></a>
</div>
-->
<?php } ?>