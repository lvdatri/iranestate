<script type="text/javascript">
	$(".browse-city").corner();
	$(".right-col").corner();
	$("#searchForm").corner();
	
	$(document).ready(function(){
		$("SELECT").selectBox();
		$('.frmPropSearch').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
		
		$('#location').setDefaultValue();
		
		$('#frmPropSearch').submit(function() {
			if($('#location').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>') {
				$('#location').val('');
			}	
			$.al.sendFormUri('frmPropSearch');
			return false;		
		});	
		
		// set slider		
			$('#featured-slider').bxSlider({
								mode: 'horizontal',
								pause: 5000,
				auto: true,
				autoControls: false,
				autoHover: false,
				pager: false,
				controls: false
			});

		
	});	
</script>



<?php if($this->recordPropertyType == Bl_Record_Property::RENT) { 
	$colSpan = 4;
	$locWidth =  338;
} else {
	$colSpan = 7;
//	$locWidth = 632;
	$locWidth = 622;
}?>
    <div id="searchForm">
    
      <form id="frmPropSearch" class="frmPropSearch" action="<?php echo $this->pathModule.'properties/'.$this->transactionPath ?>" ><input name="search" type="hidden" id="search" value="1" />

<table width="930" height="163" border="0" cellpadding="5" cellspacing="5" id="searchFormTable">
      <tr class="titles">
                <td height="0" colspan="<?php echo $colSpan ?>" valign="bottom"><strong><?php echo $this->getStr(Al_Language::SEARCH_LBL_LOCATION); ?></strong></td>
                <?php if($this->recordPropertyType == Bl_Record_Property::RENT) { ?>
                <td width="123" height="0" valign="bottom"><strong><?php echo $this->getStr(Al_Language::SEARCH_LBL_MIN_DEPOSIT); ?></strong></td>
                <td width="14" height="0" valign="bottom"></td>
                <td width="133" height="0" valign="bottom"><strong><?php echo $this->getStr(Al_Language::SEARCH_LBL_MAX_DEPOSIT); ?></strong></td>
				<?php } ?>
                <td width="20" height="0" valign="bottom"></td>
                <td width="275" height="0" valign="bottom"></td>
      </tr>
                  <tr>
                <td colspan="<?php echo $colSpan ?>" valign="top"><input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>" style="width:<?php echo $locWidth; ?>px;" type="text" name="location" value="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>" id="location" onclick="if (this.value=='<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>') this.value = ''"/></td>
                <?php if($this->recordPropertyType == Bl_Record_Property::RENT) { ?>                  
                <td valign="top"><select name="dmin" id="dmin" style="width:60px;">
				  <?php echo $this->prices->ddDepositFromPrices(Al_Utilities::get('dmin')); ?>
                </select></td>
                <td valign="top">&nbsp;</td>
                <td valign="top"><select name="dmax" id="dmax" style="width:60px;">
				  <?php echo $this->prices->ddDepositToPrices(Al_Utilities::get('dmax')); ?>
                  </select></td>
				<?php } ?>                  
                <td valign="top">&nbsp;</td>
                <td valign="top"><a href="#" onclick="$('#frmPropSearch').submit();" class="searchButt"><?php echo $this->getStr(Al_Language::SEARCH_BTN_SEARCH); ?></a><a href="<?php echo $this->pathController.$this->transactionPath.'/search/1/'; ?>" class="newListingsButt"><?php echo $this->getStr(Al_Language::SEARCH_BTN_NEW_LISTINGS); ?></a></td>
              </tr>
                  <tr>
                <td width="168" valign="bottom"><strong><?php echo $this->getStr(Al_Language::SEARCH_LBL_PROP_TYPE); ?></strong></td>
                <td width="28" valign="bottom">&nbsp;</td>
                <td width="129" valign="bottom"><strong><?php echo $this->getStr(Al_Language::SEARCH_LBL_BEDROOMS); ?></strong></td>
                <td width="40" valign="bottom">&nbsp;</td>
                <td valign="bottom"><strong><?php echo $this->getStr(Al_Language::SEARCH_LBL_MIN_PRICE); ?></strong></td>
                <td valign="bottom">&nbsp;</td>
                <td valign="bottom"><strong><?php echo $this->getStr(Al_Language::SEARCH_LBL_MAX_PRICE); ?></strong></td>
                <td>&nbsp;</td>
                <td><a href="<?php echo $this->pathController.$this->transactionPath.'/'.'advanced-search'; ?>" class="advanced-search"><?php echo $this->getStr(Al_Language::SEARCH_LINK_ADVANCED_SEARCH); ?></a></td>
              </tr>
                  <tr>
                <td valign="top"><select name="type" id="type" style="width:170px;">
            <?php echo $this->propertyTypes->ddData(Al_Utilities::get('type'),$this->getStr(Al_Language::SEARCH_ANY)); ?>
                </select></td>
                <td valign="top">&nbsp;</td>
                <td valign="top"><select name="bed" id="bed" style="width:60px;">
				  <?php echo $this->bedBathPark->ddData(Al_Utilities::get('bed')); ?>
                </select></td>
                <td valign="top">&nbsp;</td>
                <td valign="top"><select name="pmin" id="pmin" style="width:60px;">
				  <?php echo $this->prices->ddFromPrices(Al_Utilities::get('pmin')); ?>
                </select></td>
                <td valign="top">&nbsp;</td>
                <td valign="top"><select name="pmax" id="pmax" style="width:60px;">
				  <?php echo $this->prices->ddToPrices(Al_Utilities::get('pmax')); ?>
                </select></td>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
              </tr>
        </table>
      </form>
    </div>
            <div class="feature-collection">
              <h2 class="featured"><?php echo $this->featTitle; ?></h2>
            </div>
<ul id="featured-slider"> 
                <?php foreach($this->featured as $items) {  ?> 
                <li>
                <?php $i=0; foreach($items as $item) {  $this->property->loadFromArray($item); $i++; ?>   

<a  class="featured-property" title="<?php echo $this->property->getCity()." ".$this->property->getPropertyType()." ".$this->property->getTransactionType();?>" href="<?php echo $this->property->getUrl(); ?>" >

        <div style="cursor:pointer;" class="feature-box <?php if($i==3) { echo "nopad"; } ?>" > 
        	<img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>/featureribbon.png" width="135" height="83" class="ribbon"/> 
            <img  title="<?php echo $this->property->getCity()." ".$this->property->getPropertyType()." ".$this->property->getTransactionType();?>"  src="<?php echo $this->property->getImageSml(); ?>" width="261" height="196" class="feature-image"/>
            <h3><?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?></h3>
            <div class="left-block">
                  <h2><?php echo $this->property->getCity(); ?> </h2>
                </div>
            <div class="right-block">
                  <p><?php echo $this->property->getPrice(true); ?></p>
                </div>
	            <div class="feature-icons">                
                <?php if($this->property->getBedrooms() > 0) { ?>
                  <div class="icon"> <img src="<?php echo $this->_settings->path_images; ?>ico-bed-big.png" width="25" height="25" alt="beds" />
                  <p><?php echo $this->property->getBedrooms(); ?></p>
                </div>
                <?php } ?>
                <?php if($this->property->getBathrooms() > 0) { ?>
                  <div class="icon"> <img src="<?php echo $this->_settings->path_images; ?>ico-bath-big.png" width="25" height="25" alt="bathroom" />
                <p><?php echo $this->property->getBathrooms(); ?></p>
              </div>
              <?php } ?>
              <?php if($this->property->getParking() > 0) { ?>
                  <div class="icon"> <img src="<?php echo $this->_settings->path_images; ?>ico-garage-big.png" width="25" height="25" alt="garage" />
                <p><?php echo $this->property->getParking(); ?></p>
              </div>
              <?php } ?>
              <?php if($this->property->getLandArea() > 0) { ?>
                  <div class="icon"> <img src="<?php echo $this->_settings->path_images; ?>ico-roomsize-big.png" width="25" height="25" alt="area" />
                <p><?php echo $this->property->getLandArea(); ?>m<span>2</span></p>
              </div>
              <?php } ?>
                </div>
            <p class="info"> <?php echo $this->property->getIntroduction(true,3); ?> </p>
          </div>
          <!--end feature -->
</a>          
<?php } // end item loop ?> 
                </li>				
<?php } // end items loop ?>                
			</ul>    
<div style="display:block; height:20px; clear:both;"></div>    


	
       

              

        <!--end feature collection-->
        
        <div class="left-col">
              <div class="browse-city"> <img src="<?php echo $this->_settings->path_images; ?>search_browsecities.png" width="26" height="23" alt="browse cities" />
            <h2><?php echo $this->getStr(Al_Language::HOME_TITLE_BROWSE_NATIONAL_CITIES); ?></h2>
            <div>
				<?php $i=0; foreach($this->featuredNationalCities as $items) { $i++;?>
                    <ul class="cityList <?php if($i==3) { echo 'cityPad'; } ?>">
                    <?php foreach($items as $item) { $this->city->loadFromArray($item); ?>
                        <?php if($this->city->getId() != '') { ?>
                        <li><a href="<?php echo $this->city->getPropertySearchPath($this->transactionPath); ?>"><?php echo $this->city->getCity(); ?></a></li>
                        <?php } ?>
                    <?php } ?>
                  </ul>
              <?php } ?>
             </div>
          </div>
              <div class="browse-city"> <img src="<?php echo $this->_settings->path_images; ?>search_browsecities.png" width="26" height="23" alt="browse cities" />
            <h2><?php echo $this->getStr(Al_Language::HOME_TITLE_BROWSE_INTERNATIONAL_CITIES); ?></h2>
            <div>
				<?php $i=0; foreach($this->featuredInternationalCities as $items) { $i++;?>
                    <ul class="cityList <?php if($i==3) { echo 'cityPad'; } ?>">
                    <?php foreach($items as $item) { $this->city->loadFromArray($item); ?>
                        <?php if($this->city->getId() != '') { ?>
                        <li><a href="<?php echo $this->city->getPropertySearchPath($this->transactionPath); ?>"><?php echo $this->city->getCity(); ?></a></li>
                        <?php } ?>
                    <?php } ?>
                  </ul>
              <?php } ?>
                </div>
          </div>
            </div>
        <!--END LEFT COL-->
        
        <div class="right-col">
              <h2><?php echo $this->getStr(Al_Language::HOME_TITLE_RECENT_NEWS); ?></h2>
              
              <?php foreach($this->news as $item) { ?>
              <a title="<?php echo $item->getDescription(); ?>" href="<?php echo $item->getPath(true); ?>">
				<?php if($item->getImage() != '') { ?>
                    <img width="150" title="<?php echo $item->getDescription(); ?>" height="50" border="0" src="<?php echo $item->getImageSml(); ?>" />
                <?php } ?>
                

              <p><?php echo $item->getDateCreated(true); ?> - <?php echo $item->getDescription(); ?></p>
			   </a>
   		      <?php } ?>              
            </div>
        <!--END RIGHT COL--> 