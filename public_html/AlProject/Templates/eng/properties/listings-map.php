	<script type="text/javascript">


	var mapActive = false;
	var map;

	var pointMap = function() {
			var self = this;
			this.map = null;
			this.markersArray = [];			
			this.data = [];
			this.apiPath = '<?php echo $this->pathModule.'properties/api-map/'; ?>';
			
			// http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
			this.infowindow = new google.maps.InfoWindow(
				{
					maxWidth: 850
				} 
			);
/*			this.infobox = new InfoBox({
					 content: document.getElementById("infobox"),
					 disableAutoPan: false,
					 maxWidth: 750,
					 maxHeight: 260,
					 zIndex: null,
					closeBoxMargin: "12px 4px 2px 2px",
					closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
					infoBoxClearance: new google.maps.Size(1, 1)
				});			*/
		
			this.loadClientLocations = function(cid,clid) {
				var dd = $("#jobs_location_id");		
			}
			
			self.iconCluster = new google.maps.MarkerImage(
				'<?php echo $this->_settings->path_images.'map/cluster-x1.png'; ?>',
			    // This marker is 20 pixels wide by 32 pixels tall.
			    new google.maps.Size(28, 29),
			    // The origin for this image is 0,0.
			    new google.maps.Point(0,0),
			    // The anchor for this image is the base of the flagpole at 0,32.
			    new google.maps.Point(14, 14)
			);				
			
			this.initialise = function() {
		        var mapOptions = {
		          center: new google.maps.LatLng(35.67514743608467,51.416015625),
        		  zoom: 11,
				  streetViewControl:false,
				  zoomControl: true,
				  zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL
				  },
				  panControl:false,
		          mapTypeId: google.maps.MapTypeId.ROADMAP
		        };
        		self.map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
				
				  google.maps.event.addListener(self.map, 'dragend', function() {
					self.loadPoints();
				  });			
				  
				google.maps.event.addListener(self.map, 'zoom_changed', function() {
					self.loadPoints();
				  });				
				  
				google.maps.event.addListenerOnce(self.map, 'idle', function(){
					self.loadPoints();
				});				  
				
			}
			
			this.addMarker = function(lat,lon,title,image,type,key) {
				/* var icon1 = new google.maps.Icon({
					anchor: new google.maps.Point(200,200),
					url:image
				}); */
				var icon;
				if(type == 'c') {
					icon = self.iconCluster;
				} else {
					icon = image;
				}
				
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(lat,lon),
					title:title,
					icon:image,
					map: self.map
				}); 	
				self.markersArray.push(marker);	 
				
				google.maps.event.addListener(marker, 'click', (function(marker, key) {
					return function() {

						if(self.data[key].type == 'c') { 
						
							if( (self.map.getZoom() >= 19) || (self.map.getZoom() >= 15 && self.data[key].count < 10)) {
								// zoom, tileCord
								var r = new $.al.request;
								r.setData({
									"tileCord":self.data[key].tileCord,
									"zoom":self.data[key].zoom,
									"search":self.getSearchFields(), 
									"transType":'<?php echo $this->recordPropertyType; ?>'
								});
								r.send(
									self.apiPath,
									'getProperties',
									function(response) {
										self.infowindow.setContent('<div style="width:430px;">'+response.data+'</div>');
										self.infowindow.open(self.map, marker);
									},
									function(response) {
										self.infowindow.setContent('no content found...');
									}
								);		 	
								self.infowindow.setContent('loading...');
								self.infowindow.open(self.map, marker);  

							} else {
								var zoom = self.map.getZoom()+1
								if(zoom > 21) {
									zoom = 21
								}
								self.map.setZoom(zoom);
							    self.map.setCenter(marker.getPosition());								
							}
						
						} else {
							var r = new $.al.request;
							r.setData({
								"pid":self.data[key].id
							});
							r.send(
								self.apiPath,
								'getProperty',
								function(response) {
									self.infowindow.setContent(response.data);
									self.infowindow.open(self.map, marker);
								},
								function(response) {
									self.infowindow.setContent('no content found...');
								}
							);		 	
							self.infowindow.setContent('loading...');
							self.infowindow.open(self.map, marker);  
						}
						
						
						  
					}
			    })(marker, key));				
			}
			
			this.showAllMarkers = function() {
			  if (self.markersArray) {
				for (i in self.markersArray) {
				  self.markersArray[i].setMap(self.map);
				}
			  }
			}	
			
			this.deleteAllMarkers = function() {
			  if (self.markersArray) {
				for (i in self.markersArray) {
				  self.markersArray[i].setMap(null);
				}
				self.markersArray.length = 0;
				self.data.length = 0;
			  }
			}					
			
			this.getSearchFields = function() {
				var location = $('#location').val();
				if(location == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>') { $('#location').val(''); }
				
				var lmin = $('#lmin').val();
				if(lmin == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>') { $('#lmin').val(''); }
				
				var lmax = $('#lmax').val();
				if(lmax == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>') { $('#lmax').val(''); }
				
				var bmin = $('#bmin').val();
				if(bmin == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>') { $('#bmin').val(''); }
				
				var bmax = $('#bmax').val();
				if(bmax == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>') { $('#bmax').val(''); }				
				
				var fields = $.al.getAdvSrchUri('refineSearchForm');
				
				$('#location').val(location);
				$('#lmin').val(lmin);
				$('#lmax').val(lmax);
				$('#bmin').val(bmin);
				$('#bmax').val(bmax);				

				return fields;				
				
			}
			
			this.loadPoints = function() {
				var r = new $.al.request;
				var b = self.map.getBounds();
				
								
				
				// get map bounds
				r.setData({
					"neLat":b.getNorthEast().lat(),
					"neLon":b.getNorthEast().lng(),
					"swLat":b.getSouthWest().lat(),
					"swLon":b.getSouthWest().lng(),
					"zoom":self.map.getZoom(),
					"search":self.getSearchFields(), 
					"transType":'<?php echo $this->recordPropertyType; ?>'
				});
				
				r.send(
					self.apiPath,
					'getPoints',
					function(response) {
						self.deleteAllMarkers();
						self.data = response.data;
						for (var key in self.data) {
							var countLbl = '';
							if(self.data[key].count > 1) {
								countLbl = self.data[key].count + ' <?php echo $this->getStr(Al_Language::MAP_LBL_PROPERTIES); ?>';
							} else {
								countLbl = self.data[key].count + ' <?php echo $this->getStr(Al_Language::MAP_LBL_PROPERTY); ?>';
							}
							
							self.addMarker(
								self.data[key].lat,
								self.data[key].lon,
								countLbl,
								self.data[key].image,
								self.data[key].type,
								key
							);
						}
					},
					
					function(response) {
						alert ('error adding record');
					}
				);		 				
			}
		}


	$(".result-form").corner();
	$(".news-col").corner();
	$(".result-block").corner();
	$(".page-nav").corner();

	$(document).ready(function(){
		$("SELECT").selectBox();
		
		$('.refineSearchForm').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
		$('.sortbyForm').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});

		$('#lmin').setDefaultValue();
		$('#lmax').setDefaultValue();
		$('#bmin').setDefaultValue();
		$('#bmax').setDefaultValue();				
		$('#location').setDefaultValue();
		
		$('#refineSearchForm').submit(function(event) {
			if(mapActive==true) {
				map.loadPoints();
			} else {
				if($('#location').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>') { $('#location').val(''); }
				if($('#lmin').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>') { $('#lmin').val(''); }
				if($('#lmax').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>') { $('#lmax').val(''); }
				if($('#bmin').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>') { $('#bmin').val(''); }
				if($('#bmax').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>') { $('#bmax').val(''); }
				location='<?php echo $this->pathController.$this->transactionPath; ?>/'+$.al.getAdvSrchUri('refineSearchForm');
			}
			return false;
		});			  
		
		jQuery.al.refineSearchInitialise();
		


		

	}); // end on ready
	
	
	function activateMap () {
		if(mapActive == false) {
			$("#sectMap").show();
			$("#sectListings").hide();
			
			$(document).ready(function() { 
				map = new pointMap();		
				map.initialise();
			});		
			mapActive = true;
		}
	}
	
	function activateListings() {
		mapActive = false;
		$('#refineSearchForm').submit();
	}
	
	function sortOrderChange() {
		location='<?php echo $this->pathController.$this->transactionPath.'/'.$this->propertySearch->getSearchParams(); ?>sort/'+$('#sortId').val();		
	}
	</script>
        
        <div class="result-right-col"><!--RESULT-RIGHT-COL---->

       
<div id="sectMap" style="display:none; margin-bottom:15px; ">       
<div id="map_canvas" style="width:620px; height:650px; border:solid 1px #CCCCCC; "></div>
</div>

        
<div id="sectListings" >
        <div  style=" height: 30px;margin: 0 0 12px 0;position: relative;">


        	<p class="listings-heading">

            
            	<?php echo $this->pager->total_num_of_records; ?> <?php echo $this->getStr(Al_Language::SEARCH_LBL_PROPERTIES_FOUND); ?>
            </p>


<form class="sortbyForm" >
  
  
  <select onChange="sortOrderChange();" name="sortId" id="sortId" style="width:130px;">
        <?php echo $this->propertySort->ddData($this->propertySearch->getSortId()); ?>
        </select>
                  </form>
            
        </div>
              
          
    <?php while($item = $this->properties->fetch_row()) { $this->property->loadFromArray($item); ?>          
          <div class="result-block">
            <div class="result-header-block <?php if($this->property->getFeatured()=='0') { echo 'result-header-green'; } ?>">
                  <div class="result-header-title">
                              <a href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#FFF;">
                <p><?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?></p>
                </a>
              </div>
                  <div class="result-header-icons">
			<a href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#FFF;">


                <p><?php echo $this->property->getPrice(true); ?></p>

                

                <img src="<?php echo $this->_settings->path_images; ?>vert-line.png" width="3" height="28" alt="vert" />
                
                <?php if($this->property->getBedrooms() > 0) { ?>
                    <img src="<?php echo $this->_settings->path_images; ?>ico-bed-big-white.png" width="25" height="21" title="bedrooms" />
                    <p><?php echo $this->property->getBedrooms(); ?></p>
                <?php } ?>    
				<?php if($this->property->getBathrooms() > 0) { ?>
                <img src="<?php echo $this->_settings->path_images; ?>ico-bath-big-white.png" width="16" height="22" title="bathrooms" />
                <p><?php echo $this->property->getBathrooms(); ?></p>
                <?php } ?>
                
                <?php if($this->property->getParking() > 0) { ?>
                <img src="<?php echo $this->_settings->path_images; ?>ico-garage-big-white.png" width="22" height="21" title="garages" />
                <p><?php echo $this->property->getParking(); ?></p>
                <?php } ?>

            </a>
                
                
              </div>
                </div>
            <div class="result-image-block"> 

            <a href="<?php echo $this->property->getUrl(); ?>" title="<?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?>" class="clearlink">
            <img alt="<?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?>" src="<?php echo $this->property->getImageThumb(); ?>" />
            
            <?php if($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_SALE && $this->property->getPropertyStatusId()==Bl_Data_Statuses::BUY_SOLD) { ?>
            	<img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\sold.png" width="74" height="73" alt="featured" class="sticker"/>            
            <?php } else if($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_RENT && $this->property->getPropertyStatusId()==Bl_Data_Statuses::RENT_RENTED) { ?>
            	<img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\rented.png" width="74" height="73" alt="featured" class="sticker"/>                            
            <?php } else if($this->property->getFeatured()=='1') { ?>
            	<img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\sticker.png" width="74" height="73" alt="featured" class="sticker"/>
            <?php } ?>
            
            
            </a>            
            </div>
            <div class="result-text-block">
            <a href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#444444;">
                  <p> <strong><?php echo $this->getStr(Al_Language::FLD_PROP_FEATURES); ?>:</strong> <?php echo $this->property->getFeatures(true); ?> </p>
                  <p> <strong><?php echo $this->getStr(Al_Language::FLD_PROP_ADDRESS); ?>:</strong> <?php echo $this->property->getAddress() .', ' . $this->property->getZone() . ' '. $this->property->getCountry() . ' '.$this->property->getPostalCode(); ?></p>
                  
                  <?php if($this->recordPropertyType == Bl_Record_Property::RENT) { ?>
					<?php if($this->property->getDeposit() > 0) { ?>
	                  <p><strong><?php echo $this->getStr(Al_Language::FLD_PROP_DEPOSIT); ?>:</strong> <?php echo $this->property->getDeposit(true); ?></p>
                      <?php } ?>
                  <?php } ?>
				<?php if($this->property->getLandArea() > 0) { ?>
                <p><strong><?php echo $this->getStr(Al_Language::FLD_PROP_LAND_AREA); ?>:</strong> <?php echo $this->property->getLandArea(); ?>m<span>2</span> </p>
                <?php } ?>
			</a>
                </div>
          </div>
		<?php } ?>          
          
<?php if($this->pager->num_of_pages > 1) { ?>          
          <div class="page-nav">

          	<div class="pagination">
                <ul>
					<?php $this->pager->display_nav(); ?>	                    
                </ul>
            </div>
            
            

          </div>
<?php } ?>          
          
</div> <!-- end sectListings -->          
          
            </div><!--END RESULT-RIGHT-COL ----> 


        <div class="result-left-col" style="margin-bottom:15px;"><!--RESULT-LEFT-COL---->
              <div class="result-form">
            <h2><?php echo $this->getStr(Al_Language::SEARCH_LBL_REFINE_SEARCH); ?></h2>
     
           
<form class="refineSearchForm" name="refineSearchForm" id="refineSearchForm" action="<?php echo $this->pathModule.'properties/'.$this->transactionPath ?>" method="get">
<input name="search" type="hidden" id="search" value="1" />    




<div style="padding-bottom:5px;">  
<input class="submit-next-btn" name="" type="button" value="<?php echo $this->getStr(Al_Language::SEARCH_BTN_LISTINGS); ?>" onclick="activateListings();"  />
<input class="submit-next-btn" name="" type="button" value="<?php echo $this->getStr(Al_Language::SEARCH_BTN_MAP); ?>" onclick="activateMap();"  />
</div>  
<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sect-pad" style="margin-bottom:10px;"/>                     

                  <input  name="location" type="text" class="location-input" id="location"  title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>" value="<?php echo $this->propertySearch->getVal('location'); ?>"/>

                  <input class="location-btn" name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>"  />


                  <img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sect-pad" style="margin-top:8px;"/>

<!-- start price -->
<a id="priceTitle" href="#" class="sect" >
	<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_PRICE); ?></h3>
    <?php if($this->propertySearch->getPropertyPrice() != '') { ?>
    <div id="priceSearchRes" class="searchRes">
    <?php echo $this->propertySearch->getPropertyPrice(); ?>
    </div>
    <?php } ?>
</a>
<div id="priceForm" class="sectForm" style="display:none;" >
      <div class="form-elem-select">
        <select name="pmin" id="pmin" style="width:60px;">
              <?php echo $this->prices->ddFromPrices($this->propertySearch->getVal('priceMin')); ?>
        </select>
      </div>
            <div class="form-elem-text"> <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> </div>
        <div class="form-elem-select">
        <select name="pmax" id="pmax" style="width:60px;">
          <?php echo $this->prices->ddToPrices($this->propertySearch->getVal('priceMax')); ?>
        </select>
        </div>
      <div style="clear:both; display:block; padding-top:5px;"><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>
</div>  
<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sect-pad"/>            
<!-- end price -->

<!-- start deposit -->
<?php if($this->recordPropertyType == Bl_Record_Property::RENT) { ?>               
<a id="depositTitle" href="#" class="sect" >                
    <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_DEPOSIT); ?></h3>
    <?php if($this->propertySearch->getPropertyDeposit()!= '') { ?>
    <div id="depositSearchRes" class="searchRes">
	   <?php echo $this->propertySearch->getPropertyDeposit(); ?>
    </div>    
    <?php } ?>
</a>       
<div id="depositForm" style="display:none;" class="sectForm">   
    <div class="form-elem-select">
        <select name="dmin" id="dmin" style="width:60px;">
        <?php echo $this->prices->ddDepositFromPrices($this->propertySearch->getVal('depositMin')); ?>
        </select>
    </div>
    <div class="form-elem-text"> <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> </div>
    <div class="form-elem-select">
        <select name="dmax" id="dmax" style="width:60px;">
        <?php echo $this->prices->ddDepositToPrices($this->propertySearch->getVal('depositMax')); ?>
        </select>
    </div>
    <div style="clear:both; display:block; padding-top:5px;"><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>                                
</div>
	<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sect-pad"/>
<?php } ?>      
<!-- end deposit -->


<!-- property type -->      
<a id="propTypeTitle" href="#" class="sect" >                
              <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_PROPERTY_TYPE); ?></h3>
    <div id="propTypeSearchRes" class="searchRes">
		<?php echo $this->propertySearch->getPropertyTypes(); ?>
    </div>    
</a>       
<div id="propTypeForm" style="display:none;" class="sectForm">              
					<?php foreach($this->propTypes as $section) { ?>
                      <ul class="form-elem-checkbox">
	                      <?php foreach($section as $item) { ?>
                            <?php if($item['label'] != '') { ?>
                            <li>
								<div class="result-form-cb">
		                          <input name="t_<?php echo $item['id']; ?>" type="checkbox" id="t_<?php echo $item['id']; ?>" value="<?php echo $item['id']; ?>" <?php echo $this->propertySearch->getPropertyTypesCbVal($item['id']); ?> />
                              </div>
								<div class="result-form-cbl">
                                  <label class="lbl" for="t_<?php echo $item['id']; ?>" ><?php echo $item['label']; ?></label>                              
                                </div>
                    </li>
                            <?php } ?>
                          <?php } ?>
                      </ul>
                   <?php } ?>
			  <div style="clear:both; display:block; padding-top:5px;"><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>
</div>                            
              <img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sect-pad"/>
<!-- end property type -->                  
                  
<!-- bedrooms -->  
<a id="bedTitle" href="#" class="sect" >                                
    <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_BEDROOMS); ?></h3>
<?php if($this->propertySearch->getPropertyBedrooms() != '') { ?>    
    <div id="bedSearchRes" class="searchRes">
	    <?php echo $this->propertySearch->getPropertyBedrooms(); ?>
    </div>    
<?php } ?>    
</a>       
<div id="bedForm" style="display:none;" class="sectForm">    
    <div class="form-elem-select">
        <select name="bed" id="bed" style="width:80px;">
          <?php echo $this->bedBathPark->ddData($this->propertySearch->getVal('bedrooms')); ?>
        </select>
    </div>
    <div ><input class="submit-next-btn" name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>   
</div>                                                	
<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sec-pad"/>
<!-- end bedrooms -->
                  
<!-- bathrooms -->   
<a id="bathTitle" href="#" class="sect" >                                               
	<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_BATHROOMS); ?></h3>
    <?php if($this->propertySearch->getPropertyBathrooms() != '') { ?>
    <div id="bathSearchRes" class="searchRes">
	    <?php echo $this->propertySearch->getPropertyBathrooms(); ?>
    </div>    
    <?php } ?>
</a>       
<div id="bathForm" style="display:none;" class="sectForm">           
    <div class="form-elem-select">
        <select name="bath" id="bath" style="width:80px;">
          <?php echo $this->bedBathPark->ddData($this->propertySearch->getVal('bathrooms')); ?>
        </select>
    </div>
    <div><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" class="submit-next-btn" /></div>                                               	
</div>          
<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sec-pad"/>         
<!-- end bathrooms -->          
                  
                  
<!-- parking -->         
<a id="parkTitle" href="#" class="sect" >         
      <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_PARKING); ?></h3>
<?php if($this->propertySearch->getPropertyParking() != '' ) { ?>      
    <div id="parkSearchRes" class="searchRes">
	    <?php echo $this->propertySearch->getPropertyParking(); ?>
    </div>    
<?php } ?>    
</a>       
<div id="parkForm" style="display:none;" class="sectForm">      
        <div class="form-elem-select">
            <select name="park" id="park" style="width:80px;">
              <?php echo $this->bedBathPark->ddData($this->propertySearch->getVal('parking')); ?>
            </select>
      </div>
        <div><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" class="submit-next-btn" /></div>                                               	
</div>        
		  <img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sec-pad"/>      
<!-- end parking -->                            

<a name="marker"></a>
<!-- introduction -->
<a id="introTitle" href="#" class="sect" >         
          <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_INTRODUCTION); ?></h3>
<?php if($this->propertySearch->getPropertyIntroduction() != '') { ?>          
    <div id="introSearchRes" class="searchRes">
	    <?php echo $this->propertySearch->getPropertyIntroduction(); ?>
    </div>    
<?php } ?>    
</a>    
<div id="introForm" style="display:none;" class="sectForm">            
	  <?php foreach($this->propIntroduction as $section) { ?>
          <ul class="form-elem-checkbox">
          
              <?php foreach($section as $item) { ?>
                <?php if($item['label'] != '') { ?>
                <li>
                    <div class="result-form-cb">
                          <input id="i_<?php echo $item['id']; ?>" name="i_<?php echo $item['id']; ?>" type="checkbox" value="<?php echo $item['id']; ?>" <?php echo $this->propertySearch->getIntroductionCbVal($item['id']); ?> >
                    </div>
                    <div class="result-form-cbl">
                      <label  class="lbl"  for="i_<?php echo $item['id']; ?>"><?php echo $item['label']; ?></label>                                                     
                    </div>
                </li>
                <?php } ?>
              <?php } ?>
          </ul>
       <?php } ?>
	<div style="clear:both; display:block; padding-top:5px;"><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>
</div>        
<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sec-pad"/>
<!-- end introduction -->                  
                  
<!-- features -->      
<a id="featTitle" href="#" class="sect" >                     
    <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_FEATURES); ?></h3>
    <?php if($this->propertySearch->getPropertyFeatures() != '') { ?>
    <div id="featSearchRes" class="searchRes">
	    <?php echo $this->propertySearch->getPropertyFeatures(); ?>
    </div>    
    <?php } ?>
</a>
<div id="featForm" style="display:none;" class="sectForm">
			<?php foreach($this->propFeatures as $section) { ?>
              <ul class="form-elem-checkbox">
                  <?php foreach($section as $item) { ?>
                    <?php if($item['label'] != '') { ?>
                    <li>
                        <div class="result-form-cb">
                          <input id="f_<?php echo $item['id']; ?>" name="f_<?php echo $item['id']; ?>" type="checkbox" value="<?php echo $item['id']; ?>"  <?php echo $this->propertySearch->getFeatureCbVal($item['id']); ?> />
                        </div>
                        <div class="result-form-cbl">
                          <label  class="lbl"  for="f_<?php echo $item['id']; ?>" ><?php echo $item['label']; ?></label>                              
                        </div>
                    </li>
                    <?php } ?>
                  <?php } ?>
              </ul>
           <?php } ?>
        <div style="clear:both; display:block; padding-top:5px;"><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>
</div>        
<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sec-pad"/>
<!-- end features -->

<!-- land area -->
<a id="landTitle" href="#marker" class="sect" >                      
          <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_LAND_AREA); ?></h3>
<?php if ($this->propertySearch->getPropertyLandArea()!= '') { ?>
    <div id="landSearchRes" class="searchRes">
	    <?php echo $this->propertySearch->getPropertyLandArea(); ?>
    </div>    
<?php } ?>    
</a>          
<div id="landForm" style="display:none;" class="sectForm">
                <div class="form-elem-select">
    <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>" name="lmin" type="text" id="lmin" value="<?php echo $this->propertySearch->getVal('landMin','0'); ?>" size="7" />
    <div class="form-elem-text"> m<span class="met-sq">2</span>&nbsp;&nbsp; <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> &nbsp;</div>
    <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>" name="lmax" type="text" id="lmax" value="<?php echo $this->propertySearch->getVal('landMax','0'); ?>" size="7" />
    <div class="form-elem-text">m<span class="met-sq">2</span></div></div>
	<div class="submit-bottom-btn"><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>                                               	
</div>            
<img src="<?php echo $this->_settings->path_images; ?>dottedline.png" width="261" height="3" class="sec-pad"/>
<!-- end land area -->            

<!-- build area -->  
<a id="buildTitle" href="#marker" class="sect" >                               
            <h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_BUILD_AREA); ?></h3>
    <?php if($this->propertySearch->getPropertyBuildArea() != '') { ?>
    <div id="buildSearchRes" class="searchRes">
	    <?php echo $this->propertySearch->getPropertyBuildArea(); ?>
    </div>    
    <?php } ?>
</a>      
<div id="buildForm" style="display:none;" class="sectForm">       
            <div class="form-elem-select">
            <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>" name="bmin" type="text" id="bmin" value="<?php echo $this->propertySearch->getVal('buildMin','0'); ?>" size="7" />
            <div class="form-elem-text"> m<span class="met-sq">2</span>&nbsp;&nbsp; <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> &nbsp;</div>
            <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>" name="bmax" type="text" id="bmax" value="<?php echo $this->propertySearch->getVal('buildMax','0'); ?>" size="7" />
            <div class="form-elem-text">m<span class="met-sq">2</span></div>
            </div>
            <div class="submit-bottom-btn" ><input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_LBL_UPDATE); ?>" /></div>                                               	
</div>            
<!-- end build area -->            
<div style="clear:both;"></div>            
</form>
          </div>
              
            </div>
        <!--END RESULT-LEFT-COL ---->
        
