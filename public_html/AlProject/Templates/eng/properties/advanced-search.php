<script type="text/javascript">

	$(".result-form").corner();

	$(document).ready(function(){
		$("SELECT").selectBox();
		
		$('.refineSearchForm').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});

		$('#lmin').setDefaultValue();
		$('#lmax').setDefaultValue();
		$('#bmin').setDefaultValue();
		$('#bmax').setDefaultValue();				
		$('#location').setDefaultValue();
		
		

		$('#refineSearchForm').submit(function(event) {
			if($('#location').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>') { $('#location').val(''); }
			if($('#lmin').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>') { $('#lmin').val(''); }
			if($('#lmax').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>') { $('#lmax').val(''); }
			if($('#bmin').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>') { $('#bmin').val(''); }
			if($('#bmax').val() == '<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>') { $('#bmax').val(''); }
			location='<?php echo $this->pathController.$this->transactionPath; ?>/'+$.al.getAdvSrchUri('refineSearchForm');
			return false;
		});			  
	}); // end on ready
	
</script>

<div class="single-column" >
<h1><?php echo $this->getStr(Al_Language::SEARCH_LINK_ADVANCED_SEARCH); ?></h1> 
<div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;" >
<form class="refineSearchForm" name="refineSearchForm" id="refineSearchForm" action="<?php echo $this->pathModule.'properties/'.$this->transactionPath ?>" method="get">
<input name="search" type="hidden" id="search" value="1" />      
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_LOCATION); ?></h3>
                  <input name="location" type="text" class="extra" id="location" style="width:400px;" title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_LOCATION); ?>" value="<?php echo $this->propertySearch->getVal('location'); ?>"/>

                  <div class="imgpad"></div>

<!-- start price -->
<div class="result-form-adv-price">
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_PRICE); ?></h3>
<div id="priceForm" class="sectForm"  >
      <div class="form-elem-select">
        <select name="pmin" id="pmin" style="width:60px;">
              <?php echo $this->prices->ddFromPrices($this->propertySearch->getVal('priceMin')); ?>
        </select>
      </div>
            <div class="form-elem-text"> <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> </div>
        <div class="form-elem-select">
        <select name="pmax" id="pmax" style="width:60px;">
          <?php echo $this->prices->ddToPrices($this->propertySearch->getVal('priceMax')); ?>
        </select>
        </div>
</div>  
</div>
<!-- end price -->

<!-- start deposit -->
<?php if($this->recordPropertyType == Bl_Record_Property::RENT) { ?>               
<div class="result-form-adv-deposit" >
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_DEPOSIT); ?></h3>
<div id="depositForm"  class="sectForm">   
    <div class="form-elem-select">
        <select name="dmin" id="dmin" style="width:60px;">
        <?php echo $this->prices->ddDepositFromPrices($this->propertySearch->getVal('depositMin')); ?>
        </select>
    </div>
    <div class="form-elem-text"> <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> </div>
    <div class="form-elem-select">
        <select name="dmax" id="dmax" style="width:60px;">
        <?php echo $this->prices->ddDepositToPrices($this->propertySearch->getVal('depositMax')); ?>
        </select>
    </div>
</div>
</div>
<?php } ?>
<div class="imgpad"></div>
<!-- end deposit -->


<!-- property type -->      
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_PROPERTY_TYPE); ?></h3>
<div id="propTypeForm" class="sectForm">              
					<?php foreach($this->propTypes as $section) { ?>
                      <ul class="form-elem-checkbox-adv" >
	                      <?php foreach($section as $item) { ?>
                            <?php if($item['label'] != '') { ?>
                            <li>
								<div class="result-form-cb-adv">
		                          <input name="t_<?php echo $item['id']; ?>" type="checkbox" id="t_<?php echo $item['id']; ?>" value="<?php echo $item['id']; ?>" <?php echo $this->propertySearch->getPropertyTypesCbVal($item['id']); ?> />
                              </div>
								<div class="result-form-cbl-adv">
                                  <label for="t_<?php echo $item['id']; ?>" ><?php echo $item['label']; ?></label>                              
                                </div>
                    </li>
                            <?php } ?>
                          <?php } ?>
                      </ul>
                   <?php } ?>

</div>                            
<div class="imgpad"></div>
<!-- end property type -->                  
                  
<!-- bedrooms -->  
<div class="result-form-adv-bed">
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_BEDROOMS); ?></h3>
<div id="bedForm"  class="sectForm">    
    <div class="form-elem-select">
        <select name="bed" id="bed" style="width:80px;">
          <?php echo $this->bedBathPark->ddData($this->propertySearch->getVal('bedrooms')); ?>
        </select>
    </div>
</div>                                                	
</div>
<!-- end bedrooms -->
                  
<!-- bathrooms -->   
<div class="result-form-adv-bath">
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_BATHROOMS); ?></h3>
<div id="bathForm"  class="sectForm">           
    <div class="form-elem-select">
        <select name="bath" id="bath" style="width:80px;">
          <?php echo $this->bedBathPark->ddData($this->propertySearch->getVal('bathrooms')); ?>
        </select>
    </div>
</div>          
</div>
<!-- end bathrooms -->          
                  
                  
<!-- parking -->    
<div class="result-form-adv-park">     
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_PARKING); ?></h3>
<div id="parkForm"  class="sectForm">      
        <div class="form-elem-select">
            <select name="park" id="park" style="width:80px;">
              <?php echo $this->bedBathPark->ddData($this->propertySearch->getVal('parking')); ?>
            </select>
      </div>
</div>        
</div>
<div class="imgpad"></div>
<!-- end parking -->                            

<!-- introduction -->
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_INTRODUCTION); ?></h3>
<div id="introForm"  class="sectForm">            
	  <?php foreach($this->propIntroduction as $section) { ?>
          <ul class="form-elem-checkbox-adv" >
          
              <?php foreach($section as $item) { ?>
                <?php if($item['label'] != '') { ?>
                <li>
                    <div class="result-form-cb-adv">
                          <input id="i_<?php echo $item['id']; ?>" name="i_<?php echo $item['id']; ?>" type="checkbox" value="<?php echo $item['id']; ?>" <?php echo $this->propertySearch->getIntroductionCbVal($item['id']); ?> >
                    </div>
                    <div class="result-form-cbl-adv">
                      <label for="i_<?php echo $item['id']; ?>"><?php echo $item['label']; ?></label>                                                     
                    </div>
                </li>
                <?php } ?>
              <?php } ?>
          </ul>
       <?php } ?>
</div>        
<div class="imgpad"></div>
<!-- end introduction -->                  
                  
<!-- features -->      
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_FEATURES); ?></h3>
<div id="featForm"  class="sectForm">
			<?php foreach($this->propFeatures as $section) { ?>
              <ul class="form-elem-checkbox-adv">
                  <?php foreach($section as $item) { ?>
                    <?php if($item['label'] != '') { ?>
                    <li>
                        <div class="result-form-cb-adv">
                          <input id="f_<?php echo $item['id']; ?>" name="f_<?php echo $item['id']; ?>" type="checkbox" value="<?php echo $item['id']; ?>"  <?php echo $this->propertySearch->getFeatureCbVal($item['id']); ?> />
                        </div>
                        <div class="result-form-cbl-adv">
                          <label for="f_<?php echo $item['id']; ?>" ><?php echo $item['label']; ?></label>                              
                        </div>
                    </li>
                    <?php } ?>
                  <?php } ?>
              </ul>
           <?php } ?>
</div>        
<div class="imgpad"></div>
<!-- end features -->

<!-- land area -->
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_LAND_AREA); ?></h3>
<div id="landForm"  class="sectForm">
                <div class="form-elem-select">
    <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>" name="lmin" type="text" id="lmin" value="<?php echo $this->propertySearch->getVal('landMin','0'); ?>" size="7" />
    <div class="form-elem-text"> m<span class="met-sq">2</span>&nbsp;&nbsp; <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> &nbsp;</div>
    <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>" name="lmax" type="text" id="lmax" value="<?php echo $this->propertySearch->getVal('landMax','0'); ?>" size="7" />
    <div class="form-elem-text">m<span class="met-sq">2</span></div>
       	  	    </div>
</div>            
<div class="imgpad"></div>
<!-- end land area -->            

<!-- build area -->  
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::SEARCH_LBL_BUILD_AREA); ?></h3>
        <div id="buildForm"  class="sectForm">       
            <div class="form-elem-select">
            <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MIN); ?>" name="bmin" type="text" id="bmin" value="<?php echo $this->propertySearch->getVal('buildMin','0'); ?>" size="7" />
            <div class="form-elem-text"> m<span class="met-sq">2</span>&nbsp;&nbsp; <?php echo $this->getStr(Al_Language::SEARCH_LBL_TO); ?> &nbsp;</div>
            <input title="<?php echo $this->getStr(Al_Language::SEARCH_INPUT_MAX); ?>" name="bmax" type="text" id="bmax" value="<?php echo $this->propertySearch->getVal('buildMax','0'); ?>" size="7" />
            <div class="form-elem-text">m<span class="met-sq">2</span></div>
            </div>

</div>
<div class="imgpad"></div>
<!-- end build area -->  
<input name="" type="submit" value="<?php echo $this->getStr(Al_Language::SEARCH_BTN_SEARCH); ?>" />
<div style="clear:both;"></div>            
</form>
          </div>
              
            </div>

        
