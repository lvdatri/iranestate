    <?php while($item = $this->properties->fetch_row()) { $this->property->loadFromArray($item); ?>          
          <div class="result-block">
            <div class="result-header-block <?php if($this->property->getFeatured()=='0') { echo 'result-header-green'; } ?>">
                  <div class="result-header-title">
                              <a  target="_blank"  href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#FFF;">
                <p><?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?></p>
                </a>
              </div>
                  <div class="result-header-icons">
			<a  target="_blank"  href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#FFF;">


                <p><?php echo $this->property->getPrice(true); ?></p>

                

                <img src="<?php echo $this->_settings->path_images; ?>vert-line.png" width="3" height="28" alt="vert" />
                
                <?php if($this->property->getBedrooms() > 0) { ?>
                    <img src="<?php echo $this->_settings->path_images; ?>ico-bed-big-white.png" width="25" height="21" title="bedrooms" />
                    <p><?php echo $this->property->getBedrooms(); ?></p>
                <?php } ?>    
				<?php if($this->property->getBathrooms() > 0) { ?>
                <img src="<?php echo $this->_settings->path_images; ?>ico-bath-big-white.png" width="16" height="22" title="bathrooms" />
                <p><?php echo $this->property->getBathrooms(); ?></p>
                <?php } ?>
                
                <?php if($this->property->getParking() > 0) { ?>
                <img src="<?php echo $this->_settings->path_images; ?>ico-garage-big-white.png" width="22" height="21" title="garages" />
                <p><?php echo $this->property->getParking(); ?></p>
                <?php } ?>

            </a>
                
                
              </div>
                </div>
            <div class="result-image-block"> 

            <a  target="_blank"  href="<?php echo $this->property->getUrl(); ?>" title="<?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?>" class="clearlink">
            <img alt="<?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?>" src="<?php echo $this->property->getImageThumb(); ?>" />
            
            <?php if($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_SALE && $this->property->getPropertyStatusId()==Bl_Data_Statuses::BUY_SOLD) { ?>
            	<img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\sold.png" width="74" height="73" alt="featured" class="sticker"/>            
            <?php } else if($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_RENT && $this->property->getPropertyStatusId()==Bl_Data_Statuses::RENT_RENTED) { ?>
            	<img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\rented.png" width="74" height="73" alt="featured" class="sticker"/>                            
            <?php } else if($this->property->getFeatured()=='1') { ?>
            	<img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\sticker.png" width="74" height="73" alt="featured" class="sticker"/>
            <?php } ?>
            
            
            </a>            
            </div>
            <div class="result-text-block">
            <a href="<?php echo $this->property->getUrl(); ?>" target="_blank" style="text-decoration:none; color:#444444;">
                  <p> <strong><?php echo $this->getStr(Al_Language::FLD_PROP_FEATURES); ?>:</strong> <?php echo $this->property->getFeatures(true); ?> </p>
                  <p> <strong><?php echo $this->getStr(Al_Language::FLD_PROP_ADDRESS); ?>:</strong> <?php echo $this->property->getAddress() .', ' . $this->property->getZone() . ' '. $this->property->getCountry() . ' '.$this->property->getPostalCode(); ?></p>
                  
                  <?php if($this->recordPropertyType == Bl_Record_Property::RENT) { ?>
					<?php if($this->property->getDeposit() > 0) { ?>
	                  <p><strong><?php echo $this->getStr(Al_Language::FLD_PROP_DEPOSIT); ?>:</strong> <?php echo $this->property->getDeposit(true); ?></p>
                      <?php } ?>
                  <?php } ?>
				<?php if($this->property->getLandArea() > 0) { ?>
                <p><strong><?php echo $this->getStr(Al_Language::FLD_PROP_LAND_AREA); ?>:</strong> <?php echo $this->property->getLandArea(); ?>m<span>2</span> </p>
                <?php } ?>
			</a>
                </div>
          </div>
		<?php } ?> 