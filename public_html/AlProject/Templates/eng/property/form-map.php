<script type="text/javascript" >

$("#intro-box").corner();

$(document).ready(function() {
	$('#frmR').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
});
</script>
<div class="single-column" >
<?php $this->message->show(); ?>
<h1><?php echo $this->pageHeading; ?></h1> 

<?php $this->tabs->displayProperty(); ?>
<div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;" >
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_SELECT_MAP_LOCATION); ?></h3>   
<div style="clear:both; display:block; padding:6px 0px;">      
<?php echo $this->getStr(Al_Language::PROP_LBL_MAP_HINT); ?>
</div>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" >
<table width="932" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="49%"><script type="text/javascript"> 
				$(document).ready(function() { 
				
					var gmap_map = jQuery.al.gmap_set(
						'map_canvas',
						<?php echo $this->form->getLat(); ?>,
						<?php echo $this->form->getLon(); ?>,
						<?php echo $this->form->getDepth(); ?>
					);
					
				  	var gmap_marker = jQuery.al.gmap_setMarker (
						gmap_map,
						<?php echo $this->form->getLat(); ?>,
						<?php echo $this->form->getLon(); ?>,
						'<?php echo $this->_settings->path_images; ?>/marker.png',
						'<?php echo $this->_settings->path_images; ?>/shadow.png',
						true
					);
	  
					google.maps.event.addListener(gmap_marker,"mouseup", function(){ 																   
						var latLong = gmap_marker.getPosition();
						$("#lat").val(latLong.lat());
						$("#lon").val(latLong.lng());
					}); 					
		  
					google.maps.event.addListener(gmap_map, 'zoom_changed', function() {		 
						$("#depth").val(gmap_map.getZoom());
					}); 			
				
				}); 
                </script>
      <div id="map_canvas" style="width: 515px; height: 300px; border:solid 1px #CCC; background-color:#FFF; "></div></td>
    <td width="51%" valign="top" style="vertical-align:top;">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td class="label map-field"  ><?php echo $this->getStr(Al_Language::PROP_LBL_LATITUDE); ?>:</td>
        <td  class="field" ><input name="lat" type="text"   id="lat" value="<?php echo $this->form->getLat(); ?>" size="25" readonly="readonly" /></td>
      </tr>
      <tr>
        <td class="label map-field"  ><?php echo $this->getStr(Al_Language::PROP_LBL_LONGITUDE); ?>:</td>
        <td  class="field" ><input name="lon" type="text"   id="lon" value="<?php echo $this->form->getLon(); ?>" size="25" readonly="readonly" /></td>
      </tr>
      <tr>
        <td class="label map-field"  ><?php echo $this->getStr(Al_Language::PROP_LBL_DEPTH); ?>:</td>
        <td class="field"  ><input name="depth" type="text"   id="depth" value="<?php echo $this->form->getDepth(); ?>" size="25" readonly="readonly" /></td>
      </tr>
    </table></td>
  </tr>
</table>
<div class="imgpad"></div>
<input name="buySearch" type="submit" value="<?php echo $this->getStr(Al_Language::PROP_BTN_MAP_NEXT); ?>"> 
</form>
</div>
<?php $this->tabs->displayEnd(); ?>
</div>