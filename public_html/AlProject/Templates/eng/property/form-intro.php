<script type="text/javascript" >

	$("#intro-box").corner();

$(document).ready(function() {
	$("SELECT").selectBox();
	
	$('#frmR').submit(function() {
		if($('#location').val() == 'eg. Tehran or ID') {
			$('#location').val('');
		}	
		$.al.sendFormUri('frmPropSearch');
		return false;		
	});	
	
	$('#frmR').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
	
	
	$('#country_id').change(function() {$.al.countryChange()});
	$('#city_id').change(function() {$.al.cityChange()});
	$('#property_transaction_id').change(function() {$.al.transactionChange()});	
	
	$.al.transactionChange('<?php echo $this->form->getPropertyStatusId(); ?>');
	$.al.countryChange('<?php echo $this->form->getCityId(); ?>');
	$.al.cityChange('<?php echo $this->form->getCityId(); ?>','<?php echo $this->form->getZoneId(); ?>');
	
});

jQuery.al.transactionChange = function(statusId) {

	var id = $('#property_transaction_id').val();
	if(id == '2' || id == '3') {
		$('#itemPriceType').show();
		$('#itemDeposit').show();
	} else {
		$('#itemPriceType').hide();		
		$('#itemDeposit').hide();		
	}
	
	if(id == '1') {
		$('#itemTerrace').show();
		$('#itemLandWidth').show();
		$('#itemLandHeight').show();
		$('#itemAverageCeilingHeight').show();
		$('#locatedOnLevel').show();
		$('#numberOfLevels').show();
		$('#ceilingHeight').show();
	} else {
		$('#itemTerrace').hide();
		$('#itemLandWidth').hide();
		$('#itemLandHeight').hide();
		$('#itemAverageCeilingHeight').hide();
		$('#locatedOnLevel').hide();
		$('#numberOfLevels').hide();
		$('#ceilingHeight').hide();
	}
	
	if($('#  atus').length >0) {

		var path = '<?php echo $this->pathController; ?>';
		
		switch(id) {
			case '1' :
				path += 'get-buy-statuses';
				break;
			case '2':
				path += 'get-rent-statuses';
				break;
			case '3':
				path += 'get-book-statuses';			
				break;
			default:
				path = '';
				break;
		}
		
		if(path == '') {
			$('#  atus').hide();
			$('#property_status_id').html('<option value=""></option>');			
		} else {
			$('#property_status_id').html('<option value="">loading...</option>');
			
			$('#  atus').show();
			if(statusId != undefined && statusId != '') {	
				path += '/sid/'+statusId;
			}
			
			$.get(path, function(data) {
				$('#property_status_id').html(data);
			});
		}
		
	}
}

jQuery.al.countryChange = function(cityId) {
	var cid = $('#country_id').val();
	
	$('#city_id').html('<option value="">loading...</option>');
	$.al.hideZone();
	
	var path = '<?php echo $this->pathController; ?>get-cities/id/'+cid; 
	if(cityId != undefined && cityId != '') {	
		path += '/cid/'+cityId;
	}
	
	$.get(path, function(data) {
		if(data == '') {
			$('#city_id').html('<option value="">no cities</option>');
			$('#cityItem').hide();
		} else {
			$('#city_id').html(data);
			$("#city_id").selectBox('options', data);
			$('#cityItem').show();			
		}
	});
};

jQuery.al.hideZone = function() {
	$('#zone_id').html('<option value="">no zones</option>');
	$('#zoneItem').hide();
}

jQuery.al.showZone = function() {
	$('#zoneItem').show();
}

jQuery.al.cityChange = function(cityId,zoneId) {
	if(cityId != undefined && cityId != '') {	
		var id = cityId;
	} else {
		var id = $('#city_id').val();		
	}

	
	$('#zone_id').html('<option value="">loading...</option>');

	var path = '<?php echo $this->pathController; ?>get-zones/id/'+id

	if(zoneId != undefined) {
		path += '/zid/'+zoneId;
	}
	
	$.get(path, function(data) {
		if(data == '') {
			$.al.hideZone();
		} else {
			$("#zone_id").selectBox('options', data);
			$.al.showZone();
		}
	});
}
</script>

<div class="single-column" >



<?php $this->message->show(); ?>
<h1><?php echo $this->pageHeading; ?></h1> 

<?php $this->tabs->displayProperty(); ?>

<div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;" >    


<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" >
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_GENERAL_INFORMATION); ?></h3>
<table width="100%" border="0" cellspacing="0" >
              <tr>
                <td width="99%" colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr  >
                    <td width="119"  class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_TRANSACTION); ?>:</td>
                    <td width="87%"  class="field" >
                    
                    
						<?php if($this->property->getTemp()=='0') { ?>
                            <span class="fieldText" ><?php echo $this->property->getTransactionType(); ?>	</span>
                            <input name="property_transaction_id" id="property_transaction_id" type="hidden" value="<?php echo $this->property->getPropertyTransactionId(); ?>" />
                        <?php } else { ?>
                            <select name="property_transaction_id" id="property_transaction_id" style="width:120px;">
                              <?php echo $this->form->ddPropertyTransactionId(); ?>
                            </select>                        
                        <?php } ?>
					</td>
                  </tr>
                  <?php if(
						$this->property->getTemp()=='0' && 
						$this->property->getPropertyStatusId() != Bl_Data_Statuses::PENDING_APPROVAL && 
						$this->property->getPropertyStatusId() != Bl_Data_Statuses::EXPIRED
					){ ?>
                      <tr id="  atus"  >
                        <td class="label"  ><?php echo $this->getStr(Al_Language::PROP_LBL_STATUS); ?>:</td>
                        <td  class="field"><select name="property_status_id" id="property_status_id"  style="width:120px;">
                          <?php echo $this->form->ddPropertyStatusId(); ?>
                        </select></td>
                      </tr>
                  <?php } ?>
                  <?php if(
						$this->property->getPropertyStatusId() == Bl_Data_Statuses::PENDING_APPROVAL || 
						$this->property->getPropertyStatusId() == Bl_Data_Statuses::EXPIRED
					){ ?>
                      <tr id="  atus"  >
                        <td  class="label"><?php echo $this->getStr(Al_Language::PROP_LBL_STATUS); ?>:</td>
                        <td  class="field" ><?php echo $this->property->getStatus(); ?></td>
                      </tr>                    
                  <?php } ?>                    
                  <tr  >
                    <td  class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_PROPERTY_TYPE); ?>:</td>
                    <td class="field" ><select name="property_type_id" id="property_type_id"  style="width:120px;">
                      <?php echo $this->form->ddPropertyTypeId(); ?>
                    </select></td>
                  </tr>
                  <tr  >
                    <td  class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_PRICE); ?>:</td>
                    <td  class="field"><input   name="price" type="text" id="price" value="<?php echo $this->form->getPrice(); ?>" style="width:110px;" /> 

                      <div class="post-price-currency"><span class="fieldText" style="padding-right:5px;"><?php echo $this->getStr(Al_Language::PROP_LBL_MILLION_TOMAN); ?></span> </div>

                      <div class="post-price-type" id="itemPriceType">
                          <select name="property_price_type_id" id="property_price_type_id"  style="width:110px;">
                              <option value="">Price Type</option>
                              <?php echo $this->form->ddPropertyPriceTypeId(); ?>
                          </select>
                      </div>
                      
                      <div style="clear:both; display:block; padding-top:6px;">
                      <?php echo $this->getStr(Al_Language::PROP_LBL_PRICE_HINT); ?>
                      </div>
                    </td>
                  </tr>
                  <tr  id="itemDeposit">
                    <td class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_DEPOSIT); ?>:</td>
                    <td  class="field"><input   name="deposit" type="text" id="deposit" value="<?php echo $this->form->getDeposit(); ?>" size="15" />   <span class="fieldText">                     <?php echo $this->getStr(Al_Language::PROP_LBL_MILLION_TOMAN); ?></span>
                      </td>
                  </tr>
                  <tr  >
                    <td class="label" style="padding-top:5px;" ><?php echo $this->getStr(Al_Language::PROP_LBL_SWIMMING_POOL ); ?>:</td>
                    <td  class="field">
                   	  <input name="swimming_pool" type="radio" id="swimming_pool" value="<?php echo Bl_Data_PoolTypes::NO; ?>" <?php echo $this->form->rbSwimmingPool(Bl_Data_PoolTypes::NO); ?> />
                   	  <span class="fieldText"><?php echo $this->getStr(Al_Language::PROP_LBL_SWIMMING_POOL_NO); ?></span>
                      <input name="swimming_pool" type="radio" id="swimming_pool" value="<?php echo Bl_Data_PoolTypes::PRIVATE_POOL; ?>" <?php echo $this->form->rbSwimmingPool(Bl_Data_PoolTypes::PRIVATE_POOL); ?> />
                      <span class="fieldText"><?php echo $this->getStr(Al_Language::PROP_LBL_SWIMMING_POOL_Private); ?></span>
                      <input name="swimming_pool" type="radio" id="swimming_pool" value="<?php echo Bl_Data_PoolTypes::PUBLIC_POOL; ?>" <?php echo $this->form->rbSwimmingPool(Bl_Data_PoolTypes::PUBLIC_POOL); ?> />
                      <span class="fieldText"><?php echo $this->getStr(Al_Language::PROP_LBL_SWIMMING_POOL_Public); ?></span></td>
                  </tr>
                </table>                  </td>
        </tr>
            </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="vertical-align:top;" width="33%">
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="label" width="119" nowrap="nowrap"  ><?php echo $this->getStr(Al_Language::PROP_LBL_LAND_AREA); ?>:</td>
                      <td class="field" width="65%"  ><input   name="land_area" type="text" id="land_area" value="<?php echo $this->form->getLandArea(); ?>" style="width:110px;" />
                        <span class="fieldText">m2</span></td>
                    </tr>
                    <tr  >
                      <td class="label" nowrap="nowrap"  ><?php echo $this->getStr(Al_Language::PROP_LBL_BUILD_AREA); ?>:</td>
                      <td class="field"  ><input   name="build_area" type="text" id="build_area" value="<?php echo $this->form->getBuildArea(); ?>" style="width:110px;" />
                      <span class="fieldText">m2</span></td>
                    </tr>
                    <tr id="itemLandWidth">
                      <td class="label"  nowrap="nowrap"><?php echo $this->getStr(Al_Language::PROP_LBL_LAND_WIDTH); ?>:</td>
                      <td class="field" ><input name="land_width" type="text" id="land_width" value="<?php echo $this->form->getLandWidth(); ?>" style="width:110px;" />
                      <span class="fieldText"><?php echo $this->getStr(Al_Language::PROP_LBL_METERS); ?></span></td>
                    </tr>
                    <tr id="itemLandHeight">
                      <td class="label"  nowrap="nowrap"><?php echo $this->getStr(Al_Language::PROP_LBL_LAND_LENGTH); ?>:</td>
                      <td class="field" ><input name="land_height" type="text" id="land_height" value="<?php echo $this->form->getLandHeight(); ?>" style="width:110px;" />
                      <span class="fieldText"><?php echo $this->getStr(Al_Language::PROP_LBL_METERS); ?></span></td>
                    </tr>
                </table></td>
                <td style="vertical-align:" width="33%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr  >
                    <td width="35%" class="label"  ><?php echo $this->getStr(Al_Language::PROP_LBL_BEDROOMS); ?>:</td>
                    <td width="65%" class="field" ><input   name="bedrooms" type="text" id="bedrooms" value="<?php echo $this->form->getBedrooms(); ?>" style="width:110px;" /></td>
                  </tr>
                  <tr  >
                    <td class="label"  ><?php echo $this->getStr(Al_Language::PROP_LBL_BATHROOMS); ?>:</td>
                    <td  class="field" ><input   name="bathrooms" type="text" id="bathrooms" value="<?php echo $this->form->getBathrooms(); ?>" style="width:110px;" /></td>
                  </tr>
                  <tr id="itemTerrace">
                    <td class="label"  width="13%"><?php echo $this->getStr(Al_Language::PROP_LBL_TERRACE); ?>:</td>
                    <td class="field"  width="87%"><input name="terrace_m2" type="text" id="terrace_m2" value="<?php echo $this->form->getTerraceM2(); ?>" style="width:110px;" />
                      <span class="fieldText">m2</span></td>
                  </tr>
                  <tr id="itemAverageCeilingHeight">
                    <td class="label"  nowrap="nowrap"><?php echo $this->getStr(Al_Language::PROP_LBL_CEILING_HEIGHT); ?>:</td>
                    <td class="field" ><input name="average_ceiling_height" type="text" id="average_ceiling_height" value="<?php echo $this->form->getAverageCeilingHeight(); ?>" style="width:110px;" />
                      <span class="fieldText"><?php echo $this->getStr(Al_Language::PROP_LBL_METERS); ?></span></td>
                  </tr>
                </table></td>
                <td style="vertical-align:" width="33%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr  >
                    <td width="35%" nowrap="nowrap" class="label"  ><?php echo $this->getStr(Al_Language::PROP_LBL_PARKING); ?>:</td>
                    <td width="65%" class="field"  ><input   name="parking2" type="text" id="parking2" value="<?php echo $this->form->getParking(); ?>" style="width:110px;"/></td>
                  </tr>
                  <tr id="locatedOnLevel">
                    <td width="13%" nowrap="nowrap" class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_LOCATED_ON_LEVEL); ?>:</td>
                    <td width="87%" class="field" ><input name="located_on_level" type="text" id="located_on_level" value="<?php echo $this->form->getLocatedOnLevel(); ?>" style="width:110px;" /></td>
                  </tr>
                  <tr id="numberOfLevels">
                    <td nowrap="nowrap" class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_NUMBER_OF_LEVELS); ?>	    :</td>
                    <td class="field" ><input name="number_of_levels" type="text" id="number_of_levels" value="<?php echo $this->form->getNumberOfLevels(); ?>" style="width:110px;" /></td>
                  </tr>
                  <tr id="ceilingHeight">
                    <td nowrap="nowrap" class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_AVERAGE_CEILING_HEIGHT); ?>:</td>
                    <td class="field" ><input name="ceiling_height" type="text" id="ceiling_height" value="<?php echo $this->form->getCeilingHeight(); ?>" style="width:110px;" />
                     <span class="fieldText"> <?php echo $this->getStr(Al_Language::PROP_LBL_METERS); ?></span></td>
                  </tr>
                </table></td>
              </tr>
            </table>
<div class="imgpad"></div>
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_LOCATION); ?></h3>                 
<table width="100%"  border="0" cellspacing="0" cellpadding="0"> 
              <tr> 
   	              <td class="label" width="119"><label><?php echo $this->getStr(Al_Language::PROP_LBL_ADDRESS); ?>:</label></td> 
                  <td width="791" class="field"><span  >
                    <input   name="address" type="text" id="address" value="<?php echo $this->form->getAddress(); ?>" size="30" />
                  </span></td> 
              </tr>
              <tr>
                <td class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_COUNTRY); ?>:</td>
                <td class="field"><select name="country_id" id="country_id" >
                  <?php echo $this->form->ddCountryId(); ?>
                </select></td>
              </tr>
              <tr id="cityItem" style="display:none;">
                <td class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_CITY); ?>:</td>
                <td class="field"><select name="city_id" id="city_id">
                  <?php echo $this->form->ddCityId(); ?>
                </select></td>
              </tr>
              <tr id="zoneItem" style="display:none;">
                <td class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_ZONE); ?>:</td>
                <td class="field"><select name="zone_id" id="zone_id">
                </select></td>
              </tr> 
              <tr>
                  <td class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_NEW_ZONE); ?>:</td> 
                  <td class="field"><input name="zone" type="text" id="zone" value="<?php echo $this->form->getZone(); ?>" size="30" /></td> 
              </tr> 
              <tr> 
   	              <td class="label" ><?php echo $this->getStr(Al_Language::PROP_LBL_POSTAL_CODE); ?>:</td> 
                  <td class="field"><input name="postal_code" type="text" id="postal_code" value="<?php echo $this->form->getPostalCode(); ?>" size="30" /></td> 
              </tr> 
              <tr> 
   	              <td colspan="2" valign="top"></td> 
              </tr> 
          </table>
<div class="imgpad"></div>
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_COMMENTS); ?></h3>        
        
<table width="200" border="0">
   	                  <tr>
   	                    <td colspan="2">
                        <div style="clear:both; display:block; padding-top:6px;"><?php echo $this->getStr(Al_Language::PROP_LBL_COMMENTS_HINT); ?></div></td>
                    </tr>
                  <tr>
   	                    <td class="label"><label><?php echo $this->getStr(Al_Language::PROP_LBL_COMMENTS_ENGLISH); ?>:</label></td>
   	                    <td class="label"><label><?php echo $this->getStr(Al_Language::PROP_LBL_COMMENTS_PERSIAN); ?>:</label></td>
                    </tr>
   	                  <tr>
   	                    <td class="field" style="padding:3px 0px;"><textarea   name="comments_eng" cols="45" rows="6" id="comments_eng"><?php echo $this->form->getCommentsEng(); ?></textarea></td>
   	                    <td class="field" style="padding:3px 6px;"><textarea   name="comments_per" cols="45" rows="6" id="comments_per"><?php echo $this->form->getCommentsPer(); ?></textarea></td>
                    </tr>

                </table>        
        
<div class="imgpad"></div>
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_INTRODUCTION); ?></h3>
<?php $options = $this->form->cbGroupOptionsIntroduction(); ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                
                
				<?php foreach($options as $section) { ?>
                <tr>
                    <?php foreach($section as $item) { ?>
					<td width="33%" style="padding:0px;">	
                      <?php if($item['label'] != '') { ?>
                       	<div class="cbBox">
                          <input name="introduction[<?php echo $item['id']; ?>]" type="checkbox" id="introduction[<?php echo $item['id'] ?>]" value="<?php echo $item['id']; ?>" <?php echo $this->form->cbGroupIntroduction($item['id']); ?>>
					    </div>                              
   	                      <label class="cbLabel" for="introduction[<?php echo $item['id']; ?>]"><?php echo $item['label']; ?></label>
                      <?php } else { ?>
                       	&nbsp;
                      <?php } ?>
                    </td>
                    <?php } ?>
			    </tr>
                <?php } ?>
            </table>
<div class="imgpad"></div>
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_PROPERTIES); ?></h3>            
    <?php $options = $this->form->cbGroupOptionsFeature(); ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<?php foreach($options as $section) { ?>
                <tr>
                    <?php foreach($section as $item) { ?>
					<td  width="33%" >	
                      <?php if($item['label'] != '') { ?>
                       	<div class="cbBox" >
                          <input name="features[<?php echo $item['id']; ?>]" type="checkbox" id="features_<?php echo $item['id'] ?>" value="<?php echo $item['id']; ?>" <?php echo $this->form->cbGroupFeature($item['id']); ?>>
                        </div>
   	                      <label class="cbLabel" for="features[<?php echo $item['id']; ?>]"><?php echo $item['label']; ?></label>                        
                      <?php } else { ?>
                       	&nbsp;
                      <?php } ?>
                    </td>
                    <?php } ?>
			    </tr>
                <?php } ?>
            </table>
<div class="imgpad"></div>
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_CONTACT_DETAILS); ?></h3>            
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr  >
              <td class="label" width="13%"  ><?php echo $this->getStr(Al_Language::PROP_LBL_CONTACT_NAME); ?>:</td>
              <td width="87%" class="field" ><input   name="contact_name" type="text" id="contact_name" value="<?php echo $this->form->getContactName(); ?>" size="30" /></td>
            </tr>
            <tr  >
              <td class="label"><?php echo $this->getStr(Al_Language::PROP_LBL_CONTACT_PHONE); ?>:</td>
              <td class="field" ><input   name="contact_phone" type="text" id="contact_phone" value="<?php echo $this->form->getContactPhone(); ?>" size="30" /></td>
            </tr>
            <tr  >
              <td  class="label"><?php echo $this->getStr(Al_Language::PROP_LBL_CONTACT_EMAIL); ?>:</td>
              <td  class="field"><input   name="contact_email" type="text" id="contact_email" value="<?php echo $this->form->getContactEmail(); ?>" size="30" /></td>
            </tr>

        </table>

<div class="imgpad"></div>
		<input name="buySearch" type="submit" value="<?php echo $this->getStr(Al_Language::PROP_BTN_INTRO_NEXT); ?>">        
</form>
<?php $this->tabs->displayEnd(); ?>

</div>

</div>