<script type="text/javascript" >

$("#intro-box").corner();

$(document).ready(function() {
	$('#frmR').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
});
</script>

<div class="single-column" >
<?php $this->message->show(); ?>
<h1><?php echo $this->pageHeading; ?></h1> 

<?php $this->tabs->displayProperty(); ?>


<div class="result-form" id="intro-box" style="width:910px; margin-bottom:15px;" >    

<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" enctype="multipart/form-data" >

<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_UPLOAD_IMAGES); ?></h3>      
<div style="clear:both; display:block; padding:6px 0px;">      
<?php echo $this->getStr(Al_Language::PROP_LBL_UPLOAD_IMAGES_HINT); ?>
</div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="label"   width="13%"><?php echo $this->getStr(Al_Language::PROP_LBL_IMAGE_1); ?>:
                  <input name="__frm" type="hidden" id="__frm" value="1">
</td>
                <td  width="87%"><input type="file" name="image_1" id="image_1"> <?php echo $this->form->getImage1(); ?> </td>
            </tr>
            <tr>
                <td class="label"   width="13%"><?php echo $this->getStr(Al_Language::PROP_LBL_IMAGE_2); ?>:</td>
                <td  width="87%"><input type="file" name="image_2" id="image_2"> <?php echo $this->form->getImage2(); ?> </td>
            </tr>
            <tr>
                <td class="label"   width="13%"><?php echo $this->getStr(Al_Language::PROP_LBL_IMAGE_3); ?>:</td>
                <td  width="87%"><input type="file" name="image_3" id="image_3"> <?php echo $this->form->getImage3(); ?> </td>
            </tr>
            <tr>
                <td  class="label"  width="13%"><?php echo $this->getStr(Al_Language::PROP_LBL_IMAGE_4); ?>:</td>
                <td  width="87%"><input type="file" name="image_4" id="image_4"> <?php echo $this->form->getImage4(); ?> </td>
            </tr>
            <tr>
                <td class="label"   width="13%"><?php echo $this->getStr(Al_Language::PROP_LBL_IMAGE_5); ?>:</td>
                <td  width="87%"><input type="file" name="image_5" id="image_5"> <?php echo $this->form->getImage5(); ?> </td>
            </tr>                                                
            <tr>
              <td  >&nbsp;</td>
              <td  >
                <input name="btnUpload" type="submit" value="<?php echo $this->getStr(Al_Language::PROP_BTN_UPLOAD_IMAGES); ?>" id="btnUpload">
</td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>












</form>

<?php if(count($this->images) > 0) { ?>
<div class="imgpad"></div>
<h3 class="right-arrow"><?php echo $this->getStr(Al_Language::PROP_SUB_PROPERTY_IMAGES); ?></h3>
<div style="background-color:#FFF; padding:10px; border: solid 1px #CCC;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php foreach($this->images as $group) { ?>
  <tr>
  	<?php foreach($group as $image) { ?>

        <td width="25%" align="center">
        <?php if($image['id'] != '') { ?>
        <img src="<?php echo $image['path_th']; ?>" />
        <?php } else { echo '&nbsp;'; } ?>
        </td>	
    <?php } ?>
  </tr>
  <tr>
  	<?php foreach($group as $image) { ?>  
        <td align="center" style="padding:4px; padding-bottom:10px;">
        <?php if($image['id'] != '') { ?>
            <?php if($image['main_image'] == '1') { ?>
                <?php echo $this->getStr(Al_Language::PROP_LBL_MAIN); ?>
                <?php } else { ?>
                <a href="<?php echo $this->pathController . 'set-main-image/iid/'.$image['id']; ?>"><?php echo $this->getStr(Al_Language::PROP_LBL_SET_AS_MAIN); ?></a>
                <?php } ?> | <a href="<?php echo $this->pathController . 'delete-image/iid/'.$image['id']; ?>"><?php echo $this->getStr(Al_Language::PROP_LBL_DELETE); ?></a>
            <?php } else { echo '&nbsp;'; } ?>
        </td>
	<?php } ?>        
  </tr>
<?php } ?>
</table>
</div>
<?php } ?>

<div class="imgpad"></div>
		<input onClick="location='<?php echo $this->pathController; ?>map'" name="buySearch" type="button" value="<?php echo $this->getStr(Al_Language::PROP_BTN_IMAGES_NEXT); ?>">  
</div>        
<?php $this->tabs->displayEnd(); ?>

</div>