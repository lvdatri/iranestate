<script type="text/javascript"> 
$(".page-nav-sml").corner();
$(document).ready(function() 
    { 


    } 
);
</script> 
<div class="single-column" >
  <div class="page-nav-sml">
    <div style="padding-left:10px;"><a href="<?php echo $this->cmsPath; ?>"><?php echo $this->getStr(Al_Language::TMPLT_ARTICLES); ?></a> > <a href="<?php echo $this->cms->getPath(true); ?>"><?php echo $this->cms->getTitle(); ?></a></div>
  </div>

<div id="articleContent">

<?php if($this->cms->getImage()!='') { ?>
<div id="articleTop">
<img src="<?php echo $this->cms->getImageLrg(); ?>" />
</div>
<?php } ?>

<div id="articleBottom">
<h1><?php echo $this->cms->getTitle(); ?></h1>
<?php echo $this->cms->getContent(); ?>
</div>

<div class="imgpad"></div>

<div id="articleSumary">
        <table width="100%" border="0" style="text-align:center;">
          <tr>
            <td style="width:33%;">&nbsp;</td>
            <td style="width:33%;"><?php echo $this->getStr(Al_Language::TMPLT_ARTICLE_ID); ?>: <?php echo $this->cms->getId(); ?></td>
            <td style="width:33%;"><?php echo $this->getStr(Al_Language::TMPLT_LISTED_DATE); ?>: <?php echo $this->cms->getDateCreated(true); ?></td>
          </tr>
        </table>
</div>


</div>
</div>