<style>

.batchTable {
	border-collapse:collapse;
	
}

.batchTable td {
	font-family:Verdana, Geneva, sans-serif;
	padding:3px;
	font-size:12px;
	border-bottom:solid 1px #CCC;
}

</style>
<form name="form1" method="post" action="/batch/form">
<table class="batchTable" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="1%" nowrap>Use</td>
    <td width="1%" nowrap>Field</td>
    <td width="1%" nowrap>Type</td>
    <td width="1%" nowrap bgcolor="#F1F0C7">Validation</td>
    <td width="1%" nowrap bgcolor="#F1F0C7">&nbsp;</td>
    <td width="1%" nowrap>Form</td>
    <td nowrap>Browse</td>
  </tr>
  <?php foreach($this->data as $field) {  ?>
  <tr>
    <td valign="top">
      <input name="use__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('use__'.$field['field_name']); ?> value="1">
</td>
    <td valign="top" nowrap><?php echo $field['field_name']; ?></td>
    <td valign="top"><select name="type__<?php echo $field['field_name']; ?>" >
      <option value="field" <?php $this->setDd('type__'.$field['field_name'],'field'); ?> >field</option>
      <option value="date" <?php $this->setDd('type__'.$field['field_name'],'date'); ?>>date</option>
    </select></td>
    <td valign="top" nowrap="nowrap" bgcolor="#F1F0C7">
    <div>    <input name="required__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('required__'.$field['field_name']); ?> value="1"> required</div>
      <div><input name="v_email__<?php echo $field['field_name']; ?>" type="checkbox"  <?php $this->setCb('v_email__'.$field['field_name']); ?> value="1"> email
        </div>
      <div><input name="v_int__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('v_int__'.$field['field_name']); ?>  value="1"> int
        </div>
      <div><input name="v_date__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('v_date__'.$field['field_name']); ?>  value="1"> date
        </div>
      <div><input name="v_dbUnique__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('v_dbUnique__'.$field['field_name']); ?>  value="1"> dbUnique</div>    
      
    </td>
    <td valign="top" nowrap="nowrap" bgcolor="#F1F0C7">
    <div>
    	min length
          <input name="v_length_min__<?php echo $field['field_name']; ?>" type="text" id="textfield" value="<?php echo Al_Utilities::post('v_length_min__'.$field['field_name']); ?>" size="5" />
</div>        
    <div>
   		 max length
	     <input name="v_length_max__<?php echo $field['field_name']; ?>" type="text" id="textfield" value="<?php 
	 
		 if($field['type'] == Al_Record_Field::STRING  && array_key_exists('v_length_max__'.$field['field_name'],$_POST)==false) {
			echo $field['max_length'];			 
		 } else {
			echo Al_Utilities::post('v_length_max__'.$field['field_name']); 			 
		 }
			
		 ?>" size="5" />
    </div>        
    <div>
    	 
    	not equal to
          <input name="v_notEqualTo_toField__<?php echo $field['field_name']; ?>" type="text" id="textfield" value="<?php echo Al_Utilities::post('v_notEqualTo_toField__'.$field['field_name']); ?>" size="20" />
    </div>            
    
    <div>
    	equal to
          <input name="v_equalTo_toField__<?php echo $field['field_name']; ?>" type="text" id="textfield" value="<?php echo Al_Utilities::post('v_equalTo_toField__'.$field['field_name']); ?>" size="20" />
    </div>     
    
    </td>
    <td valign="top" nowrap="nowrap">
<div>
    section name
    <input name="f_section_name__<?php echo $field['field_name']; ?>" type="text" id="textfield" value="<?php echo Al_Utilities::post('f_section_name__'.$field['field_name']); ?>" size="20" />
</div> 
<div>
    label
    <input name="f_label__<?php echo $field['field_name']; ?>" type="text" id="textfield" value="<?php echo Al_Utilities::post('f_label__'.$field['field_name']); ?>" size="20" />
</div> 

<div>
    type
	<select name="f_type__<?php echo $field['field_name']; ?>" >
      <option value="text" <?php $this->setDd('f_type__'.$field['field_name'],'text'); ?> >text</option>
      <option value="textarea" <?php $this->setDd('f_type__'.$field['field_name'],'textarea'); ?>>textarea</option>
      <option value="dropdown" <?php $this->setDd('f_type__'.$field['field_name'],'dropdown'); ?>>dropdown</option>
      <option value="checkbox" <?php $this->setDd('f_type__'.$field['field_name'],'checkbox'); ?>>checkbox</option>
      <option value="password" <?php $this->setDd('f_type__'.$field['field_name'],'password'); ?>>password</option>            
      <option value="textarea_vertical" <?php $this->setDd('f_type__'.$field['field_name'],'textarea_vertical'); ?>>textarea_vertical</option>      
    </select>
</div>    
    </td>
    <td valign="top">
		<div><input name="c_sort_column__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('c_sort_column__'.$field['field_name']); ?> value="1"> 
		  sort column 
          <input name="c_default_sort_column__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('c_default_sort_column__'.$field['field_name']); ?> value="1">  default
        </div>
        <div><input name="c_search_field__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('c_search_field__'.$field['field_name']); ?> value="1"> search field</div>
        <div><input name="c_select_field__<?php echo $field['field_name']; ?>" type="checkbox" <?php $this->setCb('c_select_field__'.$field['field_name']); ?> value="1"> 
        select field</div>        
    </td>
  </tr>
  <?php } ?>
  <tr>
    <td colspan="7"><input type="submit" name="button" id="button" value="Submit">
      <input onclick="location='<?php echo $this->pathAction; ?>clear'" type="button" name="button2" id="button2" value="Clear" />
      <input name="action" type="hidden" id="action" value="1">
      </td>
  </tr>
</table>
</form>