<script type="text/javascript">
	$(".browse-city404").corner();
	$(".right-col").corner();
</script>
<div class="single-column" >
<h1><?php echo $this->getStr(Al_Language::ERROR_404_TITLE); ?></h1> 

<div class="left-col">

<?php 
	$city = new Bl_Record_City();
	$featuredNationalCities = $city->retrieveNatioanlFeatured();
	$noOfRecords = count($featuredNationalCities);
	$featuredNationalCities = array_chunk($featuredNationalCities, ceil($noOfRecords / 3));

?>

<div class="browse-city404"> <img src="<?php echo $this->_settings->path_images; ?>search_browsecities.png" width="26" height="23" alt="browse cities" />
<h2><?php echo $this->getStr(Al_Language::ERROR_404_PROPERTIES_FOR_SALE); ?></h2>
<div>
		<?php $i=0; foreach($featuredNationalCities as $items) { $i++;?>
            <ul class="cityList <?php if($i==3) { echo 'cityPad'; } ?>">
            <?php foreach($items as $item) { $city->loadFromArray($item); ?>
                <?php if($city->getId() != '') { ?>
                <li><a href="<?php echo $city->getPropertySearchPath(Bl_Record_Property::BUY_PATH); ?>"><?php echo $city->getCity(); ?></a></li>
                <?php } ?>
            <?php } ?>
          </ul>
      <?php } ?>
</div>
</div>

<div class="browse-city404"> <img src="<?php echo $this->_settings->path_images; ?>search_browsecities.png" width="26" height="23" alt="browse cities" />
<h2><?php echo $this->getStr(Al_Language::ERROR_404_PROPERTIES_FOR_RENT); ?></h2>
<div>
		<?php $i=0; foreach($featuredNationalCities as $items) { $i++;?>
            <ul class="cityList <?php if($i==3) { echo 'cityPad'; } ?>">
            <?php foreach($items as $item) { $city->loadFromArray($item); ?>
                <?php if($city->getId() != '') { ?>
                <li><a href="<?php echo $city->getPropertySearchPath(Bl_Record_Property::RENT_PATH); ?>"><?php echo $city->getCity(); ?></a></li>
                <?php } ?>
            <?php } ?>
          </ul>
      <?php } ?>
</div>
</div>


<div class="browse-city404" style="margin-bottom:0px;"> <img src="<?php echo $this->_settings->path_images; ?>search_browsecities.png" width="26" height="23" alt="browse cities" />
<h2><?php echo $this->getStr(Al_Language::ERROR_404_PROPERTIES_FOR_BOOKING); ?></h2>
<div>
		<?php $i=0; foreach($featuredNationalCities as $items) { $i++;?>
            <ul class="cityList <?php if($i==3) { echo 'cityPad'; } ?>">
            <?php foreach($items as $item) { $city->loadFromArray($item); ?>
                <?php if($city->getId() != '') { ?>
                <li><a href="<?php echo $city->getPropertySearchPath(Bl_Record_Property::BOOK_PATH); ?>"><?php echo $city->getCity(); ?></a></li>
                <?php } ?>
            <?php } ?>
          </ul>
      <?php } ?>
</div>
</div>

</div> 
<!-- end left column -->

        <div class="right-col">
              <h2><?php echo $this->getStr(Al_Language::HOME_TITLE_RECENT_NEWS); ?></h2>
              
              <?php 
			  $cms = new Bl_Record_Cms();
   			  $news = $cms->retrieveByType(Bl_Data_CmsTypes::news,3);
			  foreach($news as $item) { ?>
              <a title="<?php echo $item->getDescription(); ?>" href="<?php echo $item->getPath(true); ?>">
				<?php if($item->getImage() != '') { ?>
                    <img width="150" title="<?php echo $item->getDescription(); ?>" height="50" border="0" src="<?php echo $item->getImageSml(); ?>" />
                <?php } ?>
                

              <p><?php echo $item->getDateCreated(true); ?> - <?php echo $item->getDescription(); ?></p>
			   </a>
   		      <?php } ?>              
            </div>
            <!--END RIGHT COL--> 

</div>


