<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Add Property',
        minWidth:'80',
        style: Ext.al.btn.floatRight,
        icon:'<?php echo $this->_settings->path_images;?>icons/insert.png',
        handler: function(e) {
            this.disable();
            location='<?php echo $this->pathController; ?>insert';
        }
    });
    Ext.get('formSearch').on('submit',function (e,t) {
        e.stopEvent();
        Ext.al.sendFormUri('formSearch');
    });
}
</script>
<div class="page_header">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php $this->nav->show(); ?></h1> </td>
        <td width="29%" ><div id="pageActions"></div></td>
    </tr>
</table>
</div>
<div class="page">
<?php $this->message->show(); ?>

<form name="formSearch" id="formSearch" action="<?php echo $this->pathController.'browse/'; ?>" method="get" ><?php echo $this->params->to_fields(array(Al_UrlParams::SORTER)); ?>
	<div class="frmBody">
      <table  width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" colspan="2" nowrap="nowrap" class="frmL">Id: <span class="frmFBg">
            <input class="frmF" name="prop" type="text" id="prop" value="<?php echo Al_Utilities::eGet('prop'); ?>" size="16" />
          </span>Transaction:
<select name="trans" id="trans">
<?php echo $this->form->ddPropertyTransactionId(Al_Utilities::eGet('trans')); ?>
</select>
			Type: 
            <select name="type" id="type">
				<?php echo $this->form->ddPropertyTypeId(Al_Utilities::eGet('type')); ?>
            </select>            
         
          </td>
        </tr>
        <tr>
          <td colspan="2" nowrap="nowrap" class="frmL">Country:<span class="frmFBg">
            <select name="country" id="country">
				<?php echo $this->form->ddCountryId(Al_Utilities::eGet('country')); ?>
            </select>
            
          City:
          <input class="frmF" name="city" type="text" id="city" value="<?php echo Al_Utilities::eGet('city'); ?>" size="20" />
          Email:
          <input class="frmF" name="email" type="text" id="email" value="<?php echo Al_Utilities::eGet('email'); ?>" size="20" />          
          </span></td>
        </tr>
        <tr>
          <td class="frmSepTD" colspan="2">
			<div class="frmSep"></div>
          </td>
        </tr>        
        <tr>
          <td class="frmTD" colspan="2"><input class="btn" type="submit" name="btnSearch" id="btnSearch" value="Search" />
            <?php if($this->searchTriggered==true) { ?>
            <input onclick="location='<?php echo $this->pathController.'browse/'; ?>page/1/<?php echo $this->params->get(array(Al_UrlParams::SORTER)) ?>';" class="btn" type="button" name="btnClear" id="btnClear" value="Clear" />
            <?php } ?>
          </td>
        </tr>
      </table>
	</div>
</form>


    <table class="grid" id="dataGrid" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th width="1%">id</th>
            <th width="1%"><?php $this->sorter->show_column('property_transaction_id'); ?></th>
            <th width="1%"><?php $this->sorter->show_column('property_type_id'); ?></th>
            <th width="1%"><?php $this->sorter->show_column('property_status_id'); ?></th>
            <th width="1%"><?php $this->sorter->show_column('price'); ?></th>
            <th><?php $this->sorter->show_column('country_id'); ?>,               <?php $this->sorter->show_column('city_id'); ?>,              <?php $this->sorter->show_column('zone_id'); ?>,              <?php $this->sorter->show_column('address'); ?>,              <?php $this->sorter->show_column('postal_code'); ?></th>
            <th width="1%">&nbsp;</th>
        </tr>
    <?php while($row = $this->result->fetch_row()) { 
			$this->property->loadFromArray($row);
	?>
        <tr>
          <td valign="top"><?php echo $this->property->getPropertyId(); ?></td>
            <td valign="top"><?php echo $this->property->getTransactionType(); ?></td>
            <td valign="top"><?php echo $row['property_type_eng']; ?></td>
            <td valign="top"><?php echo $row['status_eng']; ?></td>
            <td valign="top"><?php echo $row['price']; ?></td>
            <td>
			<div style="margin-bottom:4px;"><?php echo $row['country_eng']; ?> - <?php echo $row['city_eng']; ?>
             <?php if($row['zone_eng'] != '') { ?>- <?php echo $row['zone_eng']; ?> <?php } ?>
             - <?php echo $row['postal_code']; ?></div>
			<div style="font-size:10px;">
			<?php echo $row['address']; ?>
            </div>
            
            </td>
            
            <td nowrap="nowrap">
                <input onclick="location='<?php echo $this->pathController; ?>view/pid/<?php echo $row['id']; ?>'" class="btnSml" type="button" name="btnView" id="btnView" value="View" />
                <input onclick="Ext.al.confirm('Delete Item?','<?php echo $this->pathController; ?>delete/id/<?php echo $row['id']; ?>');" class="btnSml" type="button" name="btnDelete" id="btnDelete" value="Delete" />
          </td>
        </tr>
    <?php } ?>
        <?php if($this->pager->total_num_of_records < 1) { ?>
        <tr>
            <td colspan="7">No records found.</td>
        </tr>
        <?php } ?>
    </table>
<?php if($this->pager->total_num_of_records > 0) { ?>
<div class="gridFoot">
    <div class="gridNav">
        <?php $this->pager->display_nav(); ?>
    </div>
    <div class="gridFootTxt">
        <?php echo $this->pager->starting_record_num; ?> to <?php echo $this->pager->ending_record_num; ?> of <?php echo $this->pager->total_num_of_records; ?> records
    </div>
</div> 
<?php } ?>
</div>