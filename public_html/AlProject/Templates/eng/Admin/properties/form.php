<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
	
	<?php if($this->form->getMode() == Al_Form::UPDATE) { ?>	
		new Ext.form.DateField({
				format: 'd/m/Y',
				width: 110,
				id: 'expiry_date',
				renderTo: 'f-expiry-date',
				value: '<?php echo $this->form->getExpiryDate(); ?>'
		});	
	<?php } ?>		
	
	Ext.get('country_id').on('change',function(){Ext.al.countryChange()});	
	Ext.get('city_id').on('change',function(){Ext.al.cityChange()});
	
	Ext.al.cityChange('<?php echo $this->form->getZoneId(); ?>');

}


Ext.al.hideZone = function() {
	Ext.getDom('zone_id').innerHTML = '<option value="">no zones</option>'	
	var ce = Ext.get('zoneItem');
	ce.setVisibilityMode(Ext.Element.DISPLAY);
	ce.hide();
}

Ext.al.showZone = function() {
	var ce = Ext.get('zoneItem');	
	ce.setVisibilityMode(Ext.Element.DISPLAY);
	ce.show();	
}

Ext.al.countryChange = function(){
	var conn = new Ext.data.Connection();
	var e = Ext.getDom('country_id');
	var cid = e[e.selectedIndex].value;
	
//	Ext.al.setChContact();
	
	Ext.getDom('city_id').innerHTML = '<option value="">loading...</option>'
	Ext.al.hideZone();
	
	conn.request({
		url: '<?php echo $this->pathController; ?>get-cities/id/'+cid,
		params: {
			action: 'get',
		},
		success: function(resp,opt) { 
			var response = new Ext.ux.al.Response;
			response.setResponse(resp);
			var ds = response.getDataSection('data');
			if(ds == '') {
				Ext.getDom('city_id').innerHTML = '<option value="">no cities</option>'
			} else {
				Ext.getDom('city_id').innerHTML = ds;
			}
		},
		failure: function(resp,opt) { 
		} 
  });
}

Ext.al.cityChange = function(zoneId){

	var conn = new Ext.data.Connection();
	var e = Ext.getDom('city_id');
	var cid = e[e.selectedIndex].value;
	
	var path = '<?php echo $this->pathController; ?>get-zones/id/'+cid;
	if(zoneId != undefined) {
		path += '/zid/'+zoneId;
	}
	
	conn.request({
		url: path,
		params: {
			action: 'get',
		},
		success: function(resp,opt) { 
			var response = new Ext.ux.al.Response;
			response.setResponse(resp);
			var ds = response.getDataSection('data');
			if(ds == '') {
//				Ext.getDom('zone_id').innerHTML = '<option value="">no zones</option>';
				Ext.al.hideZone();
			} else {
				Ext.getDom('zone_id').innerHTML = ds;
				Ext.al.showZone();
			}
		},
		failure: function(resp,opt) { 
		} 
  });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php if($this->form->getMode() == Al_Form::UPDATE) { ?>
	<?php $this->tabs->display($this->form->getId()); ?>
    <?php $this->tab_nav->show(); ?>
<?php } ?>
<?php $this->message->show(); ?>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="frmH2f" colspan="2">General Information</td>
            </tr>
            <tr>
              <td colspan="2" class="frmL"><table width="100%" border="0" cellspacing="0" cellpadding="10">
                <tr>
                  <td width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class="frm">
                      <td class="frmL">Member</td>
                      <td class="frmFBg"><select name="member_id" id="member_id">
                        <?php echo $this->form->ddMemberId(); ?>
                      </select></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Transaction:</td>
                      <td class="frmFBg"><select name="property_transaction_id" id="property_transaction_id">
                        <?php echo $this->form->ddPropertyTransactionId(); ?>
                      </select></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Property Type:</td>
                      <td class="frmFBg"><select name="property_type_id" id="property_type_id">
                        <?php echo $this->form->ddPropertyTypeId(); ?>
                      </select></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Price Type:</td>
                      <td class="frmFBg"><select name="property_price_type_id" id="property_price_type_id">
                        <?php echo $this->form->ddPropertyPriceTypeId(); ?>
                      </select></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Status:</td>
                      <td class="frmFBg"><select name="property_status_id" id="property_status_id">
                        <?php echo $this->form->ddPropertyStatusId(); ?>
                      </select></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Price:</td>
                      <td class="frmFBg"><input class="frmF" name="price" type="text" id="price" value="<?php echo $this->form->getPrice(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Deposit:</td>
                      <td class="frmFBg"><input class="frmF" name="deposit" type="text" id="deposit" value="<?php echo $this->form->getDeposit(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Swimming Pool:</td>
                      <td class="frmFBg">
                      	<input name="swimming_pool" type="radio" id="swimming_pool" value="<?php echo Bl_Data_PoolTypes::NO; ?>" <?php echo $this->form->rbSwimmingPool(Bl_Data_PoolTypes::NO); ?> />
                      	No
                        <input name="swimming_pool" type="radio" id="swimming_pool" value="<?php echo Bl_Data_PoolTypes::PRIVATE_POOL; ?>" <?php echo $this->form->rbSwimmingPool(Bl_Data_PoolTypes::PRIVATE_POOL); ?> />
                        Private
                        <input name="swimming_pool" type="radio" id="swimming_pool" value="<?php echo Bl_Data_PoolTypes::PUBLIC_POOL; ?>" <?php echo $this->form->rbSwimmingPool(Bl_Data_PoolTypes::PUBLIC_POOL); ?> />
                        Public</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Country:</td>
                      <td class="frmFBg"><select name="country_id" id="country_id" >
                        <?php echo $this->form->ddCountryId(); ?>
                      </select></td>
                    </tr>
                    <tr class="frm" id="cityItem">
                      <td class="frmL">City:</td>
                      <td class="frmFBg"><select name="city_id" id="city_id">
                        <?php echo $this->form->ddCityId(); ?>
                      </select></td>
                    </tr>
                    <tr class="frm" id="zoneItem" style="display:none;">
                      <td class="frmL">Zone:</td>
                      <td class="frmFBg"><select name="zone_id" id="zone_id">
                      </select></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Address:</td>
                      <td class="frmFBg"><input class="frmF" name="address" type="text" id="address" value="<?php echo $this->form->getAddress(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Postal Code:</td>
                      <td class="frmFBg"><input class="frmF" name="postal_code" type="text" id="postal_code" value="<?php echo $this->form->getPostalCode(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">New Zone:</td>
                      <td class="frmFBg"><input class="frmF" name="zone" type="text" id="zone" value="<?php echo $this->form->getZone(); ?>" size="30" /></td>
                    </tr>
                  </table></td>
                  <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class="frm">
                      <td class="frmL">Land Area:</td>
                      <td class="frmFBg"><input class="frmF" name="land_area" type="text" id="land_area" value="<?php echo $this->form->getLandArea(); ?>" size="30" />
                        m2</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Build Area:</td>
                      <td class="frmFBg"><input class="frmF" name="build_area" type="text" id="build_area" value="<?php echo $this->form->getBuildArea(); ?>" size="30" />
                        m2</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Terrace:</td>
                      <td class="frmFBg"><input class="frmF" name="terrace_m2" type="text" id="terrace_m2" value="<?php echo $this->form->getTerraceM2(); ?>" size="30" /> 
                        m2</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Land Width:</td>
                      <td class="frmFBg"><input class="frmF" name="land_width" type="text" id="land_width" value="<?php echo $this->form->getLandWidth(); ?>" size="30" />
                        meters</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Land Height:</td>
                      <td class="frmFBg"><input class="frmF" name="land_height" type="text" id="land_height" value="<?php echo $this->form->getLandHeight(); ?>" size="30" /> 
                        meters</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Average Ceiling Height:</td>
                      <td class="frmFBg"><input class="frmF" name="average_ceiling_height" type="text" id="average_ceiling_height" value="<?php echo $this->form->getAverageCeilingHeight(); ?>" size="30" /> 
                        meters</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Located On Level:</td>
                      <td class="frmFBg"><input class="frmF" name="located_on_level" type="text" id="located_on_level" value="<?php echo $this->form->getLocatedOnLevel(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Number Of Levels:</td>
                      <td class="frmFBg"><input class="frmF" name="number_of_levels" type="text" id="number_of_levels" value="<?php echo $this->form->getNumberOfLevels(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Ceiling Height:</td>
                      <td class="frmFBg"><input class="frmF" name="ceiling_height" type="text" id="ceiling_height" value="<?php echo $this->form->getCeilingHeight(); ?>" size="30" /> 
                        meters</td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Bedrooms:</td>
                      <td class="frmFBg"><input class="frmF" name="bedrooms" type="text" id="bedrooms" value="<?php echo $this->form->getBedrooms(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Bathrooms:</td>
                      <td class="frmFBg"><input class="frmF" name="bathrooms" type="text" id="bathrooms" value="<?php echo $this->form->getBathrooms(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">Parking:</td>
                      <td class="frmFBg"><input class="frmF" name="parking" type="text" id="parking" value="<?php echo $this->form->getParking(); ?>" size="30" /></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">&nbsp;</td>
                      <td class="frmFBg">&nbsp;</td>
                    </tr>
					<?php if($this->form->getMode() == Al_Form::UPDATE) { ?>
                    <tr class="frm">
                      <td class="frmL">Expiry Date:</td>
                      <td class="frmFBg"><div id="f-expiry-date"></div></td>
                    </tr>
                    <tr class="frm">
                      <td class="frmL">&nbsp;</td>
                      <td class="frmFBg">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="frmL" width="13%">Featured:</td>
                        <td class="frmFBg" width="87%"><input name="featured" type="checkbox" id="featured" value="1" <?php echo $this->form->cbFeatured(); ?> /></td>
                    </tr>                    
                    <?php } ?>
                  </table></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
                <td colspan="2" valign="top" class="frmL"><table width="100%" border="0">
                <tr>
                      <td  class="frmH2f">Comments English</td>
                      <td  class="frmH2f">Comments Persian</td>
                    </tr>
                    <tr>
                      <td><textarea style="width:95%" class="frmF" name="comments_eng" cols="45" rows="6" id="comments_eng"><?php echo $this->form->getCommentsEng(); ?></textarea></td>
                      <td><textarea style="width:95%" class="frmF ralign" name="comments_per" cols="45" rows="6" id="comments_per"><?php echo $this->form->getCommentsPer(); ?></textarea></td>
                    </tr>
                </table></td>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Introduction</td>
            </tr>
            <tr>
              <td width="13%" class="frmL">&nbsp;</td>
              <td width="87%" class="frmFBg">
			<?php $options = $this->form->cbGroupOptionsIntroduction(); ?>
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<?php foreach($options as $section) { ?>
                    <tr>
                        <?php foreach($section as $item) { ?>
						<td width="33%" style="padding:3px;">	
                          <?php if($item['label'] != '') { ?>
	                          <input name="introduction[]" type="checkbox" id="introduction_<?php echo $item['id'] ?>" value="<?php echo $item['id']; ?>" <?php echo $this->form->cbGroupIntroduction($item['id']); ?>>
    	                      <label for="introduction_<?php echo $item['id']; ?>"><?php echo $item['label']; ?></label>
                          <?php } else { ?>
                          	&nbsp;
                          <?php } ?>
                        </td>
                        <?php } ?>
					</tr>
                    <?php } ?>
                </table>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Features</td>
            </tr>
            <tr>
              <td class="frmL">&nbsp;</td>
              <td class="frmFBg">
			<?php $options = $this->form->cbGroupOptionsFeature(); ?>
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<?php foreach($options as $section) { ?>
                    <tr>
                        <?php foreach($section as $item) { ?>
						<td  width="33%" style="padding:3px;">	
                          <?php if($item['label'] != '') { ?>
	                          <input name="features[]" type="checkbox" id="features_<?php echo $item['id'] ?>" value="<?php echo $item['id']; ?>" <?php echo $this->form->cbGroupFeature($item['id']); ?>>
    	                      <label for="features_<?php echo $item['id']; ?>"><?php echo $item['label']; ?></label>
                          <?php } else { ?>
                          	&nbsp;
                          <?php } ?>
                        </td>
                        <?php } ?>
					</tr>
                    <?php } ?>
                </table>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Contact Details</td>
            </tr>
            <tr>
              <td class="frmL">Contact Name:</td>
              <td class="frmFBg"><input class="frmF" name="contact_name" type="text" id="contact_name" value="<?php echo $this->form->getContactName(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td class="frmL">Contact Phone:</td>
              <td class="frmFBg"><input class="frmF" name="contact_phone" type="text" id="contact_phone" value="<?php echo $this->form->getContactPhone(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td class="frmL">Contact Email:</td>
              <td class="frmFBg"><input class="frmF" name="contact_email" type="text" id="contact_email" value="<?php echo $this->form->getContactEmail(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>