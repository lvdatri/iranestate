<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'navTabActions',
        text:'Update Property',
        minWidth:'80',
        style: Ext.al.btn.floatRight,
        icon:'<?php echo $this->_settings->path_images;?>icons/update.png',
        handler: function(e) {
            this.disable();
            location='<?php echo $this->pathController; ?>update/pid/<?php echo $this->property->getId(); ?>';
        }
    });
    Ext.get('formSearch').on('submit',function (e,t) {
        e.stopEvent();
        Ext.al.sendFormUri('formSearch');
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->tabs->display($this->property->getId()); ?>
<?php echo $this->tab_nav->show(); ?>
<?php $this->message->show(); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td style="padding-right:5px;" width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="viewLbl">Property Id:</td>
        <td class="viewData"><?php echo $this->property->getPropertyId(); ?></td>
      </tr>
      <tr>
        <td class="viewLbl">Member:</td>
        <td class="viewData"><?php echo $this->property->getMemberName(); ?></td>
      </tr>
      <tr>
        <td width="40%" class="viewLbl">Transaction:</td>
        <td width="60%" class="viewData"><?php echo $this->property->getTransactionType(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Property Type:</td>
        <td class="viewData"><?php echo $this->property->getPropertyType(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Price Type:</td>
        <td class="viewData"><?php echo $this->property->getPriceType(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Status:</td>
        <td class="viewData"><?php echo $this->property->getStatus(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Price:</td>
        <td class="viewData"><?php echo $this->property->getPrice(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Swimming Pool:</td>
        <td class="viewData"><?php echo $this->property->getSwimmingPool(true); ?></td>
      </tr>
      <tr>
        <td class="viewLbl">Country:</td>
        <td class="viewData"><?php echo $this->property->getCountry(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">City:</td>
        <td class="viewData"><?php echo $this->property->getCity(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Zone:</td>
        <td class="viewData"><?php echo $this->property->getZone(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Address:</td>
        <td class="viewData"><?php echo $this->property->getAddress(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Postal Code:</td>
        <td class="viewData"><?php echo $this->property->getPostalCode(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">&nbsp;</td>
        <td class="viewData">&nbsp;</td>
        </tr>
      <tr>
        <td class="viewLbl">Comments English:</td>
        <td class="viewData"><?php echo $this->property->getCommentsEng(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Comments Persian:</td>
        <td class="viewData"><?php echo $this->property->getCommentsPer(); ?></td>
      </tr>
      <tr>
        <td class="viewLbl">Introduction:</td>
        <td class="viewData"><?php echo $this->property->getIntroduction(true); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Features:</td>
        <td class="viewData"><?php echo $this->property->getFeatures(true); ?></td>
        </tr>
      </table></td>
    <td style="padding-left:5px;" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="40%" class="viewLbl">Land Area:</td>
        <td width="60%" class="viewData"><?php echo $this->property->getLandArea(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Build Area:</td>
        <td class="viewData"><?php echo $this->property->getBuildArea(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Bedrooms:</td>
        <td class="viewData"><?php echo $this->property->getBedrooms(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Bathrooms:</td>
        <td class="viewData"><?php echo $this->property->getBathrooms(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Parking:</td>
        <td class="viewData"><?php echo $this->property->getParking(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">&nbsp;</td>
        <td class="viewData">&nbsp;</td>
        </tr>
      <tr>
        <td class="viewLbl">Date Created:</td>
        <td class="viewData"><?php echo $this->property->getCreationDate(true); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Expiry Date:</td>
        <td class="viewData"><?php echo $this->property->getExpiryDate(true); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Featured:</td>
        <td class="viewData"><?php echo $this->property->getFeatured(true); ?></td>
      </tr>
      <tr>
        <td class="viewLbl">&nbsp;</td>
        <td class="viewData">&nbsp;</td>
        </tr>
      <tr>
        <td class="viewLbl">Contact Name:</td>
        <td class="viewData"><?php echo $this->property->getContactName(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Contact Phone:</td>
        <td class="viewData"><?php echo $this->property->getContactPhone(); ?></td>
        </tr>
      <tr>
        <td class="viewLbl">Contact Email:</td>
        <td class="viewData"><?php echo $this->property->getContactEmail(); ?></td>
        </tr>
      </table></td>
  </tr>
  </table>
</div>