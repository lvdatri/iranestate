<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->message->show(); ?>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="frmH2f" colspan="2">Zone Details</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Zone English:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="zone_eng" type="text" id="zone_eng" value="<?php echo $this->form->getZoneEng(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Zone Persian:</td>
                <td class="frmFBg" width="87%"><input class="frmF ralign" name="zone_per" type="text" id="zone_per" value="<?php echo $this->form->getZonePer(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Sort Order:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="sort_order" type="text" id="sort_order" value="<?php echo $this->form->getSortOrder(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>