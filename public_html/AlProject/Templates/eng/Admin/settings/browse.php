<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Update Setting',
        minWidth:'80',
        style: Ext.al.btn.floatRight,
        icon:'<?php echo $this->_settings->path_images;?>icons/insert.png',
        handler: function(e) {
            this.disable();
            location='<?php echo $this->pathController; ?>update';
        }
    });

}
</script>
<div class="page_header">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php $this->nav->show(); ?></h1> </td>
        <td width="29%" ><div id="pageActions"></div></td>
    </tr>
</table>
</div>
<div class="page">
<?php $this->message->show(); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20%" class="viewLbl">Emali From:</td>
        <td width="80%" class="viewData"><span class="frmFBg"><?php echo $this->record->getEmailFrom();?></span></td>
    </tr>
      <tr>
        <td class="viewLbl">Email From Name:</td>
        <td class="viewData"><span class="frmFBg"><?php echo $this->record->getEmailFromName(); ?></span></td>
    </tr>
      <tr>
        <td class="viewLbl">Currency 1 USD = </td>
        <td class="viewData"><span class="frmFBg"><?php echo $this->record->getUsdRate(true); ?> IRR </span></td>
    </tr>
      <tr>
        <td class="viewLbl">Currency 1 GBP = </td>
        <td class="viewData"><span class="frmFBg"><?php echo $this->record->getGbpRate(true); ?> IRR </span></td>
    </tr>
      <tr>
        <td class="viewLbl">Currency 1 AUD = </td>
        <td class="viewData"><span class="frmFBg"><?php echo $this->record->getAudRate(true); ?> IRR </span></td>
    </tr>        
      <tr>
        <td class="viewLbl">Require Property Approval</td>
        <td class="viewData"><span class="frmFBg">
          <?php 
	if($this->record->getRequirePropertyApproval()=='1') { 
		echo 'Yes';
	} else {
		echo 'No';
	}; 
	?>
        </span></td>
    </tr>
      <tr>
        <td valign="top" class="viewLbl">Default Map View:</td>
        <td class="viewData">
		<script type="text/javascript"> 
			$(document).ready(function() { 
				var gpoint = $('#map_canvas').googleMaps({
					latitude: <?php echo $this->record->getMapLat(); ?>,
					longitude: <?php echo $this->record->getMapLong(); ?>,
					depth: <?php echo $this->record->getMapDepth(); ?>,
				}); 
			}); 
        </script> 
        <div id="map_canvas" style="width: 515px; height: 300px; margin-bottom:15px;"></div> 
        </td>
    </tr>
      </table>
</div>
