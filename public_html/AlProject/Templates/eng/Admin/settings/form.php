<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->message->show(); ?>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="frmH2f" colspan="2">Email Details</td>
            </tr>
            <tr>
                <td width="1%" class="frmL">Email From:</td>
              <td class="frmFBg"><input class="frmF" name="email_from" type="text" id="email_from" value="<?php echo $this->form->getEmailFrom(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL">Email From Name:</td>
              <td class="frmFBg"><input class="frmF" name="email_from_name" type="text" id="email_from_name" value="<?php echo $this->form->getEmailFromName(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmH2f" colspan="2">Currency Rate</td>
            </tr>
            <tr>
                <td class="frmL ralign">1 USD =</td>
                <td class="frmFBg"><input class="frmF" name="usd_rate" type="text" id="usd_rate" value="<?php echo $this->form->getUsdRate(); ?>" size="20" /> 
                  IRR</td>
            </tr>
            <tr>
                <td class="frmL ralign">1 GBP =</td>
                <td class="frmFBg"><input class="frmF" name="gbp_rate" type="text" id="gbp_rate" value="<?php echo $this->form->getGbpRate(); ?>" size="20" /> 
                  IRR</td>
            </tr>
            <tr>
                <td class="frmL ralign">1 AUD =</td>
                <td class="frmFBg"><input class="frmF" name="aud_rate" type="text" id="aud_rate" value="<?php echo $this->form->getAudRate(); ?>" size="20" /> 
                  IRR</td>
            </tr>                        
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmH2f" colspan="2">Property Settings</td>
            </tr>
            <tr>
                <td nowrap class="frmL">Require Property Approval:</td>
              <td class="frmFBg"><input name="require_property_approval" type="checkbox" id="require_property_approval" value="1" <?php echo $this->form->cbRequirePropertyApproval(); ?> /></td>
          </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>

        </table>
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="2" colspan="2" class="frmH2f">Map</td>
          </tr>
          <tr>
            <td colspan="2" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="49%"><script type="text/javascript"> 
				$(document).ready(function() { 
					var gpoint = $('#map_canvas').googleMaps({
						latitude: <?php echo $this->form->getMapLat(); ?>,
						longitude: <?php echo $this->form->getMapLong(); ?>,
						depth: <?php echo $this->form->getMapDepth(); ?>,
					}); 
						  
        			GEvent.addListener($.googleMaps.gMap, 'moveend', function() {		 
						$("#map_lat").val($.googleMaps.gMap.getCenter().lat());
						$("#map_long").val($.googleMaps.gMap.getCenter().lng());
						$("#map_depth").val($.googleMaps.gMap.getBoundsZoomLevel($.googleMaps.gMap.getBounds()));
					});   					  
				}); 
                </script>
                  <div id="map_canvas" style="width: 515px; height: 300px; margin-bottom:15px;"></div></td>
                <td width="51%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="frmL ralign">Latitude:</td>
                    <td class="frmFBg"><input class="frmF" name="map_lat" type="text" id="map_lat" value="<?php echo $this->form->getMapLat(); ?>" size="30" /></td>
                  </tr>
                  <tr>
                    <td class="frmL ralign">Longitude:</td>
                    <td class="frmFBg"><input class="frmF" name="map_long" type="text" id="map_long" value="<?php echo $this->form->getMapLong(); ?>" size="30" /></td>
                  </tr>
                  <tr>
                    <td class="frmL ralign">Depth:</td>
                    <td class="frmFBg"><input class="frmF" name="map_depth" type="text" id="map_depth" value="<?php echo $this->form->getMapDepth(); ?>" size="30" /></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
          </tr>
          <tr>
            <td class="frmTD" colspan="2" id="pageActions"></td>
          </tr>
      </table>
    </div>
</form>
</div>