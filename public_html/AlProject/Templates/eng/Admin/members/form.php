<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->message->show(); ?>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="frmL" width="13%">Name:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="name" type="text" id="name" value="<?php echo $this->form->getName(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Email:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="email" type="text" id="email" value="<?php echo $this->form->getEmail(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td class="frmL">Address:</td>
              <td class="frmFBg"><input class="frmF" name="address" type="text" id="address" value="<?php echo $this->form->getAddress(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td class="frmL">City:</td>
              <td class="frmFBg"><input class="frmF" name="city" type="text" id="city" value="<?php echo $this->form->getCity(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td class="frmL">Postcode:</td>
              <td class="frmFBg"><input class="frmF" name="postcode" type="text" id="postcode" value="<?php echo $this->form->getPostcode(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td class="frmL">Phone:</td>
              <td class="frmFBg"><input class="frmF" name="phone" type="text" id="phone" value="<?php echo $this->form->getPhone(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td class="frmL">Newsletter Subscription:</td>
              <td class="frmFBg"><input name="newsletter_subscription" type="checkbox" id="newsletter_subscription" value="1" <?php echo $this->form->cbNewsletterSubscription(); ?> /></td>
            </tr>
            <tr>
              <td class="frmL">Language:</td>
              <td class="frmFBg"><select name="language" id="language">
                <?php echo $this->form->ddLanguage(); ?>
              </select></td>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Password</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Password:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="password" type="password" id="password" value="<?php echo $this->form->getPassword(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Password Confirm:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="password_confirm" type="password" id="password_confirm" value="<?php echo $this->form->getPasswordConfirm(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>