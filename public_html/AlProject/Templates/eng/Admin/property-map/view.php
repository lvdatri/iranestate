<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'navTabActions',
        text:'Set Map Point',
        minWidth:'80',
        style: Ext.al.btn.floatRight,
        icon:'<?php echo $this->_settings->path_images;?>icons/insert.png',
        handler: function(e) {
            this.disable();
            location='<?php echo $this->pathController; ?>update/pid/<?php echo $this->property->getId(); ?>';
        }
    });
    Ext.get('formSearch').on('submit',function (e,t) {
        e.stopEvent();
        Ext.al.sendFormUri('formSearch');
    });
}
</script>
<div class="page_header">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php $this->nav->show(); ?></h1> </td>
        <td width="29%" ><div id="pageActions"></div></td>
    </tr>
</table>
</div>
<div class="page">
<?php $this->tabs->display($this->property->getId()); ?>
<?php $this->tab_nav->show(); ?>
<?php $this->message->show(); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20%" valign="top" class="viewLbl">Default Map View:</td>
        <td width="80%" class="viewData">
		<script type="text/javascript"> 
			$(document).ready(function() {
				
				var gmap_map = jQuery.al.gmap_set(
					'map_canvas',
					<?php echo $this->lat; ?>,
					<?php echo $this->lon; ?>,
					<?php echo $this->depth; ?>
				);
				<?php if($this->property->getLat()!='') { ?>
					var gmap_marker = jQuery.al.gmap_setMarker (
						gmap_map,
						<?php echo $this->lat; ?>,
						<?php echo $this->lon; ?>,
						'<?php echo $this->_settings->path_images; ?>/marker.png',
						'<?php echo $this->_settings->path_images; ?>/shadow.png',
						false
					);			
				<?php } ?>

			}); 
        </script> 
        <div id="map_canvas" style="width: 515px; height: 300px; margin-bottom:15px;"></div> 
        </td>
    </tr>
      <tr>
        <td width="20%" valign="top" class="viewLbl">Latitude:</td>
        <td class="viewData"><span class="frmFBg"><?php echo $this->property->getLat();?></span></td>
      </tr>
      <tr>
        <td valign="top" class="viewLbl">Longidtude:</td>
        <td class="viewData"><span class="frmFBg"><?php echo $this->property->getLon();?></span></td>
      </tr>
      <tr>
        <td valign="top" class="viewLbl">Depth:</td>
        <td class="viewData"><span class="frmFBg"><?php echo $this->property->getDepth();?></span></td>
      </tr>
      </table>
</div>
