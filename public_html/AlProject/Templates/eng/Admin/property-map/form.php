<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->tabs->display($this->property->getId()); ?>
<?php $this->tab_nav->show(); ?>
<?php $this->message->show(); ?>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="200%" colspan="2" class="frmH2f">Map</td>
            </tr>
            <tr>
              <td colspan="2" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="49%">              
			<script type="text/javascript"> 
				$(document).ready(function() { 
				
					var gmap_map = jQuery.al.gmap_set(
						'map_canvas',
						<?php echo $this->form->getLat(); ?>,
						<?php echo $this->form->getLon(); ?>,
						<?php echo $this->form->getDepth(); ?>
					);
					
				  	var gmap_marker = jQuery.al.gmap_setMarker (
						gmap_map,
						<?php echo $this->form->getLat(); ?>,
						<?php echo $this->form->getLon(); ?>,
						'<?php echo $this->_settings->path_images; ?>/marker.png',
						'<?php echo $this->_settings->path_images; ?>/shadow.png',
						true
					);
	  
					google.maps.event.addListener(gmap_marker,"mouseup", function(){ 																   
						var latLong = gmap_marker.getPosition();
						$("#lat").val(latLong.lat());
						$("#lon").val(latLong.lng());
					}); 					
		  
					google.maps.event.addListener(gmap_map, 'zoom_changed', function() {		 
						$("#depth").val(gmap_map.getZoom());
					}); 				
				
				}); 
			</script> 
                <div id="map_canvas" style="width: 515px; height: 300px; margin-bottom:15px;"></div> </td>
                      <td width="51%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td class="frmL ralign">Latitude:</td>
                          <td class="frmFBg"><input class="frmF" name="lat" type="text" id="lat" value="<?php echo $this->form->getLat(); ?>" size="30" /></td>
                        </tr>
                        <tr>
                          <td class="frmL ralign">Longitude:</td>
                          <td class="frmFBg"><input class="frmF" name="lon" type="text" id="lon" value="<?php echo $this->form->getLon(); ?>" size="30" /></td>
                        </tr>
                        <tr>
                          <td class="frmL ralign">Depth:</td>
                          <td class="frmFBg"><input class="frmF" name="depth" type="text" id="depth" value="<?php echo $this->form->getDepth(); ?>" size="30" /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>              

              </td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>