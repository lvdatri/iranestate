<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->message->show(); ?>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" enctype="multipart/form-data" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
        	<?php if($this->customPath) { ?>
                <tr>
                    <td class="frmH2f" colspan="2">Url Path</td>
                </tr>
                <tr>
                  <td class="frmL">Path:</td>
                  <td class="frmFBg"><?php echo $this->_settings->path_web_ssl; ?><input class="frmF" name="path" type="text" id="path" value="<?php echo $this->form->getPath(); ?>" size="30" /></td>
                </tr>
                <tr>
                  <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
                </tr>
            <?php } ?>
            <tr>
              <td class="frmH2f" colspan="2">Meta Data Persian</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Keywords:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="keywords_eng" type="text" id="keywords_eng" value="<?php echo $this->form->getKeywordsEng(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td valign="top" class="frmL">Description:</td>
              <td class="frmFBg"><textarea name="description_eng" cols="60" rows="3" class="frmF" id="description_eng"><?php echo $this->form->getDescriptionEng(); ?></textarea></td>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Meta Data Persian</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Keywords:</td>
                <td class="frmFBg" width="87%"><input class="frmF ralign" name="keywords_per" type="text" id="keywords_per" value="<?php echo $this->form->getKeywordsPer(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td width="13%" valign="top" class="frmL">Description:</td>
                <td class="frmFBg" width="87%"><textarea name="description_per" cols="60" rows="3" class="frmF ralign" id="description_per"><?php echo $this->form->getDescriptionPer(); ?></textarea></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmH2f" colspan="2">Article English</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Title:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="title_eng" type="text" id="title_eng" value="<?php echo $this->form->getTitleEng(); ?>" size="30" /></td>
            </tr>
            <tr>
              <td valign="top" class="frmL">Content:</td>
              <td class="frmFBg"><textarea name="content_eng" cols="60" rows="6" class="frmF" id="content_eng"><?php echo $this->form->getContentEng(); ?></textarea></td>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Article Persian</td>
            </tr>
            <tr>
                <td class="frmL" width="13%"> Title:</td>
                <td class="frmFBg" width="87%"><input class="frmF ralign" name="title_per" type="text" id="title_per" value="<?php echo $this->form->getTitlePer(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td width="13%" valign="top" class="frmL">Content Persian:</td>
                <td class="frmFBg" width="87%"><textarea name="content_per" cols="60" rows="7" class="frmF ralign" id="content_per"><?php echo $this->form->getContentPer(); ?></textarea></td>
            </tr>
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Image</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Image:</td>
                <td class="frmFBg" width="87%">
                <input name="image" type="file" id="image" size="30">
                <?php echo $this->form->getImage(); ?></td>
            </tr>
            <?php if($this->sortOrder==true) { ?>            
            <tr>
              <td class="frmSepTD" colspan="2"><div class="frmSep"></div></td>
            </tr>
            <tr>
              <td class="frmH2f" colspan="2">Sort Order</td>
            </tr>
            <tr>
                <td class="frmL" width="13%"> Sort Order:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="sort_order" type="text" id="sort_order" value="<?php echo $this->form->getSortOrder(); ?>" size="10" /></td>
            </tr> 
            <?php } ?>           
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>