<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="71%"><h1 class="page_title">Logout</h1> </td>
    <td width="29%" >&nbsp;</td>
  </tr>
</table>
</div>
<div class="page" align="center">
 
	<div class="frmBody" style="width:270px; margin-top:100px;">
	  <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="frmH2f">You have successfully logged out.
          </td>
        </tr>
        <tr>
          <td class="frmL" style="padding:20px;">Click <a href="<?php echo $this->pathController.'login'; ?>">here</a> to log back in.</td>
        </tr>
      </table>
	</div>

</div>