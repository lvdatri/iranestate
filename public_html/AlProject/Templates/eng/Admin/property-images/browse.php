<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'navTabActions',
        text:'Add Property Images',
        minWidth:'80',
        style: Ext.al.btn.floatRight,
        icon:'<?php echo $this->_settings->path_images;?>icons/insert.png',
        handler: function(e) {
            this.disable();
            location='<?php echo $this->pathController; ?>insert-multiple/pid/<?php echo $this->property->getId(); ?>';
        }
    });
    Ext.get('formSearch').on('submit',function (e,t) {
        e.stopEvent();
        Ext.al.sendFormUri('formSearch');
    });
}
</script>
<div class="page_header">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php $this->nav->show(); ?></h1> </td>
        <td width="29%" ><div id="pageActions"></div></td>
    </tr>
</table>
</div>
<div class="page">
<?php $this->tabs->display($this->property->getId()); ?>
<?php $this->tab_nav->show(); ?>
<?php $this->message->show(); ?>

    <table class="grid" id="dataGrid" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th><?php $this->sorter->show_column('image_name'); ?></th>
            <th><?php $this->sorter->show_column('main_image'); ?></th>
            <th width="1%">&nbsp;</th>
        </tr>
    <?php while($row = $this->result->fetch_row()) { ?>
        <tr>
            <td><img src="<?php echo $row['image_path']; ?>" border="0" /></td>
            <td valign="top">
            <?php if($row['main_image'] == '1') {  ?>
            main
            <?php } else { ?>
            <a href="<?php echo $this->pathController; ?>set-as-main/id/<?php echo $row['id']; ?>">set as main</a><?php } ?>
          </td>
            <td valign="top" nowrap="nowrap"><input onclick="Ext.al.confirm('Delete Item?','<?php echo $this->pathController; ?>delete/pid/<?php echo $this->property->getId(); ?>/id/<?php echo $row['id']; ?>');" class="btnSml" type="button" name="btnDelete" id="btnDelete" value="Delete" />
            </td>
        </tr>
    <?php } ?>
        <?php if($this->pager->total_num_of_records < 1) { ?>
        <tr>
            <td colspan="5">No records found.</td>
        </tr>
        <?php } ?>
    </table>
<?php if($this->pager->total_num_of_records > 0) { ?>
<div class="gridFoot">
    <div class="gridNav">
        <?php $this->pager->display_nav(); ?>
    </div>
    <div class="gridFootTxt">
        <?php echo $this->pager->starting_record_num; ?> to <?php echo $this->pager->ending_record_num; ?> of <?php echo $this->pager->total_num_of_records; ?> records
    </div>
</div> 
<?php } ?>
</div>
