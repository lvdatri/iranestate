<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->tabs->display($this->property->getId()); ?>
<?php $this->tab_nav->show(); ?>
<?php $this->message->show(); ?>
<form action="<?php echo $this->form->getAction(); ?>" method="post" enctype="multipart/form-data" name="frmR" class="rForm" id="frmR" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="frmH2f" colspan="2">Property Images                  <input name="__frm" type="hidden" id="__frm" value="1"></td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Image 1:</td>
                <td class="frmFBg" width="87%"><input type="file" name="image_1" id="image_1"> <?php echo $this->form->getImage1(); ?> </td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Image 2:</td>
                <td class="frmFBg" width="87%"><input type="file" name="image_2" id="image_2"> <?php echo $this->form->getImage2(); ?> </td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Image 3:</td>
                <td class="frmFBg" width="87%"><input type="file" name="image_3" id="image_3"> <?php echo $this->form->getImage3(); ?> </td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Image 4:</td>
                <td class="frmFBg" width="87%"><input type="file" name="image_4" id="image_4"> <?php echo $this->form->getImage4(); ?> </td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Image 5:</td>
                <td class="frmFBg" width="87%"><input type="file" name="image_5" id="image_5"> <?php echo $this->form->getImage5(); ?> </td>
            </tr>                                                
            <tr>
              <td class="frmL">&nbsp;</td>
              <td class="frmFBg">&nbsp;</td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>