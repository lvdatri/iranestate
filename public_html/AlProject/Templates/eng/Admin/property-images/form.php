<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->message->show(); ?>
<form action="<?php echo $this->form->getAction(); ?>" method="post" enctype="multipart/form-data" name="frmR" class="rForm" id="frmR" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="frmH2f" colspan="2">Image Details 
                <input name="__frm" type="hidden" id="__frm" value="1"></td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Image:</td>
                <td class="frmFBg" width="87%"><input type="file" name="image_name" id="image_name"> <?php echo $this->form->getImageName(); ?> </td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Main Image:</td>
                <td class="frmFBg" width="87%"><input name="main_image" type="checkbox" id="main_image" value="1" <?php echo $this->form->cbMainImage(); ?> /></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>