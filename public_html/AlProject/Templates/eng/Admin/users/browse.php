<script type="text/javascript">
Ext.al.pageReady = function() {
	var b = new Ext.Button({
		renderTo:'pageActions',
		text:'Add User',
		minWidth:'80',
		style: Ext.al.btn.floatRight,
		icon:'<?php echo $this->_settings->path_images;?>icons/insert.png',		
		handler: function(e) {
			this.disable();
			location='<?php echo $this->pathController; ?>insert';
		}
	});
	
	Ext.get('formSearch').on('submit',function (e,t) {
		e.stopEvent();
		Ext.al.sendFormUri('formSearch');
	});
}
</script>
<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="71%"><h1 class="page_title"><?php $this->nav->show(); ?></h1> </td>
    <td width="29%" ><div id="pageActions"></div></td>
  </tr>
</table>
</div>
<div class="page">
<?php $this->message->show(); ?>
<form name="formSearch" id="formSearch" action="<?php echo $this->pathController.'browse/'; ?>" method="get" ><?php echo $this->params->to_fields(array(Al_UrlParams::SORTER)); ?>
	<div class="frmBody">
      <table  width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="8%" nowrap="nowrap" class="frmL">First Name:</td>
          <td class="frmFBg"  width="92%"><input class="frmF" name="first_name" type="text" id="first_name" value="<?php echo Al_Utilities::eGet('first_name'); ?>" size="30" />
            
          Last Name: <input class="frmF" name="last_name" type="text" id="last_name" value="<?php echo Al_Utilities::eGet('last_name'); ?>" size="30" /></td>
        </tr>
        <tr>
          <td class="frmSepTD" colspan="2">
			<div class="frmSep"></div>
          </td>
        </tr>        
        <tr>
          <td class="frmTD" colspan="2"><input class="btn" type="submit" name="btnSearch" id="btnSearch" value="Search" />
            <?php if($this->searchTriggered==true) { ?>
            <input onclick="location='<?php echo $this->pathController.'browse/'; ?>page/1/<?php echo $this->params->get(array(Al_UrlParams::SORTER)) ?>';" class="btn" type="button" name="btnClear" id="btnClear" value="Clear" />
            <?php } ?>
          </td>
        </tr>
      </table>
	</div>
</form>

<table class="grid" id="dataGrid" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th><?php $this->sorter->show_column('first_name'); ?></th>
      <th><?php $this->sorter->show_column('last_name'); ?></th>
      <th><?php $this->sorter->show_column('email'); ?></th>
      <th><?php $this->sorter->show_column('username'); ?></th>
      <th width="1%">&nbsp;</th>
    </tr>
    <?php while($row = $this->result->fetch_row()) { ?>
        <tr>
          <td><?php echo $row['first_name']; ?></td>
          <td><?php echo $row['last_name']; ?></td>
          <td><?php echo $row['email']; ?></td>
          <td><?php echo $row['username']; ?></td>
          <td nowrap="nowrap"><input onclick="location='<?php echo $this->pathController; ?>update/id/<?php echo $row['id']; ?>'" class="btnSml" type="button" name="btnUpdate" id="btnUpdate" value="Update" />
            <input onclick="Ext.al.confirm('Delete Item?','<?php echo $this->pathController; ?>delete/id/<?php echo $row['id']; ?>');" class="btnSml" type="button" name="btnDelete" id="btnDelete" value="Delete" />

		</td>
        </tr>
    <?php } ?>    
    <?php if($this->pager->total_num_of_records < 1) { ?>
    <tr>
      <td colspan="5">No records found.</td>
    </tr>
    <?php } ?>
  </table>
  <?php if($this->pager->total_num_of_records > 0) { ?>
<div class="gridFoot">
      <div class="gridNav">
      <?php $this->pager->display_nav(); ?>
    </div>
      <div class="gridFootTxt">
	  <?php echo $this->pager->starting_record_num; ?> to <?php echo $this->pager->ending_record_num; ?> of <?php echo $this->pager->total_num_of_records; ?> records
      </div>
  </div> 
  <?php } ?>
</div>