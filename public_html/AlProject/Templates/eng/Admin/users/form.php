<script type="text/javascript">
Ext.al.pageReady = function() {
	var b = new Ext.Button({
		renderTo:'pageActions',
		text:'Save',
		type:'submit',
		minWidth:'80',
		icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
		style: Ext.al.btn.floatLeft,
		handler: function(e) {
			this.disable();
			Ext.getDom('frmR').submit();
		}
	});
	
	var b = new Ext.Button({
		renderTo:'pageActions',
		text:'Cancel',
		minWidth:'80',
		icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
		style: Ext.al.btn.floatLeft,
		handler: function(e) {
			this.disable();
			location='<?php echo $this->form->getCancelAction(); ?>';
		}
	});
}
</script>
<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>
<div class="page">
<?php $this->message->show(); ?> 
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
	<div class="frmBody">
      <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="frmH2f" colspan="2">User Details</td>
        </tr>
        <tr>
          <td class="frmL" width="13%">First Name:</td>
          <td class="frmFBg" width="87%"><input name="first_name" type="text" class="frmF" id="first_name" value="<?php echo $this->form->getFirstName(); ?>" size="30"></td>
        </tr>
        <tr>
          <td class="frmL">Last Name:</td>
          <td class="frmFBg"><input class="frmF" name="last_name" type="text" id="last_name" value="<?php echo $this->form->getLastName(); ?>" size="30" /></td>
        </tr>
        <tr>
          <td class="frmL">Email:</td>
          <td class="frmFBg"><input class="frmF" name="email" type="text" id="email" value="<?php echo $this->form->getEmail(); ?>" size="30" /></td>
        </tr>

        <tr>
          <td class="frmSepTD" colspan="2">
			<div class="frmSep"></div>
          </td>
        </tr>
        <tr>
          <td class="frmH2f" colspan="2">Login Details</td>
        </tr>
        <tr>
          <td class="frmL">Username</td>
          <td class="frmFBg"><input class="frmF" name="username" type="text" id="username" value="<?php echo $this->form->getUsername(); ?>" size="25" /></td>
        </tr>
        <tr>
          <td class="frmL">Password</td>
          <td class="frmFBg"><input class="frmF" name="password" type="password" id="password" value="<?php echo $this->form->getPassword(); ?>" size="25" /></td>
        </tr>
        <tr>
          <td class="frmL">Password Confirm:</td>
          <td class="frmFBg"><input class="frmF" name="password_confirm" type="password" id="password_confirm" value="<?php echo $this->form->getPasswordConfirm(); ?>" size="25" /></td>
        </tr>
        <tr>
          <td class="frmSepTD" colspan="2">
			<div class="frmSep"></div>
          </td>
        </tr>
        <tr>
          <td class="frmTD" colspan="2" id="pageActions">
          </td>
        </tr>
      </table>
	</div>
</form>
</div>