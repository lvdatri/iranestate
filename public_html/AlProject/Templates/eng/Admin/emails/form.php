<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Save',
        type:'submit',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/save.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
        	this.disable();
        	Ext.getDom('frmR').submit();
        }
    });

    new Ext.Button({
        renderTo:'pageActions',
        text:'Cancel',
        minWidth:'80',
        icon:'<?php echo $this->_settings->path_images; ?>icons/cancel.png',
        style: Ext.al.btn.floatLeft,
        handler: function(e) {
            	this.disable();
            	location='<?php echo $this->form->getCancelAction(); ?>';
        }
    });
}
</script>

<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>
    </tr>
</table>
</div>

<div class="page">
<?php $this->message->show(); ?>
<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >
    <div class="frmBody">
        <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="frmH2f" colspan="2">Email Details</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Title:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="title" type="text" id="title" value="<?php echo $this->form->getTitle(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Description:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="description" type="text" id="description" value="<?php echo $this->form->getDescription(); ?>" size="30" /></td>
            </tr>
            <?php if($this->form->getMode() == Al_Form::UPDATE) { ?>
            <tr>
              <td valign="top" class="frmL">Tags:</td>
              <td class="frmFBg"><?php $tags = $this->email->getTagsArray(); ?>
              	<?php foreach($tags as $tag) { ?>
               	<div style="display:block; margin-bottom:3px;"><?php echo $tag; ?></div>
                <?php } ?>
              </td>
           </tr>
           <?php } ?>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmH2f" colspan="2">English Email Content</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Subject:</td>
                <td class="frmFBg" width="87%"><input class="frmF" name="subject_eng" type="text" id="subject_eng" value="<?php echo $this->form->getSubjectEng(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL" valign="top" width="13%">Content:</td>
                <td class="frmFBg" width="87%"><textarea class="frmF" name="content_eng" cols="60" rows="8" id="content_eng"><?php echo $this->form->getContentEng(); ?></textarea></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmH2f" colspan="2">Persian Email Content</td>
            </tr>
            <tr>
                <td class="frmL" width="13%">Subject:</td>
                <td class="frmFBg" width="87%"><input class="frmF ralign" name="subject_per" type="text" id="subject_per" value="<?php echo $this->form->getSubjectPer(); ?>" size="30" /></td>
            </tr>
            <tr>
                <td class="frmL" valign="top" width="13%">Content:</td>
                <td class="frmFBg" width="87%"><textarea class="frmF ralign" name="content_per" cols="60" rows="8" id="content_per"><?php echo $this->form->getContentPer(); ?></textarea></td>
            </tr>
            <tr>
                <td class="frmSepTD" colspan="2">
                    <div class="frmSep"></div>
                </td>
            </tr>
            <tr>
                <td class="frmTD" colspan="2" id="pageActions"></td>
            </tr>
        </table>
    </div>
</form>
</div>