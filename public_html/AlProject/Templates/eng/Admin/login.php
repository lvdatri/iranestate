<script type="text/javascript">
Ext.al.pageReady = function() {
	new Ext.Button({
		renderTo:'pageActions',
		text:'Login',
		type:'submit',
		minWidth:'80',
		style: Ext.al.btn.floatLeft,
		handler: function(e) {
			this.disable();
			Ext.getDom('frmR').submit();
		}
	});
}
</script>
<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="71%"><h1 class="page_title">Login</h1> </td>
    <td width="29%" >&nbsp;</td>
  </tr>
</table>
</div>
<div class="page" align="center">
 
<form id="frmR" name="frmR" method="post" action="<?php echo $this->pathAction; ?>" class="rForm" >
	<div class="frmBody" style="width:270px; margin-top:100px;">
    <?php $this->message->show(); ?>
      <table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="frmH2f" colspan="2">Please enter your login details
          </td>
        </tr>
        <tr>
          <td class="frmL" width="13%">Username:</td>
          <td class="frmFBg" width="87%"><input class="frmF" name="username" type="text" id="username" value="<?php echo $this->data['username']; ?>" size="30" /></td>
        </tr>
        <tr>
          <td class="frmL">Password:</td>
          <td class="frmFBg"><input class="frmF" name="password" type="password" id="password" value="<?php echo $this->data['password']; ?>" size="30" /></td>
        </tr>
        <tr>
          <td class="frmSepTD" colspan="2">
			<div class="frmSep"></div>
          </td>
        </tr>
        <tr>
          <td class="frmTD" colspan="2" id="pageActions"></td>
        </tr>
      </table>
	</div>
</form>
</div>