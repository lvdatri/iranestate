<script type="text/javascript">
Ext.al.pageReady = function() {
    new Ext.Button({
        renderTo:'pageActions',
        text:'Add Property Feature',
        minWidth:'80',
        style: Ext.al.btn.floatRight,
        icon:'<?php echo $this->_settings->path_images;?>icons/insert.png',
        handler: function(e) {
            this.disable();
            location='<?php echo $this->pathController; ?>insert';
        }
    });
    Ext.get('formSearch').on('submit',function (e,t) {
        e.stopEvent();
        Ext.al.sendFormUri('formSearch');
    });
}
</script>
<div class="page_header">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="71%"><h1 class="page_title"><?php $this->nav->show(); ?></h1> </td>
        <td width="29%" ><div id="pageActions"></div></td>
    </tr>
</table>
</div>
<div class="page">
<?php $this->message->show(); ?>
    <table class="grid" id="dataGrid" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th width="1%"><?php $this->sorter->show_column('sort_order'); ?></th>
          <th><?php $this->sorter->show_column('feature_eng'); ?></th>
            <th><?php $this->sorter->show_column('feature_per'); ?></th>
            <th width="1%">&nbsp;</th>
        </tr>
    <?php while($row = $this->result->fetch_row()) { ?>
        <tr>
          <td><?php echo $row['sort_order']; ?></td>
          <td><?php echo $row['feature_eng']; ?></td>
            <td class="ralign"><?php echo $row['feature_per']; ?></td>
            <td nowrap="nowrap">
                <input onclick="location='<?php echo $this->pathController; ?>update/id/<?php echo $row['id']; ?>'" class="btnSml" type="button" name="btnUpdate" id="btnUpdate" value="Update" />
                <input onclick="Ext.al.confirm('Delete Item?','<?php echo $this->pathController; ?>delete/id/<?php echo $row['id']; ?>');" class="btnSml" type="button" name="btnDelete" id="btnDelete" value="Delete" />
            </td>
        </tr>
    <?php } ?>
        <?php if($this->pager->total_num_of_records < 1) { ?>
        <tr>
            <td colspan="5">No records found.</td>
        </tr>
        <?php } ?>
    </table>
<?php if($this->pager->total_num_of_records > 0) { ?>
<div class="gridFoot">
    <div class="gridNav">
        <?php $this->pager->display_nav(); ?>
    </div>
    <div class="gridFootTxt">
        <?php echo $this->pager->starting_record_num; ?> to <?php echo $this->pager->ending_record_num; ?> of <?php echo $this->pager->total_num_of_records; ?> records
    </div>
</div> 
<?php } ?>
</div>
