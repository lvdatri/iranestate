<div class="page_header"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="71%"><h1 class="page_title">Home</h1> </td>
    <td width="29%" >&nbsp;</td>
  </tr>
</table>
</div>
<div class="page">
<?php $this->message->show(); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php foreach($this->data as $item) { ?>
    <tr>
      <td width="10%" nowrap="nowrap" class="viewLbl" style="font-weight:normal; text-align:right;"><?php echo $item['label']; ?></td>
    	<td class="viewData"><a style="text-decoration:none;" href="<?php echo $item['url']; ?>"><?php echo $item['count']; ?></a></td>
	</tr>
<?php } ?>
</table>
</div>