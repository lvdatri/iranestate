<script type="text/javascript">

    $(".result-form").corner();
    $(".news-col").corner();
    $(".result-block").corner();
    $(".page-nav").corner();

    $(document).ready(function () {
        $("SELECT").selectBox();
    }); // end on ready

    function sortOrderChange() {
        location = '<?php echo $this->pathController.$this->transactionPath.'/'.$this->propertySearch->getSearchParams(); ?>sort/' + $('#sortId').val();
    }


    function propTypeChange() {
        location = '<?php echo $this->pathController.$this->agentPath.'/'.$this->propertySearch->getSearchParams(); ?>pt/' + $('#ptId').val() + '/sort/' + $('#sortId').val();
    }

</script>

<div class="result-right-col"><!--RESULT-RIGHT-COL---->


    <div style="height: 30px;margin: 0 0 12px 0;position: relative;">


        <p class="listings-heading">


            <?php echo $this->pager->total_num_of_records; ?> <?php echo $this->getStr(Al_Language::SEARCH_LBL_PROPERTIES_FOUND); ?>
        </p>


        <form class="sortbyForm">

            <select onChange="sortOrderChange();" name="sortId" id="sortId" style="width:130px;">
                <?php echo $this->propertySort->ddData($this->propertySearch->getSortId()); ?>
            </select>
        </form>

    </div>


    <?php while ($item = $this->properties->fetch_row()) {
        $this->property->loadFromArray($item); ?>
        <div class="result-block">
            <div class="result-header-block <?php if ($this->property->getFeatured() == '0') {
                echo 'result-header-green';
            } ?>">
                <div class="result-header-title">
                    <a href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#FFF;">
                        <p><?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?></p>
                    </a>
                </div>
                <div class="result-header-icons">
                    <a href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#FFF;">


                        <p><?php echo $this->property->getPrice(true); ?></p>


                        <img src="<?php echo $this->_settings->path_images; ?>vert-line.png" width="3" height="28" alt="vert"/>

                        <?php if ($this->property->getBedrooms() > 0) { ?>
                            <img src="<?php echo $this->_settings->path_images; ?>ico-bed-big-white.png" width="25" height="21" title="bedrooms"/>
                            <p><?php echo $this->property->getBedrooms(); ?></p>
                        <?php } ?>
                        <?php if ($this->property->getBathrooms() > 0) { ?>
                            <img src="<?php echo $this->_settings->path_images; ?>ico-bath-big-white.png" width="16" height="22" title="bathrooms"/>
                            <p><?php echo $this->property->getBathrooms(); ?></p>
                        <?php } ?>

                        <?php if ($this->property->getParking() > 0) { ?>
                            <img src="<?php echo $this->_settings->path_images; ?>ico-garage-big-white.png" width="22" height="21" title="garages"/>
                            <p><?php echo $this->property->getParking(); ?></p>
                        <?php } ?>

                    </a>


                </div>
            </div>
            <div class="result-image-block">

                <a href="<?php echo $this->property->getUrl(); ?>" title="<?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?>" class="clearlink">
                    <img alt="<?php echo $this->property->getCity(); ?> <?php echo $this->property->getPropertyType(); ?> <?php echo $this->property->getTransactionType(); ?>" src="<?php echo $this->property->getImageThumb(); ?>"/>

                    <?php if ($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_SALE && $this->property->getPropertyStatusId() == Bl_Data_Statuses::BUY_SOLD) { ?>
                        <img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\sold.png" width="74" height="73" alt="featured" class="sticker"/>
                    <?php
                    } else {
                        if ($this->property->getPropertyTransactionId() == Bl_Data_Transactions::FOR_RENT && $this->property->getPropertyStatusId() == Bl_Data_Statuses::RENT_RENTED) {
                            ?>
                            <img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\rented.png" width="74" height="73" alt="featured" class="sticker"/>
                        <?php
                        } else {
                            if ($this->property->getFeatured() == '1') {
                                ?>
                                <img src="<?php echo $this->_settings->path_images; ?><?php echo $this->_settings->languagePrefix; ?>\sticker.png" width="74" height="73" alt="featured" class="sticker"/>
                            <?php
                            }
                        }
                    } ?>


                </a>
            </div>
            <div class="result-text-block">
                <a href="<?php echo $this->property->getUrl(); ?>" style="text-decoration:none; color:#444444;">
                    <p>
                        <strong><?php echo $this->getStr(Al_Language::FLD_PROP_FEATURES); ?>:</strong> <?php echo $this->property->getFeatures(true); ?>
                    </p>

                    <p>
                        <strong><?php echo $this->getStr(Al_Language::FLD_PROP_ADDRESS); ?>:</strong> <?php echo $this->property->getAddress() . ', ' . $this->property->getZone() . ' ' . $this->property->getCountry() . ' ' . $this->property->getPostalCode(); ?>
                    </p>

                    <?php if ($this->recordPropertyType == Bl_Record_Property::RENT) { ?>
                        <?php if ($this->property->getDeposit() > 0) { ?>
                            <p>
                                <strong><?php echo $this->getStr(Al_Language::FLD_PROP_DEPOSIT); ?>:</strong> <?php echo $this->property->getDeposit(true); ?>
                            </p>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($this->property->getLandArea() > 0) { ?>
                        <p>
                            <strong><?php echo $this->getStr(Al_Language::FLD_PROP_LAND_AREA); ?>:</strong> <?php echo $this->property->getLandArea(); ?>m<span>2</span>
                        </p>
                    <?php } ?>
                </a>
            </div>
        </div>
    <?php } ?>

    <?php if ($this->pager->num_of_pages > 1) { ?>
        <div class="page-nav">

            <div class="pagination">
                <ul>
                    <?php $this->pager->display_nav(); ?>
                </ul>
            </div>


        </div>
    <?php } ?>

</div>
<!--END RESULT-RIGHT-COL ---->


<div class="result-left-col" style="margin-bottom:15px;"><!--RESULT-LEFT-COL---->


    <div class="result-form">
        <h2 style="background-image: none;"><?php echo $this->getStr(Al_Language::AGENT_HEAD_AGENT_DETAILS); ?></h2>


        <!--        <div class="imgpad"></div>-->

        <form class="">

            <select onChange="propTypeChange();" name="ptId" id="ptId" style="width:130px;">
                <?php echo $this->transactionTypes->ddData($this->propertySearch->getTransactionType()); ?>
            </select>
        </form>
        <div class="imgpad"></div>
        <h3><?php echo $this->getStr(Al_Language::AGENT_SUB_AGENT_DETAILS); ?></h3>

        <table class="account-view" cellpadding="5px;">
            <tbody>

            <?php if ($this->member->getCompanyName() != '') { ?>
                <tr>
                    <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_COMPANY); ?>:&nbsp;</strong></td>
                    <td><?php echo $this->member->getCompanyName(); ?></td>
                </tr>
            <?php } ?>

            <tr>
                <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_NAME); ?>:&nbsp;</strong></td>
                <td><?php echo $this->member->getName(); ?></td>
            </tr>

            <?php if ($this->member->getProfileEmail() != '') { ?>
                <tr>
                    <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_EMAIL); ?>:&nbsp;</strong></td>
                    <td><?php echo $this->member->getProfileEmail(); ?></td>
                </tr>
            <?php } ?>

            <?php if ($this->member->getPhone() != '') { ?>
                <tr>
                    <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_PHONE); ?>:&nbsp;</strong></td>
                    <td><?php echo $this->member->getPhone(); ?></td>
                </tr>
            <?php } ?>

            <?php if ($this->member->getWebsite() != '') { ?>
                <tr>
                    <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_WEBSITE); ?>:&nbsp;</strong></td>
                    <td><?php echo $this->member->getWebsite(); ?></td>
                </tr>
            <?php } ?>

            <?php if ($this->member->getAddress() != '') { ?>
                <tr>
                    <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_ADDRESS); ?>:&nbsp;</strong></td>
                    <td><?php echo $this->member->getAddress(); ?></td>
                </tr>
            <?php } ?>

            <?php if ($this->member->getCity() != '') { ?>
                <tr>
                    <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_CITY); ?>:&nbsp;</strong></td>
                    <td><?php echo $this->member->getCity(); ?></td>
                </tr>
            <?php } ?>
            <?php if ($this->member->getPostcode() != '') { ?>
                <tr>
                    <td><strong><?php echo $this->getStr(Al_Language::AGENT_LBL_POSTCODE); ?>:&nbsp;</strong></td>
                    <td><?php echo $this->member->getPostcode(); ?></td>
                </tr>
            <?php } ?>

            </tbody>
        </table>

        <?php if ($this->member->getProfileDescription() != '') { ?>
            <div class="imgpad"></div>
            <h3><?php echo $this->getStr(Al_Language::AGENT_SUB_PROFILE); ?></h3>

            <div><?php echo $this->member->getProfileDescription(); ?></div>
        <?php } ?>

    </div>
</div>
<!--END RESULT-LEFT-COL ---->
        
