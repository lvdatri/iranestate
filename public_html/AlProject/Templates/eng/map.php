
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
	  
	  
	  
.infobox-wrapper {
    display:none;
}
#infobox {
    border:1px solid black;
    margin-top: 0px;
    margin-bottom: 15px;	
    background:#fff;
    color:#FFF;
/*    font-family:Arial, Helvetica, sans-serif;
    font-size:12px; */
    padding: 2px;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;

    -webkit-box-shadow: 0 0  8px #000;
    box-shadow: 0 0 8px #000;
	max-height:300px;
	overflow:auto;
	width:650px;
}	  
	  
	  
    </style>
    

<!--    
<script type="text/javascript" src="<?php // echo $this->_settings->path_js; ?>infobox_packed.js"></script>
-->
    <script type="text/javascript">


		 
		var pointMap = function() {
			var self = this;
			this.map = null;
			this.markersArray = [];			
			this.data = [];
			this.apiPath = '<?php echo $this->pathModule.'properties/api-map/'; ?>';
			
			// http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
			this.infowindow = new google.maps.InfoWindow(
				{
					maxWidth: 850
				} 
			);
/*			this.infobox = new InfoBox({
					 content: document.getElementById("infobox"),
					 disableAutoPan: false,
					 maxWidth: 750,
					 maxHeight: 260,
					 zIndex: null,
					closeBoxMargin: "12px 4px 2px 2px",
					closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
					infoBoxClearance: new google.maps.Size(1, 1)
				});			*/
		
			this.loadClientLocations = function(cid,clid) {
				var dd = $("#jobs_location_id");		
			}
			
			self.iconCluster = new google.maps.MarkerImage(
				'<?php echo $this->_settings->path_images.'map/cluster-x1.png'; ?>',
			    // This marker is 20 pixels wide by 32 pixels tall.
			    new google.maps.Size(28, 29),
			    // The origin for this image is 0,0.
			    new google.maps.Point(0,0),
			    // The anchor for this image is the base of the flagpole at 0,32.
			    new google.maps.Point(14, 14)
			);				
			
			this.initialise = function() {
		        var mapOptions = {
		          center: new google.maps.LatLng(35.67514743608467,51.416015625),
        		  zoom: 11,
				  streetViewControl:false,
				  zoomControl: true,
				  zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL
				  },
				  panControl:false,
		          mapTypeId: google.maps.MapTypeId.ROADMAP
		        };
        		self.map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
				
				  google.maps.event.addListener(self.map, 'dragend', function() {
					self.loadPoints();
				  });			
				  
				google.maps.event.addListener(self.map, 'zoom_changed', function() {
					self.loadPoints();
				  });				
				  
				google.maps.event.addListenerOnce(self.map, 'idle', function(){
					self.loadPoints();
				});				  
				  
				  				  	
				
			}
			
			this.addMarker = function(lat,lon,title,image,type,key) {
				/* var icon1 = new google.maps.Icon({
					anchor: new google.maps.Point(200,200),
					url:image
				}); */
				var icon;
				if(type == 'c') {
					icon = self.iconCluster;
				} else {
					icon = image;
				}
				
							
				
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(lat,lon),
					title:title,
					icon:image,
					map: self.map
				}); 	
				self.markersArray.push(marker);	 
				
				google.maps.event.addListener(marker, 'click', (function(marker, key) {
					return function() {

						if(self.data[key].type == 'c') { 
						
							if( (self.map.getZoom() >= 19) || (self.map.getZoom() >= 15 && self.data[key].count < 10)) {
								// zoom, tileCord
								var r = new $.al.request;
								r.setData({
									"tileCord":self.data[key].tileCord,
									"zoom":self.data[key].zoom
								});
								r.send(
									self.apiPath,
									'getProperties',
									function(response) {
										self.infowindow.setContent('<div style="width:650px;">'+response.data+'</div>');
										self.infowindow.open(self.map, marker);
									},
									function(response) {
										self.infowindow.setContent('no content found...');
									}
								);		 	
								self.infowindow.setContent('loading...');
								self.infowindow.open(self.map, marker);  
								
								
								
//								alert('listings displayed');
							} else {
								var zoom = self.map.getZoom()+1
								if(zoom > 21) {
									zoom = 21
								}
								self.map.setZoom(zoom);
							    self.map.setCenter(marker.getPosition());								
							}
						
						} else {
							var r = new $.al.request;
							r.setData({
								"pid":self.data[key].id
							});
							r.send(
								self.apiPath,
								'getProperty',
								function(response) {
									self.infowindow.setContent(response.data);
									self.infowindow.open(self.map, marker);
								},
								function(response) {
									self.infowindow.setContent('no content found...');
								}
							);		 	
							self.infowindow.setContent('loading...');
							self.infowindow.open(self.map, marker);  
						}
						
						
						  
					}
			    })(marker, key));				
				
				
				 
			}
			
			this.showAllMarkers = function() {
			  if (self.markersArray) {
				for (i in self.markersArray) {
				  self.markersArray[i].setMap(self.map);
				}
			  }
			}	
			
			this.deleteAllMarkers = function() {
			  if (self.markersArray) {
				for (i in self.markersArray) {
				  self.markersArray[i].setMap(null);
				}
				self.markersArray.length = 0;
				self.data.length = 0;
			  }
			}					
			
			this.loadPoints = function() {
				var r = new $.al.request;
				var b = self.map.getBounds();
				
			
				r.setData({
					"neLat":b.getNorthEast().lat(),
					"neLon":b.getNorthEast().lng(),
					"swLat":b.getSouthWest().lat(),
					"swLon":b.getSouthWest().lng(),
					"zoom":self.map.getZoom()
				});
				r.send(
					self.apiPath,
					'getPoints',
					function(response) {
						self.deleteAllMarkers();
						self.data = response.data;
						for (var key in self.data) {
							self.addMarker(
								self.data[key].lat,
								self.data[key].lon,
								self.data[key].count+' properties', // - ' + data[key].tile,
								self.data[key].image,
								self.data[key].type,
								key
							);
						}
					},
					
					function(response) {
						alert ('error adding record');
					}
				);		 				
			}
			
/*		    this.getMapBounds = function() {
				map.clearMarkers();
				var b = map.getBounds();
				var z = map.getZoom();
				
				alert(		b.getNorthEast().lat());
				
				if(b!=null && b!=undefined) {
				var ne = b.getNorthEast();
				var sw = b.getSouthWest();
				alert(ne.lat());
				alert(b);			
				}
		  }	*/		
			
		}
		
		

		


		var map;
	
		$(document).ready(function() { 
			map = new pointMap();		
			map.initialise();
		});
	  
	  function showModal()
	  {
		  map.deleteAllMarkers();
//		return false;
//		  $.modal("<div><h1>Heading</h1> </span><h3>sub modal</h3><p> data here </p></div>");
	  }
	  
	 function loadPoints()
	 {
		 map.infowindow.setContent('Loading...');
		 
//		map.loadPoints();		 
	 }
	  
	  
    </script>

            <div class="result-right-col"><!--RESULT-RIGHT-COL---->
    <div id="map_canvas" style="width:700px; height:650px; "></div>
    
<br>
<br>
<!-- <div>
  <a onclick="loadPoints(); return false;" href="#">map bounds</a> |
  
  <input onclick="showModal();" type="button" name="button" id="button" value="Button" />
</div>  -->
    <div style="clear:both;"></div>
    </div>

<div class="infobox-wrapper">
    <div id="infobox">
        The contents of your info box. It's very easy to create and customize.
    </div>
</div>