<script type="text/javascript"> 
$(".page-nav").corner();
</script> 

<div class="single-column" >
<h1><?php echo $this->getStr(Al_Language::TMPLT_NEWS); ?></h1> 

<table class="article" width="100%" border="0" cellspacing="0" cellpadding="0" >
<?php while($row = $this->articles->fetch_row()) { $this->cms->setFromArray($row); ?>
  <tr>
    <td width="1%" >
    	<?php if($this->cms->getImage() != '') { ?>
	        <a href="<?php echo $this->cms->getPath(true); ?>" class="articleClearlink">	
	    	<img border="0" src="<?php echo $this->cms->getImageSml(); ?>" alt="" />
            </a>
		<?php } ?>    
    </td>
    <td>
    	<a href="<?php echo $this->cms->getPath(true); ?>" class="articleClearlink">
    	<div style="font-weight:bold; margin-bottom:5px; font-size:14px;"><?php echo $this->cms->getTitle(); ?></div>
        <div><?php echo $this->cms->getContent(true); ?>...</div>
        </a>
    </td>
  </tr>
<?php } ?>  
</table>
<?php if($this->pager->num_of_pages > 1) { ?>          
          <div class="page-nav">
       		<div class="pagination">
               <ul><?php $this->pager->display_nav(); ?></ul>
            </div>
          </div>
<?php } ?>  
</div>