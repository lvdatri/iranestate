<script type="text/javascript">
	$(".greyBox").corner();

	$(document).ready(function(){
		$('#form-register').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
		$('#form-login').jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
	});
	
</script>
<?php $this->message->show(); ?>
<div style="float:left; margin-left:25px;">
<div class="greyBox" style="width:375px; margin-bottom:100px;" >    
        <h6><?php echo $this->getStr(Al_Language::REGISTER_TITLE); ?></h6>
		<form action='<?php echo $this->pathModule; ?>register' method='post' id="form-register">
    	<table border="0">
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_NAME); ?>:</td>
	            <td class="field"><input name="name" type="text" value="<?php echo Al_Utilities::ePost('name'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_EMAIL); ?>:</td>
	            <td class="field"><input name="email" type="text" id="email" value="<?php echo Al_Utilities::ePost('email'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_PASSWORD); ?>:</td>
	            <td class="field"><input name="password" type="password" value="<?php echo Al_Utilities::ePost('password'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::REGISTER_REPEAT_PASSWORD); ?>:</td>
	            <td class="field"><input name="password_confirm" type="password" id="password_confirm" value="<?php echo Al_Utilities::ePost('password_confirm'); ?>" ></td>
            </tr>
        	<tr>
	            <td  class="label" valign="middle"><?php echo $this->getStr(Al_Language::REGISTER_QUESTION); ?> <img src="<?php echo $this->pathModule.'register/captcha'; ?>" border="0" /></td>
	            <td class="field"><input name="captcha" type="text" id="captcha"  /></td>
            </tr>
        	<tr>
	            <td></td>
	            <td><input type="submit" name="register" value="<?php echo $this->getStr(Al_Language::REGISTER_BTN_REGISTER); ?>"></td>
            </tr>
        </table>    
		</form>     
</div>   
</div>
<div style="float:left; margin-left:40px;">

<div class="greyBox" style="width:375px;" >    
        <h6><?php echo $this->getStr(Al_Language::LOGIN_TITLE); ?></h6>    
		<form action='<?php echo $this->pathModule; ?>login' method='post' id="form-login">
    	<table border="0">
        	<tr>
	            <td class="label" height="25"><?php echo $this->getStr(Al_Language::LOGIN_EMAIL); ?>:</td>
	            <td class="field"><input name="lemail" type="text" id="lemail" value="<?php echo Al_Utilities::ePost('lemail'); ?>" ></td>
            </tr>
        	<tr>
	            <td class="label"><?php echo $this->getStr(Al_Language::LOGIN_PASSWORD); ?>:</td>
	            <td class="field"><input name="password" type="password" value="<?php echo Al_Utilities::ePost('password'); ?>" ></td>
            </tr>
        	<tr>
	            <td>&nbsp;</td>
	            <td class="label" valign="top">
	              <input type="submit" name="login" value="<?php echo $this->getStr(Al_Language::LOGIN_BTN_LOGIN); ?>" />
                  <a href="<?php echo $this->pathModule; ?>forgot-password" style="color:#000;">
                <?php echo $this->getStr(Al_Language::LOGIN_FORGOT_PASSWORD); ?></a></td>
            </tr>
        </table>
		</form>
</div>

</div>