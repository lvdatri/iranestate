<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title><?PHP echo $this->_settings->tag_title; ?></title>
	<style type="text/css">img, div, a { behavior: url(<?php echo $this->_settings->path_css ?>iepngfix.htc) }</style>    
    
    <meta name="description" content="<?PHP echo $this->_settings->tag_description; ?>"  />
    <meta name="keywords" content="<?PHP echo $this->_settings->tag_keywords; ?>"  />
    <meta name="geo.region" content="IR-07" />
    <meta name="geo.placename" content="Tehran" />
    <meta name="geo.position" content="35.696111;51.423056" />
    <meta name="ICBM" content="35.696111, 51.423056" />
    <meta property="fb:page_id" content="103782523009435" />
    <?php 
        $this->_template->display_css();
        $this->_template->display_js();
    ?>

	<script type="text/javascript">
//    $("#header .main-menu li a").corner("5px");

	$(document).ready(function() { 
	
		$("#tmlt-frm-register").jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
		$("#tmlt-frm-login").jqTransform({imgPath:'<?php echo $this->_settings->path_images; ?>form/'});
	});	
	
	

	<!-- analytics english -->	
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-9866030-1']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();	
    </script>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
	</head>
	<body>
<div id="tile">
      <div id="wrapper">
    <div id="page">
         <div id="header">
            <div class="language-menu">
                <?php if($this->_user->logged_in()) { ?>
                  <ul>            
                    <li><a href='<?php echo $this->_settings->path_web_ssl.$this->_settings->languagePrefix; ?>/account/summary' class='basic'><?php echo $this->getStr(Al_Language::TMPLT_MY_ACCOUNT); ?></a></li>
                    <li> <a href="<?php echo $this->_settings->path_web_ssl.$this->_settings->languagePrefix; ?>/logout"><?php echo $this->getStr(Al_Language::TMPLT_LOGOUT); ?></a></li>                
                <?php } else { ?>
                  <ul class="non-member" >            
                    <li> 
						<a href="#" onclick="$('#basic-modal-content').modal();"><?php echo $this->getStr(Al_Language::TMPLT_MY_ACCOUNT); ?></a>
                    </li>            
                <?php } ?>
                <li> <img src="<?php echo $this->_settings->path_images; ?>vert-divider.png" alt="divider" /> </li>
                <?php if($this->_settings->languagePrefix == Bl_Settings::LANG_PERSIAN_PREFIX) { ?>
                <li> <a href="<?php echo $this->_settings->path_web_ssl.Bl_Settings::LANG_ENGLISH_PREFIX.$this->getRequestUriNoLang(); ?>">English</a></li>
                <?php } else { ?> 
                <li class="farsi"> <a href="<?php echo $this->_settings->path_web_ssl.Bl_Settings::LANG_PERSIAN_PREFIX.$this->getRequestUriNoLang(); ?>">فارسی</a></li>
                <?php } ?>
              </ul>
            </div>
            <a href="http://iranestate.com/" title="&#1576;&#1575;&#1606;&#1705; &#1575;&#1740;&#1606;&#1578;&#1585;&#1606;&#1578;&#1740; &#1605;&#1588;&#1575;&#1608;&#1585; &#1575;&#1605;&#1604;&#1575;&#1705; &#1575;&#1740;&#1585;&#1575;&#1606;"><div class="ielogo" style="position:absolute; width:210px; height:85px; top:5px; left:18px;"></div></a>
            <div class="main-menu">
               <ul>
                <li><a href="<?php echo $this->_settings->path_web_ssl.$this->_settings->languagePrefix; ?>/properties/buy" class="<?php echo $this->_menu->getBuyClass(); ?>"><?php echo $this->getStr(Al_Language::TMPLT_BUY); ?></a></li>
                <li><a href="<?php echo $this->_settings->path_web_ssl.$this->_settings->languagePrefix; ?>/properties/rent" class="<?php echo $this->_menu->getRentClass(); ?>"><?php echo $this->getStr(Al_Language::TMPLT_RENT); ?></a></li>
                <li><a href="<?php echo $this->_settings->path_web_ssl.$this->_settings->languagePrefix; ?>/properties/book" class="<?php echo $this->_menu->getBookClass(); ?>"><?php echo $this->getStr(Al_Language::TMPLT_BOOK); ?></a></li>
                <li><a href="<?php echo $this->_settings->path_web_ssl.$this->_settings->languagePrefix; ?>/property/intro/new" class="<?php echo $this->_menu->getPostClass(); ?>"><?php echo $this->getStr(Al_Language::TMPLT_POST_AD); ?></a></li>
              </ul>
            </div>
            <div class="language-bar">
                  <p><a href="<?php echo $this->_settings->path_web_ssl.Bl_Settings::LANG_PERSIAN_PREFIX; ?>"> مشاور املاک اینترنتی ایران </a></p>
            </div>
      </div>

          <div id="content">
<!--          <div style="float:left;"> -->
				<?php  require($this->_page_path); ?> 
<!-- </div> -->
		  </div>

        </div>
  </div>
  <div style="clear:both"></div>
    </div>
<div id="footer">
      <div id="inner-footer">
    <div class="footer-box"> <img src="<?php echo $this->_settings->path_images; ?>footer-icon-1.png" width="111" height="33" alt="Country" class="footer-icon"/>    
       <ul class="flags">
        <li><img src="<?php echo $this->_settings->path_images; ?>iranflag.png" width="35" height="24" alt="iran" /> (913) 216-9581</li>
        <li><img src="<?php echo $this->_settings->path_images; ?>americaflag.png" width="35" height="24" alt="america" /> (650) 646-2553</li>
        <li><img src="<?php echo $this->_settings->path_images; ?>englandflag.png" width="36" height="24" alt="england" /> (020) 3286-6269</li>
        <li><img src="<?php echo $this->_settings->path_images; ?>aussieflag.png" width="35" height="25" alt="aussie" /> (02) 8005-6468</li>        <li>
         <div style="margin-bottom:0; padding:2px; width:100%;">
                <div style="float:right; width:75px;"><g:plusone></g:plusone></div>
                <div style="width:75px;">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) {return;}
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#appId=235837059802442&xfbml=1";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-like" data-href="http://iranestate.com" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-font="arial"></div>
                </div>
         </div>
        </li>
      </ul>
        </div>
    <div class="footer-box">
          <ul>
        <li class="footer-title1"><?php echo $this->getStr(Al_Language::TMPLT_IRAN_ESTATE); ?></li>
  		<?php foreach($this->_pages as $item) { ?>
      	  <li><a href="<?php echo $item->getPath(true); ?>"><?php echo $item->getTitle(); ?></a></li>
       	<?php } ?>        
      </ul>
        </div>
    <div class="footer-box">
          <ul>
        <li class="footer-title2"><?php echo $this->getStr(Al_Language::TMPLT_ARTICLES); ?></li>
       	<?php foreach($this->_articles as $item) { ?>
       		<li><a href="<?php echo $item->getPath(true); ?>"><?php echo $item->getTitle(); ?></a></li>
        <?php } ?>
        <li><a href="<?php echo $this->pathModule.'articles'; ?>"><?php echo $this->getStr(Al_Language::TMPLT_MORE); ?></a></li>        
        </ul>
      </ul>
        </div>
    <div class="footer-box footer-nopad nobord">
          <ul>
        <li class="footer-title3"><?php echo $this->getStr(Al_Language::TMPLT_SERVICES); ?></li>
        <ul>
       		<?php foreach($this->_services as $item) { ?>
	       	<li><a href="<?php echo $item->getPath(true); ?>"><?php echo $item->getTitle(); ?></a></li>
          	<?php } ?>
        </ul>
        <li><a href="<?php echo $this->pathModule.'services'; ?>"><?php echo $this->getStr(Al_Language::TMPLT_MORE); ?></a></li>        

      </ul>
        </div>
  </div>
</div>

<?php if(!$this->_user->logged_in()) { ?>
<div id="basic-modal-content" style="display:none;">
	<span> <h3><?php echo $this->getStr(Al_Language::POPUP_LOGIN_REGISTER_TITLE); ?></h3>
	<?php echo $this->getStr(Al_Language::POPUP_LOGIN_REGISTER_DESCRIPTION); ?>
	</span>
	<?php include($this->_settings->path_templates.'login-register-popup.php'); ?>
</div>
<?php } ?>    

</body>
</html>