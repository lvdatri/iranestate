<?php
	class Al_Crypt {
		
		/**
		 * generic initialising vector
		 *
		 * @var string
		 */
		private $iv='��sW�@Yp';
		
		/**
		 * encryption handler, used internaly
		 *
		 * @var variable
		 */
		private $handler;
		
		/**
		 * returns encrypted data
		 * as binary or base64 encoded
		 *
		 * @param string $data
		 * @param string $key
		 * @return string
		 */
		function encrypt($data,$key,$base64=false){
		    if($data == '') {
		        return '';
		    }
			$this->open($key);
			$enc = mcrypt_generic($this->handler, $data);
			$this->close();
			if($base64)
				return base64_encode($enc);
			else
				return $enc;	
		}
		
		/**
		 * retruns decrypted data
		 *
		 * @param string $data
		 * @param string $key
		 * @param bool base64
		 * @return string
		 */
		function decrypt($data,$key,$base64=false) {
		    if($data == '') {
		        return '';
		    }		    
			if($base64) {
				$data = base64_decode($data);
			}
			$this->open($key);
			$enc = trim(mdecrypt_generic($this->handler, $data));
			$this->close();
			return $enc;
		}
		
		/**
		 * returns true if data and enc_data are matching
		 *
		 * @param string $data
		 * @param string $enc_data
		 * @param string $key
		 * @return bool
		 */
		function match($data,$enc_data,$key) {
		    if($enc_data == '') {
		        return false;
		    }		    
			$t = $this->decrypt($enc_data,$key);
			if($t=$data) {
				return true;
			}
			else { 	
				return false;
			}
		}
		
		/**
		 * open the cipher
		 *
		 * @param string $key
		 */
		private function open($key) {
			$this->handler = mcrypt_module_open('tripledes', '', 'ecb', '');
			mcrypt_generic_init($this->handler, $key, $this->iv);
		}
		
		/**
		 * close the cipher
		 *
		 */
		private function close() {
			mcrypt_generic_deinit($this->handler);
			mcrypt_module_close($this->handler);
		}
	}