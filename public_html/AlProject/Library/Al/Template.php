<?php
    class Al_Template {
        
        private static $instance;
        
        /**
         * stores scripts that will be displayed within templat header
         * @var string
         */
        private $js_code;
        private $js_core_code;
        
        /**
         * list of javascript includes that are displayed, core is displayed first
         * @var string
         */
        private $js_includes;
        private $js_core_includes;
        
        private $css_includes;
        private $css_core_includes;
        
        private $extjs_on_ready_code;
        
        private $css_code;
        
        private $nl = "\n";
        
		/* @var $settings Bl_Settings */        
        private $settings;
        
        private function __construct() {
            $this->js_code = '';
            $this->js_core_code = '';
            $this->js_includes = '';
            $this->js_core_includes = '';
            $this->css_includes = '';
            $this->css_core_includes = '';
            $this->css_code = '';
            $this->extjs_on_ready_code = '';
            $this->settings = Bl_Settings::get_instance();
		}
		
	    public static function get_instance() {
	        if (!isset(self::$instance)) {
	            $c = __CLASS__;
	            self::$instance = new $c;
	        }
	        return self::$instance;
	    }	

	    
	    public function js_include($path) {
			$this->js_includes .= '<script type="text/javascript" src="'.$this->settings->path_js.$path.'"></script>'.$this->nl;
	    }
	    
	    public function js_core_include($path,$full=false) {
	    	if($full) {
	    		$this->js_core_includes .= '<script type="text/javascript" src="'.$path.'"></script>'.$this->nl;
	    	} else {
            	$this->js_core_includes .= '<script type="text/javascript" src="'.$this->settings->path_js.$path.'"></script>'.$this->nl;
	    	}	        
	    }
	    
	    public function js_attach($code) {
	        $this->js_code .= $code.$this->nl;
	    }
	    
	    public function js_core_attach($code) {
	        $this->js_core_code .= $code.$this->nl;
	    }
	    
	    public function css_include($path,$partial=true) {
	    	if($partial) {
	    		$path = $this->settings->path_css.$path;
	    	}
	        $this->css_includes .= '<link rel="stylesheet" type="text/css" href="'.$path.'">'.$this->nl;
	    }
	    
	    public function css_core_include($path,$partial=true) {
	    	if($partial) {
	    		$path = $this->settings->path_css.$path;
	    	}
	        $this->css_core_includes .= '<link rel="stylesheet" type="text/css" href="'.$path.'">'.$this->nl;
	    }
	    
	    public function css_attach($code) {
	        $this->css_code .= $code;
	    }	    
	    
	    public function display_js() {
	    	$this->extjs_set_ready();
	        
            echo $this->js_core_includes.$this->js_includes . $this->nl;

            if($this->js_core_code != '' || $this->js_code != '') {
	            echo	'<script type="text/javascript">'.$this->nl .
                        $this->js_core_code . $this->nl .
	                    $this->js_code . $this->nl .
	            		'</script>'.$this->nl;
            }
	    }
	    
	    public function display_css() {
	        echo $this->css_core_includes.$this->css_includes.$this->nl;
	        if($this->css_code != '') {
	            echo    '<style type="text/css">'.$this->nl.
	                    $this->css_code.$this->nl.
	                    '</style>'.$this->nl;
	        }
	    }
	    
	    public function jquery_set_core() {
	   		$this->js_core_include('jquery/jquery-1.6.1.min.js'); 	
	    }
	    
		public function jquery_set_gmaps() {
			$this->js_core_include('https://maps.google.com/maps/api/js?sensor=false',true);
//			$this->js_core_include('http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key='.$this->settings->gmap_key,true);
			$this->js_include('gmap.js');
		}	    
	    
		public function extjs_set_core() {
		    $this->css_core_include($this->settings->path_js.'extjs/resources/css/ext-all.css',false);
		    $this->css_core_include($this->settings->path_js.'extjs/resources/css/xtheme-gray.css',false);
		    $this->js_core_include('extjs/adapter/ext/ext-base.js');
		    $this->js_core_include('extjs/ext-all.js');
		    $this->js_core_include('extjs/ux/Response.js');
		    
//		    $this->js_core_attach("Ext.namespace('Ext.al');");
		    
		    $this->js_attach("Ext.al.login_path='".$this->settings->user_login_path."';");
		    $this->js_attach("Ext.al.path='".$this->settings->path_web_ssl."';");
		}
		
    	public function extjs_set_color_field() {
		    $this->css_include($this->settings->path_js.'extjs/ux/css/Ext.ux.ColorField.css',false);
		    $this->js_include('extjs/ux/Ext.ux.ColorField.js');
		}
		
		/**
		 * sets grid to allow locking columns
		 */
		public function extjs_set_grid_locking_view() {
		    $this->css_include($this->settings->path_js.'extjs/ux/css/LockingGridView.css',false);
		    $this->js_include('extjs/ux/LockingGridView.js');
		}
		
		public function extjs_set_row_actions() {
		    $this->css_include($this->settings->path_js.'extjs/ux/css/RowActions.css',false);
		    $this->js_include('extjs/ux/RowActions.js');		    
		}
		
		/**
		 * set greed to support multiline column headers
		 */
		public function extjs_set_grid_multiline_header() {
            $this->css_include('gridMultiLineHeader.css');
            $this->js_include('extjs/ux/GridMultilineHeader.js');
		}
		
        public function extjs_set_grid_check_column() {
            $this->js_include('extjs/ux/CheckColumn.js');
		}
		
		private function extjs_set_ready() {
		    if($this->extjs_on_ready_code != '') {
		        $this->js_core_attach(
		            "Ext.onReady(function() {".$this->nl.
		                $this->extjs_on_ready_code.$this->nl.
		        	"});".$this->nl
		        );
		    }
		}
		
		public function extjs_attach_on_ready($code) {
	        $this->extjs_on_ready_code .= $code . $this->nl;
		}	    
		
		public function al_set_grid_hover($id,$class='gridRowOver') {
		    $this->extjs_attach_on_ready("Ext.al.gridHover('".$id."','".$class."');");
		}
    }