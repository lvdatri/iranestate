<?php
	/**
	 * 
	 * @version 0.03
	 *
	 */
	class Al_Language {
		
		const VAL_ERROR_REQUIRED = '1';
		const VAL_ERROR_NOT_EQUAL_TO = '2';
		const VAL_ERROR_EMAIL = '3';
		const VAL_ERROR_INT = '4';
		const VAL_ERROR_DATE_INVALID = '5';
		const VAL_ERROR_DATE_FORMAT = '6';
		const VAL_ERROR_LENGTH_MIN = '7';
		const VAL_ERROR_LENGTH_MAX = '8';
		const VAL_ERROR_LENGTH_BETWEEN = '9';
		const VAL_ERROR_EQUAL_TO = '10';
		const VAL_ERROR_DB_UNIQUE = '11';
		const VAL_ERROR_FILE_REQUIRED = '12';
		const VAL_ERROR_FILE_MAX_SIZE = '13';
		const VAL_ERROR_FILE_MIN_SIZE = '14';
		const VAL_ERROR_FILE_INVALID_EXTENSION = '15';
		const VAL_ERROR_MATH_CAPTCHA = '16';
		const VAL_ERROR_DB_MATCH = '17';
		const VAL_ERROR_INVALID_VALUE = '18';
		const VAL_ERROR_NUMBER = '19';
		
		const MSG_SUCCESSFUL_LOGOUT = '100';
		const MSG_INVALID_LOGIN = '101';
		const MSG_REGISTRATION_CONFIRMATION_ERROR = '102';
		const MSG_REGISTRATION_CONFIRMATION_SUCCESS = '103';
		const MSG_ACCOUNT_DETAILS_UPDATED = '104';
		const MSG_ERROR_SAVING_ACCOUNT_DETAILS = '105';
		const MSG_ERROR_LOADING_ACCOUNT_DETAILS = '106';
		const MSG_CURRENT_PASSWORD_NO_MATCH = '107';
		const MSG_ACCOUNT_PASSWORD_UPDATED = '108';
		const MSG_PROP_IMAGES_UPLOAD_SUCCESS = '109';
		const MSG_PROP_MAIN_IMAGE_SET_SUCCESS = '110';
		const MSG_PROP_MAIN_IMAGE_SET_ERROR = '111';
		const MSG_PROP_IMAGE_DELETED = '112';
		const MSG_PROP_CREATED_SUCCESS = '113';
		const MSG_PROP_UPDATE_SUCCESS = '114';
		const MSG_PROP_DELETED = '115';
		const MSG_PROP_CONTACT_EMAIL_SENT = '116';
		const MSG_PROP_FRIEND_EMAIL_SENT = '117';
		const MSG_FORGOT_PASSWORD_INVALID_EMAIL = '118';
		
		
		const FLD_ACCOUNT_CURRENT_PASSWORD = '200';
		const FLD_ACCOUNT_NEW_PASSWORD = '201';
		const FLD_ACCOUNT_NEW_PASSWORD_CONFIRM = '202';
		const FLD_ACCOUNT_NAME = '203';
		const FLD_ACCOUNT_EMAIL = '204';
		
		const FLD_PROP_TRANSACTION = '205';
		const FLD_PROP_TYPE = '206';
		const FLD_PROP_STATUS = '207';
		const FLD_PROP_PRICE = '208';
		const FLD_PROP_PRICE_TYPE = '209';
		const FLD_PROP_BEDROOMS = '210';
		const FLD_PROP_BATHROOMS = '211';
		const FLD_PROP_PARKING = '212';
		const FLD_PROP_ADDRESS = '213';
		const FLD_PROP_POSTAL_CODE = '214';
		const FLD_PROP_CONTACT_NAME = '215';
		const FLD_PROP_CONTACT_PHONE = '216';
		const FLD_PROP_CONTACT_EMAIL = '217';
		const FLD_PROP_INTRODUCTION = '218';
		const FLD_PROP_FEATURES = '219';
		const FLD_PROP_SWIMMING_POOL = '220';
		const FLD_PROP_LAND_AREA = '221';
		const FLD_PROP_BUILD_AREA = '222';
		const FLD_PROP_COUNTRY = '223';
		const FLD_PROP_CITY = '224';
		
		const FLD_REG_NAME = '225';
		const FLD_REG_EMAIL = '226';
		const FLD_REG_PASSWORD = '227';
		const FLD_REG_REPEAT_PASSWORD = '228';
		const FLD_REG_QUESTION = '229';
		
		const FLD_PROP_FRD_YOUR_NAME = '230';
		const FLD_PROP_FRD_YOUR_EMAIL = '231';
		const FLD_PROP_FRD_FRIENDS_NAME = '232';
		const FLD_PROP_FRD_FRIENDS_EMAIL = '233';
		const FLD_PROP_FRD_SUBJECT = '234';
		const FLD_PROP_FRD_DESCRIPTION = '235';
		const FLD_PROP_FRD_QUESTION = '236';
		
		const FLD_PROP_CONT_YOUR_NAME = '237';
		const FLD_PROP_CONT_YOUR_EMAIL = '238';
		const FLD_PROP_CONT_YOUR_PHONE = '239';
		const FLD_PROP_CONT_SUBJECT = '240';
		const FLD_PROP_CONT_DESCRIPTION = '241';
		const FLD_PROP_CONT_QUESTION = '242';
		
		const FLD_PROP_TERRACE = '243';
		const FLD_PROP_LAND_WIDTH = '244';
		const FLD_PROP_LAND_HEIGHT = '245';
		const FLD_PROP_AVERAGE_CEILING_HEIGHT = '246';
		const FLD_PROP_LOCATED_ON_LEVEL = '247';
		const FLD_PROP_NUM_OF_LEVELS = '248';
		const FLD_PROP_CEILING_HEIGHT = '249';
		const FLD_PROP_DEPOSIT = '250';		
		
		const FLD_PROP_COMMENTS = '251';			    
		const FLD_PROP_VIEWED = '252';
		const FLD_PROP_LISTED_DATE = '253';
		const FLD_PROP_PROPERTY_ID = '254';
		const FLD_PROP_METERS = '255';
		
		const TAB_ACT_MY_ACCOUNT = '300';
		const TAB_ACT_NOTIFICATIONS = '301';
		const TAB_ACT_SUMMARY = '302';
		const TAB_PROP_INTRODUCTION = '303';
		const TAB_PROP_IMAGES = '304';
		const TAB_PROP_MAP = '305';
		
		const DATA_POOL_NO = '390';
		const DATA_POOL_PRIVATE_POOL = '391';
		const DATA_POOL_PUBLIC_POOL = '392';
		
		const DATA_STATUS_PENDING_APPROVAL = '403';
		const DATA_STATUS_ACTIVE = '404';
		const DATA_STATUS_EXPIRED = '405';
		
		const DATA_STATUS_BUY_UNDER_CONTRACT = '410';
		const DATA_STATUS_BUY_SOLD = '411';
		
		const DATA_STATUS_RENT_UNDER_CONTRACT = '420';
		const DATA_STATUS_RENT_RENTED = '421';
		
		const DATA_STATUS_BOOK_UNDER_CONTRACT = '430';
		const DATA_STATUS_BOOK_BOOKED = '431';
		
		const DATA_SORT_LATEST = '440';
		const DATA_SORT_LOW_TO_HIGH = '441';
		const DATA_SORT_HIGH_TO_LOW = '442';
		const DATA_SORT_PROPERTY_SIZE = '443';
		
		const DATA_TRANSACTION_FOR_SALE = '450';
		const DATA_TRANSACTION_FOR_RENT = '451';
		const DATA_TRANSACTION_FOR_BOOKING = '452';
		
		const DATA_UNDER_REQUEST = '460';
		const DATA_SEARCH_RESULTS = '461';
		
		const PAGE_TITLE_DEFAULT = '500';
		const PAGE_KEYWORDS_DEFAULT = '501';
		const PAGE_DESCRIPTION_DEFAULT = '502';
		
		const PAGE_TITLE_REGISTER = '503';
		const PAGE_KEYWORDS_REGISTER = '504';
		const PAGE_DESCRIPTION_REGISTER = '505';
		
		const PAGE_TITLE_BUY = '506';
		const PAGE_KEYWORDS_BUY = '507';
		const PAGE_DESCRIPTION_BUY = '508';
		
		const PAGE_TITLE_RENT = '509';
		const PAGE_KEYWORDS_RENT= '510';
		const PAGE_DESCRIPTION_RENT= '511';
		
		const PAGE_TITLE_BOOK = '512';
		const PAGE_KEYWORDS_BOOK= '513';
		const PAGE_DESCRIPTION_BOOK= '514';
		
		const PAGE_TITLE_LOGIN = '515';
		const PAGE_KEYWORDS_LOGIN = '516';
		const PAGE_DESCRIPTION_LOGIN = '517';
		
		const PAGE_TITLE_POST = '518';
		const PAGE_KEYWORDS_POST = '519';
		const PAGE_DESCRIPTION_POST = '520';	

		const TMPLT_MY_ACCOUNT = 'T1';
		const TMPLT_LOGOUT = 'T2';
		const TMPLT_BUY = 'T4';
		const TMPLT_RENT = 'T5';
		const TMPLT_BOOK = 'T6';
		const TMPLT_POST_AD = 'T7';
		const TMPLT_IRAN_ESTATE = 'T8';
		const TMPLT_ARTICLES = 'T9';
		const TMPLT_SERVICES = 'T10';
		const TMPLT_MORE = 'T11';
		const TMPLT_ARTICLE_ID = 'T12';
		const TMPLT_LISTED_DATE = 'T13';
		const TMPLT_NEWS = 'T10';		
		
		const SEARCH_ANY = 'S1';
		const SEARCH_LBL_LOCATION = 'S2';
		const SEARCH_LBL_MIN_DEPOSIT = 'S3';
		const SEARCH_LBL_MAX_DEPOSIT = 'S4';
		const SEARCH_LBL_MIN_PRICE = 'S5';
		const SEARCH_LBL_MAX_PRICE = 'S6';
		const SEARCH_LBL_PROP_TYPE = 'S8';
		const SEARCH_BTN_SEARCH = 'S9';
		const SEARCH_BTN_NEW_LISTINGS = 'S10';
		const SEARCH_LINK_ADVANCED_SEARCH = 'S11';
		const SEARCH_INPUT_LOCATION = 'S12';
		const SEARCH_INPUT_MAX = 'S13';
		const SEARCH_INPUT_MIN = 'S14';


	
		
		const SEARCH_LBL_REFINE_SEARCH = 'S15';
		const SEARCH_LBL_PRICE = 'S16';
		const SEARCH_LBL_DEPOSIT = 'S17';
		const SEARCH_LBL_PROPERTY_TYPE = 'S18';
		const SEARCH_LBL_BEDROOMS = 'S19';
		const SEARCH_LBL_BATHROOMS = 'S20';
		const SEARCH_LBL_PARKING = 'S21';
		const SEARCH_LBL_INTRODUCTION = 'S22';
		const SEARCH_LBL_FEATURES = 'S23';
		const SEARCH_LBL_LAND_AREA = 'S24';
		const SEARCH_LBL_BUILD_AREA = 'S25';
		const SEARCH_LBL_PROPERTIES_FOUND = 'S26';
		const SEARCH_LBL_UPDATE = 'S27';
		const SEARCH_LBL_TO = 'S28';
		
		const SEARCH_LBL_MORE_THAN = 'S29';
		const SEARCH_LBL_LESS_THAN = 'S30';
		const SEARCH_LBL_OR_MORE = 'S31';
		
		
		const SEARCH_BTN_LISTINGS = 'S101';
		const SEARCH_BTN_MAP = 'S102';
		
		const MAP_LBL_PROPERTY = 'MAP10';
		const MAP_LBL_PROPERTIES = 'MAP20';
		
		const HOME_TITLE_FEATURED_PROP_BUY = 'H1';
		const HOME_TITLE_FEATURED_PROP_RENT = 'H2';
		const HOME_TITLE_FEATURED_PROP_BOOK = 'H3';
		const HOME_TITLE_RECENT_NEWS = 'H4';
		const HOME_TITLE_BROWSE_NATIONAL_CITIES = 'H5';
		const HOME_TITLE_BROWSE_INTERNATIONAL_CITIES = 'H6';
		
		const LOGIN_EMAIL = 'RF1';
		const LOGIN_PASSWORD= 'RF2';
		const LOGIN_BTN_LOGIN = 'RF3';
		const LOGIN_FORGOT_PASSWORD = 'RF4';
		const REGISTER_NAME = 'RF5';
	    const REGISTER_EMAIL = 'RF6';
	    const REGISTER_PASSWORD = 'RF7';
	    const REGISTER_REPEAT_PASSWORD = 'RF8';
	    const REGISTER_QUESTION = 'RF9';
	    const REGISTER_BTN_REGISTER = 'RF10';	
	    const LOGIN_TITLE = 'RF11';
	    const REGISTER_TITLE = 'RF12';
	    const POPUP_LOGIN_REGISTER_TITLE = 'RF13';
	    const POPUP_LOGIN_REGISTER_DESCRIPTION = 'RF14';

	    
		const FORGOT_PWD_TITLE = 'FPWD1';
	    const FORGOT_PWD_DESCRIPTION = 'FPWD2';
	    const FORGOT_PWD_EMAIL = 'FPWD3';
	    const FORGOT_PWD_BTN = 'FPWD4';   
	    const FORGOT_PWD_EMAIL_SENT_MESSAGE = 'FPWD5';
	    
		const MY_ACCOUNT_LBL_FOR_SALE = 'MYACT1';
		const MY_ACCOUNT_LBL_FOR_RENT = 'MYACT2';
		const MY_ACCOUNT_LBL_FOR_BOOKING = 'MYACT3';
		const MY_ACCOUNT_BTN_ADD_NEW_PROPERTY = 'MYACT4';
		const MY_ACCOUNT_LBL_PREVIEW = 'MYACT5';
		const MY_ACCOUNT_LBL_EDIT = 'MYACT6';
		const MY_ACCOUNT_LBL_DELETE = 'MYACT7';
		const MY_ACCOUNT_MSG_DELETE_PROPERTY = 'MYACT8';
		const MY_ACCOUNT_TABLE_REFERENCE = 'MYACT9';
		const MY_ACCOUNT_TABLE_CREATED = 'MYACT10';
		const MY_ACCOUNT_TABLE_EXPIRES = 'MYACT11';
		const MY_ACCOUNT_TABLE_STATUS = 'MYACT12';
		const MY_ACCOUNT_TABLE_TYPE = 'MYACT13';
		const MY_ACCOUNT_TABLE_CITY = 'MYACT14';
		const MY_ACCOUNT_TABLE_ZONE = 'MYACT15';
		const MY_ACCOUNT_TABLE_PRICE = 'MYACT16';
		const MY_ACCOUNT_TABLE_VIEWS = 'MYACT17';
		const MY_ACCOUNT_TABLE_CONTACT_US = 'MYACT18';
		const MY_ACCOUNT_TABLE_EDIT = 'MYACT19';	
		const MY_ACCOUNT_HEADING = 'MYACT20';
		
		const MY_ACCOUNT_LBL_ACCOUNT_DETAILS = 'MYACT21';
		const MY_ACCOUNT_LBL_ACCOUNT_NAME = 'MYACT22';
		const MY_ACCOUNT_LBL_EMAIL = 'MYACT23';
		const MY_ACCOUNT_LBL_ADDRESS = 'MYACT24';
		const MY_ACCOUNT_LBL_CITY = 'MYACT25';
		const MY_ACCOUNT_LBL_POSTCODE = 'MYACT26';
		const MY_ACCOUNT_LBL_PHONE = 'MYACT27';
		const MY_ACCOUNT_LBL_RECEIVE_EMAILS = 'MYACT28';
		const MY_ACCOUNT_BTN_UPDATE = 'MYACT29';
		const MY_ACCOUNT_BTN_CHANGE_PASSWORD = 'MYACT30';
		const MY_ACCOUNT_BTN_SAVE = 'MYACT31';
		const MY_ACCOUNT_BTN_CANCEL = 'MYACT32';
		const MY_ACCOUNT_LBL_UPDATE_ACCOUNT_DETAILS = 'MYACT33';
		const MY_ACCOUNT_LBL_CURRENT_PASSWORD = 'MYACT34';
		const MY_ACCOUNT_LBL_NEW_PASSWORD = 'MYACT35';
		const MY_ACCOUNT_LBL_NEW_PASSWORD_CONFIRM = 'MYACT36';	
		const MY_ACCOUNT_LBL_UPDATE_ACCOUNT_PASSWORD = 'MYACT37';
        const MY_ACCOUNT_LBL_PROFILE_IMAGE = 'MYACT38';
        const MY_ACCOUNT_LBL_PROFILE_DESCRIPTION_ENG = 'MYACT39';
        const MY_ACCOUNT_LBL_PROFILE_DESCRIPTION_PER = 'MYACT40';
        const MY_ACCOUNT_LBL_WEBSITE = 'MYACT41';
        const MY_ACCOUNT_LBL_SHOW_PROFILE = 'MYACT42';
        const MY_ACCOUNT_SUB_PROFILE_DESCRIPTION = 'MYACT43';
        const MY_ACCOUNT_LBL_COMPANY_NAME = 'MYACT44';
        const MY_ACCOUNT_LBL_PROFILE_EMAIL = 'MYACT45';
        const MY_ACCOUNT_SUB_PROFILE_DETAILS = 'MYACT46';


		const LISTING_SEND_TO_FRIEND = 'L1';
		const LISTING_CONTACT_AGENT = 'L2';
		const LISTING_AREA_MAP = 'L3';
		const LISTING_BACK_TO_SEARCH_RESULTS = 'L5';		
		const LISTING_PRINT_PAGE = 'L6';

        const LISTING_BTN_VIEW_AGENT_PROFILE = 'L7';

		const LISTING_CA_NAME = 'LCA1';
		const LISTING_CA_EMAIL = 'LCA2';
		const LISTING_CA_PHONE = 'LCA3';
		const LISTING_CA_YOUR_NAME = 'LCA4';
		const LISTING_CA_YOUR_EMAIL = 'LCA5';
		const LISTING_CA_YOUR_PHONE_NUMBER = 'LCA6';
		const LISTING_CA_SUBJECT = 'LCA7';
		const LISTING_CA_DESCRIPTION = 'LCA8';
		const LISTING_CA_QUESTION =  'LCA9';
		const LISTING_CA_SEND = 'LCA10';
		
		const LISTING_SF_YOUR_NAME = 'LSF1';
		const LISTING_SF_YOUR_EMAIL = 'LSF2';
		const LISTING_SF_FRIENDS_NAME = 'LSF3';
		const LISTING_SF_FRIENDS_EMAIL = 'LSF4';
		const LISTING_SF_SUBJECT = 'LSF5';
		const LISTING_SF_DESCRIPTION = 'LSF6';
		const LISTING_SF_QUESTION = 'LSF7';
		const LISTING_SF_SEND = 'LSF8';
		
		
		const PROP_SUB_GENERAL_INFORMATION = 'PSG1';
		const PROP_SUB_LOCATION = 'PSG2';
		const PROP_SUB_COMMENTS = 'PSG3';
		const PROP_SUB_INTRODUCTION = 'PSG4';
		const PROP_SUB_PROPERTIES = 'PSG5';
		const PROP_SUB_CONTACT_DETAILS = 'PSG6';
		
		const PROP_LBL_TRANSACTION = 'PSG7';
		const PROP_LBL_PROPERTY_TYPE = 'PSG8';
		const PROP_LBL_STATUS = 'PSG8-1';
		const PROP_LBL_PRICE = 'PSG9';
		const PROP_LBL_MILLION_TOMAN = 'PSG10';
		const PROP_LBL_PRICE_HINT = 'PSG11';
		const PROP_LBL_SWIMMING_POOL  = 'PSG12';
		const PROP_LBL_SWIMMING_POOL_NO = 'PSG13';
		const PROP_LBL_SWIMMING_POOL_Private = 'PSG14';
		const PROP_LBL_SWIMMING_POOL_Public = 'PSG15';
		const PROP_LBL_DEPOSIT = 'PSG16';
		const PROP_LBL_LAND_AREA = 'PSG17';
		const PROP_LBL_BUILD_AREA = 'PSG18';
		const PROP_LBL_BEDROOMS = 'PSG19';
		const PROP_LBL_BATHROOMS = 'PSG20';
		const PROP_LBL_PARKING = 'PSG21';
		const PROP_LBL_LAND_WIDTH = 'PSG22';
		const PROP_LBL_LAND_LENGTH = 'PSG23';
		const PROP_LBL_TERRACE = 'PSG24';
		const PROP_LBL_CEILING_HEIGHT = 'PSG25';
		const PROP_LBL_LOCATED_ON_LEVEL = 'PSG26';
		const PROP_LBL_NUMBER_OF_LEVELS = 'PSG27';	    
		const PROP_LBL_AVERAGE_CEILING_HEIGHT = 'PSG28';
		const PROP_LBL_METERS = 'PSG29';
		
		const PROP_LBL_ADDRESS = 'PSG30';
		const PROP_LBL_COUNTRY = 'PSG31';
		const PROP_LBL_CITY = 'PSG32';
		const PROP_LBL_ZONE = 'PSG33';
		const PROP_LBL_NEW_ZONE = 'PSG34';	    
		const PROP_LBL_POSTAL_CODE = 'PSG35';
		const PROP_LBL_COMMENTS_HINT = 'PSG36';
		const PROP_LBL_COMMENTS_ENGLISH = 'PSG37';
		const PROP_LBL_COMMENTS_PERSIAN = 'PSG38';
		const PROP_LBL_CONTACT_NAME = 'PSG39';	    
		const PROP_LBL_CONTACT_PHONE = 'PSG40';
		const PROP_LBL_CONTACT_EMAIL = 'PSG41';
		const PROP_BTN_INTRO_NEXT = 'PSG42';
		
		const PROP_SUB_UPLOAD_IMAGES = 'PSG43';
		const PROP_SUB_PROPERTY_IMAGES = 'PSG44';
		const PROP_LBL_UPLOAD_IMAGES_HINT = 'PSG45';
		const PROP_LBL_IMAGE_1 = 'PSG46';
		const PROP_LBL_IMAGE_2 = 'PSG47';
		const PROP_LBL_IMAGE_3 = 'PSG48';
		const PROP_LBL_IMAGE_4 = 'PSG49';
		const PROP_LBL_IMAGE_5 = 'PSG50';
		const PROP_LBL_MAIN = 'PSG51';
		const PROP_LBL_DELETE = 'PSG52';
		const PROP_LBL_SET_AS_MAIN = 'PSG53';
		const PROP_BTN_IMAGES_NEXT = 'PSG54';
		const PROP_BTN_UPLOAD_IMAGES = 'PSG54-1';
		
		const PROP_SUB_SELECT_MAP_LOCATION = 'PSG55';
		const PROP_LBL_MAP_HINT = 'PSG56';
		const PROP_LBL_LATITUDE = 'PSG57';
		const PROP_LBL_LONGITUDE = 'PSG58';	    
		const PROP_LBL_DEPTH = 'PSG59';
		const PROP_BTN_MAP_NEXT = 'PSG60';
		const PROP_HEAD_POST_PROPERTY = 'PSG61';
		const PROP_HEAD_UPDATE_PROPERTY = 'PSG62';		
		
		
		const ERROR_404_TITLE = 'ERROR1';
		const ERROR_404_PROPERTIES_FOR_SALE = 'ERROR2';
		const ERROR_404_PROPERTIES_FOR_RENT = 'ERROR3';
		const ERROR_404_PROPERTIES_FOR_BOOKING = 'ERROR4';		

		const RSS_BUY_TITLE = 'RSS1';
		const RSS_BUY_DESCRIPTION = 'RSS2';
		const RSS_RENT_TITLE = 'RSS3';
		const RSS_RENT_DESCRIPTION = 'RSS4';
		const RSS_BOOK_TITLE = 'RSS5';
		const RSS_BOOK_DESCRIPTION = 'RSS6';
		const RSS_ALL_TITLE = 'RSS7';
		const RSS_ALL_DESCRIPTION = 'RSS8';


        const AGENT_HEAD_AGENT_DETAILS = 'agent0';
        const AGENT_SUB_AGENT_DETAILS = 'agent1';
        const AGENT_LBL_COMPANY = 'agent2';
        const AGENT_LBL_NAME = 'agent3';
        const AGENT_LBL_PHONE = 'agent4';
        const AGENT_LBL_EMAIL = 'agent5';
        const AGENT_LBL_WEBSITE = 'agent6';
        const AGENT_LBL_PROFILE = 'agent7';
        const AGENT_LBL_ADDRESS = 'agent8';
        const AGENT_LBL_CITY = 'agent9';
        const AGENT_LBL_POSTCODE = 'agent10';
        const AGENT_SUB_PROFILE = 'agent11';


		const MAINTENANCE = 'MAINT';
		
		
		private static $instance;
		
		private $activeLanguage;
		
		public $language;
		
		private function __construct() {
//			$this->setLanguage();
			$language = array();
		}
		
		public function setLanguage($lang)
		{
			if($lang == Bl_Settings::LANG_PERSIAN) {
				if(!isset($this->language[Bl_Settings::LANG_PERSIAN])) {
					$this->language[Bl_Settings::LANG_PERSIAN] = Bl_Language_Persian::get();
				}
				$this->activeLanguage = Bl_Settings::LANG_PERSIAN;
			} else {
				$this->language[Bl_Settings::LANG_ENGLISH] = Bl_Language_English::get();				
				$this->activeLanguage = Bl_Settings::LANG_ENGLISH;
			}
			
		}
		
	    public static function get_instance() {
	        if (!isset(self::$instance)) {
	            $c = __CLASS__;
	            self::$instance = new $c;
	        }
	        return self::$instance;
	    }
	    
	    public function getValue($messageId) {
	    	if(array_key_exists($messageId,$this->language[$this->activeLanguage])) {
	    		if($this->activeLanguage == Bl_Settings::LANG_ENGLISH) {
	    			return $this->language[$this->activeLanguage][$messageId];
	    		} else {
	    			return html_entity_decode($this->language[$this->activeLanguage][$messageId], ENT_NOQUOTES, 'UTF-8');
	    		}
	    	}
	    	return '';
	    }
	    
	    public static function concat($val_eng,$val_per) {
	    	$val = $val_eng;
	    	if($val_per != '') {
	    		$val .= ' | ' . $val_per;
	    	}
	    	return $val;
	    }
	    
	}