<?php
	class Al_Form {
		const INSERT = '1';
		const UPDATE = '2';
		const DELETE = '3';
		
		private $_mode;
		public $_fields;
		private $_validationErrors;
		private $_posted = null;
		private $_id;
		private $_action;
		private $_cancel_action;
		
		public $_customSettings;
		
		public function __construct($customSettings=null) {
			$this->_fields = array();
			$this->_customSettings = $customSettings;
			$this->initialiseForm();
			$this->setAction(urldecode($_SERVER['REQUEST_URI']));
		}
		
		public function setAction($value) {
		    $this->_action = $value;
		}
		
		public function getAction() {
		    return $this->_action;    
		}		
		
		public function setCancelAction($value) {
			$this->_cancel_action = $value;
		}
		
		public function getCancelAction() {
			return $this->_cancel_action;
		}
		
		public function setMode($mode) {
			$this->_mode = $mode;
			$this->initialiseForm();
		}
		
		public function getMode() {
			return $this->_mode;
		}
		
		protected function initialiseForm() {
			
		}
		
		protected function _addField($field) {
			$field->setForm($this);
			$this->_fields[$field->getName()] = $field;
		}
		
		public function _getField($field) {
			if(array_key_exists($field,$this->_fields)) {
				return $this->_fields[$field];
			}
			return null;
		}
		
		public function beforeValidation() {
			
		}
		
		public function _validate() {
			$this->beforeValidation();
			
			$this->_validationErrors = array();
			foreach($this->_fields as $field) {
				if($field->valid()==false) {
					$this->_addValidationError($field->getName(),$field->getLabel(),$field->getValidationError());
				}
			}

			if(count($this->_validationErrors)>0) {
				return false;
			} else {
				return true;
			}
		}		
		
		public function getValidationErrors() {
			return $this->_validationErrors;
		}
		
		public function _addValidationError($field,$label,$message) {
			$this->_validationErrors[$field] = array(
				'field' => $field,
				'label' => $label,
				'message' => $message,
			);
		}
		
		public function posted($setFromPost=true) {
			if($this->_posted === null) {
				$this->_posted = false;		

				if(array_key_exists('__frm',$_POST)) {
					$this->_posted = true;
				} else {
					$field = reset($this->_fields);
					if(isset($_POST[$field->getName()])) {
						$this->_posted = true;	
					} 					
				}
				
				if($this->_posted==true && $setFromPost) {
					$this->setFromPost();
				}				
			} 
			return $this->_posted;
		}
		
		public function setFromPost() {
			foreach($this->_fields as $field) {
				$field->setFromPost();
			}
		}
		
		public function setFromArray($data) {
			foreach($this->_fields as &$field) {
				if(array_key_exists($field->getName(),$data)) {
					$field->setValue($data[$field->getName()]);
				}
			}
		}
		
		public function setFromDbArray($data) {
			foreach($this->_fields as &$field) {
				if(array_key_exists($field->getName(),$data)) {
					$field->setDbValue($data[$field->getName()]);
				}
			}
			$this->afterDbData();
		}

		public function save($id) {
			$this->setId($id);
			foreach($this->_fields as &$field) {
				$field->save();
			}
		}
		

		public function afterDbData() {
			
		}
		
		public function setId($value) {
			$this->_id = intval($value);
		}
		
		public function getId() {
			return $this->_id;
		}
		
		public function valid() {
			return $this->_validate();
		}		
		
		public function getDbDataArray() {
			$data = array();
			foreach($this->_fields as $field) {
				$data[$field->getName()] = $field->getDbValue();
			}
			return $data;
		}		
		
		public function _cbValue($value) {
			if($value=='1') {
				return ' checked="checked" ';
			} else {
				return '';
			}			
		}
		
		public function _rbValue($value,$option) {
			if($value == $option) {
				return ' checked="checked" ';
			} else {
				return '';
			}
		}
		
	   	
	}