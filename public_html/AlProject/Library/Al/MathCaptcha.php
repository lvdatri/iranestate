<?php
	class Al_MathCaptcha {
		private $sessionVar='math-captcha';
		private $_settings;
		private $bgR=255;
		private $bgG=255;
		private $bgB=255;
		
		
		public function __construct($type='') {
			if($type != '') {
				$this->sessionVar .= '-'.$type;
			} 
			$this->_settings = Bl_Settings::get_instance();
		}
		
		public function setBgColour($r,$g,$b) {
			$this->bgR = $r;
			$this->bgG = $g;
			$this->bgB = $b;
		}
		
		public function generateCode() 
		{
			$num = array(1,2,3,4,5,6,7,8,9);
			$plus = "+";
			$first  = mt_rand(0,8);
			$second = mt_rand(0,8);
			$answer = $num[$first] + $num[$second]; 
			$question = $num[$first] . " + " . $num[$second] . " = ";
			
			$this->setSession(array(
				'a' => $answer,
				'q' => $question
			));	
			session_write_close();

			return $question;
		}
		
		public function display($generate=true) 
		{
			if($generate) {
				$question = $this->generateCode();	
			} else {
				$ses = $this->getSessionValue();
				$question = $ses['q'];
				if($question == '') {
					$question = $this->generateCode();
				}
			}
			
			$this->displayImage($question);
		}
		
		private function setSession($value) {
			$_SESSION[$this->_settings->session][$this->sessionVar] = $value;			
		}
		
		private function displayImage($question) {
			$height = 18;
			$width = strlen($question)*8;

			$image = @imagecreate($width,$height);
			imagecolorallocate($image, $this->bgR, $this->bgB, $this->bgG);

			$text_color = imagecolorallocate($image, 68, 86, 86);
			imagestring($image, 4, 2, 2, $question, $text_color);
			 
			header('Content-type: image/png');			 
			imagepng($image);			
		}
		
		public function clear() {
			$this->setSession(array(
				'a' => mt_rand(1,50),
				'q' => '1 + 1 =',
			));
		}
		
		public function getSessionValue() 
		{
			if(isset($_SESSION[$this->_settings->session][$this->sessionVar])) { 
				return $_SESSION[$this->_settings->session][$this->sessionVar]; 
			} else {
				return '';
			}			
		}
		
		public function isValid($value) {
			$sesVal = $this->getSessionValue();

			if(isset($sesVal['a']) && $sesVal['a'] == $value) {
				return true;
			} else {
				$this->clear();
				return false;
			}
		}
	}