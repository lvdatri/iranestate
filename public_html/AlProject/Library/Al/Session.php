<?php
	ini_set('session.gc_probability', 1);
	ini_set('session.gc_divisor', 100);
	/**
	 * 
	 * @version 0.1
	 *
	 */
	class Al_Session {
	   public $life_time;
	   private $_db;
	
	   function __construct() {

			// Read the maxlifetime setting from PHP
			$this->life_time = 1440; // number of seconds before session will expire.
			$this->_db = Al_Db::get_instance();
	
	      	// Register this object as the session handler
			session_set_save_handler( 
				array( &$this, "open" ), 
				array( &$this, "close" ),
				array( &$this, "read" ),
				array( &$this, "write"),
				array( &$this, "destroy"),
				array( &$this, "gc" )
			);
		}
	
		function open( $save_path, $session_name ) {
		  	return true;
		}
		
		function close() {
			return true;
		}
	
		function read( $ses_id ) {
			// Fetch session data from the selected database
			$res = $this->_db->query("
				SELECT session_data AS d
				FROM sessions
				WHERE session_id = '" . Al_Db::escape($ses_id) . "'
				AND session_expires > ".time() 
			);
			if($res->num_rows()) {
				$row = $res->fetch_row();
				return $row['d'];
			}
			else {
				return '';
			}
		}
	
	   	function write( $ses_id, $ses_data ) {
			// new session-expire-time
			$new_exp = time() + $this->life_time;
			// is a session with this id in the database?
			$res = $this->_db->query("
				SELECT * FROM sessions
			    WHERE session_id = '" . Al_Db::escape($ses_id) . "'
			");

			if($res->num_rows()>0) {
			    // ...update session-data
			    $res = $this->_db->query("
			    	UPDATE sessions SET 
			    		session_expires = '".Al_Db::escape($new_exp) . "',
			            session_data = '".Al_Db::escape($ses_data)."'
			        WHERE session_id = '".Al_Db::escape($ses_id) . "'
			  	");
			}
			// if no session-data was found,
			else {
			    // create a new row
			    $this->_db->query("
			    	INSERT INTO sessions (
						session_id,
						session_expires,
						session_data
					) VALUES (
                 		'".Al_Db::escape($ses_id)."',
                 		'".Al_Db::escape($new_exp)."',
                 		'".Al_Db::escape($ses_data)."'
                 	)
				");

			}
			return true;	   	
	   	}
		function destroy( $ses_id ) {
			// delete session-data
			$this->_db->query("
				DELETE FROM sessions 
				WHERE session_id = '".Al_Db::escape($ses_id) . "'
			");
			return true;
	   	}
	
	   	function gc() {
			// delete old sessions
			$this->_db->query("
				DELETE FROM sessions 
				WHERE session_expires < ".time()
			); 	      
	      	return true;
	   	}
	}
?>