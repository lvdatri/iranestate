<?php
	class Al_Image {
		private $imageSource=null;
		private $imageWidth;
		private $imageHeight;
		private $imageType;
		
		private $watermark;
		private $watermarkPath;
		private $watermarkVerticalAlign;
		private $watermarkHorizontalAlign;
		
		private $newWidth;
		private $newHeight;
		
		const GIF = 1;
		const JPG = 2;
		const PNG = 3;
		
		public function setNewWidth($value) {
			$this->newWidth = intval($value);
		}
		
		public function setNewHeight($value) {
			$this->newHeight = intval($value);
		}
		
		public function setWatermarkPath($value) {
			$this->watermark = true;
			$this->watermarkPath = $value;
		}
		
		public function setWatermarkVerticalAlign($value) {
			$this->watermarkVerticalAlign = $value;
		}
		
		public function setWatermarkHorizontalAlign($value) {
			$this->watermarkHorizontalAlign = $value;
		}		
		
		public function clear() {
			$this->newHeight = 0;
			$this->newWidth = 0;
			$this->watermark = false;
		}
		
		
		
		
		public function saveTo($toPath) {
			if($this->newWidth == 0 && $this->newHeight == 0) {
				// no width, height set
				$width = $this->imageWidth;
				$height = $this->imageHeight;
			}
			else if ($this->newWidth > 0 && $this->newHeight > 0) {
				// both width and height set
				$width = $this->newWidth;
				$height = $this->newHeight;
			} else {
				// it will resize to max width or height of a picture
				$ratio = max($this->imageWidth, $this->imageHeight) / $this->newWidth;
				$ratio = max($ratio, 1.0);
				$width = (int)($this->imageWidth / $ratio);
				$height = (int)($this->imageHeight / $ratio);
			}
			
			
			Al_File::delete($toPath);
			
			$destImage = ImageCreateTrueColor($width,$height);
			imagecopyresampled($destImage,$this->imageSource,0,0,0,0,$width,$height,$this->imageWidth,$this->imageHeight); 
			
			if($this->watermark) {
				$this->setWatermark($destImage,$width,$height);
			}
			
			imagejpeg($destImage,$toPath,95); 
			
			imagedestroy($destImage);
		}

		private function setWatermark(&$destImage,$width,$height) {
			// read the watermark image 
			$watermark=imagecreatefrompng($this->watermarkPath);   
			$watermark_width=imagesx($watermark);   
			$watermark_height=imagesy($watermark);   
			
			switch($this->watermarkVerticalAlign) {
				case 't': // top
					$dest_y=5;
					break;
				case 'm': // middle
					$dest_y=intval($height/2)-($watermark_height/2);
					break;
				default: // bottom
					$dest_y=$height-$watermark_height-5;		
			}
			
			switch($this->watermarkHorizontalAlign) {
				case 'l': // top
					$dest_x=5;					
					break;
				case 'c': // middle
					$dest_x=intval($width/2)-intval($watermark_width/2);
					break;
				default: // bottom
					$dest_x=$width-$watermark_width-5;
					break;
			}
			
			imagecopy($destImage, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height); 
//			imagesavealpha($destImage, true); 			
			
		}
		
		public function close() {
			if($this->imageSource !== null) {
				imagedestroy($this->imageSource);
			} 	
		}
		
		public function load($path) {
			$this->imageSource = null;
		
			$size = getimagesize($path);
			list($width, $height, $type, $attr) = @getimagesize($path);
	        if($width === null) {			
				return false;
			}
			
			$this->imageWidth = $width;
			$this->imageHeight = $height;
			$this->imageType = $type;
			
			
			switch ($this->imageType) {
				case self::GIF:
					$this->imageSource = imagecreatefromgif($path);
					break;
				case self::JPG:
					$this->imageSource = imagecreatefromjpeg($path);
					break;
				case self::PNG:
					$this->imageSource = imagecreatefrompng($path);
					break;
			}
			
			if($this->imageSource===null) {
				return false;
			}
			
			return true;
		}		
	}