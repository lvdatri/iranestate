<?php
	class Al_Message {
		
		private $_settings;	
		private $messages = array();
		private $type;		

		const ERROR = 'error';
		const SUCCESS = 'success';
		
		public function __construct() {
			$this->_settings = Bl_Settings::get_instance();
			$this->load();
			$this->type = self::SUCCESS;
		}
		
		public function addRecordMessages($messages) {
			foreach($messages as $message) {
				$this->add($message['label'].': '.$message['message'],self::ERROR);
			}
		}
		
		public function add($message,$type='') {
			if($type = '') {
				$type = $this->type;
			}
			if(array_key_exists($type,$this->messages)==false) {
				$this->messages[$type] = array();
			}
	 		$this->messages[$type][] = $message;	
	 		return $this;	
		}
		
		public function show() {
			if(count($this->messages)>0) {
				$msg = '<div class="al-message"><ul>';
				foreach ($this->messages as $type=>$messages) {
					if($type == 'error') 
						$class = ' class="error" ';
					else 
						$class = '';
					foreach ($messages as $message) {
						$msg .= '<li'.$class.'>'.$message.'</li>';
					}
				}
				$msg .= '</ul></div>';
				$this->clear();				
				print $msg;
			}
		}
		
		public function save() {
			$_SESSION[$this->_settings->session][$this->_settings->session_messages] = $this->messages;
		}
		
		public function clear() {
			$this->messages = array();
			$this->save();
		}
		
		private function load() {
			if(isset($_SESSION[$this->_settings->session][$this->_settings->session_messages])) {
				$this->messages = $_SESSION[$this->_settings->session][$this->_settings->session_messages];
			}
			else {
				$this->messages = array();
			}
		}
	}