<?php

class Al_ServiceHandler {

    public $response;
    public $request;
    private $_user;

    public function __construct() {
        $this->_user = Al_User::get_instance();
    }

    public function load($userAccess=null, $request = null) {
        $this->response = new Al_JsonResponse();
        
        if($userAccess == null) {
        	$this->request = new Al_JsonRequest();
        	if ($this->request->load($request)) {
        		return true;
        	} else {
        		$this->response = $this->request->getErrorResponse();
        	}        	
        } else {
	        if ($this->_user->logged_in()) {
	            if ($this->_user->allow($userAccess)) {
	                $this->request = new Al_JsonRequest();
	                if ($this->request->load($request)) {
	                    return true;
	                } else {
	                    $this->response = $this->request->getErrorResponse();
	                }
	            } else {
	                $this->response->setStatus(Al_JsonResponse::$INSUFFICIENT_ACCESS);
	                $this->response->setMessage(Al_JsonResponse::$MSG_INSUFFICIENT_ACCESS);
	            }
	        } else {
	            if($userAccess==null) {
	                $this->request = new Al_JsonRequest();
	                if ($this->request->load($request)) {
	                    return true;
	                } else {
	                    $this->response = $this->request->getErrorResponse();
	                }                
	            } else {
	                $this->response->setStatus(Al_JsonResponse::$LOGIN_ERROR);
	            }
	        }
        }
        return false;        
    }

    public function showResponse() {
        $this->response->show();
    }

}