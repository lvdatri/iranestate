<?php
	class Al_Form_FieldCheckboxGroup extends Al_Form_Field {
		public $optionItems = array();
		public $values = array();
		
		public function setOptions($values) 
		{
			$this->optionItems = array();
			
			foreach($values as $item) {
				$item['value'] = '';
				$this->optionItems[$item['id']] = $item;	
			}

		}
		
		public function getOptions() 
		{
			return $this->optionItems;
		}
		
		public function getOptionValue($id) 
		{
			if(array_key_exists($id,$this->optionItems)) {
				return $this->optionItems[$id]['value'];
			}
			return '';
		}
		
		public function setValue($value) 
		{
			$this->clearValues();
			$items = explode(',',$value);
			foreach($items as $id) {
				if(array_key_exists($id,$this->optionItems)) {
					$this->optionItems[$id]['value'] = '1';
				}
			}
			
			$this->setValueFromOptions();	
		}
		
		public function setValueFromOptions() 
		{
			$str = '';
			foreach($this->optionItems as $option) {
				if($option['value'] == '1') {
					$str .= $option['id'].',';
				}
			}
			$this->_value = rtrim($str,',');
		}
		
		public function clearValues() 
		{
			foreach($this->optionItems as &$option) {
				$option['value'] = '';
			}
		}
		
		public function setDbValue($value) {
			$this->setValue($value);
		}	
		
		public function getDbValue() 
		{
			return $this->getValue();
		}
		
		public function setFromPost() 
		{
			$postOptions = Al_Utilities::post($this->getName());
			$this->clearValues();
			if(is_array($postOptions)) { 
				foreach($postOptions as $id) {
					if(isset($this->optionItems[$id])) {
						$this->optionItems[$id]['value'] = '1';
					}
				}
			}		
			$this->setValueFromOptions();
		}
		
		public function getSelectedLabels($labelKey='label') {
			$str = '';
			
			
			foreach($this->optionItems as $option) {
				if($option['value'] != '') {
					$str .= $option[$labelKey] . ', ';
				}
			}		
			return rtrim($str,', ');
		}
		
		public function retrieveFormOptions($chunks=3) {
		    $options = array_chunk($this->optionItems,$chunks);
	    	$lastOption = count($options)-1;
	    	$itemsCount = count($options[$lastOption]);
	    	
	    	if($itemsCount!=3) {
	    		// fill empty values
				for($i=$itemsCount;$i<3;$i++) {
					$options[$lastOption][] = array(
						'id' => '_'.$i,
						'label' => '',
					);
				}    		
	    	}	
	    	return $options;		
		}

	
	}