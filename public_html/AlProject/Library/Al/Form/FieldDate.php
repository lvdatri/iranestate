<?php
	class Al_Form_FieldDate extends Al_Form_Field {
		public function setDbValue($value) {
			if($value == '') {
				$this->setValue('');
			} else {
				if (!Zend_Date::isDate($value, $this->_settings->date_format_db)) {
					$this->setValue('');
				} else {
					$date = new Zend_Date();
					$date->set($value,$this->_settings->date_format_db);
					$this->setValue($date->toString($this->_settings->date_format));
				}			
			}
		}	
		
		public function getDbValue() {
			$value = $this->getValue();
			if($value != '') {
				if (Zend_Date::isDate($value, $this->_settings->date_format)) {
					$date = new Zend_Date();
					$date->set($value,$this->_settings->date_format);
					$value = $date->toString($this->_settings->date_format_db);
				} 				
			}
			return $value;
		}
	}