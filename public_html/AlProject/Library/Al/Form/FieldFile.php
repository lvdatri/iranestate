<?php
	class Al_Form_FieldFile extends Al_Form_Field {
		public $path='';
		public $extension=false;
		public $images = null;
		
		
		private $fileUploadedPath='';
		private $fileUploadedName='';
		private $fileUploadedSize='0';
		private $fileSavedPath='';
		private $fileSavedName='';
		private $fileSavedSize='0';
		
		public function initialise() {
			$this->setFromSession();
		}
		
		
		public function setPath($value,$path='') {
			if($path == '') {
				$path = $this->_settings->path_public;
			}
			$this->path = $path.$value;
		}
		public function getPath() {
			return $this->path;
		}
		
		public function setExtension($value) {
			$this->extension = $value;
		}
		
		
		
		
		public function getExtension() {
			return $this->extension;
		}
		
		public function setImages($value) {
			$this->images = $value;
		}
		
		public function getImages() {
			return $this->images;
		}
		
		
		public function isEmpty() {
			$rule = new Al_Validation_RuleFileRequired();
			$rule->setField($this);
			$rule->setForm($this->_form);
			if($rule->valid()) {
				return false;
			} else {
				return true;
			}
		}		
		
		public function setFromPost() {
			$file = $this->getFileDetails();
			$this->setValue('');			

			if($file !== null) {
				$fu = new Al_FileUpload();
				$fu->saveTempFile($file['tmp_name']);
				$this->setValue($file['name']);
				$this->fileUploadedName = $file['name'];
				$this->fileUploadedPath = $fu->getTempPath();
				$this->fileUploadedSize = $file['size'];
			} else {
				if($this->fileUploadedPath!='') {
					$this->setValue($this->fileUploadedName);
				} else if($this->fileSavedName != '') {
					$this->setValue($this->fileSavedName);
				}
			}
			$this->saveToSession();
		}		
		
		public function getFileDetails() {
			$name = $this->getName();
			if(array_key_exists($name,$_FILES)) {
				$file = $_FILES[$name];
				if(!empty($file['name']) && $file['error']==0) {
					if($file['size'] != '') {
						return $file;
					}
				}
			}
			return null;			
		}
		
		public function setDbValue($value) {
			if($value == '') {
				$this->clearUploadData();
			} else {
				$this->clearUploadData();
				$this->fileSavedName = $value;
			}
			$this->saveToSession();
			$this->setValue($value);
		}		
		
		public function save() {
			// make sure that path is writable
			if($this->path=='') {
				echo get_class($this->_form).' - Al_Form_FieldFile - '. $this->getName() .' path not set';
				exit;
			}
			
			if(!is_writable($this->path)) {
				echo get_class($this->_form).' - Al_Form_FieldFile - '. $this->getName() .' path not writable - <BR>' . $this->path;
				exit;
			}
			
			if($this->fileUploadedPath != '') {
				// file uploaded, save it
				$path = Al_Utilities::getPathFromNo($this->_form->getId(),$this->path);
				Al_File::mkdir($path);
				
				if($this->images === null) {
					$this->processFile($path);
				} else {
					$this->processImageFile($path);
				}
			}
			$this->clearUploadData();
			$this->saveToSession();
		}
		
		public function processFile($path) {
			$fu = new Al_FileUpload();
			$fu->saveFile($this->fileUploadedPath, $path.$this->_form->getId());
		}
		
		public function processImageFile($path) {
			$fileId = $this->_form->getId();
			
			$image = new Al_Image();
			if($image->load($this->fileUploadedPath)) {
				
				foreach($this->images as $item) {
					$image->clear();
					$postfix = '';
					$prefix = '';
					
					
					if(array_key_exists('width',$item)) {
						$image->setNewWidth(intval($item['width']));
					}
					
					if(array_key_exists('height',$item)) {
						$image->setNewHeight(intval($item['height']));
					}
					
					if(array_key_exists('watermarkPath',$item)) {
						$image->setWatermarkPath($item['watermarkPath']);
						
						if(array_key_exists('watermarkVerticalAlign',$item)) {
							$image->setWatermarkVerticalAlign($item['watermarkVerticalAlign']);
						}
						
						if(array_key_exists('watermarkHorizontalAlign',$item)) {
							$image->setWatermarkHorizontalAlign($item['watermarkHorizontalAlign']);
						}
						
					}
					
					
					
					if(array_key_exists('prefix',$item)) {
						$prefix = $item['prefix'];	
					}
					
					if(array_key_exists('postfix',$item)) {
						$postfix = $item['postfix'];	
					}
					
					$image->saveTo($path.$prefix.$fileId.$postfix.'.jpg');
					
				}
				$image->close();
				Al_File::delete($this->fileUploadedPath);
			}

		}

		public function clearUploadData() {
			$this->fileUploadedPath='';
			$this->fileUploadedName='';
			$this->fileUploadedSize='0';
			$this->fileSavedPath='';
			$this->fileSavedName='';
			$this->fileSavedSize='0';	
		}
		
		public function setFromSession() {
			$this->clearUploadData();
			$formName = get_class($this->_form);
			$fieldName = $this->getName();
			if( isset($_SESSION[$this->_settings->session]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName]) && 
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName]['file_uploaded_path'])
			){ 
				$data = $_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName];
				$this->fileUploadedPath=$data['file_uploaded_path'];
				$this->fileUploadedName=$data['file_uploaded_name'];
				$this->fileUploadedSize=$data['file_uploaded_size'];
				$this->fileSavedPath=$data['file_saved_path'];
				$this->fileSavedName=$data['file_saved_name'];
				$this->fileSavedSize=$data['file_saved_size'];					
			}
		}
		
		public function saveToSession() {
			$formName = get_class($this->_form);
			$_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$this->getName()] = array(
				'file_uploaded_path' => $this->fileUploadedPath,
				'file_uploaded_name' => $this->fileUploadedName,
				'file_uploaded_size' => $this->fileUploadedSize,
				'file_saved_path' => $this->fileSavedPath,
				'file_saved_name' => $this->fileSavedName,
				'file_saved_size' => $this->fileSavedSize,				
			);
		}
		
	}