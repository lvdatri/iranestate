<?php
	
	class Al_Form_Generate {
		private $_db;
		private $_clsRecord;
		private $_tableName;
		private $_className;
		private $_tableSchema;
		private $_html;
		
		public $labelWidth = '13%';
		public $inputWidth = '87%';
		
		private $_isPosted = null;
		
		private $_sessionName = 'generate-form';
		
		private $nl="\n";
		private $tab="    ";
		
		public function __construct() {
			$this->_db = Al_Db::get_instance();
			$this->_html = '';
			$this->setFormData();
		}
		
		public function setFormData() {
			if($this->posted()) {
				$_SESSION[$this->_sessionName] = $_POST;
			} else {
				if(array_key_exists($this->_sessionName,$_SESSION)) {
					$_POST = $_SESSION[$this->_sessionName];
				} else {
					$_POST = array();
				}
			}
		}
		
		public function clearSaved() {
			$this->_SESSION[$this->_sessionName] = array();
			$_POST = array();
		}
		
		public function posted() {
			if($this->_isPosted===null) {
				if(isset($_POST['action'])) {
					$this->_isPosted = true;
				} else {
					$this->_isPosted = false;
				}
			}
			return $this->_isPosted;
		}
		
		public function getFormTableSchema($tableName) {
			$res = $this->_db->query("
				DESCRIBE ".$tableName."
			");
			$data = array();
			while($row = $res->fetch_row()) {
				$row['var_name'] = $this->getVarName($row['Field']);
				$row['field_name'] = $row['Field'];
				$row['field_label'] = ucwords(str_replace('_',' ',$row['field_name']));
				$this->setFieldType($row);
				$data[$row['Field']] = $row;
			}			
			return $data;
		}

		public function generateForm($tableName,$className) {
			$this->_tableName = $tableName;
			$this->_className = $className;
			$this->setTableSchema();
			
			$this->generateFormHtml();
			
			$this->generateFormClass();
			
			
//			Al_Utilities::a($_POST);
		}
		public function generateFormHtml() {
			$this->setHtmlFormJavascript();
			$this->setHtmlFormPageHeader();
			$this->setHtmlFormStart();
			$this->setHtmlFormBody();
			$this->setHtmlFormEnd();
						
			$this->display($this->_html,'form - Bl_Form_'.$this->_className);
		}
		
		public function setHtmlFormJavascript() {
			$this->_html .= '';
			$this->_html .= '<script type="text/javascript">'.$this->nl;
			$this->_html .= 'Ext.al.pageReady = function() {'.$this->nl;
			$this->_html .= $this->tab.'new Ext.Button({'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'renderTo:\'pageActions\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'text:\'Save\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'type:\'submit\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'minWidth:\'80\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'icon:\'<?php echo $this->_settings->path_images; ?>icons/save.png\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'style: Ext.al.btn.floatLeft,'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'handler: function(e) {'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'	this.disable();'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'	Ext.getDom(\'frmR\').submit();'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'}'.$this->nl;
			$this->_html .= $this->tab.'});'.$this->nl.$this->nl;
				
			$this->_html .= $this->tab.'new Ext.Button({'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'renderTo:\'pageActions\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'text:\'Cancel\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'minWidth:\'80\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'icon:\'<?php echo $this->_settings->path_images; ?>icons/cancel.png\','.$this->nl;
			$this->_html .= $this->tab.$this->tab.'style: Ext.al.btn.floatLeft,'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'handler: function(e) {'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'	this.disable();'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'	location=\'<?php echo $this->form->getCancelAction(); ?>\';'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'}'.$this->nl;
			$this->_html .= $this->tab.'});'.$this->nl;
			$this->_html .= '}'.$this->nl;
			$this->_html .= '</script>'.$this->nl.$this->nl;
		}

		public function setHtmlFormPageHeader() {
			$this->_html .= '<div class="page_header"> '.$this->nl;
			$this->_html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'.$this->nl;
			$this->_html .= $this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'<td width="71%"><h1 class="page_title"><?php echo $this->nav->show(); ?></h1> </td>'.$this->nl;
			$this->_html .= $this->tab.'</tr>'.$this->nl;
			$this->_html .= '</table>'.$this->nl;
			$this->_html .= '</div>'.$this->nl.$this->nl;			
		}
		
		public function setHtmlFormStart() {
			$this->_html .= '<div class="page">'.$this->nl;
			$this->_html .= '<?php $this->message->show(); ?>'.$this->nl;
			$this->_html .= '<form id="frmR" name="frmR" method="post" action="<?php echo $this->form->getAction(); ?>" class="rForm" >'.$this->nl;
			$this->_html .= $this->tab.'<div class="frmBody">'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'<table class="frm"  width="100%" border="0" cellspacing="0" cellpadding="0">'.$this->nl;			
		}
		
		public function setHtmlFormBody() {
//			Al_Utilities::a($this->_tableSchema);
		
			$first = true;
			foreach($this->_tableSchema as $field) {
				$name = $field['field_name'];				
				if(array_key_exists('use__'.$name,$_POST)) {				
					$varName = $field['var_name'];
					
					$f = Al_Utilities::tpost('f_section_name__'.$name);
					if($f!='') {
						$this->setHtmlFormBodySection($f,$first);
						if($first) {
							$first = false;
						}
					}
					
					$f = Al_Utilities::tpost('f_label__'.$name);
					if($f!='') {
						$label = $f;
					} else {
						$label = $field['field_label'];
					}
					
					
					switch(Al_Utilities::tpost('f_type__'.$name)) {
						case 'text':
							$this->setHtmlFormBodyFieldText($label,$name,'get'.$varName);
							break;
						case 'textarea':
							$this->setHtmlFormBodyFieldTextarea($label,$name,'get'.$varName);
							break;					
						case 'dropdown':
							$this->setHtmlFormBodyFieldDropDown($label,$name,'dd'.$varName);
							break;
						case 'checkbox':
							$this->setHtmlFormBodyFieldCheckBox($label,$name,'cb'.$varName);
							break;
						case 'textarea_vertical':
							$this->setHtmlFormBodyFieldTextareaVertical($label,$name,'get'.$varName);
							break;
						case 'password':
							$this->setHtmlFormBodyFieldPassword($label,$name,'get'.$varName);
							$this->setHtmlFormBodyFieldPassword($label.' Confirm',$name.'_confirm','get'.$varName.'Confirm');
							break;
					}	
				}			
			}	
		}
		
		public function setHtmlFormBodyFieldText($label,$fieldName,$fieldGetMethod) {
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmL" width="'.$this->labelWidth.'">'.$label.':</td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmFBg" width="'.$this->inputWidth.'"><input class="frmF" name="'.$fieldName.'" type="text" id="'.$fieldName.'" value="<?php echo $this->form->'.$fieldGetMethod.'(); ?>" size="30" /></td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;			
		}
		
		public function setHtmlFormBodyFieldPassword($label,$fieldName,$fieldGetMethod) {
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmL" width="'.$this->labelWidth.'">'.$label.':</td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmFBg" width="'.$this->inputWidth.'"><input class="frmF" name="'.$fieldName.'" type="password" id="'.$fieldName.'" value="<?php echo $this->form->'.$fieldGetMethod.'(); ?>" size="30" /></td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;			
		}		
		
		public function setHtmlFormBodyFieldTextarea($label,$fieldName,$fieldGetMethod) {
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmL" valign="top" width="'.$this->labelWidth.'">'.$label.':</td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmFBg" width="'.$this->inputWidth.'"><textarea class="frmF" name="'.$fieldName.'" cols="45" rows="4" id="'.$fieldName.'"><?php echo $this->form->'.$fieldGetMethod.'(); ?></textarea></td>'.$this->nl;			
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
		}		
		
		public function setHtmlFormBodyFieldTextareaVertical($label,$fieldName,$fieldGetMethod) {
            $this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
            $this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td colspan="2" valign="top" class="frmL">'.$label.':</td>'.$this->nl;
            $this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
            $this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
            $this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td colspan="2" valign="top" class="frmFBg"><textarea class="frmF" name="'.$fieldName.'" cols="45" rows="4" id="'.$fieldName.'"><?php echo $this->form->'.$fieldGetMethod.'(); ?></textarea></td>'.$this->nl;
            $this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;			
		}		
		
		public function setHtmlFormBodyFieldCheckBox($label,$fieldName,$fieldGetMethod) {
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmL" width="'.$this->labelWidth.'">'.$label.':</td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmFBg" width="'.$this->inputWidth.'"><input name="'.$fieldName.'" type="checkbox" id="'.$fieldName.'" value="1" <?php echo $this->form->'.$fieldGetMethod.'(); ?> /></td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
		}

		public function setHtmlFormBodyFieldDropDown($label,$fieldName,$fieldGetMethod) {
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmL" width="'.$this->labelWidth.'">'.$label.':</td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmFBg" width="'.$this->inputWidth.'"><select name="'.$fieldName.'" id="'.$fieldName.'"><?php echo $this->form->'.$fieldGetMethod.'(); ?></select></td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
		}		
		
		
		
		
		
		
		public function setHtmlFormBodySection($sectionName,$first=false) {
			if($first==false) {
				$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
				$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmSepTD" colspan="2">'.$this->nl;
				$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.$this->tab.'<div class="frmSep"></div>'.$this->nl;
				$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'</td>'.$this->nl;
				$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
			}
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmH2f" colspan="2">'.$sectionName.'</td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;			
		}
		
		public function setHtmlFormEnd() {
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmSepTD" colspan="2">'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.$this->tab.'<div class="frmSep"></div>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'</td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmTD" colspan="2" id="pageActions"></td>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
			$this->_html .= $this->tab.$this->tab.'</table>'.$this->nl;
			$this->_html .= $this->tab.'</div>'.$this->nl;
			$this->_html .= '</form>'.$this->nl;
			$this->_html .= '</div>';			
		}		
		
		
		
		
		public function generateFormClass() {

			$this->setFormClass();
			$this->setInitialiseMethod();			
			$this->setGetters();
			$this->setSetters();
			$this->setCheckBoxes();
			$this->setDropDowns();
						
			$this->display('<?php'.$this->nl.$this->_clsRecord->generate(),'class - Bl_Form_'.$this->_className);					
		}
		
	
		private function setFormClass() {
			$this->_clsRecord = new Zend_CodeGenerator_Php_Class();
			$this->_clsRecord->setName('Bl_Form_'.$this->_className);
			$this->_clsRecord->setExtendedClass('Al_Form');
		}		
		
		
		public function generateRecord($tableName,$className) {
			$this->_tableName = $tableName;
			$this->_className = $className;

			$this->setTableSchema();
			$this->setRecordClass();
			$this->setInitialiseMethod();
			$this->setSetters();
			$this->setGetters();
			
			$this->setDefaultMethods();
			
			$this->display('<?php'.$this->nl.$this->_clsRecord->generate());
			
//			Al_Utilities::a($this->_tableSchema);

		}
		
		public function generateSetters($tableName,$varName) {
			$this->_tableName = $tableName;
			$content = '';
			$this->setTableSchema();
			foreach($this->_tableSchema as $field) {
				if($field['type']!=Al_Record_Field::ID) {
					$content .= '$'.$varName.'->set'.$field['var_name'].'(\'\');'.$this->nl;
				}
			}
			$this->display($content);
		}
		
		public function setTableSchema() {
			$res = $this->_db->query("
				DESCRIBE ".$this->_tableName."
			");
			$this->_tableSchema = array();
			while($row = $res->fetch_row()) {
				$row['var_name'] = $this->getVarName($row['Field']);
				$row['field_name'] = $row['Field'];
				$row['field_label'] = ucwords(str_replace('_',' ',$row['field_name']));
				$this->setFieldType($row);
				$this->_tableSchema[$row['Field']] = $row;
			}			
		}
		
		public function setFieldType(&$field) {
			$type = explode('(',$field['Type']);
			$dbType = strtolower(trim($type[0]));
			if(array_key_exists('1',$type)) {
				$dbTypeParm = $type[1];
			} else {
				$dbTypeParm = '';
			}
			if($field['Key']=='PRI') {
				$type = Al_Record_Field::ID;
			} else {
				switch ($dbType) {
					case 'varchar':
	                case 'char':
	                	$type = Al_Record_Field::STRING;
						$field['max_length'] = intval($dbTypeParm);
	                    break;
					case 'text':
						$type = Al_Record_Field::TEXT;
						break;	                        
	                case 'bigint':
	                case 'int':                        
	                case 'smallint': 
	                case 'tinyint':                  
	                	$type = Al_Record_Field::INT;
	                    break;
					case 'decimal':
						$type = Al_Record_Field::FLOAT;
						$dp = explode(',',$dbTypeParm);
						$field['decimal_points'] = intval($dp[1]);
	                    break; 
	                case 'date':
	                	$type = Al_Record_Field::DATE;
	                	break;
					case 'timestamp':                	
	                case 'datetime':
	                	$type = Al_Record_Field::DATE_TIME;
	                    break;
					case 'time':
	                	$type = Al_Record_Field::TIME;
						break;    				
				}
			}
			$field['type'] = $type;
		}
		
		public function getVarName($string) {
			$string = str_replace('_', ' ', strtolower($string));
	        $string = preg_replace('/[^a-z0-9 ]/', '', $string);	
	        $string = str_replace(' ', '', ucwords($string));
	        return $string;			
		}
		
		public function display($content,$title='') {
			if($title != '') {
				echo '<div style="font-weight:bold; font-family:Verdana; font-size:12px; padding:4px; display:block; margin-top:10px;">'.$title.'</div>';
			}
            echo '<textarea name="textfield" id="textfield" style="width:100%; height:50;">';
			echo htmlentities($content,ENT_QUOTES,'UTF-8');
            echo '</textarea>';			
		}
		
		private function setRecordClass() {
			$this->_clsRecord = new Zend_CodeGenerator_Php_Class();
			$this->_clsRecord->setName('Bl_Record_'.$this->_className);
			$this->_clsRecord->setExtendedClass('Al_Record');
		}
		
		private function setInitialiseMethod() {
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('initialiseForm');
			$method->setBody($this->getInitialiseBody());
			$this->_clsRecord->setMethod($method);
			
		}
		
		private function getInitialiseBody() {
			$body = '';
			foreach($this->_tableSchema as $field) {
				$name = $field['field_name'];
				
				if(array_key_exists('use__'.$name,$_POST)) {
					switch ($_POST['type__'.$name]) {
						case 'field':
							$body.='$field = new Al_Form_Field($this);'.$this->nl;
							$body.='$field->setName(\''.$name.'\');'.$this->nl;
							break;
						case 'date':
							$body.='$field = new Al_Form_FieldDate($this);'.$this->nl;
							$body.='$field->setName(\''.$name.'\');'.$this->nl;							
							break;
					}
					
					$f = Al_Utilities::tpost('f_label__'.$name);
					if($f!='') {
						$body.='$field->setLabel(\''.$f.'\');'.$this->nl;						
					}
					
					if(array_key_exists('required__'.$name,$_POST)) {
						$body.='$field->addRule(\'required\');'.$this->nl;
					}
					
					if(array_key_exists('v_int__'.$name,$_POST)) {
						$body.='$field->addRule(\'int\');'.$this->nl;
					}
					
					if(array_key_exists('v_date__'.$name,$_POST)) {
						$body.='$field->addRule(\'date\');'.$this->nl;
					}					
					
					if(array_key_exists('v_email__'.$name,$_POST)) {
						$body.='$field->addRule(\'email\');'.$this->nl;
					}
					
					if(array_key_exists('v_dbUnique__'.$name,$_POST)) {
						$body.='$field->addRule(\'dbUnique\',array('.$this->nl;
	        			$body.=$this->tab.'\'table\' => \''.$this->_tableName.'\','.$this->nl;
	        			$body.='));'.$this->nl;
					}
					
					$min = Al_Utilities::tpost('v_length_min__'.$name);
					$max = Al_Utilities::tpost('v_length_max__'.$name);
					if($min != '' || $max != '') {
						$body.='$field->addRule(\'length\',array('.$this->nl;
						if($min != '') {
							$body.=$this->tab.'\'min\' => \''.intval($min).'\','.$this->nl;
						}
						
						if($max != '') {
							$body.=$this->tab.'\'max\' => \''.intval($max).'\','.$this->nl;
						}
						$body.='));'.$this->nl;
					}
					
					$f = Al_Utilities::tpost('v_notEqualTo_toField__'.$name);
					if($f != '') {
						$body.= '$field->addRule(\'notEqualTo\',array('.$this->nl;
        				$body.=$this->tab.'\'toField\' => \''.$f.'\','.$this->nl;
        				$body.='));'.$this->nl;        
					}
					
					$f = Al_Utilities::tpost('v_equalTo_toField__'.$name);
					if($f != '') {
						$body.= '$field->addRule(\'equalTo\',array('.$this->nl;
        				$body.=$this->tab.'\'toField\' => \''.$f.'\','.$this->nl;
        				$body.='));'.$this->nl;
					}
					
					$body.='$this->_addField($field);'.$this->nl.$this->nl;

					if(Al_Utilities::tpost('f_type__'.$name) == 'password') {
						$body.='$field = new Al_Form_Field($this);'.$this->nl;
						$body.='$field->setName(\''.$name.'_confirm\');'.$this->nl;						
						$body.='$field->addRule(\'required\');'.$this->nl;
						$body.='$field->addRule(\'equalTo\',array('.$this->nl;
				        $body.=$this->tab.'\'toField\' => \'password\','.$this->nl;
				        $body.='));'.$this->nl;
						$body.='$this->_addField($field);'.$this->nl.$this->nl;
					}
					
				}	
			}		
			return $body;
		}
		
		
		
		private function setCheckBoxes() {
			foreach($this->_tableSchema as $field) {
				if(array_key_exists('use__'.$field['field_name'],$_POST)) {
					if(Al_Utilities::tpost('f_type__'.$field['field_name']) == 'checkbox') {					
						$body = 'return $this->_cbValue($this->get'.$field['var_name'].'());';
						$method = new Zend_CodeGenerator_Php_Method();
						$method->setName('cb'.$field['var_name']);
						$method->setBody($body);
						$this->_clsRecord->setMethod($method);		
					}			
				}
			}
		}
		
		private function setDropDowns() {
			foreach($this->_tableSchema as $field) {
				if(array_key_exists('use__'.$field['field_name'],$_POST)) {
					if(Al_Utilities::tpost('f_type__'.$field['field_name']) == 'dropdown') {					
						$body = '// array based'.$this->nl;
						$body .= '$data = array('.$this->nl;
						$body .= $this->tab.'\'1\' => \'option 1\','.$this->nl;
						$body .= $this->tab.'\'2\' => \'option 2\','.$this->nl;
						$body .= $this->tab.'\'3\' => \'option 3\','.$this->nl;
						$body .= ');'.$this->nl;
						$body .= '$dd = new Al_DropDown();'.$this->nl;
						$body .= '$dd->setFirstOption(true,\'set '.$this->_className.'->dd'.$field['var_name'].'() method\',\'\');'.$this->nl;
						$body .= 'return $dd->setFromArray($data,$this->get'.$field['var_name'].'(false));'.$this->nl.$this->nl.$this->nl;						
						
						$body .= '// db query based'.$this->nl;
						$body .= '$dd = new Al_DropDown();'.$this->nl;
						$body .= '$dd->setFirstOption(true,\'set '.$this->_className.'->dd'.$field['var_name'].'() method\',\'\');'.$this->nl;
						$body .= 'return $dd->setFromDbQuery("'.$this->nl;
						$body .= $this->tab.'SELECT id, label'.$this->nl; 
						$body .= $this->tab.'FROM table'.$this->nl;
						$body .= '",$this->get'.$field['var_name'].'(false));'.$this->nl;						
						
						$method = new Zend_CodeGenerator_Php_Method();
						$method->setName('dd'.$field['var_name']);
						$method->setBody($body);
						$this->_clsRecord->setMethod($method);		
					}			
				}
			}
		}		
		
		private function setSetters() {
			foreach($this->_tableSchema as $field) {
				if(array_key_exists('use__'.$field['field_name'],$_POST)) {
					$body = '$this->_setField(\''.$field['field_name'].'\')->setValue($escaped);';			
					$method = new Zend_CodeGenerator_Php_Method();
					$method->setName('set'.$field['var_name']);
					$method->setParameters( array(
						array(
							'name' => 'value',
						),
					));					
					$method->setBody($body);
					$this->_clsRecord->setMethod($method);					
				}
			}
		}
		
		private function setGetters() {
			foreach($this->_tableSchema as $field) {
				if(array_key_exists('use__'.$field['field_name'],$_POST)) {
					$body = 'return $this->_getField(\''.$field['field_name'].'\')->getValue($escaped);';			
					$method = new Zend_CodeGenerator_Php_Method();
					$method->setName('get'.$field['var_name']);
					$method->setParameters( array(
						array(
							'name' => 'escaped',
							'defaultValue' => true,
						),
					));					
					$method->setBody($body);
					$this->_clsRecord->setMethod($method);					
				}
			}		
		}

		private function setDefaultMethods() {
			$body = '$this->_qb = new Al_QueryBuilder();'.$this->nl.
					'$this->_qb->where("r.id = \'".intval($id)."\'");'.$this->nl.
					'return $this->_load_record();';
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('load');
			$method->setParameters( array(
				array(
					'name' => 'id',
				),			
			));
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);		

//			$body = '$this->_qb = new Al_QueryBuilder();'.$this->nl.
//					'if($this->_id === null) {'.$this->nl.
//					$this->tab.'$this->_insert();'.$this->nl.
//					'} else {'.$this->nl.
//					$this->tab.'$this->_update();'.$this->nl.
//					'}';
//			
//			$method = new Zend_CodeGenerator_Php_Method();
//			$method->setName('save');
//			$method->setBody($body);
//			$this->_clsRecord->setMethod($method);		
		}
	}