<?php
	class Al_Form_Field {
		const STRING='string';
		const DATE='date';
		const TIME='time';
		const DATE_TIME='date_time'; 
		
		protected $_value='';
		private $_name;
		private $_label;
		private $_type;
		private $_rules;
		protected $_form;
		private $_validationError;
		private $_emptyValues;
		
		private $_read = true;
		private $_write = true;
		
		protected $_settings;
		
		public function __construct($form) {
			$this->_form = $form;
			$this->_rules = array();
			$this->_settings = Bl_Settings::get_instance();
			$this->setEmptyValues(array(''));
		}
		
		public function getRead() {
			return $this->_read;	
		}
		
		public function setRead($value) {
			$this->_read = $value;
		}
		
		public function getWrite() {
			return $this->_write;
		}
		
		public function setWrite($value) {
			$this->_write = $value;
		}
		
		public function initialise() {
			
		}
		
		public function setEmptyValues($value) {
			$this->_emptyValues = $value;
		}
		
		public function getEmptyValues() {
			return $this->_emptyValues;
		}
		
		public function setForm(&$value) {
			$this->_form = $value;
		}
		
		public function getForm() {
			return $this->_form;	
		}
		
		public function setName($value,$label='') {
			$this->_name = $value;
			if($label=='') {
				$this->_label = ucwords(str_replace('_',' ',$value));
			} else {
				$this->_label = $label;
			}
		}
		
		public function getLabel() {
			$language = Al_Language::get_instance();
			$label = $language->getValue('field_'.$this->getName());
			if($label == '') {
				return $this->_label;
			} else {
				return $label;				
			}
		}
		
		public function setLabel($value) {
			$this->_label = $value;
		}
		
		public function getName() {
			return $this->_name;
		}		
		
		public function setValue($value) {
			$this->_value = $value;
		}		
		
		public function getValue($escaped=false) {
			if($escaped) {
				return htmlentities($this->_value,ENT_QUOTES,'UTF-8');	
			} else {
				return $this->_value;
			}
		}
		
		public function setDbValue($value) {
			$this->_value = $value;
		}
		
		public function getDbValue() {
			return $this->_value;
		}
		
		public function setType($value) {
			$this->_type = $value;
		}
		
		public function getType() {
			return $this->_type;
		}		
		
		public function save() {
			
		}
		
		public function addRule($name,$options=array()) {
			$class = 'Al_Validation_Rule'.ucfirst($name);
			$rule = new $class($options);
			$rule->setField($this);
			$rule->setForm($this->_form);
			$this->_rules[$name] = $rule;
			$this->initialise();
		}
		
		public function getRules() {
			return $this->_rules;
		}
		
		public function getRule($rule) {
			if(array_key_exists($rule,$this->_rules)) {
				return $this->_rules[$rule];
			}
			return false;
		}
		
		public function valid() {
			$valid = true;
			$this->_validationError = '';
			if(	$this->requiredSet()==true || 
				($this->requiredSet()==false && $this->isEmpty()==false) ||
				$this->requiredConditionalActive()
				) { // && $this->isEmpty()==false) {
				foreach($this->_rules as $rule) {
					if($rule->valid()==false) {
						$this->_validationError = $rule->getErrorMessage();
						return false;
					}
				}
			} 
			return true;
		}
		
		public function isEmpty() {
			$rule = new Al_Validation_RuleRequired();
			$rule->setField($this);
			$rule->setForm($this->_form);
			$rule->setEmptyValues($this->getEmptyValues());
			if($rule->valid()) {
				return false;
			} else {
				return true;
			}
		}

		public function requiredConditionalActive() {
			if(array_key_exists('requiredConditional',$this->_rules)) {
				if($this->_rules['requiredConditional']->triggerFieldActive()) {
					return true;
				}
			}
			return false;
		}
		
		public function requiredSet() {
			if(array_key_exists('required',$this->_rules) || array_key_exists('fileRequired',$this->_rules)) {
				return true;
			} else {
				return false;
			}
		}
		
		public function getValidationError() {
			return $this->_validationError;
		}
		
		public function setFromPost() {
			if(isset($_POST[$this->getName()])) {
				$this->setValue($_POST[$this->getName()]);
			} else {
				$this->setValue('');
			}
		}
	}