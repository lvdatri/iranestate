<?php
	class Al_JsonRequest {
		
		public static $SUCCESS = 0;
		public static $JSON_PARSE_ERROR = 1;
		public static $ACTION_MISSING = 2; 
		public static $UNKNOWN_ACTION = 3;
		
		private $status = 0;
		private $message;
		private $data;
		private $action;
		
		public function getStatus() {
			return $this->status;
		}
	
		public function getMessage() {
			return $this->message;
		}
	
		public function getAction() {
			return $this->action;
		}
	
		public function setStatus($status) {
			$this->status = $status;
		}
	
		public function setMessage($message) {
			$this->message = $message;
		}
	
		public function setData($data) {
			$this->data = $data;
		}
	
		public function setAction($action) {
			$this->action = $action;
		}

		public function __construct() 
		{
			$this->status = self::$SUCCESS;
			$this->message = '';
			$this->data = array();
			$this->action = '';
		}
		
		public function getData($section='') {
			if($section == '') {
				return $this->data;
			} else {
				if(array_key_exists($section, $this->data)) {
					return $this->data[$section];
				}	
			}
			return '';
		}
		
		public function getDataItem($field,$section='data') 
		{
			if(array_key_exists($section, $this->data)) {
				if(array_key_exists($field, $this->data[$section])) {
					return $this->data[$section][$field];
				}
			}
			return '';
		}
		
		public function load($request=null) 
		{
			
			if($request == null) {
				$handle = fopen('php://input','r');
				$request = fgets($handle);
			} 
		
			$res = json_decode($request,true);
			
			if($res !== null && $res !== false) {
				if(is_array($res) && array_key_exists('action', $res) && $res['action']!='') {
					$this->action = $res['action'];
//					Al_Utilities::a($res);
					$this->data = $res;
//					if(array_key_exists('data', $res) && $res['data']!='') {
//						$this->data = $res['data'];
//					} else {
//						$this->data = array();
//					}
					return true;						
				} else {
					$this->message = Al_JsonResponse::$MSG_ACTION_MISSING;
					$this->status = Al_JsonResponse::$ACTION_MISSING;					
				}
			} else {
				$this->message = Al_JsonResponse::$MSG_JSON_PARSE_ERROR;
				$this->status = Al_JsonResponse::$JSON_PARSE_ERROR;
			}			
			return false;
		}
		
		public function getErrorResponse() 
		{
			$response = new Al_JsonResponse();
			$response->setStatus($this->status);
			$response->setMessage($this->message);
			return $response;
		}

	}