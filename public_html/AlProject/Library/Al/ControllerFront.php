<?php
/**

about - index/add - action used - 1 params 
index/about - controler, action - 2 params 
user/add - controller, action - 2 params

admin - module - 1 params
admin/about - 1 params
admin/add - module, action - 2 params
admin/index/add - module, controller, action 3 params
admin/user/add - module controller, action
 
 
 */

	class Al_ControllerFront {

		const URI_DELIMITER = '/';
		
		public $settings;
		private $modules;
		public $menu;		
		
		private $_request_uri;
		private $_path_info;
		
		private $default_module;
		
		public $_module;
		public $_controller;
		public $_action;
		
		private $_beforePathProcessClass = null;
        private $_beforePathProcessClassMethod = null;
		
		public $_requested_module;
		public $_requested_controller;
		public $_requested_action;
				
		private $_params;
	    
	    public $controller;
	    
		public $beforeActionCallClass = null;
		public $beforeActionCallClassMethod = null;	    
	    
		
		public function __construct() {
			$this->settings = Bl_Settings::get_instance();
			$this->modules = array();
			$this->set_default_module();
		}
		
		public function set_default_module() {
			$this->default_module = array(
				'name' => 'default',
				'path' => $this->settings->path_controllers,
			);
		}
		
		public function add_module($name,$path) {
			$this->modules[$name] = array(
				'name' => $name,
				'path' => $path,
			);
		}
		
		public function show() {
			echo
				'request uri - ' . $this->_request_uri . '<BR>'.
				'path info - ' . $this->_path_info . '<BR>'. 
				'module - ' . $this->_module . '<BR>'.
				'controller - ' . $this->_controller . '<BR>'.
				'action - ' . $this->_action . '<BR>'.
				'request_module - ' . $this->_requested_module . '<BR>'.
				'request_controller - ' . $this->_requested_controller . '<BR>'.
				'request_action - ' . $this->_requested_action . '<BR>';
		}
		
		public function initialise() {
			$this->set_request();
			$this->set_route();			
		}
		
		public function get_module_name() {
			return $this->_module;
		}
		
		public function dispatch() {
			$this->_dispatch();
//			$this->show();
		}
		
		private function _dispatch() {
			$class_name = $this->get_controller_class();
			$class_path = $this->get_controller_class_path($class_name);

			if(!is_readable($class_path)) {

				// try to process as default controller
				$this->_action = $this->_controller;
				$this->_controller = 'index';
				$class_name = $this->get_controller_class();
				$class_path = $this->get_controller_class_path($class_name);				
				
				if(!is_readable($class_path)) {
					// error class
					$this->_controller = 'error';
					$this->_action = 'index';
					$class_name = $this->get_controller_class();
					$class_path = $this->get_controller_class_path($class_name);
				}
			}

			$controller = $this->load_controller($class_name,$class_path);
		
			if(!$this->call_controller_action($controller)) {
				/**
				 * controller action not found
				 */
				$this->_controller = 'error';
				$this->_action = 'index';
				$class_name = $this->get_controller_class();
				$class_path = $this->get_controller_class_path($class_name);
				$controller = $this->load_controller($class_name,$class_path);
				$this->call_controller_action($controller);				
			}					
		}
		
		private function load_controller($class_name,$class_path) {
			if(!class_exists($class_name)) {
				$class_name = $this->load_class($class_name,$class_path);
				
				$this->controller = new $class_name();
				if (!$this->controller instanceof Al_ControllerAction) {
					echo "Controller '".$class_name."' is not an instance of Zend_Controller_Action";
					exit;
				}
			}
			return $this->controller;
		}
		
		private function call_controller_action(&$controller) {
			$status = false;

			$action = $this->get_action_name();
			if(method_exists($controller,$action)) {
				$status = true;
			} else if(method_exists($controller,'errorAction')) {
				$status = true;
				$this->_action = 'error';		
				$action = 'errorAction';
			}
			
			if($status==true) {
				
				$this->set_params();		
				$controller->initialise($this);
				$controller->beforeAction();
				if($this->beforeActionCallClass !== null) {
					$this->beforeActionCallClass->{$this->beforeActionCallClassMethod}($controller);
				}				
				$controller->$action();
				
				$controller->afterAction();				
			}
			
			return $status;
		}		
		
		
//		private function call_controller_action1(&$controller) {
//			$status = false;
//
//			$action = $this->get_action_name();
//			if(method_exists($controller,$action)) {
//				$status = true;
//				$this->set_params();		
//				$controller->setModuleName($this->_module);				
//				$controller->setControllerName($this->_controller);
//				$controller->setActionName($this->_action);
//				$controller->set_settings($this->settings);
//				$controller->_initialise();
//				$controller->$action();
//				$controller->afterAction();
//			} else if(method_exists($controller,'errorAction')) {
//				$status = true;
//				$this->_action = 'error';				
//				$this->set_params();
//				$controller->setModuleName($this->_module);				
//				$controller->setControllerName($this->_controller);	
//				$controller->setActionName('error');			
//				$controller->set_settings($this->settings);	
//				$controller->_initialise();			
//				$controller->errorAction();
//				$controller->afterAction();
//			}
//			return $status;
//		}
		
		private function set_params() {
        	$this->_params = array();
        	
        	if($this->_action!='error') { //  && $this->_controller!='error'
        		
        		$path = trim($this->_path_info, self::URI_DELIMITER);

        		if ($path != '') {
		            $path = explode(self::URI_DELIMITER, $path);
		            
		            if($path[0] == $this->settings->languagePrefix) {
		            	array_shift($path);
		            }
		            
		            if(count($path)>0) {
						if ($path[0] == $this->_module) {
							array_shift($path);
						}
						
						if($this->_controller=='error') {
							if (count($path) && $path[0] == $this->_requested_controller) {
								array_shift($path);
							}            
						} else {
							if (count($path) && $path[0] == $this->_controller) {
								array_shift($path);
							}	    
						}        
						
						if($this->_action=='error') {
							if (count($path) && $path[0] == $this->_requested_action) {
								array_shift($path);
							}
						} else {
							if (count($path) && $path[0] == $this->_action) {
								array_shift($path);
							}
						}
	
			            while(count($path)>0) {
			            	$param = $path[0];
			            	array_shift($path);
			            	if(array_key_exists(0,$path)) {
			            		$value = $path[0];
			            		array_shift($path);
			            	} else {
			            		$value = '';
			            	}
			            	$this->_params[$param] = $value;
			            	$_GET = $this->_params;
			            }
		            }
		        }
        	}
//        	Al_Utilities::a($_GET);
//			$this->show();
//	        Al_Utilities::a($this->_params);
//	        exit;			
		}
		
		private function get_controller_class_path($name) {
			$module = $this->default_module;
			
			if($this->_module!='' && isset($this->modules[$this->_module])) {
				$module = $this->modules[$this->_module];
			} 
			return $module['path'].$name.'.php';
		}
		
		public function get_action_name() {
			$action = $this->_action;
			if($action=='') {
				$this->_action = 'index';
				$action = $this->_action;
			} else {
				$action = str_replace('-', ' ', strtolower($action));
		        $action = preg_replace('/[^a-z0-9 ]/', '', $action);	
		        $action = str_replace(' ', '', ucwords($action));

				if($action != '') {
					$action[0] = strtolower($action[0]);	        	
	        	}		        
			}
            return $action.'Action';
		}		
		
		private function get_controller_class() {
			if($this->_controller=='') {
				$name = 'index'; 
			} else {
				$name = trim(strtolower($this->_controller));
				$name = str_replace('-', ' ', strtolower($name));
		        $name = preg_replace('/[^a-z0-9 ]/', '', $name);	
		        $name = str_replace(' ', '', ucwords($name));				
			}
            return ucfirst($name).'Controller';
		}
		
		public function setBeforePathProcess($object,$method) {
        	$this->_beforePathProcessClass = $object;
        	$this->_beforePathProcessClassMethod = $method;
        	if(!is_callable(array($this->_beforePathProcessClass,$this->_beforePathProcessClassMethod))) {
			    echo 'unknown class callback - '.$this->_beforePathProcessClassMethod;
			    exit;
			}     	
        }		
		
		private function set_route() {
        	$values = array();
        	$params = array();
        	$path   = trim($this->_path_info, self::URI_DELIMITER);
        	
//        	echo $path;
//        	exit;
        	
			$this->_requested_module = '';
			$this->_requested_controller = '';
			$this->_requested_action = '';        
			
	        if ($path != '') {
	            $path = explode(self::URI_DELIMITER, $path);
	            if($this->_beforePathProcessClass != null) {
					$this->_beforePathProcessClass->{$this->_beforePathProcessClassMethod}($path);	            
	            }
	            
	            if(count($path) > 0) { 	            
					if ($this->is_valid_module($path[0])) {
						$this->_module = array_shift($path);
						$this->_requested_module = $this->_module;
		            } else {
		            	$this->_module = 'default';
		            }
	
		            if (count($path) && !empty($path[0])) {
		                $this->_controller = array_shift($path);
		                $this->_requested_controller = $this->_controller;
		            } else {
		            	$this->_controller = 'index';
		            }
		
		            if (count($path) && !empty($path[0])) {
		                $this->_action = array_shift($path);
		            }
					$this->_requested_action = $this->_action;
	            } else {
	            	$this->_action = 'index';
		        	$this->_module = 'default';
		        	$this->_controller = 'index';
	            }

	        } else {
	        	if($this->_beforePathProcessClass != null) {
					$this->_beforePathProcessClass->{$this->_beforePathProcessClassMethod}($path);	            
	            }
	        	$this->_action = 'index';
	        	$this->_module = 'default';
	        	$this->_controller = 'index';
	        }
		}

		private function is_valid_module($module_name) {
			foreach($this->modules as $module) {
				if($module['name'] == $module_name) {
					return true;
				}
			}
			
			return false;
		}
		
		private function set_request() {
		    if (isset($_SERVER['REQUEST_URI'])) {
                $requestUri = urldecode($_SERVER['REQUEST_URI']);
				$this->_path_info = parse_url($requestUri, PHP_URL_PATH);

				if($this->settings->controllerBaseDir != '') {
					$this->_path_info = str_ireplace('/'.$this->settings->controllerBaseDir,'',$this->_path_info);	
				}
				
                $query_string = parse_url($requestUri, PHP_URL_QUERY);
                $this->_request_uri = $this->_path_info . ((empty($query_string)) ? '' : '?' . $query_string);
            } else {
				$this->_path_info = '';
				$this->_request_uri = '';            	
            }			
		}
		
		public function getRequestUri() {
			return $this->_request_uri;
		}
		
		public function getPathInfo() {
			return $this->_path_info;
		}
		
	    public function load_class($class_name,$class_path) {
	        if (!include_once $class_path) {
	            echo 'Cannot load controller class "' . $class_name . '" from file "' . $class_path . "'";
	            exit;
	        }

	        if (!class_exists($class_name, false)) {
	            echo 'Invalid controller class ("' . $class_name . '")';
	            exit;
	        }
	        return $class_name;
	    }
	    
		public function beforeActionCall($object,$method) 
		{
        	$this->beforeActionCallClass = $object;
        	$this->beforeActionCallClassMethod = $method;
        	if(!is_callable(array($this->beforeActionCallClass,$this->beforeActionCallClassMethod))) {
			    echo 'unknown class callback - '.$this->beforeActionCallClassMethod;
			    exit;
			}
		}
		
		
	
	}
