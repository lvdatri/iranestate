<?php
	/**
	 * 
	 * @version 0.01
	 * 
	 */
	class Al_Utilities {
	    /**
	    * displays content of array in a easy to read format
	    *
	    * it does not work on old versions of php 4.1 since ob_start() dosen't work
	    *
	    * @param    string  $data_array  array contnet to be displayed
	    * @return   string  returns a nicly formated array
	    * @access	public  
	    */        
	    public static function a($array_data,$echo=true) {
	        if($echo) {
	            echo $str = '<div style="text-align:left;background-color:#FFFFFF; clear:both;"><pre>';
	            print_r($array_data);
	            echo "</pre></div>";
	        }
	        else {
	            ob_start();
	            print_r($array_data);
	            $d = "<pre>" . ob_get_contents() . "</pre>";
	            ob_end_clean();
	            return $d;
	        }
	    }
	    
	    /**
	    * returns value from post or default value
	    *
	    * @param    string  $post_name  value from $_POST to be returned
	    * @param    string  $default_value  if requested post value is not set $default_value is returned
	    * @return   string  post vlaue or $defaul_value
	    * @access	public  
	    */   
	    public static function post($post_name, $default_value = "") {
	        if (isset($_POST[$post_name]))
	            return $_POST[$post_name];
	        else
	            return $default_value;
	    }
	    
		public static function ePost($post_name, $default_value = "") {
			return htmlentities(self::tpost($post_name,$default_value),ENT_QUOTES,'UTF-8');
	    }	

	    public static function persianDateFormat($dateString,$format="Y/m/d") 
	    {
	    	$timestamp = strtotime($dateString);
//			echo date($format,$timestamp);
			return Al_JalaliDate::date($format,$timestamp);
//	    	$d = new Al_JalaliDate($timestamp);
//    		return $d->date($format);
	    }
	    
	    public static function html($value) {
	    	return htmlentities($value,ENT_QUOTES,'UTF-8');
	    }
	    
	    /**
	     * trims post value before returning
	     *
	    * @param    string  $post_name  value from $_POST to be returned
	    * @param    string  $default_value  if requested post value is not set $default_value is returned
	    * @return   string  post vlaue or $defaul_value
	     */
	    public static function tpost($post_name, $default_value = "") {
	    	return trim(self::post($post_name,$default_value));
	    }	    
	
	    /**
	    * returns value from get or default value
	    *
	    * @param    string  $get_name  value from $_GET to be returned
	    * @param    string  $default_value  if requested get value is not set $default_value is returned
	    * @return   string  get vlaue or $defaul_value
	    */   
	    public static function get($get_name, $default_value = "") {
	        if (isset($_GET[$get_name])) 
	            return $_GET[$get_name];
	        else
	            return $default_value;
	    }
	    
	    /**
	     * trims get value 
	     *
	    * @param    string  $get_name  value from $_GET to be returned
	    * @param    string  $default_value  if requested get value is not set $default_value is returned
	    * @return   string  get vlaue or $defaul_value
	     */
	    public static function tget($get_name, $default_value = "") {
	    	return trim(self::get($get_name,$default_value));
	    }
	    
		public static function eGet($get_name, $default_value = "") {
	    	return htmlentities(trim(self::get($get_name,$default_value)),ENT_QUOTES,'UTF-8');
	    }	    
	    
	    /**
	    * redirects page to a specific URL
	    * after redirection script will be exited
	    * 
	    * @param    string  $url  link of desired page location
	    * @access	public  
	    */   
	    public static function redirect($url,$permenant=false) {
	    	if($permenant) {
	    		header('HTTP/1.1 301 Moved Permanently');
	    	}
	        header('Location: ' . $url);
	        self::_exit();
	    }
	    
	    public static function _exit() {
	        session_write_close();
	        exit;
	    } 
	    
		/**
		 * return true or false if the passed id is integer
		 */
		public static function valid_id($id) {
			if(ereg('(^[0-9]+$)',$id)) {
	            return true;		
			} 
			return false;
		}
		
		/**
		 * partial string
		 * returns specified number of characters from string
		 * 
		 * @param string $str
		 * @param int $length
		 * @return string
		 */
		public static function pstr($str,$length) {
			return substr($str,0,$length);
		}
		
	    public static function getPathFromNo($id,$path='') {
            $main = 10000;
            $sub = 100;
            
            $m = floor($id/$main);
            
            if($id%$main==0) {
                $s = 0;
            }
            else {
                $s = floor(($id - ($main*$m))/$sub); 
            }
            
            if($path=='') {
	            return $m.'/'.$s.'/';
            } else {
				return $path.$m.'/'.$s.'/';
            }       
	    }	    

	    public static function datetime_now_mysql() {
	        return date("Y-m-d H:i:s");  
	    }
	    
	    public static function date_now_mysql() {
	        return date("Y-m-d");
	    }
	    
		/**
		 * Returns return_path and by default it will clear its value.
		 *
		 * @param bool $clear
		 */
		public static function get_return_path($clear=true,$default='') {
			$path = $default;
			$settings = Bl_Settings::get_instance();
			
			if(@isset($_SESSION[$settings->session][$settings->session_history]["return_path"])) {
				$path = $_SESSION[$settings->session][$settings->session_history]["return_path"];
				if($clear) {
					$_SESSION[$settings->session][$settings->session_history]["return_path"] = '';
				}
			}
			return $path;
		}	    

		/**
		 * stores the return path for calling page.
		 * 
		 * E.G. When protected page redirects user to login screen, after login user can be taken back
		 * to requested page. Protected page will set_return_path('to its self') and then redirect user to login page
		 *
		 * @param unknown_type $path
		 */
		public static function set_return_path($path) {
			$settings = Bl_Settings::get_instance();
			$_SESSION[$settings->session][$settings->session_history]["return_path"] = $path;
		}		
		
		/**
		 * returns current url path
		 */
		public static function get_current_url_path() {
			$settings = Bl_Settings::get_instance();
			
			/* if($settings->frontend == true) {
				return $settings->path_web_ssl.$settings->languagePrefix.$_SERVER['REQUEST_URI'];
			} else { */
				return $settings->path_web_ssl.ltrim($_SERVER['REQUEST_URI'],'/');
//			}
		}
		
		public static function p($value,$pre=false) {
	            echo $str = '<div style="text-align:left;background-color:#FFFFFF; clear:both; display:block; padding:5px; margin-bottom:5px;"><pre>';
	            print($value);
	            echo "</pre></div>";
		}
		
		public static function cb($value,$selectedValue='1') {
			if($value == $selectedValue) {
				return '  checked="checked" ';
			}
			return '';
		}
		
		public static function formatDbDate($value,$format='') 
		{
			$settings = Bl_Settings::get_instance();
			if($format == '') {
				$format = $settings->date_format;
			}
			if (!Zend_Date::isDate($value, $settings->date_format_db)) {
				$value = '';
			} else {
				$date = new Zend_Date();
				$date->set($value,$settings->date_format_db);
				$value = $date->toString($format);
			}    
			return $value;	
		}
		
	    public static function getRandomString ($length, $useupper=true, $usespecial=true, $usenumbers=true) 
	    {
			$charset = "abcdefghijklmnopqrstuvwxyz";
			if ($useupper) { 
			    $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";   
			}
			if ($usenumbers) {
			    $charset .= "0123456789";
			}
			if ($usespecial) { 
			    //$charset .= "~@#$%^*()_+-={}|]["; // Note: using all special characters this reads: "~!@#$%^&*()_+`-={}|\\]?[\":;'><,./";
//                $charset .= "~@#$%^*_+-="; // Note: using all special characters this reads: "~!@#$%^&*()_+`-={}|\\]?[\":;'><,./";			    
			}
		
			$strLen = strlen($charset)-1;
			$key = '';
			for ($i=0; $i<$length; $i++){ 
			    $key .= $charset[(mt_rand(0,$strLen))];
			}
			return $key;
	    }

	    public static function initialiseMail() {
	    	$settings = Bl_Settings::get_instance();

//	    	if($settings->environment == Bl_Settings::ENV_PRODUCTION) {
//				$tr = new Zend_Mail_Transport_Smtp('localhost');
//				Zend_Mail::setDefaultTransport($tr);	    		
//	    	} else {

				$config = array(
					'auth' => 'login',
			        'username' => $settings->gmail_user,
	   	            'password' => $settings->gmail_pass,
	   	            'ssl' => 'tls',
	   	            'port' => 587
				);
				
						
			 
			    $mailTransport = new Zend_Mail_Transport_Smtp('smtp.gmail.com',$config);
			    Zend_Mail::setDefaultTransport($mailTransport);
//	    	}
	    }
	    
	    
		/** 
		 *  This function implements all the strn*pos functions, which return the $nth occurrence of $needle 
		 *  in $haystack, or false if it doesn't exist / when illegal parameters have been supplied. 
		 * 
		 *  @param  string  $haystack       the string to search in. 
		 *  @param  MIXED   $needle         the string or the ASCII value of the character to search for. 
		 *  @param  integer $nth            the number of the occurrence to look for. 
		 *  @param  integer $offset         the position in $haystack to start looking for $needle. 
		 *  @param  bool    $insensitive    should the function be case insensitive? 
		 *  @param  bool    $reverse        should the function work its way backwards in the haystack? 
		 *  @return MIXED   integer         either the position of the $nth occurrence of $needle in $haystack, 
		 *               or boolean         false if it can't be found. 
		 */ 
		public static function strnripos_generic( $haystack, $needle, $nth, $offset, $insensitive, $reverse ) 
		{ 
		    //  If needle is not a string, it is converted to an integer and applied as the ordinal value of a character. 
		    if( ! is_string( $needle ) ) { 
		        $needle = chr( (int) $needle ); 
		    } 
		
		    //  Are the supplied values valid / reasonable? 
		    $len = strlen( $needle ); 
		    if( 1 > $nth || 0 === $len ) { 
		        return false; 
		    } 
		
		    if( $insensitive ) { 
		        $haystack = strtolower( $haystack ); 
		        $needle   = strtolower( $needle   ); 
		    } 
		
		    if( $reverse ) { 
		        $haystack = strrev( $haystack ); 
		        $needle   = strrev( $needle   ); 
		    } 
		
		    //  $offset is incremented in the call to strpos, so make sure that the first 
		    //  call starts at the right position by initially decreasing $offset by $len. 
		    $offset -= $len; 
		    do 
		    { 
		        $offset = strpos( $haystack, $needle, $offset + $len ); 
		    } while( --$nth  && false !== $offset ); 
		
		    return false === $offset || ! $reverse ? $offset : strlen( $haystack ) - $offset; 
		} 
		
		/** 
		 *  @see    strnripos_generic 
		 */ 
		public static function strnpos( $haystack, $needle, $nth, $offset = 0 ) 
		{ 
		    return self::strnripos_generic( $haystack, $needle, $nth, $offset, false, false ); 
		} 
		
		/** 
		 *  @see    strnripos_generic 
		 */ 
		public static function strnipos( $haystack, $needle, $nth, $offset = 0 ) 
		{ 
		    return self::strnripos_generic( $haystack, $needle, $nth, $offset, true, false ); 
		} 
		
		/** 
		 *  @see    strnripos_generic 
		 */ 
		public static function strnrpos( $haystack, $needle, $nth, $offset = 0 ) 
		{ 
		    return self::strnripos_generic( $haystack, $needle, $nth, $offset, false, true ); 
		} 
		
		/** 
		 *  @see    strnripos_generic 
		 */ 
		public static function strnripos( $haystack, $needle, $nth, $offset = 0 ) 
		{ 
		    return self::strnripos_generic( $haystack, $needle, $nth, $offset, true, true ); 
		} 	   

		public static function substr($haystack,$needle,$nth) 
		{
    		$pos = self::strnpos($haystack, ',', $nth);
    		if($pos !== false) {
	    		$haystack = substr($haystack, 0,$pos);
    		}
    		return $haystack;
		}
	    
	    
				
	}