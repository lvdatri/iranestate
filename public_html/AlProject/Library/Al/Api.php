<?php

class Al_Api {

    public $serviceHandler;
    public $accessLevels = '1';
    public $requestData = null;

    public function __construct() {
        
    }

    public function beforeActionCall() {
        
    }

    public function afterActionCall() {
        
    }
    
    public function initialise() {
    	
    }

    public function process($request = null) {

    	$this->initialise();
        $this->serviceHandler = new Al_ServiceHandler();
        if ($this->serviceHandler->load($this->accessLevels, $request)) {
            $this->requestData = $this->serviceHandler->request->getData('data');
            $action = $this->getActionName($this->serviceHandler->request->getAction());
            if (method_exists($this, $action)) {
                $this->beforeActionCall();
                $this->$action();
                $this->afterActionCall();
            } else {
                $this->serviceHandler->response->setStatus(Al_JsonResponse::$UNKNOWN_ACTION);
                $this->serviceHandler->response->setMessage(Al_JsonResponse::$MSG_UNKNOWN_ACTION);
            }
        }
        $this->serviceHandler->showResponse();
    }

    private function getActionName($action) {
        $action = str_replace('-', ' ', strtolower($action));
        $action = preg_replace('/[^a-z0-9 ]/', '', $action);
        $action = str_replace(' ', '', ucwords($action));
        if ($action != '') {
            $action[0] = strtolower($action[0]);
        }
        return $action . 'Action';
    }

}