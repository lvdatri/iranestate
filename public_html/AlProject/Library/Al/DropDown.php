<?php 
	class Al_DropDown {
		private $firstOption;
		private $firstOptionValue;
		private $firstOptionLabel;
		
		private $optionSelected;
		
		private $selectedValue;
		
		private $escapeLabel;
		
		private $options;
		
		private $nl = "\n";
		
		private $callbackClass=null;
		private $callbackClassMethod=null;
		
		private $_settings;
		
		public function __construct() {
			$this->_settings = Bl_Settings::get_instance();

			$this->clear();
		}
		
		public function setCallbackClass($object,$method) {
        	$this->callbackClass = $object;
        	$this->callbackClassMethod= $method;
        	if(!is_callable(array($this->callbackClass,$this->callbackClassMethod))) {
			    echo 'unknown class callback - '.$this->callbackClassMethod;
			    exit;
			}     	
        }

		public function setFirstOption($enabled,$label='',$value='') {
			$this->firstOption = $enabled;
			$this->firstOptionLabel = $label;
			$this->firstOptionValue = $value;
		}
		
		public function setFromArray($data,$selectedValue='',$label='',$value='') {
			$this->setSelectedValue($selectedValue);
			if($this->callbackClass===null) {
				if($label == '') {
					foreach($data as $value=>$label) {
						$this->addOption($label,$value);
					}
				} else {
					foreach($data as $item) {
						$this->addOption($item[$label],$item[$value]);
					}
				}
			} else {
				foreach($data as $item) {
					$this->callbackClass->{$this->callbackClassMethod}($item);
					$this->addOption($item['label'],$item['value']);
				}
			}
			$this->_setFirstOption();			
			return $this->options;
		}
		
		private function _setFirstOption() {
			if($this->options != '') {
				if($this->firstOption) {
					$this->addOption($this->firstOptionLabel,$this->firstOptionValue,false);
				}
			}
		}
		
		public function setFromDbQuery($query,$selectedValue='') {
			$db = Al_Db::get_instance();
			$this->setSelectedValue($selectedValue);
			
			$res = $db->query($query);
			if($res->num_rows()>0) {
				while($row = $res->fetch_row(Al_Db::ORDERED)) {
					$this->addOption($row[1],$row[0]);
				}
			}
			
			$this->_setFirstOption();
			return $this->options;
		}
		
		public function setSelectedValue($value) {
			$this->selectedValue = (string) $value;
		}
		
		public function clear() {
			$this->firstOption = false;
			$this->firstOptionLabel= '';
			$this->firstOptionValue = '';
			$this->escapeLabel = true;
			$this->options = '';
			$this->optionSelected = false;
		}
		
		private function addOption($label,$value,$append=true) {
			$value = (string) $value;
			$selected = '';
			if($value == $this->selectedValue && $this->optionSelected==false && $value != '') {
				$this->optionSelected = true;
				$selected = 'selected="selected" ';
			}
			
			if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
				if($this->escapeLabel) {
					$label = htmlentities($label,ENT_QUOTES,'UTF-8');
				}
			}
			
			if($append) {
				// appending
				$this->options .= '<option '.$selected.'value="'.htmlentities($value,ENT_QUOTES,'UTF-8').'">'.$label.'</option>'. $this->nl;
			} else {
				// prepending
				$this->options = '<option '.$selected.'value="'.htmlentities($value,ENT_QUOTES,'UTF-8').'">'.$label.'</option>'. $this->nl.$this->options;
			}
		}
	}