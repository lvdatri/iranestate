<?php
    class Al_UrlParams {
        public $params;
        
        const BROWSE="browse";
        const EXTERNAL="external";
        const SEARCH="search";
        const SORTER="sorter";
        const PAGER="pager";
        
        public function __construct() {
            $this->params = array();
        }
        
        /**
         * set params group for later retrieval
         * @param string $group
         * @param string $params
         */
        public function set($group,$params) {
            $this->params[$group] = $params;
        }
        
        public function add($group,$param,$value) {
            if(!isset($this->params[$group])) {
                $this->params[$group] = '';
            }
            $param_str = $param.'='.urlencode($value);
            if($this->params[$group] == '') {
                $this->params[$group] = $param_str;
            } else {
                $this->params[$group] .= '&'.$param_str;
            }
        }
        
        /**
         * adds parameter from get value
         * @param $group
         * @param $param
         */
        public function add_from_get($group,$param) {
            if(isset($_GET[$param])) {
                $this->add($group,$param,Al_Utilities::tget($param));
            }
        }        
        
        /**
         * set params from get fields
         *  - 
         * @param $group
         * @param $fields
         */
        public function set_get($group,$fields) {
            $params='';
            foreach($fields as $field) {
            	$value = Al_Utilities::tget($field);
            	if($value != '') {
                	$params .= $field.'/'.urlencode($value).'/';
            	}
            }
            $params = rtrim($params,'/');
            $this->set($group,$params);
        }
        
        /**
         * get params for a specific group
         *  - multiple grups are passed in array
         *    array('group1','group2')
         * @param array $groups
         */
        public function get($groups,$append=false) {
            $params = '';
            foreach($groups as $group) {
                if(isset($this->params[$group])) {
                    if($this->params[$group] != '') {
                        $params .= $this->params[$group].'/';
                    }
                }
            }
            if($params != '') { 
	            if($append==true) {
	                return '/'.rtrim($params,'/');
	            } else {
	                return rtrim($params,'/');
	            }
            }
        }
        
        /**
         * returns paramaters from all groups
         */
        public function get_all() {
            $params = '';
            foreach($this->params as $group=>$value) {
                if($value != '') {
                    $params .= $value.'/';
                }
            }
            return rtrim($params);
        }
        
        public function to_fields($groups) {
            $url_query = $this->get($groups);
		    $fields_str = '';
            if($url_query != '') {
                $path = explode( '/', $url_query );
                while(count($path)>0) {
	            	$param = $path[0];
	            	array_shift($path);
	            	if(array_key_exists(0,$path)) {
	            		$value = urldecode($path[0]);
	            		array_shift($path);
	            	} else {
	            		$value = '';
	            	}
					$fields_str .= '<input name="'.htmlentities($param,ENT_QUOTES,'UTF-8').'" type="hidden" id="'.htmlentities($param).'" value="'.htmlentities($value).'" />';        	
                }
		    }
		    return $fields_str;            
        }
    }