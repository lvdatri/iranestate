<?php
	class Al_Validation_RuleDbMatch extends Al_Validation_Rule {
		private $dbField=null;
		private $table=null;
		private $pkField='id';
		private $recordId='';
		private $encrypted=false;
		
		public function setEncrypted($value) {
			$this->encrypted = $value;
		}
		
		public function getEncrypted() {
			return $this->encrypted;
		}
		
		public function setDbField($value) {
			$this->dbField = $value;
		}
		public function setTable($value) {
			$this->table = $value;
		}
		public function setPkField($value) {
			$this->pkField = $value;
		}
		public function setRecordId($value) {
			$this->recordId = $value;
		}						

		public function getDbField() {
			return $this->dbField;
		}

		public function getTable() {
			return $this->table;
		}

		public function getPkField() {
			return $this->pkField;
		}		
		
		public function getRecordId() {
			return $this->recordId;
		}		
		
		public function validate() {
			if($this->form->getMode()==Al_Form::UPDATE) {
				if($this->table === null) {
					$this->setErrorMessage('ruelDbUnique: table not set');
					return false;
				}
				
				if($this->dbField === null) {
					$this->dbField = $this->field->getName();
				}
				
				$value = $this->field->getValue();
				
				if($this->encrypted) {
			    	$crypt = new Al_Crypt();
			    	$value = $crypt->encrypt($value,$this->_settings->key,true);
				}
				
				$qb = new Al_QueryBuilder();
				$qb->select($this->dbField);
				$qb->from($this->table);
				$qb->where($this->dbField . " = '" . Al_Db::escape($value) . "'");
				$qb->where($this->pkField." = '" . Al_Db::escape($this->recordId) . "'");
				
				$db = Al_Db::get_instance();
				$res = $db->query($qb->get_query());
				if($res->num_rows() == 0) {
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_DB_MATCH));
					return false;
	            }
			}
			
			return true;
		}
	}