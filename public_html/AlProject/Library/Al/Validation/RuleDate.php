<?php
	class Al_Validation_RuleDate extends Al_Validation_Rule {
		public $format; 
		public function setFormat($value) {
			$this->format = $value;
		}
		public function getFormat() {
			return $this->format;
		}
		
		public function validate() {
			if($this->format === null) {
				$settings = Bl_Settings::get_instance();
				$this->format = $settings->date_format;
			}
			
			$value = $this->field->getValue();
			if (!Zend_Date::isDate($value, $this->format)) {
                if ($this->checkFormat($value) === false) {
                	$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_DATE_FORMAT),$value,$this->format);                	
                } else {
                	$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_DATE_INVALID),$value);                	
                }
                return false;
            }			
			return true;
		}
		
	   	private function checkFormat($value) {
	        try {
	            require_once 'Zend/Locale/Format.php';
	            $parsed = Zend_Locale_Format::getDate($value, array(
	                                                  'date_format' => $this->format, 'format_type' => 'iso',
	                                                  'fix_date' => false));
	            if (isset($parsed['year']) and ((strpos(strtoupper($this->format), 'YY') !== false) and
	                (strpos(strtoupper($this->format), 'YYYY') === false))) {
	                $parsed['year'] = Zend_Date::getFullYear($parsed['year']);
	            }
	        } catch (Exception $e) {
	            // Date can not be parsed
	            return false;
	        }
	
	        if (((strpos($this->format, 'Y') !== false) or (strpos($this->format, 'y') !== false)) and
	            (!isset($parsed['year']))) {
	            // Year expected but not found
	            return false;
	        }
	
	        if ((strpos($this->format, 'M') !== false) and (!isset($parsed['month']))) {
	            // Month expected but not found
	            return false;
	        }
	
	        if ((strpos($this->format, 'd') !== false) and (!isset($parsed['day']))) {
	            // Day expected but not found
	            return false;
	        }
	
	        if (((strpos($this->format, 'H') !== false) or (strpos($this->format, 'h') !== false)) and
	            (!isset($parsed['hour']))) {
	            // Hour expected but not found
	            return false;
	        }
	
	        if ((strpos($this->format, 'm') !== false) and (!isset($parsed['minute']))) {
	            // Minute expected but not found
	            return false;
	        }
	
	        if ((strpos($this->format, 's') !== false) and (!isset($parsed['second']))) {
	            // Second expected  but not found
	            return false;
	        }
	
	        // Date fits the format
	        return true;
		}		
	}