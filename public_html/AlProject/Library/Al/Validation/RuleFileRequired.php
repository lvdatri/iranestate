<?php
	class Al_Validation_RuleFileRequired extends Al_Validation_Rule {
		
		public function validate() {
			$name = $this->field->getName();
			if(array_key_exists($name,$_FILES)) {
				$file = $_FILES[$name];
				if(!empty($file['name']) && $file['error']==0) {
					return true;
				}
			}
			
			if($this->fileAlreadyUploaded()) {
				return true;
			} else {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_FILE_REQUIRED));
				return false;
			}
		}
		
		public function fileAlreadyUploaded() {
			$fieldName = $this->field->getName();
			$formName = get_class($this->form);
			if( isset($_SESSION[$this->_settings->session]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName]) && 
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName]['file_uploaded_path'])
			){
				$data = $_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName];				
				if($data['file_uploaded_path'] != '' || $data['file_saved_name'] != '') {
					return true;
				}
			}
			return false;
		}		
	}