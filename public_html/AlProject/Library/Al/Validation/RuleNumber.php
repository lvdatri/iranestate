<?php
	class Al_Validation_RuleNumber extends Al_Validation_Rule {
		public function validate() {
// 			if(!ereg('(^[0-9]+\.$)|(^[0-9]+$)|(^[0-9]+\.[0-9]+$)|(^\.[0-9]+$)',$value))
//            	return "Please enter a numeric value.";			
			
			if(!preg_match("/(^[0-9]+\.$)|(^[0-9]+$)|(^[0-9]+\.[0-9]+$)|(^\.[0-9]+$)/smi", $this->field->getValue())) {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_NUMBER));
				return false;
			}
			return true;
		}
	}