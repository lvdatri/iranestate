<?php
	class Al_Validation_RuleInt extends Al_Validation_Rule {
		public function validate() {
			if(!preg_match("/^[0-9]+$/smi", $this->field->getValue())) {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_INT));
				return false;
			}
			return true;
		}
	}