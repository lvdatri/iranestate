<?php
	class Al_Validation_RuleDbUnique extends Al_Validation_Rule {
		private $dbField=null;
		private $table=null;
		private $pkField='id';
		private $recordId='';
		
		private $prefix='';
		private $urlValue = false;
		
		
		public function setUrlValue($value) {
			$this->urlValue = $value;
		}
		
		public function getUrlValue() {
			return $this->urlValue;
		}
		
		public function setPrefix($value) {
			$this->prefix = $value;
		}
		
		public function getPrefix() {
			return $this->prefix;
		}
		
		public function setDbField($value) {
			$this->dbField = $value;
		}
		public function setTable($value) {
			$this->table = $value;
		}
		public function setPkField($value) {
			$this->pkField = $value;
		}
		public function setRecordId($value) {
			$this->recordId = $value;
		}						

		public function getDbField() {
			return $this->dbField;
		}

		public function getTable() {
			return $this->table;
		}

		public function getPkField() {
			return $this->pkField;
		}		
		
		public function getRecordId() {
			return $this->recordId;
		}		
		
		public function validate() {
			if($this->table === null) {
				$this->setErrorMessage('ruelDbUnique: table not set');
				return false;
			}
			
			if($this->dbField === null) {
				$this->dbField = $this->field->getName();
			}
			
			$value = $this->field->getValue();
			
			if($this->urlValue) {
				$value = $this->convertToUrlValue($value);	
			}
			
			$value = $this->prefix.$value;
			
			$qb = new Al_QueryBuilder();
			$qb->select($this->dbField);
			$qb->from($this->table);
			$qb->where($this->dbField . " = '" . Al_Db::escape($value) . "'");
			
			if($this->form->getMode()==Al_Form::UPDATE) {
				$qb->where($this->pkField." <> '" . Al_Db::escape($this->recordId) . "'");
			}
			
			$db = Al_Db::get_instance();
			$res = $db->query($qb->get_query());
			if($res->num_rows() > 0) {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_DB_UNIQUE));
				return false;
            }	
			
			return true;
		}
		
		private function convertToUrlValue($value) 
		{
			$name = trim(strtolower($value));
		    $name = preg_replace('/[^a-z0-9 ]/', '', $name);
			$name = preg_replace('/ {2,}/', ' ', $name);		    	
		    $name = str_replace(' ', '-', $name);
		    return $name;
		}
	}