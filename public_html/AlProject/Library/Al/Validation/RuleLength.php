<?php
	class Al_Validation_RuleLength extends Al_Validation_Rule {
		private $min = null;
		private $max = null;
		
		public function setMin($value) {
			$this->min = intval($value);
		}
		
		public function getMin() {
			return $this->min;
		}
		
		public function setMax($value) {
			$this->max = intval($value);
		}
		
		public function getMax() {
			return $this->max;
		}
		
		public function validate() {
			$length = strlen(trim($this->field->getValue()));
			
			if($this->min !== null && $this->max !== null) {
				// between action
				if($length < $this->min || $length > $this->max) {
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_LENGTH_BETWEEN),$this->min,$this->max);
					return false;	
				}
			}
			
			if($this->min !== null) {
				if($length < $this->min) {
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_LENGTH_MIN),$this->min);
					return false;	
				}
			}
			
			if($this->max != null) {
				if($length > $this->max) {
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_LENGTH_MAX),$this->max);
					return false;	
				}				
			}

			return true;
		}
	}