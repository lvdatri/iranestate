<?php
	class Al_Validation_Rule {
		
		private $_type;
		protected $field;
		protected $form;
		protected $_settings;
		
		protected $_message = null;
		
		private $_errorMessage;
		
		public function __construct($options=array())
	    {
	        foreach ($options as $optionName => $optionValue) {
	            $methodName = 'set' . $optionName;
	            if (method_exists($this, $methodName)) {
	                $this->{$methodName}($optionValue);
	            }
	        }
	        $this->_settings = Bl_Settings::get_instance();
	        return $this;
	    }		
	    
	    public function setField($value) {
	    	$this->field = $value;
	    }
	    
	    public function getField() {
	    	return $this->field;
	    }
	    
	    public function setForm($value) {
	    	$this->form = $value;
	    }
	    
	    public function getForm() {
	    	return $this->form;
	    }
		
		public function setErrorMessage($message) 
		{
			if($this->_message===null) {
				$params = func_get_args();
				$this->_errorMessage = call_user_func_array ('sprintf',$params);
			} else {
				$this->_errorMessage = $this->_message;
			}			
		}

		public function getErrorMessage() 
		{
			return $this->_errorMessage;
		}
		
		public function valid() {
			return $this->validate();
		}
		
		public function setMessage($value) {
			$this->_message = $value;
		}
		
		public function getMessage() {
			return $this->_message;
		}
	}