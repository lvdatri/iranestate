<?php
	class Al_Validation_RuleAllowedValues extends Al_Validation_Rule {
		private $allowedValues=array();
		
		public function setAllowedValues($value) {
			$this->allowedValues = explode(',',$value);
		}
		
		public function getAllowedValues($value) {
			return $this->allowedValues;
		}
		
		public function validate() {
			$value = $this->field->getValue();
			foreach($this->allowedValues as $key=>$matchVal) {
				$matchVal = trim($matchVal);
				if($matchVal == $value) {
					return true;
				}
			}
			$language = Al_Language::get_instance();
			$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_INVALID_VALUE));
			return false;
		}
	}