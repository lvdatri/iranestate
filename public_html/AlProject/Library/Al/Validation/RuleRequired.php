<?php
	class Al_Validation_RuleRequired extends Al_Validation_Rule {
		private $emptyValues=array();
		
		/**
		 * accpets array of empty values
		 * @param array $values
		 */
		public function setEmptyValues($values) {
			$this->emptyValues=$values;
		}
		
		public function getEmptyValues() {
			return $this->emptyValues;
		}
		
		public function validate() {
			$value = trim($this->field->getValue());
			if(count($this->emptyValues)>0) {
				foreach($this->emptyValues as $emptyValue) {
					if($value == $emptyValue) {
						$language = Al_Language::get_instance();
						$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_REQUIRED));
						return false;
					}
				}
			} else if($value=='') {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_REQUIRED));
				return false;
			}
			return true;
		}
	}