<?php
	class Al_Validation_RuleEqualTo extends Al_Validation_Rule {
		private $toField='';
		
		public function setToField($value) {
			$this->toField=$value;
		}
		
		public function getToField() {
			return $this->toField;
		}
		
		public function validate() {
			$toField = $this->form->_getField($this->toField);
			if($this->field->getValue()!=$toField->getValue()) {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_EQUAL_TO),$toField->getLabel());
				return false;
			}
			return true;
		}
	}