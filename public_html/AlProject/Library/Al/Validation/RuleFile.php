<?php
	class Al_Validation_RuleFile extends Al_Validation_Rule {
		
		public $validExtensions=null;
		public $maxFileSize=null;
		public $minFileSize=null;
		
		public function setValidExtensions($value) {
			$this->validExtensions = $value;
		}
		
		public function getValidExtensions() {
			return $this->validExtensions;
		}

		public function setMaxFileSize($value) {
			$this->maxFileSize = intval($value);
		}

		public function getMaxFileSize() {
			return $this->maxFileSize;
		}
		
		public function setMinFileSize($value) {
			$this->minFileSize = intval($value);
		}
		
		public function getMinFileSize() {
			return $this->minFileSize;
		}
		
		public function validate() {
			
			$file = $this->getFileDetails();
			if($file === null) {
				// no file uploaded
				if($this->fileAlreadyUploaded()) {
					// file previously uploaded
					return true;
				} else {
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_FILE_REQUIRED));
					return false;
				}				
			}
			
			$size = $file['size'];
			$name = $file['name'];
			
			
			if($this->minFileSize !== null) {
				if($this->minFileSize > $size) {
					// min size validation error
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_FILE_MIN_SIZE),Al_File::formatSize($this->minFileSize));
				}
			}
			
			if($this->maxFileSize !== null) {
				if($this->maxFileSize < $size) {
					// max size validation error
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_FILE_MAX_SIZE),Al_File::formatSize($this->maxFileSize));					
				}
			}
			
			if($this->validExtensions !== null) {
		        $extensions_array = explode(',',$this->validExtensions); 
		        if (!in_array(
		            end(explode(".", strtolower($name))), 
		            $extensions_array
		        )) {
					$language = Al_Language::get_instance();
					$this->setErrorMessage(
						$language->getValue(Al_Language::VAL_ERROR_FILE_INVALID_EXTENSION),
						htmlentities($name,ENT_QUOTES,'UTF-8'),
						str_replace(",",", ", $this->validExtensions)
					);					
		        }				
			}
			
			return true;
		}
		
		public function fileAlreadyUploaded() {
			$fieldName = $this->field->getName();
			$formName = get_class($this->form);
			
			if( isset($_SESSION[$this->_settings->session]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName]) &&
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName]) && 
				isset($_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName]['file_uploaded_path'])
			){
				$data = $_SESSION[$this->_settings->session][$this->_settings->session_forms][$formName][$fieldName];				
				if($data['file_uploaded_path'] != '' || $data['file_saved_name'] != '') {
					return true;
				}
			}
			return false;
		}		
		
		public function getFileDetails() {
			$name = $this->field->getName();
			if(array_key_exists($name,$_FILES)) {
				$file = $_FILES[$name];
				if(!empty($file['name']) && $file['error']==0) {
					if($file['size'] != '') {
						return $file;
					}
				}
			}
			return null;			
		}
		
	
	}