<?php
	class Al_Validation_RuleMathCaptcha extends Al_Validation_Rule {
		
		private $type = '';
		
		public function setType($type) {
			$this->type = $type;
		}
		
		public function getType() {
			return $this->type;
		}
		
		public function validate() {
			$captcha = new Al_MathCaptcha($this->type);
			if(!$captcha->isValid($this->field->getValue())) {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_MATH_CAPTCHA));
				return false;
			}
			return true;
		}
	}