<?php
	class Al_Validation_RuleRequiredConditional extends Al_Validation_Rule {
		private $emptyValues=array();
		private $triggerField;
		private $triggerFieldValues=array();
		
		/**
		 * accpets array of empty values
		 * @param array $values
		 */
		public function setEmptyValues($values) {
			$this->emptyValues=$values;
		}
		
		public function getEmptyValues() {
			return $this->emptyValues;
		}		
		
		public function setTriggerField($value) {
			$this->triggerField = $value;
		}
		
		public function getTriggerField() {
			return $this->triggerField;
		}
		
		/**
		 * 
		 * @param string $value - list of comma delimited values under which conditions a field will be required
		 */
		public function setTriggerFieldValues($value) {
			$value = explode(',',$value);
			$this->triggerFieldValues = array();
			foreach($value as $v) {
				$this->triggerFieldValues[] = $v;
			}
		}
		
		public function getTriggerFieldValues($value) {
			return $this->triggerFieldValues;
		}
		
		public function validate() {
			if($this->triggerFieldActive()) {
				$value = trim($this->field->getValue());
				if(count($this->emptyValues)>0) {
					foreach($this->emptyValues as $emptyValue) {
						if($value == $emptyValue) {
							$language = Al_Language::get_instance();
							$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_REQUIRED));
							return false;
						}
					}
				} else if($value=='') {
					$language = Al_Language::get_instance();
					$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_REQUIRED));
					return false;
				}
			}
			
			return true;
		}
		
		public function triggerFieldActive() {
			$triggerField = $this->form->_getField($this->triggerField);
			$value = $triggerField->getValue();
			foreach($this->triggerFieldValues as $v) {
				if($v == $value) {
					return true;
				}
			}
			return false;
		}
	}