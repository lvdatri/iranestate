<?php
	class Al_Validation_RuleEmail extends Al_Validation_Rule {
		public function validate() {
			if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/smi", $this->field->getValue())) {
				$language = Al_Language::get_instance();
				$this->setErrorMessage($language->getValue(Al_Language::VAL_ERROR_EMAIL));
				return false;
			}
			return true;
		}
	}