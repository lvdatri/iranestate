<?php
	/**
	 * This class aids in building sql query.
	 * 
	 * Different object will be creating different parts of query. 
	 * 
	 * Search object
	 * 		in most cases it will deal with where part of query
	 * Paging class
	 * 		this will handle limit part of query
	 * Column Sorter
	 * 	    will be in charge of order by statment
	 * 
	 * @version 0.1
	 *
	 */

	class Al_QueryBuilder {

		public $where = '';
		public $limit = '';
		public $order_by = '';
		public $join='';
		public $select='';
		public $from='';
		public $group_by='';
		public $insert_into='';
		public $set='';
		public $set_values='';
		public $update='';
		
		public function __construct() {
			$this->clear();
		}
	
		public function insert_into($table_name) {
		    $this->insert_into = "INSERT INTO ".$table_name." ";
		}
		
		public function update($table_name) {
		    $this->update = "UPDATE ".$table_name." ";
		}
		
		public function set($field,$value,$escaped=false) {
		    if($escaped==false) {
		        $value = "'".Al_Db::escape($value)."'";
		    }
			if($this->set == '') {
				$this->set = "SET ".$field." = ".$value." ";
			}
			else {
				$this->set .= ", ".$field." = ".$value." ";
			}    
		}
		
		public function set_values($values) {
			if($this->set_values == '') {
				$this->set_values = "VALUES (".$values.")";
			}
			else {
				$this->set_values .= ", (".$values.")";
			}
		}
		
		public function where($where,$condition='AND') {
			if($this->where == '') {
				$this->where = "WHERE ($where) ";
			}
			else {
				$this->where .= " $condition ($where) ";
			}
		}
		
		public function select($select) {
			if($this->select == '') {
				$this->select = "SELECT $select ";
			}
			else {
				$this->select .= ", $select ";
			}
		}		
		
		public function group_by($group_by) {
			if($this->group_by == '') {
				$this->group_by = "GROUP BY $group_by ";
			}
			else {
				$this->group_by .= ", $group_by ";
			}
		}	
		
		public function order_by($order_by) {
			if($order_by != '') {
				if($this->order_by=='') {
					$this->order_by = " ORDER BY $order_by ";
				}
				else {
					$this->order_by .= ", $order_by ";
				}
			}
		}
		
		public function join($join) {
			$this->join .= $join.' ';	
		}
		
		public function from($from) {
		    if($this->from == '') {
				$this->from = "FROM $from ";
			}
			else {
				$this->from .= ", $from ";
			}		    
		}
		
		public function limit($limit) {
			$this->limit = " LIMIT $limit ";
		}
		
		public function get_query() {
		    return 
		        $this->insert_into.
		        $this->update.
		        $this->set_values.
		        $this->set.
		        $this->select .
		        $this->from . 
		        $this->join .
		        $this->where .
		        $this->group_by .
		        $this->order_by .
		        $this->limit;
		}
		
		public function clear() {
			$this->where = '';
			$this->limit = '';
			$this->order_by = '';
			$this->join = '';
			$this->select='';
			$this->from='';
			$this->group_by='';	
            $this->insert_into='';
		    $this->set='';			
		    $this->update='';
		    $this->set_values='';
		}
	}