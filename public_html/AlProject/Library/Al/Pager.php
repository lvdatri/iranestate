<?php
	/**
	 * used to setup and display page naviagtion
	 * 
	 * count_query must be set before pager can be used
	 * 
	 * below is a sample of paging code that is generated
	 *       
	 *    		   <ul>
     *                <li class="disabled">&lt;&lt;</li>
     *                <li><a href="#">1</a></li>
     *                <li><a href="#">2</a></li>
     *                <li class="selected">3</li>
     *                <li><a href="#">4</a></li>
     *                <li><a href="#">5</a></li>
     *                <li><a href="#">&gt;&gt;</a></li>
     *              </ul>  
	 */
	
	class Al_Pager {
		private $_db;
		
		/**
		 * query that will be executed to find out total number of records
		 * from db result first field will be used to retrieve count value
		 *
		 * @var unknown_type
		 */
		private $count_query='';
		
		/**
		 * url parameters that will be attached to page navigation liks
		 *
		 * @var string
		 */
		private $url_params='';
		
		/**
		 * path to callilng script that will be used within paging nav
		 *
		 * @var string
		 */
		private $path='';
		
		/**
		 * number of page being displayed
		 *
		 * @var int
		 */
		public $page_num = 1; 
		
		/**
		 * total number of pages available
		 *
		 * @var int
		 */
		public $num_of_pages = 0;
		
		/**
		 * total number of records available, this value will be retreived from count query
		 *
		 * @var int
		 */
		public $total_num_of_records=0;
		
		
		/**
		 * number of records displayed within a page, value is calculated from total_num_of_records and records_per_page
		 *
		 * @var int
		 */
		public $num_of_records=0;
		
		/**
		 * max number of page links that can be displayed
		 *
		 * @var int
		 */
		private $nav_num_of_page_links = 10;

		/**
		 * maximumn number of records to be displayed per page
		 *
		 * @var int
		 */
		public $records_per_page = 20;
		
		/**
		 * Starting record number
		 *
		 * @var int
		 */
		public $starting_record_num = 0;
		
		/**
		 * ending record number
		 *
		 * @var int
		 */
		public $ending_record_num = 0;
		
		
		/**
		 * if this paramaeter is passed paging will be appended to the end of the seo url
		 */
		public $url_seo = '';
		
		/**
		 * display labels
		 *
		 */
		private $label_prev = '&lt;&lt;';//<<
		private $label_next = '&gt;&gt;';// >>
		
		
		public $url_params_page = 'page';
		private $requested_page_num = 1;
		
		private $_settings;
		
		private $_type;
		
		CONST TYPE_GET = 'get';
		CONST TYPE_SEO = 'seo';
		
		function __construct() {
			$this->_db = Al_Db::get_instance();
			$this->path = $_SERVER["PHP_SELF"];
			$this->_settings = Bl_Settings::get_instance();
			$this->_type = self::TYPE_SEO;
		}
		
		/**
		 * accepts query that will be used to retrieve number of records
		 *
		 * @param string $query
		 */
		function count_query($query) {
			$this->count_query = $query;
		}
		
		/**
		 * attaches additional parameters to every link with the nave 
		 *
		 * @param unknown_type $params
		 */
		function set_url_params($params) {
			$this->url_params = '/'.$params;
			
		}
		
		public function get_url_params($seo=true) {
			if($seo) {
				return $this->url_params_page.'/'.urlencode($this->page_num);
			} else {
			    return $this->url_params_page.'='.urlencode($this->page_num);
			}
		}
		
		function url_seo($url_seo) {
		    $this->url_seo = $url_seo;
		}
		
		/**
		 * Accepts path that will be called from paging nav
		 *
		 * @param string $path 
		 */
		public function setPath($value) {
			$this->path = $value;
		}
		
		public function getPath() {
			return $this->path;
		}
		
		
		public function set_from_get() {
		    $this->requested_page_num = Al_Utilities::get($this->url_params_page);
		}
		
		/**
		 * retrieves count for total number of rows
		 * and initialises all required paging variables
		 *
		 * @param int $page_num - number of page to be displayed
		 */
		function run($page_num='') {
		    
		    if($page_num=='') {
		        $page_num = $this->requested_page_num;
		    }
		    
			$this->num_of_records = 0;
			$this->total_num_of_records = 0;
			$this->num_of_pages = 0;
			
			if($this->count_query != '') {
				$res = $this->_db->query($this->count_query);
	
				if($res->num_rows()>0) {
					$row = $res->fetch_row(Al_Db::ORDERED);
					$this->total_num_of_records = $row[0];
				}
					
				// if records found page variables will be set
				if($this->total_num_of_records > 0) {
					
					// calcualte number of pages that are available based on total records count
					$this->num_of_pages = @ceil($this->total_num_of_records / $this->records_per_page);

					// set page number - checks performed for limits				
					$this->page_num = $page_num;
					if($page_num > $this->num_of_pages) {
						$this->page_num = $this->num_of_pages;
					}
					if($page_num <= 0)
						$this->page_num = 1;					
					
					// calcualte number of records that will be displayed on requested page
					if($this->page_num == $this->num_of_pages) {
						$this->num_of_records = $this->total_num_of_records - ($this->records_per_page * ($this->num_of_pages-1));
					}
					else {
						$this->num_of_records = $this->records_per_page;
					}
					
					// set starting record number
					if($this->page_num <=1) {
						$this->starting_record_num = 1;
					} else {
						$this->starting_record_num = ($this->page_num-1) * $this->records_per_page+1;				
					}	
					
					// set ending record number
					$this->ending_record_num = 	$this->starting_record_num + $this->num_of_records - 1;					
				}
			}
		}
		
		/**
		 * retrieves params include parameter for curent page
		 *
		 * @return unknown
		 */
		function get_curent_page_params() {
			return 'page='.$this->page_num.$this->url_params;
		}

		/**
		 * returns limit fragment of query based on pager settingss
		 *
		 * @return string  limit query 
		 */
		function get_limit() {
			if($this->total_num_of_records > 0)
				return " $this->records_per_page OFFSET ".($this->starting_record_num-1) . " ";
			else 
				return " $this->records_per_page ";
		}
		
		private function get_image($image_name,$label) {
		    return '<img class="gridHeadAr" border="0px" src="'.$this->_settings->path_images.'template/'.$image_name.'.gif" alt="'.$label.'">';
		}
		
		/**
		 * displays navigation with pages
		 * if there is only one page no nav will be displayed
		 *
		 */
		function display_nav() {
			// make sure there is more than one page
			if($this->num_of_pages > 1) {
				switch($this->_type) {
					case self::TYPE_GET :
						$this->display_get_nav();
						break;
					case self::TYPE_SEO :
						if($this->_settings->frontend) {
							$this->display_seo_nav_frontend();
						} else {
							$this->display_seo_nav_other();
						}
						break;
				}
			}
		}
		
		private function display_seo_nav_other() {
			$page_nav = '<ul>';
			    
			// find out starting and ending page numbers
			$starting_page = $this->page_num - intval(($this->nav_num_of_page_links - 1) / 2);
			if($starting_page < 1)
			    $starting_page = 1;
			
			$ending_page = $starting_page + $this->nav_num_of_page_links - 1;
			
			if($ending_page > $this->num_of_pages) {
			    $starting_page = $starting_page - $ending_page + $this->num_of_pages;
			    if($starting_page < 1) 
			        $starting_page = 1;
			    $ending_page = $this->num_of_pages;
			}
			
			
			// setup previous links
			if($this->page_num <= 1) {
			    // disabled prev link;
			//    					$page_nav .= $this->get_li($this->label_prev,1,false,'disabled');
			}
			else {
			    // active prev link
			    $page_nav .= $this->get_li_seo($this->get_image('grid_first','first'),1,true);
			    $page_nav .= $this->get_li_seo($this->get_image('grid_prev','previous'),($this->page_num-1),true);
			}				
			
			//setup page numbers
			for($i = $starting_page; $i <= $ending_page; $i++) {
			    if($i == $this->page_num)
			        $page_nav .= $this->get_li_seo($i,$i,false,'selected');
			    else
			        $page_nav .= $this->get_li_seo($i,$i,true);
			}	
			
			// setup next links
			if($this->page_num >= $this->num_of_pages) {
			    // disabled next link;
			    //    							$page_nav .= $this->get_li($this->label_next,1,false,'disabled');
			}
			else {
			    // active next link
			    $page_nav .= $this->get_li_seo($this->get_image('grid_next','next'),($this->page_num+1),true);
			    $page_nav .= $this->get_li_seo($this->get_image('grid_last','last'),$ending_page,true);
			}		
			
			$page_nav .= '</ul>';			
			print $page_nav;			
		}
		
		private function display_seo_nav_frontend() {
			$page_nav = '';
			    
			// find out starting and ending page numbers
			$starting_page = $this->page_num - intval(($this->nav_num_of_page_links - 1) / 2);
			if($starting_page < 1)
			    $starting_page = 1;
			
			$ending_page = $starting_page + $this->nav_num_of_page_links - 1;
			
			if($ending_page > $this->num_of_pages) {
			    $starting_page = $starting_page - $ending_page + $this->num_of_pages;
			    if($starting_page < 1) 
			        $starting_page = 1;
			    $ending_page = $this->num_of_pages;
			}
			
			
			// setup previous links
			if($this->page_num <= 1) {
			    // disabled prev link;
			//    					$page_nav .= $this->get_li($this->label_prev,1,false,'disabled');
			}
			else {
			    // active prev link
//			    $page_nav .= $this->get_href_seo($this->get_image('grid_first','first'),1,true);
			    $page_nav .= $this->get_href_seo('&lt;&lt;',($this->page_num-1),true,'nodiv');
			}				
			
			//setup page numbers
			for($i = $starting_page; $i <= $ending_page; $i++) {
			    if($i == $this->page_num)
			        $page_nav .= $this->get_href_seo($i,$i,false,'selected');
			    else {
			    	if($i==$ending_page) {
			        	$page_nav .= $this->get_href_seo($i,$i,true,'nodiv');
			    	} else {
			    		$page_nav .= $this->get_href_seo($i,$i,true,'');
			    	}
			    }
			}	
			
			// setup next links
			if($this->page_num >= $this->num_of_pages) {
			    // disabled next link;
			    //    							$page_nav .= $this->get_href_seo($this->label_next,1,false,'disabled');
			}
			else {
			    // active next link
			    $page_nav .= $this->get_href_seo('&gt;&gt;',($this->page_num+1),true,'nodiv');
//			    $page_nav .= $this->get_href_seo($this->get_image('grid_last','last'),$ending_page,true);
			}	
				
			
//			$page_nav .= '</ul>';			
			print $page_nav;
		}
		
		private function display_get_nav() {
			$page_nav = '<ul>';
			    
			// find out starting and ending page numbers
			$starting_page = $this->page_num - intval(($this->nav_num_of_page_links - 1) / 2);
			if($starting_page < 1)
			    $starting_page = 1;
			
			$ending_page = $starting_page + $this->nav_num_of_page_links - 1;
			
			if($ending_page > $this->num_of_pages) {
			    $starting_page = $starting_page - $ending_page + $this->num_of_pages;
			    if($starting_page < 1) 
			        $starting_page = 1;
			    $ending_page = $this->num_of_pages;
			}
			
			
			// setup previous links
			if($this->page_num <= 1) {
			    // disabled prev link;
			//    					$page_nav .= $this->get_li($this->label_prev,1,false,'disabled');
			}
			else {
			    // active prev link
			    $page_nav .= $this->get_li($this->get_image('grid_first','first'),1,true);
			    $page_nav .= $this->get_li($this->get_image('grid_prev','previous'),($this->page_num-1),true);
			}				
			
			//setup page numbers
			for($i = $starting_page; $i <= $ending_page; $i++) {
			    if($i == $this->page_num)
			        $page_nav .= $this->get_li($i,$i,false,'selected');
			    else
			        $page_nav .= $this->get_li($i,$i,true);
			}	
			
			// setup next links
			if($this->page_num >= $this->num_of_pages) {
			    // disabled next link;
			    //    							$page_nav .= $this->get_li($this->label_next,1,false,'disabled');
			}
			else {
			    // active next link
			    $page_nav .= $this->get_li($this->get_image('grid_next','next'),($this->page_num+1),true);
			    $page_nav .= $this->get_li($this->get_image('grid_last','last'),$ending_page,true);
			}		
			
			$page_nav .= '</ul>';			
			print $page_nav;
		}
		
		private function get_seo_url_from_page_no($page_no) {
            $url_seo_string = $this->url_seo.'/listings';
            
		    // get ending page
		    $seo_ending = $page_no*$this->records_per_page;
		    // get starting page
		    $seo_starting = $seo_ending - $this->records_per_page+1;
		    if($seo_starting==1) {
		        // for first record only display the url, no need for paging
		        $seo_url_link = $this->url_seo;
		    } else {
		        // display paging information
		        $seo_url_link = $url_seo_string . $seo_starting.'-'.$seo_ending;
		    }
		    // echo $page_no . ' - ' . $seo_starting . ' - ' .$seo_ending . ' - ' . $seo_url_link. '<BR>';		    
		    return $seo_url_link;   
		}
		
		/**
		 * generates li
		 *
		 * @param string $label 
		 * @param int $page page  number to display
		 * @param bool $link
		 * @param string $class name of class to display
		 * @return unknown
		 */
		function get_li($label,$page,$link=true,$class='') {
			if($class !='')
				$class = ' class="'.$class.'" ';
			
			if($link) {
				$label = '<a href="'.$this->path.'?page='.$page.htmlentities($this->url_params,ENT_QUOTES,'UTF-8').'">'.$label.'</a>';
			}
				
			return '<li'.$class.'>' .$label. '</li>';
		}

		function get_href_seo($label,$page,$link=true,$class='') {
			if($class !='')
				$class = ' class="'.$class.'" ';
			
			if($link) {
				$label = '<a href="'.$this->path.'page/'.$page.htmlentities($this->url_params,ENT_QUOTES,'UTF-8').'">'.$label.'</a>';
			}
				
			return '<li'.$class.'> ' .$label. ' </li>';
		}		
		
		function get_li_seo($label,$page,$link=true,$class='') {
			if($class !='')
				$class = ' class="'.$class.'" ';
			
			if($link) {
				$label = '<a href="'.$this->path.'page/'.$page.htmlentities($this->url_params,ENT_QUOTES,'UTF-8').'">'.$label.'</a>';
			}
				
			return '<li'.$class.'>' .$label. '</li>';
		}		
		
		/**
		 * displays details related to pager class
		 *
		 */
		function info() {
			echo '<div style="clear:both; text-align:left; padding:10px; background-color:#FFFFFF;">';
			
			echo 'page_num - ' . $this->page_num. '<BR>';
			echo 'limit query - ' . $this->get_limit() . '<BR>';
			
			echo 'starting_record_num - ' . $this->starting_record_num . '<BR>';
			echo 'ending_record_num - ' . $this->ending_record_num . '<BR>';
			
			echo 'number of pages - ' . $this->num_of_pages . '<BR>';
			echo 'total_num_of_records - ' . $this->total_num_of_records . '<BR>';
			echo 'num_of_records - ' . $this->num_of_records . '<BR>';
			$this->display_nav() .'<BR>';
			echo '</div>';
		}
	}