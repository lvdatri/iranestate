<?php
	/**
	 * @version 0.02
	 * @author darko
	 *
	 */
	class Al_Sorter {
		const SORTING_DESC = 'DESC';
		const SORTING_ASC = 'ASC';
		
		
        private $default_column;
        private $columns;	    
        private $sort_field;
        private $sort_direction;
        
        private $active_column;
        private $active_direction;
        
        public $url_params = '';
        
        public $url_params_order = 'order';
        public $url_params_sort = 'sort';
        
        private $_settings;

		/**
		 * path to callilng script that will be used within paging nav
		 *
		 * @var string
		 */
		private $path;        
        
	    public function __construct() {
	        $this->path = $_SERVER["PHP_SELF"];
	        $this->_settings = Bl_Settings::get_instance();
	        $this->clear();
	    }
	    
	    public function setPath($value) {
	    	$this->path = $value;
	    }
	    
	    public function getPath() {
	    	return $this->path;
	    }
	    
	    public function clear() {
	        $this->columns = array();
	        $this->default_column = array();
	        $this->sort_field = '';
	        $this->sort_direction = self::SORTING_ASC;
	        
            $this->active_column='';
            $this->active_direction='';	 
//            $this->path = '';       
	    }
	    
	    public function add_column($field,$label,$sql,$default=false,$default_direction=self::SORTING_ASC) {
            $column = array();
			
			$column['field'] = $field;
			$column['label'] = $label;
			$column['sql'] = $sql;
			$column['default'] = $default;
			$column['default_direction'] = $default_direction;
			
			$this->columns[$field] = $column;
			
			if($default) {
				$this->default_column = $column;	
			}	        
	    }
	    
	    public function set_url_params($url_params,$seo=true) {
	    	if($seo) {
	        	$this->url_params = '/'.$url_params;
	    	} else {
	    		$this->url_params = '&'.$url_params;
	    	}
	    }
	    
	    public function set($field,$order) {
	        $this->sort_field = $field;
	        
	        if(strtoupper($order)== self::SORTING_DESC) {
	            $this->sort_direction = self::SORTING_DESC;
	        } else {
	            $this->sort_direction = self::SORTING_ASC;
	        }
	    }
	    
	    public function set_from_get() {
	    	if(isset($_GET[$this->url_params_sort])) {
	            $this->set(Al_Utilities::get($this->url_params_sort),Al_Utilities::get($this->url_params_order));
	        }	        
	    }
	    
	    
	    private function get_sort_column() {
	        if( $this->sort_field != '' && isset($this->columns[$this->sort_field])) {
	            // set from sort field
                $column_field = $this->sort_field;
            } else {
                // get default field if no default first field will be used
                $first = true;
                foreach($this->columns as $item) {
                    if($first) {
                        // set first field as sort field                        
                        $first = false;
                        $column_field = $item['field'];
                    }

                    if($item['default']==true) {
                        $column_field = $item['field'];
                        break;
                    }
                }
                
                $this->set($column_field, $this->columns[$column_field]['default_direction']);
            }
            if($this->sort_field=='') {
                $this->set($column_field,self::SORTING_ASC);
            }
            return $this->columns[$column_field];
	    }
	    
	    public function get_url_params($seo=true) {
	    	if($seo) {
	    		return $this->url_params_sort.'/'.urlencode($this->sort_field).'/'.$this->url_params_order.'/'.urlencode($this->sort_direction);
	    	} else {
	        	return $this->url_params_sort.'='.urlencode($this->sort_field).'&'.$this->url_params_order.'='.urlencode($this->sort_direction);
	    	}
	    }
	    
	    private function get_sort_direction($column) {
	        $direction = strtoupper($this->sort_direction); 
	        
	        if($direction=='') {
	            // get direction from column
	            $direction = $column['default_direction'];
	        }
	        
	    	switch ($direction) {
				case self::SORTING_DESC: 
				case self::SORTING_ASC: 
                    break;
				default: 
                    $direction = self::SORTING_ASC;
			}

			return $direction;
	    }
	    
        public function run(&$qb) {
            $column = $this->get_sort_column();
            $direction = $this->get_sort_direction($column);
            
            if($column['sql'] == '') {

            	// set sql fragment from field name
				$qb->order_by($column['field'].' '.$direction);
			} else {
				// add direction to sql fragment
				$fragments = explode(',',$column['sql']);
				$fragments[0] .= ' '. $direction;
				$sql = '';
				foreach($fragments as $fragment) {
					$qb->order_by($fragment);
				}
			}            
        }
        
        public function show_column($colum_name) {
            echo $this->get_column($colum_name);
        }
        
        public function get_column($colum_name,$seo=true) {
            $col_str = '';
            if(isset($this->columns[$colum_name])) {
                $column = $this->columns[$colum_name];
                $field = $column['field'];
                $label = $column['label'];
                $order = self::SORTING_ASC;

                if($field == $this->sort_field) {
                    $order = $this->sort_direction == self::SORTING_DESC ? self::SORTING_ASC : self::SORTING_DESC;
					$img_order = strtolower($this->sort_direction);
					
					$image = '<img class="gridHeadAr" src="' . $this->_settings->path_images . 'template/grid_'.$img_order.'.gif" border="0">';
                } else {
                    $image = '';
                }
                if($seo) {
					$col_str = '<a class="gridHeadLink" href="'. $this->path . $this->url_params_sort.'/' . $field . '/'.$this->url_params_order.'/'.$order.$this->url_params.'">'.$label.'</a>'.$image;                	
                } else {
			    	$col_str = '<a class="gridHeadLink" href="'. $this->path . '?'.$this->url_params_sort.'=' . $field . '&'.$this->url_params_order.'='.$order.$this->url_params.'">'.$label.'</a>'.$image;
                }
            } else {
                $col_str = 'unknown '.$colum_name;             
            }
            return $col_str;
        }
	}