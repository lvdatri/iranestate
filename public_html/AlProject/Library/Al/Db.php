<?PHP
	/** 
	 * @version 0.01
	 */
    
    class Al_Db {
		public $db_connection;
		private static $instance;
		
		const ASSOC = '2';
		const ORDERED = '1';
		
		private function __construct() {
			
		}
		
	    public static function get_instance() {
	        if (!isset(self::$instance)) {
	            $c = __CLASS__;
	            self::$instance = new $c;
	            self::$instance->connect();
	        }
	        return self::$instance;
	    }		
		
        public function connect($conn_id=0) { 
        	$settings = Bl_Settings::get_instance();
        	$conn = $settings->get_db_connection($conn_id);

        	if($conn) {
				if(!$this->db_connection = @mysql_connect($conn["host"],$conn["user"], $conn["password"])) {
	                $this->showError("DATABASE ERROR","Failed to connect to MYSQL database.");
	            }
	            
	            if(!@mysql_select_db($conn["database"],$this->db_connection)) {
	                $this->showError("DATABASE ERROR","Failed to select database - " . $conn["database"]);
	            }
	            
	            mysql_set_charset('utf8');
        	} else {
        		$this->showError("DATABASE ERROR","No databse connection set - ".$conn_id);
        	}			
        }        	
        
        public function query($sql,$log=true) {
            if($result = @mysql_query($sql,$this->db_connection)) {
                $result = new Al_Db_Result($result);
                return $result;
            }
            else {
                $this->showError("MYSQL ERROR", mysql_error() . "\n query - " . $sql);
                return false;
            }
        }
       
        private function showError($type,$error) {
       		echo $error;
            exit;
        }
        
        public static function escape($value) {
        	return mysql_real_escape_string($value);
        }
    }
    
    class Al_Db_Result {
    	public $insert_id;
    	private $callback_function;
    	private $callback_class;
    	private $callback_class_method;
        public function __construct($result) {
            $this->result = $result;
            $this->insert_id = mysql_insert_id();
            $this->callback_function = NULL;
            $this->callback_class = NULL;
            $this->callback_class_method = NULL;
        }
        
        public function set_callback_function($callback_function) {
            $this->callback_function = $callback_function;
            if(!is_callable($this->callback_function)) {
				echo 'unknonw function - '.$this->callback_function;
				exit;
            }            
        }
        
        public function set_callback_class($object,$method) {
        	$this->callback_class = $object;
        	$this->callback_class_method = $method;
        	if(!is_callable(array($this->callback_class,$this->callback_class_method))) {
			    echo 'unknown class callback - '.$this->callback_class_method;
			    exit;
			}     	
        }
        
        function fetch_row($fetch_mode=Al_Db::ASSOC) {
            switch ($fetch_mode) {
                case Al_Db::ASSOC  : {
                    if($this->callback_function === NULL && $this->callback_class===NULL) {
                        return mysql_fetch_assoc($this->result);
                    } else if($this->callback_function===null) {
                    	$row = mysql_fetch_assoc($this->result); 
						if($row) {
							$this->callback_class->{$this->callback_class_method}($row);
                        }
                        return $row;                    	
                    } else {
                        $row = mysql_fetch_assoc($this->result);
                        if($row) {
                            $func = $this->callback_function;
                            $func($row);
                        }
                        return $row;
                    }                    
                    break;
                }
                case Al_Db::ORDERED : {
                    return mysql_fetch_row($this->result);
                    break;
                }
            }
        }

        function num_rows() {
            return mysql_num_rows($this->result);
        }
        
        function insert_id() {
        	return $this->insert_id;
        }
        
        function reset() {
			if($this->num_rows()>0) 
	            mysql_data_seek($this->result,0);
        }
        
        function free() {
            mysql_free_result($this->result);
        }
		
		function affected_rows() {
			return mysql_affected_rows();
		}
		
		function get_records($key=null,$fetch_mode = Al_Db::ASSOC, $callback = true)
		{
			$this->reset();
			$records = array();
			while($row = $this->fetch_row($fetch_mode,$callback)) {
				if($key==null) {
					$records[] = $row;
				} else {
					$records[$row[$key]] = $row;
				}
			}
			$this->reset();
			return $records;
		}
		
		function get_records_grouped_by_field($fieldName, $key=null, $itemName=null, $fetch_mode = Al_Db::ASSOC, $callback = true)
		{
			$this->reset();
			$records = array();
			while($row = $this->fetch_row($fetch_mode,$callback)) {
				if(array_key_exists($row[$fieldName], $records)==false) {
					$records[$row[$fieldName]] = array();
		
					if($itemName != null) {
						$records[$row[$fieldName]][$itemName] = $row;
						$records[$row[$fieldName]]['items'] = array();
					}
				}
				if($key==null) {
					if($itemName == null) {
						$records[$row[$fieldName]][] = $row;
					} else {
						$records[$row[$fieldName]]['items'][] = $row;
					}
				} else {
					if($row[$key] != null) {
						if($itemName == null) {
							$records[$row[$fieldName]][$row[$key]] = $row;
						} else {
							$records[$row[$fieldName]]['items'][$row[$key]] = $row;
						}
					}
				}
			}
			$this->reset();
			return $records;
		}		
		
    }