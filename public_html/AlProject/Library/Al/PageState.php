<?php
    /**
     * 
     * @author darko
     * 
     * remembers page state by storing url vars into session
     *
     */
    class Al_PageState {
        public $pageId;
        private $ignoreParams;
        private $_settings;
        public function __construct() {
            $state = '';
            $this->ignoreParams = array();
            $this->_settings = Bl_Settings::get_instance();
        }
        
        /**
         * list of params that will not reset the state if present within the url
         * 
         * when only ignore params are loaded then attempt will be made to load params from session
         * @param $params
         */        
        public function ignoreParams($params) {
            $params = explode(',',$params);
            foreach($params as $key) {
                $key = trim($key);
                $this->ignoreParams[$key] = true; 
            }
        }
        
        public function set($pageId) {
			$this->pageId = $pageId;

            if($this->canLoadFromGet()) {
                $this->saveGetToSession();
            } else {
                $this->loadGetFromSession();
            }
        }
        
        private function canLoadFromGet() {
            if(count($_GET)>0) {
                foreach($_GET as $key=>$value) {
                    if(!isset($this->ignoreParams[$key])) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        private function saveGetToSession() {
            $_SESSION[$this->_settings->session][$this->_settings->session_pages][$this->pageId]=$_GET;
        }
        
        private function loadGetFromSession() {
            if(    isset($_SESSION[$this->_settings->session]) &&
                   isset($_SESSION[$this->_settings->session][$this->_settings->session_pages]) &&
                   isset($_SESSION[$this->_settings->session][$this->_settings->session_pages][$this->pageId])
            ) {
            	$_GET = $_SESSION[$this->_settings->session][$this->_settings->session_pages][$this->pageId];
            }
        }
    }