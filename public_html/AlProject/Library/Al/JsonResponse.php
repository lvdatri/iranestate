<?php
	class Al_JsonResponse {
		public static $SUCCESS = 0;
		public static $JSON_PARSE_ERROR = 3;
		public static $ACTION_MISSING = 4;
		public static $UNKNOWN_ACTION = 5;
		public static $LOGIN_ERROR = 6;
		public static $INSUFFICIENT_ACCESS = 7;
		public static $CUSTOM_ERROR = 8;
		public static $NO_DATA = 50;
		
		public static $MSG_SUCCESS = 'Success';
		public static $MSG_JSON_PARSE_ERROR = 'Server - Json parse error';
		public static $MSG_ACTION_MISSING = 'Server - Action missing'; 
		public static $MSG_UNKNOWN_ACTION = 'Server - Unknown action';		
		public static $MSG_INSUFFICIENT_ACCESS = "You don't have sufficient permissions";
		public static $MSG_NO_DATA = "No Data available";
		
		
		private $status = 0;
		private $message = '';
		private $data = array();
		
		public function __construct() {
			$this->status = self::$SUCCESS;
			$this->message = 'success';
		}
		
		public function getStatus() {
			return $this->status;
		}
	
		public function getMessage() {
			return $this->message;
		}
	
		public function getData($section='data') {
			return $this->data;
		}
	
		public function setStatus($status) {
			$this->status = $status;
		}
	
		public function setMessage($message) {
			$this->message = $message;
		}
	
		public function setData($data,$section='data') {
			$this->data[$section] = $data;
		}
		
		public function load($request) {
			
			$res = @json_decode($request);
			
			if($res !== null && $res !== false) {
				$this->message = 'clients JSON parsing error';
				$this->status = self::$JSON_PARSE_ERROR;
				return true;
			}
			
			return false;
		}
		
		public function show() {
			header('Content-type: application/json');
			$r = array();
			if($this->message != '') { 
				$r['message'] = $this->message;
			}
			$r['status'] = $this->status;
			if(count($this->data)>0) {
				foreach($this->data as $key => $item) { 
					$r[$key] = $item;
				}
			}
			print json_encode(array(
				'response' => $r
			));
		}		
	}

/*
	class Al_Response {
		private $response;
		
		public function __construct() {
			$this->response = array(
				'__access' => true,
				'__accessError' => '',
				'__formValid' => true,
				'__formErrors' => array(),
				'data' => array(), 
			);
		}
		
		public function setAccess($access) {
			$this->response['__access'] = $access;
		}
		
		public function setFormValid($formValid) {
			$this->response['__formValid'] = $formValid;
		}
		
		public function setFormErrors($formErrors) {
			$this->response['__formErrors'] = $formErrors;
		}		
		
		public function setAccessError($accessError) {
			$this->response['__accessError'] = $accessError;
		}
		
		public function setData($data) {
			$this->response['data'] = $data;
		}

		public function setSection($section,$data) {
			$this->response[$section] = $data;
		}
		
		public function show() {
			print json_encode($this->response);
		}
	} */