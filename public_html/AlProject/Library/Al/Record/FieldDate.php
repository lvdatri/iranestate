<?php
	class Al_Record_FieldDate extends Al_Record_Field {
		public function initialise() {
			$this->setType(parent::DATE);
		}
		
		public function setValue($value,$escaped=false) {
			if($value=='') {
				$this->_setValue('null',true);
			} else {
				$this->_setValue($value,$escaped);
			}
		}		
	}