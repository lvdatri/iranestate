<?php
	class Al_Record_Field {
		
		const ID='id';
		const STRING='string';
		const TEXT='text';
		const INT='int';
		const FLOAT='float';
		const DATE='date';
		const TIME='time';
		const DATE_TIME='date_time';
		
		protected $_value='';
		protected $_name;
		protected $_type;
		protected $_escaped=false;
		protected $_readOnly=false;
		protected $_recordMethod;
		
		public function __construct() {
			$this->initialise();
		}
		
		protected function initialise() {
			
		}
		
		public function setName($value) {
			$this->_name = $value;
			$this->setRecordMethod($this->_name);
		}
		
		public function getName() {
			return $this->_name;
		}
		
		public function setRecordMethod($value) {
			$value = str_replace('_', ' ', strtolower($value));
		    $value = preg_replace('/[^a-z0-9 ]/', '', $value);	
		    $this->_recordMethod = str_replace(' ', '', ucwords($value));	
		}
		
		public function getRecordMethod() {
			return $this->_recordMethod;			
		}		
		
		public function setValue($value,$escaped=false) {
			$this->_setValue($value,$escaped);
		}
		
		public function _setValue($value,$escaped=false) {
			$this->_value = $value;
			if($escaped) {
				$this->_escaped = true;
			} else {
				$this->_escaped = false;
			}
		}		
		
		public function getValue() {
			if($this->_escaped=true && $this->_value=='null') {
				return '';
			} else {
				return $this->_value;
			}
		}
		
		public function getDbValue() {
			return $this->_value;
		}
		
		public function setType($value) {
			$this->_type = $value;
		}
		
		public function getType() {
			return $this->_type;
		}
		
		public function setEscaped($value) {
			$this->_escaped = $value;
		}
		
		public function getEscaped() {
			return $this->_escaped;
		}
		
		public function setReadOnly($value) {
			$this->_readOnly=$value;
		}
		
		public function getReadOnly() {
			return $this->_readOnly;
		}	
	}