<?php
	class Al_Record_FieldText extends Al_Record_Field {
		private $_maxLength;
		
		public function initialise() {
			$this->setType(parent::TEXT);
		}
		
		public function setMaxLength($value) {
			$this->_maxLength = $value;
		}
		
		public function getMaxLength() {
			return $this->_maxLength;
		}
	}