<?php
	class Al_Record_FieldTime extends Al_Record_Field {
		public function initialise() {
			$this->setType(parent::TIME);
		}
		
		public function setValue($value,$escaped=false) {
			if($value=='') {
				$this->_setValue('null',true);
			} else {
				$this->_setValue($value,$escaped);
			}
		}		
	}