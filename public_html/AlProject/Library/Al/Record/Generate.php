<?php
	
	class Al_Record_Generate {
		private $_db;
		private $_clsRecord;
		private $_tableName;
		private $_className;
		private $_tableSchema;
		
		private $nl="\n";
		private $tab="    ";
		
		public function __construct() {
			$this->_db = Al_Db::get_instance();
		}
		
		public function generateRecord($tableName,$className) {
			$this->_tableName = $tableName;
			$this->_className = $className;

			$this->setTableSchema();
			$this->setRecordClass();
			$this->setInitialiseMethod();
			$this->setSetters();
			$this->setGetters();
			
			$this->setDefaultMethods();
			
			$this->display('<?php'.$this->nl.$this->_clsRecord->generate(),'class - Bl_Record_'.$this->_className);
			
//			Al_Utilities::a($this->_tableSchema);

		}
		
		public function generateSetters($tableName,$varName) {
			$this->_tableName = $tableName;
			$content = '';
			$this->setTableSchema();
			foreach($this->_tableSchema as $field) {
				if($field['type']!=Al_Record_Field::ID) {
					$content .= '$'.$varName.'->set'.$field['var_name'].'(\'\');'.$this->nl;
				}
			}
			$this->display($content,'setters - Bl_Record_'.$this->_className);
		}
		
		public function setTableSchema() {
			$res = $this->_db->query("
				DESCRIBE ".$this->_tableName."
			");
			$this->_tableSchema = array();
			while($row = $res->fetch_row()) {
				$row['var_name'] = $this->getVarName($row['Field']);
				$row['field_name'] = $row['Field'];
				$this->setFieldType($row);
				$this->_tableSchema[$row['Field']] = $row;
			}			
		}
		
		public function setFieldType(&$field) {
			$type = explode('(',$field['Type']);
			$dbType = strtolower(trim($type[0]));
			if(array_key_exists('1',$type)) {
				$dbTypeParm = $type[1];
			} else {
				$dbTypeParm = '';
			}
			if($field['Key']=='PRI') {
				$type = Al_Record_Field::ID;
			} else {
				switch ($dbType) {
					case 'varchar':
	                case 'char':
	                	$type = Al_Record_Field::STRING;
						$field['max_length'] = intval($dbTypeParm);
	                    break;
					case 'text':
						$type = Al_Record_Field::TEXT;
						break;	                        
	                case 'bigint':
	                case 'int':                        
	                case 'smallint': 
	                case 'tinyint':                  
	                	$type = Al_Record_Field::INT;
	                    break;
					case 'decimal':
						$type = Al_Record_Field::FLOAT;
						$dp = explode(',',$dbTypeParm);
						$field['decimal_points'] = intval($dp[1]);
	                    break; 
	                case 'date':
	                	$type = Al_Record_Field::DATE;
	                	break;
					case 'timestamp':                	
	                case 'datetime':
	                	$type = Al_Record_Field::DATE_TIME;
	                    break;
					case 'time':
	                	$type = Al_Record_Field::TIME;
						break;    				
				}
			}
			$field['type'] = $type;
		}
		
		public function getVarName($string) {
			$string = str_replace('_', ' ', strtolower($string));
	        $string = preg_replace('/[^a-z0-9 ]/', '', $string);	
	        $string = str_replace(' ', '', ucwords($string));
	        return $string;			
		}
		
		public function display($content,$title='') {
			if($title != '') {
				echo '<div style="font-weight:bold; font-family:Verdana; font-size:12px; padding:4px; display:block; margin-top:10px;">'.$title.'</div>';
			}
			
            echo '<textarea name="textfield" id="textfield" style="width:100%; height:50;">';
			echo $content;
            echo '</textarea>';			
		}
		
		private function setRecordClass() {
			$this->_clsRecord = new Zend_CodeGenerator_Php_Class();
			$this->_clsRecord->setName('Bl_Record_'.$this->_className);
			$this->_clsRecord->setExtendedClass('Al_Record');
		}
		
		private function setInitialiseMethod() {
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('initialiseRecord');
			$method->setBody($this->getInitialiseBody());
			$this->_clsRecord->setMethod($method);
			
		}
		
		private function getInitialiseBody() {
			$body = '$this->_db_table = \''.$this->_tableName.'\';'.$this->nl;
			$body.=$this->nl;
			
			foreach($this->_tableSchema as $field) {
				switch ($field['type']) {
					case Al_Record_Field::STRING:
						$body.='$field = new Al_Record_FieldString();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						$body.='$field->setMaxLength('.$field['max_length'].');'.$this->nl;
						break;
					case Al_Record_Field::TEXT:
						$body.='$field = new Al_Record_FieldText();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						break;
					case Al_Record_Field::ID:
						$body.='$field = new Al_Record_FieldId();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						break;
					case Al_Record_Field::INT:
						$body.='$field = new Al_Record_FieldInt();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						break;
					case Al_Record_Field::FLOAT:
						$body.='$field = new Al_Record_FieldFloat();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						$body.='$field->setDecimalPoints('.$field['decimal_points'].');'.$this->nl;
						break;
					case Al_Record_Field::DATE:
						$body.='$field = new Al_Record_FieldDate();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						break;
					case Al_Record_Field::DATE_TIME:
						$body.='$field = new Al_Record_FieldDateTime();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						break;
					case Al_Record_Field::TIME:
						$body.='$field = new Al_Record_FieldTime();'.$this->nl;
						$body.='$field->setName(\''.$field['field_name'].'\');'.$this->nl;
						break;
				}

				if($field['Null'] == 'YES') {
					$body .= '$field->setValue(\'null\',true);'.$this->nl;
				}
				
				
				if($field['Default'] != 'NULL' && $field['Default'] != 'None' && $field['Default'] != '') {
					$body .= '$field->setValue(\''.$field['Default'].'\',true);'.$this->nl;
				}
				
				$body.='$this->_add_field($field);'.$this->nl;
				$body.=$this->nl;
			}
			
			return $body;
		}
		
		private function setSetters() {
			foreach($this->_tableSchema as $field) {
				$body = '$this->_set_field_value(\''.$field['field_name'].'\',$value,$escaped);';				
				$method = new Zend_CodeGenerator_Php_Method();
				$method->setName('set'.$field['var_name']);
				$method->setParameters( array(
					array(
						'name' => 'value',
					),			
					array(
						'name' => 'escaped',
						'defaultValue' => false,
					),
				));
				$method->setBody($body);
				$this->_clsRecord->setMethod($method);
			}
		}
		
		private function setGetters() {
			foreach($this->_tableSchema as $field) {
				$body = 'return $this->_get_field_value(\''.$field['field_name'].'\');';				
				$method = new Zend_CodeGenerator_Php_Method();
				$method->setName('get'.$field['var_name']);
				$method->setBody($body);
				$this->_clsRecord->setMethod($method);
			}		
		}

		private function setDefaultMethods() {
			$body = '$this->_qb = new Al_QueryBuilder();'.$this->nl.
					'$this->_qb->where("r.id = \'".intval($id)."\'");'.$this->nl.
					'return $this->_load_record();';
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('load');
			$method->setParameters( array(
				array(
					'name' => 'id',
				),			
			));
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);		

//			$body = '$this->_qb = new Al_QueryBuilder();'.$this->nl.
//					'if($this->_id === null) {'.$this->nl.
//					$this->tab.'$this->_insert();'.$this->nl.
//					'} else {'.$this->nl.
//					$this->tab.'$this->_update();'.$this->nl.
//					'}';
//			
//			$method = new Zend_CodeGenerator_Php_Method();
//			$method->setName('save');
//			$method->setBody($body);
//			$this->_clsRecord->setMethod($method);		
		}
	}