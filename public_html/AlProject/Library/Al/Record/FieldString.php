<?php
	class Al_Record_FieldString extends Al_Record_Field {
		private $_maxLength=255;
		
		public function initialise() {
			$this->setType(parent::STRING);
		}
		
		public function setMaxLength($value) {
			$this->_maxLength = intval($value);
		}
		
		public function getMaxLength() {
			return $this->_maxLength;
		}
		
		public function getDbValue() {
			$val = $this->_value;
			
			if(strlen($val)<=$this->getMaxLength()) {
				return $val;
			} else {
				return substr($val,0,$this->getMaxLength());
			}
		}
	}