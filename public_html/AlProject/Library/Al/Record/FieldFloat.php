<?php
	class Al_Record_FieldFloat extends Al_Record_Field {
		private $_decimalPoints;
		
		public function initialise() {
			$this->setType(parent::FLOAT);
			$this->setValue('0');
			$this->setDecimalPoints(null);
		}
		public function setDecimalPoints($value) {
			if($value === null) {
				$this->_decimalPoints = $value;
			} else {
				$this->_decimalPoints = intval($value);
			}
		}
		
		public function getDecimalPoints() {
			return $this->_decimalPoints;
		}
		
		public function getDbValue() {
			if($this->_decimalPoints===null) {
				$format = '%f';
			} else {
				$format = '%0.'.$this->_decimalPoints.'f'; 
			}
			return sprintf($format,$this->getValue());
		}
	}