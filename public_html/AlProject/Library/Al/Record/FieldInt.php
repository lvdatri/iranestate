<?php
	class Al_Record_FieldInt extends Al_Record_Field {
		public function initialise() {
			$this->setType(parent::INT);
			$this->setValue('0');
		}
		
		public function getDbValue() {
			return sprintf('%d',$this->getValue());
		}
	}