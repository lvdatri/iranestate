<?php
	/**
	 * list of functions that are associated with file manipulation.
	 */
	

    class Al_File {
	    	/**
		 * builds directories of required path.
		 *
		 * @param string $path
		 * @param string $root starting point from which path is to be built
		 */
		public static function mkdir($path,$root='',$rights=0777) {
			$dirs = explode('/', $path);
			$dir = $root;
			foreach($dirs as $part) {
				$dir .= $part . '/';
				if (!@is_dir($dir) && strlen($dir)>0) {
					if(!@mkdir($dir, $rights)) {
						return false;
					}
				}
			}
			return true;
		}
		
		/**
		 * Returns true if directory exists and it is empty
		 *
		 * @param string $dirname
		 * @return bool
		 */	
		public function is_emtpy_dir($dirname) {
		   	$result=false;
		   	if(@is_dir($dirname) ){
		       	$result=true;
		       	$handle = opendir($dirname);
				while( ( $name = readdir($handle)) !== false){
					if ($name!= "." && $name !=".."){
						$result=false;
						break;
					}
				}
		        closedir($handle);
		   	}
		   	return $result;
		}
		
		public function write($path,$content) {
			$fh = fopen($path, 'w');
			fwrite($fh, $content);
			fclose($fh);		
		}
		
		/**
		 * delets file from server
		 *
		 * @param unknown_type $path
		 */
		public static function delete($path) {
			if(file_exists($path)) {
				unlink($path);
			}
		}
		
		/**
		 * removes emtpy directories form provided path
		 *
		 * @param string $path
		 * @param string $root
		 */
		public function remove_empty_dirs($path,$root='') {
			while($path != '.')	 {
				if(cls_file::is_emtpy_dir($root.$path)) {
					rmdir($root.$path);	
				}			
				$path=dirname($path);
			}
		}
		
		/**
		 * first file that is found within a directory will be moved to desired destination
		 *
		 * @param string $source
		 * @param string $destination
		 * @return array - message
		 */
		public function move_first_file_from_dir($source,$destination) {
			$message = '';
			if (is_dir($source)) {
			    if ($dh = opendir($source)) {
			        while (($file = readdir($dh)) !== false) {
			        	if(filetype($source . $file)=='file') {
							$output='';
							//echo ('move '.str_replace('/','\\',$source.$file).' '.str_replace('/','\\',$destination,$output));
			        		$res = exec('move  "'.str_replace('/','\\',$source.$file).'" '.str_replace('/','\\',$destination,$output));
			        		if(file_exists($destination)) {
			        			// success
			        			return array(
									'error' => false,
									'message' => ''
								);			        			
			        		} else {
								return array(
									'error' => true,
									'message' => 'unable to rename file ' . $source . ' | ' . $destination
								);		        			
			        		}
			        	}
			        }
			        closedir($dh);
					return array(
						'error' => true,
						'message' => 'no file within directory - ' . $source
					);			    	
			    }
			    else {
					return array(
						'error' => true,
						'message' => 'no file within directory - ' . $source
					);			    	
			    }
			}	
			else {
				return array(
					'error' => true,
					'message' => 'invalid path - ' . $source
				);		
			}
		}	
		
		/**
		 * unzips a directory and array
		 * unzip.bat needs to be used within windows. 
		 *
		 * @param string $source
		 * @param string $destination
		 * @param string $unzip_bat - path to unzip.bat
		 * @return array
		 */
		public function unzip($source,$destination,$unzip_bat='') {
			if($unzip_bat == '')
				$unzip_bat = _CFG_UNZIP_BAT;
	
			$output = array();
			$res = exec('"'.$unzip_bat.'" '.$destination.' ' .$source,$output);
			
			$error = true;
			// go through output an make sure that success line exists
			foreach($output as $str) {
			    if(stripos($str,'Everything is Ok')!==false) {
			        // successfully extracted
			        $error = false;
			        break;
			    }
			}
	
		    if($error) {
		    	return array(
		    		'error' => true,
		    		'message' => 'error unziping - ' . '"'.$unzip_bat.'" '.$destination.' ' .$source
		    	);	    	
		    } else {
		    	return array(
		    		'error' => false,
		    		'message' => ''
		    	);	    		    	
		    }
		}

        /**
         * returns extension from file name
         * 
         * if extension cannot be determined empty string is returned
         *
         * @param string $file_name
         * @return string
         */
        public function get_file_extension($file_name) {
            $pos = strrpos($file_name,'.');
            if($pos===false) {
                return '';
            }
            else {
                return substr($file_name, $pos + 1); 
            }
        }	

        
		/**
         * make file downloadable by users browser. if path invalid, error.txt will be sent to user
         */
        public function stream($path,$file_name) {
            if(@file_exists($path)) {
                self::file_header($file_name);
                readfile($path);
                exit;
            } else {
                self::stream_message("Error downloading file.");//\n Path: $path");
            }
        }
        
        /**
         * when user request to retrieve a file, they are expecting a file to be sent,
         * in case there is an error during file download, user will be sent a text file
         * containing the error message.
         */
        public function stream_message($message) {
            self::file_header('message.txt');
            echo $message;
            exit;
        }
        
        /**
         * sets file header required for file download
         *
         * @param string $file_name
         */
        public function file_header($file_name) {
			if($file_name == "") {
		        $file_name = 'file.txt';
			} else {
			    $file_name = trim(strtolower(str_replace(' ','_',$file_name)));			
			}
			
			header("Cache-control: private");
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=".$file_name);            
			session_cache_limiter('none'); // makes file download work in IE when session is started 			
        }
        
 		public static function formatSize($size) {
	        $size = $size;
	        $kb = 1024; 
	        $mb = 1024 * $kb; 
	        $gb = 1024 * $mb; 
	        $tb = 1024 * $gb; 
	        if ($size) { 
	            if ($size < $kb) { 
	                $size = "$size Bytes"; 
	            } 
	            elseif ($size < $mb) { 
	                $final = round($size/$kb,2); 
	                $size = "$final KB"; 
	            } 
	            elseif ($size < $gb) { 
	                $final = round($size/$mb,2); 
	                $size = "$final MB"; 
	            } 
	            elseif($size < $tb) { 
	                $final = round($size/$gb,2); 
	                    $size = "$final GB"; 
	            } else { 
	                $final = round($size/$tb,2); 
	                $size = "$final TB"; 
	            } 
	        }
	        return $size; 
    	}		                
    }

