<?php
	class Al_ControllerGenerate {
		public $_tableName;
		public $_tableAlias;
		public $_className;
		public $_nameSingle;
		public $_namePlural;
		public $_formClassName;
		public $_recordClassName;
		public $_templatePath;
		public $_html = '';
		

		private $_tableSchema;
		private $_clsRecord;
		private $_db;
		private $nl="\n";
		private $tab="    ";		
		
		public function __construct() {
			$this->_db = Al_Db::get_instance();
		}		
		
		public function generateBrowsePage() {
			$this->setTableSchema();
			$this->setHtmlPageJavascript();
			$this->setHtmlPageHeader();
			$this->setHtmlPageStart();
			$this->setHtmlPageSearch();
			$this->setHtmlPageGrid();
			$this->setHtmlPageEnd();
			$this->display($this->_html,'browse - '.$this->_className);
		}
		
		public function setHtmlPageGrid() {

			$headerColumns = '';
			$dataColumns = '';
			foreach($this->_tableSchema as $field) {
				$name = $field['field_name'];				
				if(array_key_exists('use__'.$name,$_POST)) {
					if(array_key_exists('c_sort_column__'.$name,$_POST)) {
						$f = Al_Utilities::tpost('f_label__'.$name);
						if($f!='') {
							$label = $f;
						} else {
							$label = $field['field_label'];
						}
						$headerColumns .= $this->tab.$this->tab.$this->tab.'<th><?php $this->sorter->show_column(\''.$name.'\'); ?></th>'.$this->nl;

						$dataColumns .= $this->tab.$this->tab.$this->tab.'<td><?php echo $row[\''.$name.'\']; ?></td>'.$this->nl;
					}
				}
			}
									
			$this->_html.= $this->tab.'<table class="grid" id="dataGrid" width="100%" border="0" cellspacing="0" cellpadding="0">'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html.= $headerColumns;
			$this->_html.= $this->tab.$this->tab.$this->tab.'<th width="1%">&nbsp;</th>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'</tr>'.$this->nl;
			
			$this->_html.= $this->tab.'<?php while($row = $this->result->fetch_row()) { ?>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html.= $dataColumns;
			$this->_html.= $this->tab.$this->tab.$this->tab.'<td nowrap="nowrap">'.$this->nl;
			$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.'<input onclick="location=\'<?php echo $this->pathController; ?>update/id/<?php echo $row[\'id\']; ?>\'" class="btnSml" type="button" name="btnUpdate" id="btnUpdate" value="Update" />'.$this->nl;
			$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.'<input onclick="Ext.al.confirm(\'Delete Item?\',\'<?php echo $this->pathController; ?>delete/id/<?php echo $row[\'id\']; ?>\');" class="btnSml" type="button" name="btnDelete" id="btnDelete" value="Delete" />'.$this->nl;
			$this->_html.= $this->tab.$this->tab.$this->tab.'</td>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'</tr>'.$this->nl;
			$this->_html.= $this->tab.'<?php } ?>'.$this->nl;	

			$this->_html.= $this->tab.$this->tab.'<?php if($this->pager->total_num_of_records < 1) { ?>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<tr>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.$this->tab.'<td colspan="5">No records found.</td>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'</tr>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<?php } ?>'.$this->nl;
			$this->_html.= $this->tab.'</table>'.$this->nl;
			$this->_html.= '<?php if($this->pager->total_num_of_records > 0) { ?>'.$this->nl;
			$this->_html.= '<div class="gridFoot">'.$this->nl;
			$this->_html.= $this->tab.'<div class="gridNav">'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<?php $this->pager->display_nav(); ?>'.$this->nl;
			$this->_html.= $this->tab.'</div>'.$this->nl;
			$this->_html.= $this->tab.'<div class="gridFootTxt">'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<?php echo $this->pager->starting_record_num; ?> to <?php echo $this->pager->ending_record_num; ?> of <?php echo $this->pager->total_num_of_records; ?> records'.$this->nl;
			$this->_html.= $this->tab.'</div>'.$this->nl;
			$this->_html.= '</div> '.$this->nl;
			$this->_html.= '<?php } ?>'.$this->nl;			
			
			
		}
		
		public function setHtmlPageSearch() {
			$searchFields = '';
			foreach($this->_tableSchema as $field) {
				$name = $field['field_name'];				
				if(array_key_exists('use__'.$name,$_POST)) {				
					if(array_key_exists('c_search_field__'.$name,$_POST)) {
						$f = Al_Utilities::tpost('f_label__'.$name);
						if($f!='') {
							$label = $f;
						} else {
							$label = $field['field_label'];
						}						
					
						$searchFields.= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
						$searchFields.= $this->tab.$this->tab.$this->tab.$this->tab.'<td width="8%" nowrap="nowrap" class="frmL">'.$label.':</td>'.$this->nl;
						$searchFields.= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmFBg"  width="92%"><input class="frmF" name="'.$name.'" type="text" id="'.$name.'" value="<?php echo Al_Utilities::eGet(\''.$name.'\'); ?>" size="30" /></td>'.$this->nl;
						$searchFields.= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;								
					}
				}
			}
			
			if($searchFields .= '') {
				$this->_html.= '<form name="formSearch" id="formSearch" action="<?php echo $this->pathController.\'browse/\'; ?>" method="get" >'.$this->nl;
				$this->_html.= $this->tab.'<div class="frmBody">'.$this->nl;
				$this->_html.= $this->tab.$this->tab.'<?php echo $this->params->to_fields(array(Al_UrlParams::SORTER)); ?>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.'<table  width="100%" border="0" cellspacing="0" cellpadding="0">'.$this->nl;
				$this->_html.= $searchFields;
				$this->_html.= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmSepTD" colspan="2">'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.$this->tab.'<div class="frmSep"></div>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.'</td>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.'<tr>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.'<td class="frmTD" colspan="2"><input class="btn" type="submit" name="btnSearch" id="btnSearch" value="Search" />'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.$this->tab.'<?php if($this->searchTriggered==true) { ?>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.$this->tab.'<input onclick="location=\'<?php echo $this->pathController.\'browse/\'; ?>page/1/<?php echo $this->params->get(array(Al_UrlParams::SORTER)) ?>\';" class="btn" type="button" name="btnClear" id="btnClear" value="Clear" />'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.$this->tab.'<?php } ?>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.$this->tab.'</td>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.$this->tab.'</tr>'.$this->nl;
				$this->_html.= $this->tab.$this->tab.'</table>'.$this->nl;
				$this->_html.= $this->tab.'</div>'.$this->nl;
				$this->_html.= '</form>'.$this->nl;				
			}
		}
		
		public function setHtmlPageStart() {
			$this->_html.= '<div class="page">'.$this->nl;
			$this->_html.= '<?php $this->message->show(); ?>'.$this->nl;			
		}
		
		public function setHtmlPageEnd() {
			$this->_html.= '</div>'.$this->nl;
		}
		
		public function setHtmlPageJavascript() {
			$this->_html.= '<script type="text/javascript">'.$this->nl;
			$this->_html.= 'Ext.al.pageReady = function() {'.$this->nl;
			$this->_html.= $this->tab.'new Ext.Button({'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'renderTo:\'pageActions\','.$this->nl;
			$this->_html.= $this->tab.$this->tab.'text:\'Add '.$this->_nameSingle.'\','.$this->nl;
			$this->_html.= $this->tab.$this->tab.'minWidth:\'80\','.$this->nl;
			$this->_html.= $this->tab.$this->tab.'style: Ext.al.btn.floatRight,'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'icon:\'<?php echo $this->_settings->path_images;?>icons/insert.png\','.$this->nl;
			$this->_html.= $this->tab.$this->tab.'handler: function(e) {'.$this->nl;
			$this->_html.= $this->tab.$this->tab.$this->tab.'this.disable();'.$this->nl;
			$this->_html.= $this->tab.$this->tab.$this->tab.'location=\'<?php echo $this->pathController; ?>insert\';'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'}'.$this->nl;
			$this->_html.= $this->tab.'});'.$this->nl;
				
			$this->_html.= $this->tab.'Ext.get(\'formSearch\').on(\'submit\',function (e,t) {'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'e.stopEvent();'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'Ext.al.sendFormUri(\'formSearch\');'.$this->nl;
			$this->_html.= $this->tab.'});'.$this->nl;
			$this->_html.= '}'.$this->nl;
			$this->_html.= '</script>'.$this->nl;
		}
		
		public function setHtmlPageHeader() {
			$this->_html.= '<div class="page_header">'.$this->nl; 
			$this->_html.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'.$this->nl;
			$this->_html.= $this->tab.'<tr>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<td width="71%"><h1 class="page_title"><?php $this->nav->show(); ?></h1> </td>'.$this->nl;
			$this->_html.= $this->tab.$this->tab.'<td width="29%" ><div id="pageActions"></div></td>'.$this->nl;
			$this->_html.= $this->tab.'</tr>'.$this->nl;
			$this->_html.= '</table>'.$this->nl;
			$this->_html.= '</div>'.$this->nl;			
		}
		
		public function generateController() {
			$this->setTableSchema();
			$this->setControllerClass();
			$this->setBeforeAction();
			$this->setIndexAction();
			$this->setBrowseAction();
			$this->setInsertAction();
			$this->setUpdateAction();
			$this->setDeleteAction();
			$this->setSearch();
			$this->setCallback();
			
			$this->display('<?php'.$this->nl.$this->_clsRecord->generate(),'class - '.$this->_className);
		}
		
		private function setBeforeAction() {
			$body = '$this->message = new Al_Message();'.$this->nl;
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('beforeAction');
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}
		
		private function setIndexAction() {
			$body = '$this->callAction(\'browse\');'.$this->nl;
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('indexAction');
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}		

		private function setBrowseAction() {
			$searchFields = '';
			$selectFields = '';
			$sortColumns = ''; 
			foreach($this->_tableSchema as $field) {
				$name = $field['field_name'];				
				if(array_key_exists('use__'.$name,$_POST)) {				
					if(array_key_exists('c_search_field__'.$name,$_POST)) {
						$searchFields .= '\''.$name.'\', ';
					}
					
					if(array_key_exists('c_sort_column__'.$name,$_POST)) {
						
						if(array_key_exists('c_default_sort_column__'.$name,$_POST)) {
							$default_sort = ',true';
						} else {
							$default_sort = '';
						}
						
						$f = Al_Utilities::tpost('f_label__'.$name);
						if($f!='') {
							$label = $f;
						} else {
							$label = $field['field_label'];
						}
						
						
						$sortColumns .= '$this->sorter->add_column(\''.$name.'\',\''.$label.'\',\''.$this->_tableAlias.'.'.$name.'\''.$default_sort.');'.$this->nl;
					}
					
				}
				if(array_key_exists('c_select_field__'.$name,$_POST)) {
					$selectFields .= $this->_tableAlias.'.'.$name.', ';
				}
			}
			$searchFields = rtrim($searchFields,', ');
			$selectFields = rtrim($selectFields,', ');
			$sortColumns = rtrim($sortColumns,$this->nl);
			
			$body = '$this->_user->level(\'1\');'.$this->nl.$this->nl;
			
			$body.= '$pageState = new Al_PageState();'.$this->nl;
			$body.= '$pageState->set($this->pathAction);'.$this->nl.$this->nl;
			
			$body.= '$this->params = new Al_UrlParams();'.$this->nl;
			if($searchFields!='') {
				$body.= '$this->params->set_get(Al_UrlParams::SEARCH,array('.$searchFields.'));'.$this->nl;
			}
			
			$body.= '$this->nav = new Al_Navigation();'.$this->nl;
			$body.= '$this->nav->add(\''.$this->_namePlural.'\');'.$this->nl.$this->nl;
			
			$body.= '$qb = new Al_QueryBuilder();'.$this->nl;
			$body.= '$qb->select("'.$this->nl;
			$body.= $this->tab.$selectFields.$this->nl;
			$body.= '");'.$this->nl;
			$body.= '$qb->from("'.$this->_tableName.' '.$this->_tableAlias.'");'.$this->nl.$this->nl;
			
			$body.= '$this->searchTriggered = $this->setSearch($qb);'.$this->nl.$this->nl;
			
			$body.= '$this->sorter = new Al_Sorter();'.$this->nl;
			$body.= '$this->sorter->setPath($this->pathController.\'browse/\');'.$this->nl;
			$body.= $sortColumns.$this->nl;
			$body.= '$this->sorter->set_from_get();'.$this->nl;
			$body.= '$this->sorter->set_url_params($this->params->get(array(Al_UrlParams::SEARCH)));'.$this->nl;
			$body.= '$this->sorter->run($qb);'.$this->nl;
			$body.= '$this->params->set(Al_UrlParams::SORTER,$this->sorter->get_url_params());'.$this->nl.$this->nl;
			
			$body.= '$this->pager = new Al_Pager();'.$this->nl;
			$body.= '$this->pager->setPath($this->pathController.\'browse/\');'.$this->nl;
			$body.= '$this->pager->count_query("'.$this->nl;
			$body.= $this->tab.'SELECT count(id) ".'.$this->nl;
			$body.= $this->tab.'$qb->from.'.$this->nl;
			$body.= $this->tab.'$qb->where'.$this->nl;
			$body.= ');'.$this->nl.$this->nl;
			
			$body.= '$this->pager->set_url_params($this->params->get(array( Al_UrlParams::SORTER, Al_UrlParams::SEARCH)));'.$this->nl;
			$body.= '$this->pager->set_from_get();'.$this->nl;
			$body.= '$this->pager->run();'.$this->nl;
			$body.= '$this->params->set(Al_UrlParams::PAGER,$this->pager->get_url_params());'.$this->nl.$this->nl;
			
			$body.= '$qb->limit($this->pager->get_limit());'.$this->nl;
			
			$body.= '$this->result = $this->_db->query($qb->get_query());'.$this->nl;
			$body.= '$this->result->set_callback_class($this,\'onBrowseCallback\');'.$this->nl.$this->nl;
			
			$body.= '$this->_template->extjs_attach_on_ready(\'Ext.al.pageReady();\');'.$this->nl;
			$body.= '$this->_template->al_set_grid_hover(\'dataGrid\');'.$this->nl;
			$body.= '$this->load_page(\''.$this->_templatePath.'browse.php\');'.$this->nl;
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('browseAction');
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}		

		private function setInsertAction() {
			$body = '$this->_user->level(\'1\');'.$this->nl.$this->nl;
			
			$body.= '$this->nav = new Al_Navigation();'.$this->nl;
			$body.= '$this->nav->add(\''.$this->_namePlural.'\',$this->pathController.\'browse\');'.$this->nl;
			$body.= '$this->nav->add(\'Add\');'.$this->nl.$this->nl;
			
			$body.= '$this->form = new Bl_Form_'.$this->_formClassName.'();'.$this->nl;
			$body.= '$this->form->setMode(Al_Form::INSERT);'.$this->nl;
			$body.= '$this->form->setAction($this->pathController.\'insert\');'.$this->nl;
			$body.= '$this->form->setCancelAction($this->pathController.\'browse\');'.$this->nl.$this->nl;
			
			$body.= 'if($this->form->posted()) {'.$this->nl;
			$body.= $this->tab.'if($this->form->valid()) {'.$this->nl;
			$body.= $this->tab.$this->tab.'$record = new Bl_Record_'.$this->_recordClassName.'();'.$this->nl;
			$body.= $this->tab.$this->tab.'$record->setFromArray($this->form->getDbDataArray());'.$this->nl;
			$body.= $this->tab.$this->tab.'$record->save();'.$this->nl.$this->nl;
			$body.= $this->tab.$this->tab.'$this->message->add(\''.$this->_nameSingle.' created successfully.\')->save();'.$this->nl;
			$body.= $this->tab.$this->tab.'Al_Utilities::redirect($this->pathController.\'browse\');'.$this->nl;
			$body.= $this->tab.'} else {'.$this->nl;
			$body.= $this->tab.$this->tab.'$this->message->addRecordMessages($this->form->getValidationErrors());'.$this->nl;
			$body.= $this->tab.'}'.$this->nl;
			$body.= '} else {'.$this->nl;
			$body.= $this->tab.'$record = new Bl_Record_'.$this->_recordClassName.'();'.$this->nl;
			$body.= $this->tab.'$this->form->setFromDbArray($record->getDataArray());'.$this->nl;
			$body.= '}'.$this->nl.$this->nl;
			
			$body.= '$this->_template->extjs_attach_on_ready(\'Ext.al.pageReady();\');'.$this->nl;
			$body.= '$this->load_page(\''.$this->_templatePath.'form.php\');'.$this->nl;
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('insertAction');
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}			
		
		private function setUpdateAction() {
			$body = '$this->_user->level(\'1\');'.$this->nl.$this->nl;
			
			$body.= '$this->nav = new Al_Navigation();'.$this->nl;
			$body.= '$this->nav->add(\''.$this->_namePlural.'\',$this->pathController.\'browse\');'.$this->nl;
			$body.= '$this->nav->add(\'Update\');'.$this->nl.$this->nl;
						
			$body.= '$this->form = new Bl_Form_'.$this->_formClassName.'();'.$this->nl;
			$body.= '$this->form->setMode(Al_Form::UPDATE);'.$this->nl;
			$body.= '$this->form->setId(Al_Utilities::get(\'id\'));'.$this->nl;
			$body.= '$this->form->setAction($this->pathController.\'update/id/\'.$this->form->getId());'.$this->nl;
			$body.= '$this->form->setCancelAction($this->pathController.\'browse\');'.$this->nl.$this->nl;
			
			$body.= 'if($this->form->posted()) {'.$this->nl;
			$body.= $this->tab.'if($this->form->valid()) {'.$this->nl;
			$body.= $this->tab.$this->tab.'$record = new Bl_Record_'.$this->_recordClassName.'();'.$this->nl;
			$body.= $this->tab.$this->tab.'if($record->load($this->form->getId())) {'.$this->nl;
			$body.= $this->tab.$this->tab.$this->tab.'$record->setFromArray($this->form->getDbDataArray());'.$this->nl;
			$body.= $this->tab.$this->tab.$this->tab.'$record->save();'.$this->nl.$this->nl;
			$body.= $this->tab.$this->tab.$this->tab.'$this->message->add(\''.$this->_nameSingle.' updated successfully.\')->save();'.$this->nl;
			$body.= $this->tab.$this->tab.$this->tab.'Al_Utilities::redirect($this->pathController.\'browse\');'.$this->nl;
			$body.= $this->tab.$this->tab.'} else {'.$this->nl;
			$body.= $this->tab.$this->tab.$this->tab.'$this->message->add(\'Error saving '.strtolower($this->_nameSingle).'.\')->save();'.$this->nl;
			$body.= $this->tab.$this->tab.$this->tab.'Al_Utilities::redirect($this->pathController.\'browse\');'.$this->nl;
			$body.= $this->tab.$this->tab.'}'.$this->nl;
			$body.= $this->tab.'} else {'.$this->nl;
			$body.= $this->tab.$this->tab.'$this->message->addRecordMessages($this->form->getValidationErrors());'.$this->nl;
			$body.= $this->tab.'}'.$this->nl;
			$body.= '} else {'.$this->nl;
			$body.= $this->tab.'$record = new Bl_Record_'.$this->_recordClassName.'();'.$this->nl;
			$body.= $this->tab.'if($record->load($this->form->getId())) {'.$this->nl;
			$body.= $this->tab.$this->tab.'$this->form->setFromDbArray($record->getDataArray());'.$this->nl;
			$body.= $this->tab.'} else {'.$this->nl;
			$body.= $this->tab.$this->tab.'$this->message->add(\'Error loading '.strtolower($this->_nameSingle).'.\')->save();'.$this->nl;
			$body.= $this->tab.$this->tab.'Al_Utilities::redirect($this->pathController.\'browse\');'.$this->nl;
			$body.= $this->tab.'}'.$this->nl;
			$body.= '}'.$this->nl.$this->nl.$this->nl;
			
			$body.= '$this->_template->extjs_attach_on_ready(\'Ext.al.pageReady();\');'.$this->nl;
			$body.= '$this->load_page(\''.$this->_templatePath.'form.php\');'.$this->nl;
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('updateAction');
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}			
		
		private function setDeleteAction() {
			$body = '$this->_user->level(\'1\');'.$this->nl.$this->nl;
			
			$body.= '$record = new Bl_Record_'.$this->_recordClassName.'();'.$this->nl;
			$body.= '$record->setId(Al_Utilities::get(\'id\'));'.$this->nl;
			$body.= '$record->delete();'.$this->nl.$this->nl;
			
			$body.= '$this->message->add(\''.$this->_nameSingle.' deleted successfully.\')->save();'.$this->nl;
			$body.= 'Al_Utilities::redirect($this->pathController.\'browse\');'.$this->nl;
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('deleteAction');
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}
		
		private function setSearch() {
			$body = '$searchTriggered = false;'.$this->nl.$this->nl;
			
			foreach($this->_tableSchema as $field) {
				$name = $field['field_name'];				
				if(array_key_exists('use__'.$name,$_POST)) {				
					if(array_key_exists('c_search_field__'.$name,$_POST)) {
						
						$body.= '$field = Al_Utilities::tget(\''.$name.'\'); '.$this->nl;
						$body.= 'if($field != \'\') {'.$this->nl;
						$body.= $this->tab.'$searchTriggered = true;'.$this->nl;
						$body.= $this->tab.'$qb->where("'.$this->_tableAlias.'.'.$name.' like \'%".Al_Db::escape($field)."%\'");'.$this->nl;
						$body.= '}'.$this->nl.$this->nl;						
					}
				}
			}			
			
			$body.= 'return $searchTriggered;'.$this->nl;
			
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('setSearch');
			$method->setParameters( array(
				array(
					'name' => 'qb',
					'PassedByReference' => true
				),
			));					
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}		
		
		private function setCallback() {
			$body = '';
			$method = new Zend_CodeGenerator_Php_Method();
			$method->setName('onBrowseCallback');
			$method->setParameters( array(
				array(
					'name' => 'row',
					'PassedByReference' => true
				),
			));					
			$method->setBody($body);
			$this->_clsRecord->setMethod($method);					
		}
		
		private function setControllerClass() {
			$this->_clsRecord = new Zend_CodeGenerator_Php_Class();
			$this->_clsRecord->setName($this->_className);
			$this->_clsRecord->setExtendedClass('Al_ControllerAction');
		}		
		
		public function display($content,$title='') {
			if($title != '') {
				echo '<div style="font-weight:bold; font-family:Verdana; font-size:12px; padding:4px; display:block; margin-top:10px;">'.$title.'</div>';
			}
            echo '<textarea name="textfield" id="textfield" style="width:100%; height:50;">';
			echo htmlentities($content,ENT_QUOTES,'UTF-8');
            echo '</textarea>';			
		}		
		
		public function setTableSchema() {
			$res = $this->_db->query("
				DESCRIBE ".$this->_tableName."
			");
			$this->_tableSchema = array();
			while($row = $res->fetch_row()) {
				$row['var_name'] = $this->getVarName($row['Field']);
				$row['field_name'] = $row['Field'];
				$row['field_label'] = ucwords(str_replace('_',' ',$row['field_name']));
				$this->setFieldType($row);
				$this->_tableSchema[$row['Field']] = $row;
			}			
		}
		
		public function setFieldType(&$field) {
			$type = explode('(',$field['Type']);
			$dbType = strtolower(trim($type[0]));
			if(array_key_exists('1',$type)) {
				$dbTypeParm = $type[1];
			} else {
				$dbTypeParm = '';
			}
			if($field['Key']=='PRI') {
				$type = Al_Record_Field::ID;
			} else {
				switch ($dbType) {
					case 'varchar':
	                case 'char':
	                	$type = Al_Record_Field::STRING;
						$field['max_length'] = intval($dbTypeParm);
	                    break;
					case 'text':
						$type = Al_Record_Field::TEXT;
						break;	                        
	                case 'bigint':
	                case 'int':                        
	                case 'smallint': 
	                case 'tinyint':                  
	                	$type = Al_Record_Field::INT;
	                    break;
					case 'decimal':
						$type = Al_Record_Field::FLOAT;
						$dp = explode(',',$dbTypeParm);
						$field['decimal_points'] = intval($dp[1]);
	                    break; 
	                case 'date':
	                	$type = Al_Record_Field::DATE;
	                	break;
					case 'timestamp':                	
	                case 'datetime':
	                	$type = Al_Record_Field::DATE_TIME;
	                    break;
					case 'time':
	                	$type = Al_Record_Field::TIME;
						break;    				
				}
			}
			$field['type'] = $type;
		}
		
		public function getVarName($string) {
			$string = str_replace('_', ' ', strtolower($string));
	        $string = preg_replace('/[^a-z0-9 ]/', '', $string);	
	        $string = str_replace(' ', '', ucwords($string));
	        return $string;			
		}		
	}