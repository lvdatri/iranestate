<?php
    class Al_User {
        private $logged_in = false;
		private $data = array();
		private $settings;
		private static $instance;
		        
		public static function get_instance() {
	        if (!isset(self::$instance)) {
	            $c = __CLASS__;
	            self::$instance = new $c;
	        }
	        return self::$instance;
	    }
		
		public function __construct() {
		    $this->settings = Bl_Settings::get_instance(); 
		    
		    if($this->logged_in()) {
				$this->logged_in = true;
				$this->data = $_SESSION[$this->settings->session][$this->settings->session_user];
		    } else {
		        $this->logged_in = false;
		    }
		}
		
		static function logged_in() {
			$settings = Bl_Settings::get_instance();
		    if(@isset($_SESSION[$settings->session][$settings->session_user]['_logged_in']) && 
				$_SESSION[$settings->session][$settings->session_user]['_logged_in'] == true) {
                return true;
			} else {
			    return false;
			}
		}
		
    	public function set_data($data) {
			$this->data = $data;
			$this->data['_logged_in'] = true;
			
			$permissions = explode(',',$data['permissions']);
			
			$this->data['_access_levels'] = array();
			foreach($permissions as $permission) {
				$this->data['_access_levels'][$permission] = true;
			}
			$this->save();
		}

    	/**
		 * saves data to users session
		 */
		public function save() {
			$_SESSION[$this->settings->session][$this->settings->session_user] = $this->data;
		}		
		
    	/**
		 * on invalid login user will automatically be redirect to login or denied page
		 *
		 * @param string $access comma seperated access values
		 */
		public function level($access,$save_page_path=true) {
			if($this->logged_in) {
				if(!$this->allow($access)) {
					// insufficent user access
                    Al_Utilities::redirect($this->settings->user_denied_path);
                    
				}
				if($save_page_path) {
			        Al_Utilities::set_return_path(Al_Utilities::get_current_url_path());
			    }				
			}
			else {
			    if($save_page_path) {
			        Al_Utilities::set_return_path(Al_Utilities::get_current_url_path());
			    }
                Al_Utilities::redirect($this->settings->user_login_path);
			}
		}
		
    	/**
		 * checks if user has a requeste access
		 *
		 * @param string $access comma seperated access values
		 * @return boolean
		 */
		public function allow($access) {
			$access = explode(',',$access);
			if(count($access)>0) {
				foreach($access as $value) {
					if(@isset($this->data['_access_levels'][$value])) {
						return true;
					}
				}
			}
			return false;			
		}
		
    	/**
		 * retrieves user setting
		 *
		 * @param string $setting
		 */
		static function get($setting) {
			$settings = Bl_Settings::get_instance();
		    if(@isset($_SESSION[$settings->session][$settings->session_user][$setting])) {
		        return $_SESSION[$settings->session][$settings->session_user][$setting];
		    } else {
		        return '';
		    }
		}
		
		
		
		/**
		 * sets user setting
		 * setting will be set to data array as well as to the session
		 */
		public function set($setting,$value) {
			$this->data[$setting] = $value;
			$this->save();
		}		
		
    	/**
		 * logs out user from system
		 */
		function logout() {
		    $return_path = Al_Utilities::get_return_path();
			$this->data = array();
			$this->save();
			$_SESSION[$this->settings->session]	= array();
			Al_Utilities::set_return_path($return_path);		
		}		
    }