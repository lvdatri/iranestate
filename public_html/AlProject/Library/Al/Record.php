<?php
    class Al_Record {
    	
    	public $_fields;
        public $_db;
        public $_qb;
        public $_db_table;
        public $_db_table_alias;
        public $_db_table_pk;
        public $_id;
        public $_data;
        
        public function __construct() {
        	$this->_id = null;
        	$this->_data = array();
            $this->_db = Al_Db::get_instance();
            $this->_db_table_pk = 'id';
            $this->_db_table_alias = 'r';
            $this->_fields = array();
            $this->initialise();
            $this->initialiseRecord();
        }
        
        public function initialise() {
        	
        }
        
        public function getId() {
        	return $this->_id;
        }
        
        public function setId($value) {
        	$this->_id = $value;
        }
        
        protected function _add_field($field) {
        	$this->_fields[$field->getName()] = $field;
        }
        
        protected function _set_field_value($field,$value,$escaped=false) {
        	$field = $this->_get_field($field);
        	if($field) {
        		$field->setValue($value);
        		$field->setEscaped($escaped);
        		
        		if($field->getType()==Al_Record_Field::ID) {
        			$this->_id = $value;
        		}
        	} 
        }
        
    	protected function _get_field_value($field) {
        	$field = $this->_get_field($field);
        	if($field) {
        		$value = $field->getValue();
        		if($field->getEscaped() && $value=='null') {
        			// value is set to null
        			$value = '';
        		} 
        		return $value;
        	} else {
        		return '';
        	}
        }        
        
        protected function _get_field($field) {
        	if(isset($this->_fields[$field])) {
        		return $this->_fields[$field];
        	} 
        	return false;
        }

		protected function _load_record() {
            $this->_id = null;
            
            $this->_set_qb_select();
            $this->_set_qb_from();
            
            $res = $this->_db->query($this->_qb->get_query());
            $this->_data = array();
            if($res->num_rows()>0) {
            	$this->loadFromArray($res->fetch_row());
//            	$this->_data = $res->fetch_row();
//                foreach($this->_fields as &$field) {
//                	$fieldName = $field->getName();
//                	if(array_key_exists($fieldName,$this->_data)) {
//                		$val = $this->_data[$fieldName];
//                		
//                		if(is_null($val)) {
//                			/**
//                			 * db value is null
//                			 */
//                			$field->setValue('null',true);
//                		} else {
//                			$field->setValue($val);
//                		}
//                	};
//                }
//                $this->_id = $this->_data[$this->_db_table_pk];
                return true;
            }
            return false;             
		}
		
    	public function loadFromArray($data) 
	    {
		    $this->_data = $data;
		    foreach($this->_fields as &$field) {
		        $fieldName = $field->getName();
		        if(array_key_exists($fieldName,$this->_data)) {
		            $val = $this->_data[$fieldName];
		            
		            if(is_null($val)) {
		                /**
		                 * db value is null
		                 */
		                $field->setValue('null',true);
		            } else {
		                $field->setValue($val);
		            }
		        };
		    }
		    $this->_id = $this->_data[$this->_db_table_pk];   	
	    }		

		protected function _set_qb_select() {
            $alias = '';
            if($this->_db_table_alias!='') {
                $alias = $this->_db_table_alias.'.';
            }
            
            foreach($this->_fields as $field) {
            	$this->_qb->select($alias.$field->getName());
            }
        }
        
    	public function qb_set_select(&$qb,$alias='') {
            if($alias == '') {
            	$alias = $this->_db_table_alias.'.';
            } else {
            	$alias .= '.';
            }
            
            foreach($this->_fields as $field) {
            	$qb->select($alias.$field->getName());
            }
        }        
        
        
        
        protected function _set_qb_from() {
            $alias = '';
            if($this->_db_table_alias!='') {
                $alias = $this->_db_table_alias;
            }            
            $this->_qb->from($this->_db_table.' '.$alias);
        }		
        
		protected function _update() {
            $this->_qb->where($this->_db_table_pk ." = '".Al_Db::escape($this->_id)."'");
            $this->_qb->update($this->_db_table);
            
            foreach($this->_fields as $field) {
            	if($field->getType() != Al_Record_Field::ID) {
            		$this->_qb->set($field->getName(),$field->getDbValue(),$field->getEscaped());
            	}
            }
//            echo $this->_qb->get_query();
            $res=$this->_db->query($this->_qb->get_query());            
        }        
        
        protected function _insert() {
			$this->_insert_single();       
        }
        
        protected function _insert_single() {     
            $this->_qb->insert_into($this->_db_table);
            foreach($this->_fields as $field) {
            	if($field->getType() != Al_Record_Field::ID) {
            		$this->_qb->set($field->getName(),$field->getDbValue(),$field->getEscaped());
            	}
            }

            $res=$this->_db->query($this->_qb->get_query());
            $this->_id = $res->insert_id();
            $this->_set_field_value($this->_db_table_pk,$this->_id);
        }
        
        public function getDataArray() {
			$data = array();
			foreach($this->_fields as $field) {
				$method = 'get'.$field->getRecordMethod();
				$data[$field->getName()] = $this->$method();
			}
			return $data;
        }

	    public function before_insert() {
	    }
	    
	    public function before_update() {
	    }
	    
	    
	    public function save()
	    {
	        $this->_qb = new Al_QueryBuilder();
	        if($this->_id === null) {
	        	$this->before_insert();
	            $this->_insert();
	        } else {
	        	$this->before_update();
	            $this->_update();
	        }
	    }
	    
	    public function delete() {
	    	$this->_delete();
	    }
	    
	    protected function _delete() {
	    	$this->_db->query("
	    		DELETE FROM ".$this->_db_table."
	    		WHERE ".$this->_db_table_pk." = '" . intval($this->getId()) . "'
	    	");
	    }
	    
	    public function setFromArray($data) {
	    	foreach($this->_fields as &$field) {
	    		if(array_key_exists($field->getName(),$data)) {
	    			$method = 'set'.$field->getRecordMethod();
	    			$this->$method($data[$field->getName()]);
//	    			echo $field->getName().' - get'.$field->getRecordMethod().' - '.$data[$field->getName()]. '<BR>';
//	    			$field->setValue($data[$field->getName()]);
	    		}
	    	}
	    }
	    
        public function _retrieveRecords($as_array=false) {
            $alias = '';
            if($this->_db_table_alias!='') {
                $alias = $this->_db_table_alias.'.';
            }
            
            foreach($this->_fields as &$field) {            
				$this->_qb->select($alias.$field->getName());
            }
            
            $this->_qb->from($this->_db_table.' '.$this->_db_table_alias);

            $res = $this->_db->query($this->_qb->get_query());
            if($as_array == false) {
                return $res;
            } else {
	            $data = array();
	            while($row = $res->fetch_row()) {
	                $data[] = $row;
	            }
	            return $data;                
            }            
        }

        public function _getDataValue($field,$escaped=true) {
        	if(array_key_exists($field,$this->_data)) {
        		if($escaped) {
	        		return htmlentities($this->_data[$field],ENT_QUOTES,'UTF-8');
        		} else {
        			return $this->_data[$field];
        		}
        	}
        	return '';
        }
	    
    }