<?php
	class Al_Navigation {
	    private $data;
	    private $template;
	    
	    public function __construct ($template="") {
            $this->data = array();
            
            $this->set_template($template);
	    }
	    
	    public function set_template($template) {
	        $this->template = '';
	    	if($template!='') {
	            $this->template = '_'.$template;
	        }	        
	    }
	    
	    public function add($label,$link="", $message="") {
	        $this->data[] = array(   
	            "label" => $label,
	            "link" => $link,
	            "message" => $message,
	            "on_click" => $this->set_confirm($link,$message),
	        );
	    }
	    
	    function show() {
	    	$settings = Bl_Settings::get_instance();
	        if(count($this->data)>0) {
	            require($settings->path_templates."Al/navigation".$this->template.".php");
	        }
	    }
	    
	    function set_confirm($link,$message) {
	        if($message != "") {
	            return 'onClick="'."if(confirm('" . $message ."'))  { location='" . $link . "'; } else { return false; }".'"';
	        }
	        return "";
	    }
	}