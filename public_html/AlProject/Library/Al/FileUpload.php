<?php
	class Al_FileUpload {
		public $tempPath;
		public $_settings; 
		public function __construct() {
			$this->_settings = Bl_Settings::get_instance();
			$this->tempPath = $this->_settings->path_temp;
			
			if(!is_writable($this->tempPath)) {
				echo 'Al_FileUpload path not writable - '. $this->tempPath;
				exit;
			}
			
		}
		
		public function saveTempFile($path) {
			$this->tempPath = tempnam($this->tempPath,'tmp');
			move_uploaded_file($path,$this->tempPath);
			return $this->tempPath;
		}
		
		public function getTempPath() {
			return $this->tempPath;
		}
		
		public function saveFile($source,$dest) {
			copy($source,$dest);
		}
		
		public function deleteFile($path) {
			if(file_exists($path)) {
				unlink($path);
			}
		}
	}