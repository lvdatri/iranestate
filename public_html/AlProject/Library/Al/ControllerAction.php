<?php
	class Al_ControllerAction {
		protected $_db;
		protected $_user;
		protected $_template;
		protected $_frontController;
//		protected $message;
		
		public $moduleName;
		public $controllerName;
		public $actionName;
		
		public $pathController;
		public $pathModule;
		public $pathAction;
		
		public $_menu;
		
		/*@var $settings Bl_Settings */
		protected $_settings;
		
		protected $_language;
		
		public function __construct() {
			$this->_db = Al_Db::get_instance();
			$this->_user = Al_User::get_instance();
			$this->_template = Al_Template::get_instance();
			$this->_language = Al_Language::get_instance();
		}
		
		public function setController($value) {
			$this->_controller = $value;
		}
		
		public function setModuleName($value) {
			$this->moduleName = $value;
		}
		
		public function setControllerName($value) {
			$this->controllerName = $value;
		}
		
		public function setActionName($value) {
			$this->actionName = $value;
		}
		
		public function getStr($key) {
			return $this->_language->getValue($key);			
		}
		
		public function set_settings(Bl_Settings $settings) {
			$this->_settings = $settings;
			if($this->_settings->frontend==true) {
				$this->_menu = new Bl_Menu();
			}			
		}
		
		public function load_page($path,$template_path=''){
//			Al_Utilities::a($this->_settings);
			if($template_path=='') {
				$template_path = $this->_settings->path_templates;
			}
			$this->_page_path = $template_path.$path;
			include($template_path.'Template.php');
		}	
		
		public function initialise($frontController) {
			$this->_frontController = $frontController;
			
			$this->setModuleName($this->_frontController->_module);				
			$this->setControllerName($this->_frontController->_controller);
			$this->setActionName($this->_frontController->_action);
			$this->set_settings($this->_frontController->settings);			
			
			$this->initialiseAction();
		}
		
		private function initialiseAction() {
			if($this->_settings->frontend == true) {
				$this->pathModule = $this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/';
			} else {
				$this->pathModule = $this->_settings->path_web_ssl;
			}
			
			
			if($this->moduleName!='default') {
				$this->pathModule.=$this->moduleName.'/';
			}
			
			if($this->controllerName=='index') {
				$this->pathController = $this->pathModule;
			} else {
				$this->pathController = $this->pathModule.$this->controllerName.'/';
			}
			
			$this->pathAction = $this->pathController.$this->actionName.'/';	
		}
		
		public function callAction($action) {
			$this->setActionName($action);
			$this->_frontController->_action = $action;
			$this->initialiseAction();
									
			$action = $this->_frontController->get_action_name($this->actionName);
			
			$this->$action();
		}
		
		public function getRequestUri($replace='',$ltrim=false) {
			$path = $this->_frontController->getRequestUri();
			
			if($ltrim) {
				$path = ltrim($path,'/');
			}

			if($replace == '') {
				return $path;
			} else {
				return str_replace('/',$replace,$path);
			}
		}

		public function getRequestUriNoLang() {
			
			$path = $this->_frontController->getRequestUri();
			$lang = '/'.$this->_settings->languagePrefix;
			$pos = strpos($path, $lang);

			if($pos !== false && $pos == 0) {
				$path = substr_replace ($path,'',0,3);
			}
			return $path;
		}
		
		public function beforeAction() {
			
		}
		
		public function afterAction() {
			
		}
	}