<?php

class Al_Response
{
    public $response;

    public function __construct()
    {
        $this->response = array();
        $this->response['success'] = 'true';
        $this->response['error'] = array(
            'status' => '0',
            'message' => '',
        );
    }

    public function access($status, $message = '')
    {
        if ($status == true) {
            $status = '1';
        } else {
            $status = '0';
        }

        $this->response['access'] = array(
            'status' => $status,
            'message' => $message
        );
    }

    public function setData($data)
    {
        $this->response['data'] = $data;
    }

    public function setError($message)
    {
        $this->response['errorMessage'] = $message;
        $this->response['success'] = 'false';
        $this->response['error'] = array(
            'status' => '1',
            'message' => $message
        );
    }

    public function addSection($group, $data)
    {
        $this->response[$group] = $data;
    }

    public function display()
    {
        echo $this->get();
    }

    public function get()
    {
        return json_encode($this->response);
    }
}