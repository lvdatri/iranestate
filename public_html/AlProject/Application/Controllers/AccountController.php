<?php
	class AccountController extends Al_ControllerAction {
		public $tabs;
		public $message;
		public $member;
		
		public function beforeAction() 
		{
			$this->message = new Bl_Message();
		}
		
		public function indexAction() 
		{
			$this->_user->level('1');

			$this->tabs = new Bl_Tabs();
			$this->tabs->setActive(Bl_Tabs::ACT_ACCOUNT);
			
			$this->member = new Bl_Record_Member();
			$this->member->load($this->_user->get('id'));
			
			$this->load_page('account/my-account/view.php');			
		}
		
		public function updateDetailsAction() 
		{
	        $this->_user->level('1');
	        
	        $language = Al_Language::get_instance();
	        
			$this->tabs = new Bl_Tabs();
			$this->tabs->setActive(Bl_Tabs::ACT_ACCOUNT);
	        
	        $this->form = new Bl_Form_Member();
	        $this->form->setMode(Al_Form::UPDATE);
	        $this->form->setId($this->_user->get('id'));
	        $this->form->setAction($this->pathController.'update-details');
	        $this->form->setCancelAction($this->pathController);
	        
	        if($this->form->posted()) {
	            if($this->form->valid()) {
	                $record = new Bl_Record_Member();
	                if($record->load($this->form->getId())) {
	                    $record->setFromArray($this->form->getDbDataArray());
	                    $record->save();
	        			
	                    
	                    $this->message->add($language->getValue(Al_Language::MSG_ACCOUNT_DETAILS_UPDATED))->save();
	                    Al_Utilities::redirect($this->pathController);
	                } else {
	                    $this->message->add($language->getValue(Al_Language::MSG_ERROR_SAVING_ACCOUNT_DETAILS))->save();
	                    Al_Utilities::redirect($this->pathController);
	                }
	            } else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }
	        } else {
	            $record = new Bl_Record_Member();
	            if($record->load($this->form->getId())) {
	                $this->form->setFromDbArray($record->getDataArray());
	            } else {
	                $this->message->add($language->getValue(Al_Language::MSG_ERROR_LOADING_ACCOUNT_DETAILS))->save();
	                Al_Utilities::redirect($this->pathController);
	            }
	        }
	        
	        $this->load_page('account/my-account/details-form.php');			
		}
		
		public function changePasswordAction() 
		{
	        $this->_user->level('1');
	        
	        $language = Al_Language::get_instance();
	        
			$this->tabs = new Bl_Tabs();
			$this->tabs->setActive(Bl_Tabs::ACT_ACCOUNT);
	        
	        $this->form = new Bl_Form_MemberPassword();
	        $this->form->setMode(Al_Form::UPDATE);
	        $this->form->setId($this->_user->get('id'));
	        $this->form->setAction($this->pathController.'change-password');
	        $this->form->setCancelAction($this->pathController);
	        
	        if($this->form->posted()) {
	            if($this->form->valid()) {
	                $record = new Bl_Record_Member();
	                if($record->load($this->form->getId())) {
	                    $record->setFromArray($this->form->getDbDataArray());
	                    $record->save();
	        			
	                    
	                    $this->message->add($language->getValue(Al_Language::MSG_ACCOUNT_PASSWORD_UPDATED))->save();
	                    Al_Utilities::redirect($this->pathController);
	                } else {
	                    $this->message->add($language->getValue(Al_Language::MSG_ERROR_SAVING_ACCOUNT_DETAILS))->save();
	                    Al_Utilities::redirect($this->pathController);
	                }
	            } else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }
	        } else {
	            $record = new Bl_Record_Member();
	            if($record->load($this->form->getId())) {
	                $this->form->setFromDbArray($record->getDataArray());
	            } else {
	                $this->message->add($language->getValue(Al_Language::MSG_ERROR_LOADING_ACCOUNT_DETAILS))->save();
	                Al_Utilities::redirect($this->pathController);
	            }
	        }
	        
	        $this->load_page('account/my-account/password-form.php');				
		}
		
		public function summaryAction() 
		{
			$this->_user->level('1');

			$this->tabs = new Bl_Tabs();
			$this->tabs->setActive(Bl_Tabs::ACT_SUMMARY);
						
			$this->property = new Bl_Record_Property();
			$this->properties = $this->property->retrieveMembersProperties($this->_user->get('id'),true);
			
			$this->load_page('account/summary.php');	
		}
	}