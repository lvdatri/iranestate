<?php
	class BatchController extends Al_ControllerAction {
		public function indexAction() {
		}
		
		public function recordAction() {
			$tableName = 'properties';
			$rg = new Al_Record_Generate();
			$rg->generateRecord($tableName,'AdminUser');
			$rg->generateSetters($tableName,'user');			
		}
		
		public function display($content,$title='') {
			if($title != '') {
				echo '<div style="font-weight:bold; font-family:Verdana; font-size:12px; padding:4px; display:block; margin-top:10px;">'.$title.'</div>';
			}
            echo '<textarea name="textfield" id="textfield" style="width:100%; height:100%;">';
			echo htmlentities($content,ENT_QUOTES,'UTF-8');
            echo '</textarea>';			
		}			
		
		
		public function testingAction() 
		{
			$path = $this->_settings->path_app.'Bl/Record/AdminUser.php';
			$path = $this->_settings->path_app.'Bl/Data/Test.php';

			$class =  new Bl_Data_Test();//new Bl_Record_AdminUser();
			
			$generator = Zend_CodeGenerator_Php_Class::fromReflection(
			    new Zend_Reflection_Class($class)
			);


			
			$generator->removeMethod('yeah');
			
//			$generator->removeMethod('yeah');
			
//			$methods = $generator->getMethods();
			
//			unset($methods['yeah']);
//			$generator->setMethods($methods);
//			Al_Utilities::a($generator->getMethods());
//			exit;
			
//			$method = $generator->getMethod('yeah');
//			$method->
			
//			if($generator->hasMethod('yeah')) {
//				echo 'havi it';
//				$generator->
//			}
			
//			$generator->
			
			$generator->setMethod(array(
    'name'       => 'setBaz',
    'parameters' => array(
        array('name' => 'baz'),
    ),
    'body'       => '$this->_baz = $baz;' . "\n" . 'return $this;',
    'docblock'   => new Zend_CodeGenerator_Php_Docblock(array(
        'shortDescription' => 'Set the baz property',
        'tags'             => array(
            new Zend_CodeGenerator_Php_Docblock_Tag_Param(array(
                'paramName' => 'baz',
                'datatype'  => 'string'
            )),
            new Zend_CodeGenerator_Php_Docblock_Tag_Return(array(
                'datatype'  => 'string',
            )),
        ),
    )),
));
			
//			$generator = Zend_CodeGenerator_Php_File::fromReflectedFileName($path);
//			$generator->			
			
//			$generator = Zend_CodeGenerator_Php_Class::fromReflection(
//			    new Zend_Reflection_Class('Bl_Data_Test')
//			);			
			
			$code = $generator->generate();
			$this->display($code);
		}
		
		public function formAction() {
			
			
			
//			$tableName = 'logs';
//			$formClassName = 'Log';
//			$recordClassName = 'Log';
//			$recordObjectName = 'log';
//			$controllerClassName = 'LogController';
//			$controllerRecordNameSingle = 'Log';
//			$controllerRecordNamePlural = 'Logs';
//			$controllerTemplatePath ='logs/';		
//			$controllerTableAlias = 'l';
			
			
//			$tableName = 'cms';
//			$formClassName = 'Cms';
//			$recordClassName = 'Cms';
//			$recordObjectName = 'cms';
//			$controllerClassName = 'NewsController';
//			$controllerRecordNameSingle = 'News Item';
//			$controllerRecordNamePlural = 'News Items';
//			$controllerTemplatePath ='news/';		
//			$controllerTableAlias = 'c';
			
//			$tableName = 'property_logs';
//			$formClassName = 'PropertyLog';
//			$recordClassName = 'PropertyLog';
//			$recordObjectName = 'log';
//			$controllerClassName = 'PropertyLogController';
//			$controllerRecordNameSingle = 'Property Log';
//			$controllerRecordNamePlural = 'Property Logs';
//			$controllerTemplatePath ='property-logs/';		
//			$controllerTableAlias = 'l';
			
			

			$tableName = 'members';
			$formClassName = 'Member';
			$recordClassName = 'Member';
			$recordObjectName = 'member';
			$controllerClassName = 'MembersController';
			$controllerRecordNameSingle = 'Member';
			$controllerRecordNamePlural = 'Members';
			$controllerTemplatePath ='members/';
			$controllerTableAlias = 'm';
			
			
//			$tableName = 'members';
//			$formClassName = 'Admin_Member';
//			$recordClassName = 'Member';
//			$recordObjectName = 'member';
//			$controllerClassName = 'MembersController';
//			$controllerRecordNameSingle = 'Member';
//			$controllerRecordNamePlural = 'Members';
//			$controllerTemplatePath ='members/';		
//			$controllerTableAlias = 'm';			
			
//			$tableName = 'properties';
//			$formClassName = 'Admin_Property';
//			$recordClassName = 'Property';
//			$recordObjectName = 'property';
//			$controllerClassName = 'PropertiesController';
//			$controllerRecordNameSingle = 'Property';
//			$controllerRecordNamePlural = 'Properties';
//			$controllerTemplatePath ='properties/';		
//			$controllerTableAlias = 'p';
			
			
//			$tableName = 'property_images';
//			$formClassName = 'Admin_PropertyImage';
//			$recordClassName = 'PropertyImage';
//			$recordObjectName = 'propertyImage';
//			$controllerClassName = 'PropertyImagesController';
//			$controllerRecordNameSingle = 'Property Image';
//			$controllerRecordNamePlural = 'Property Images';
//			$controllerTemplatePath ='property-images/';		
//			$controllerTableAlias = 'i';			
			
//			$tableName = 'settings';
//			$formClassName = 'Admin_Settings';
//			$recordClassName = 'Setting';
//			$recordObjectName = 'setting';
//			$controllerClassName = 'SettingsController';
//			$controllerRecordNameSingle = 'Setting';
//			$controllerRecordNamePlural = 'Settings';
//			$controllerTemplatePath ='settings/';		
//			$controllerTableAlias = 's';			
			
//			$tableName = 'property_price_types';
//			$formClassName = 'Admin_PropertyPriceType';
//			$recordClassName = 'PropertyPriceType';
//			$recordObjectName = 'propertyPriceType';
//			$controllerClassName = 'PropertyPriceTypesController';
//			$controllerRecordNameSingle = 'Property Price Type';
//			$controllerRecordNamePlural = 'Property Price Types';
//			$controllerTemplatePath ='property-price-types/';		
//			$controllerTableAlias = 'p';			
			
//			$tableName = 'property_statuses';
//			$formClassName = 'Admin_PropertyStatus';
//			$recordClassName = 'PropertyStatus';
//			$recordObjectName = 'propertyStatus';
//			$controllerClassName = 'PropertyStatusesController';
//			$controllerRecordNameSingle = 'Property Status';
//			$controllerRecordNamePlural = 'Property Statuses';
//			$controllerTemplatePath ='property-statuses/';		
//			$controllerTableAlias = 'p';				
			
//			$tableName = 'property_transactions';
//			$formClassName = 'Admin_PropertyTransaction';
//			$recordClassName = 'PropertyTransaction';
//			$recordObjectName = 'propertyTransaction';
//			$controllerClassName = 'PropertyTransactionsController';
//			$controllerRecordNameSingle = 'Property Transaction';
//			$controllerRecordNamePlural = 'Property Transactions';
//			$controllerTemplatePath ='property-transactions/';		
//			$controllerTableAlias = 'p';				
			
//			$tableName = 'property_features';
//			$formClassName = 'Admin_PropertyFeature';
//			$recordClassName = 'PropertyFeature';
//			$recordObjectName = 'propertyFeature';
//			$controllerClassName = 'PropertyFeaturesController';
//			$controllerRecordNameSingle = 'Property Feature';
//			$controllerRecordNamePlural = 'Property Features';
//			$controllerTemplatePath ='property-features/';		
//			$controllerTableAlias = 'p';		
			
//			$tableName = 'property_introduction';
//			$formClassName = 'Admin_PropertyIntroduction';
//			$recordClassName = 'PropertyIntroduction';
//			$recordObjectName = 'propertyIntroduction';
//			$controllerClassName = 'PropertyIntroductionsController';
//			$controllerRecordNameSingle = 'Property Introduction';
//			$controllerRecordNamePlural = 'Property Introductions';
//			$controllerTemplatePath ='property-introductions/';		
//			$controllerTableAlias = 'p';			
						
			
//			$tableName = 'property_types';
//			$formClassName = 'Admin_PropertyType';
//			$recordClassName = 'PropertyType';
//			$recordObjectName = 'propertyType';
//			$controllerClassName = 'PropertyTypesController';
//			$controllerRecordNameSingle = 'Property Type';
//			$controllerRecordNamePlural = 'Property Types';
//			$controllerTemplatePath ='property-types/';		
//			$controllerTableAlias = 'p';			
//			
			
//			$tableName = 'zones';
//			$formClassName = 'Admin_Zone';
//			$recordClassName = 'Zone';
//			$recordObjectName = 'zone';
//			$controllerClassName = 'ZonesController';
//			$controllerRecordNameSingle = 'Zone';
//			$controllerRecordNamePlural = 'Zones';
//			$controllerTemplatePath ='zones/';		
//			$controllerTableAlias = 'z';				
			
			
			
//			$tableName = 'cities';
//			$formClassName = 'Admin_City';
//			$recordClassName = 'City';
//			$recordObjectName = 'city';
//			$controllerClassName = 'CitiesController';
//			$controllerRecordNameSingle = 'City';
//			$controllerRecordNamePlural = 'Cities';
//			$controllerTemplatePath ='cities/';		
//			$controllerTableAlias = 'c';				
			
//			$tableName = 'countries';
//			$formClassName = 'Admin_Country';
//			$recordClassName = 'Country';
//			$recordObjectName = 'country';
//			$controllerClassName = 'CountriesController';
//			$controllerRecordNameSingle = 'Country';
//			$controllerRecordNamePlural = 'Countries';
//			$controllerTemplatePath ='countries/';		
//			$controllerTableAlias = 'c';	
			
//			$tableName = 'admin_users';
//			$formClassName = 'Admin_AdminUser';
//			$recordClassName = 'AdminUser';
//			$recordObjectName = 'user';
//			$controllerClassName = 'UsersController';
//			$controllerRecordNameSingle = 'User';
//			$controllerRecordNamePlural = 'Users';
//			$controllerTemplatePath ='users/';
//			$controllerTableAlias = 'u';
			
			$fg = new Al_Form_Generate();
			if($fg->posted()) {
				echo '<a href="'.$this->pathAction.'">Back</a>';
				$fg->generateForm($tableName,$formClassName);
				
				$rg = new Al_Record_Generate();
				$rg->generateRecord($tableName,$recordClassName);
				$rg->generateSetters($tableName,$recordObjectName);	

				$cg = new Al_ControllerGenerate();
				$cg->_tableName = $tableName;
				$cg->_tableAlias = $controllerTableAlias;
				$cg->_className = $controllerClassName;
				$cg->_nameSingle = $controllerRecordNameSingle;
				$cg->_namePlural = $controllerRecordNamePlural;
				$cg->_formClassName = $formClassName;
				$cg->_recordClassName = $recordClassName;
				$cg->_templatePath = $controllerTemplatePath; 
				$cg->generateController();
				$cg->generateBrowsePage();
				
			} else {
				if(isset($_GET['clear'])) {
					$fg->clearSaved();					
				}
				$this->data = $fg->getFormTableSchema($tableName);
				$this->load_page('batch/form.php');				
			}
		}
		
		public function setCb($field) {
			if(isset($_POST[$field])) {      
         		echo ' checked="checked" ';
        	} 
		}
		
		public function setDd($field,$value) {
			if(isset($_POST[$field])) {   
				if($_POST[$field]==$value) {   
         			echo ' selected="selected" ';
				}
        	} 
		}
	}