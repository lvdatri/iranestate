<?php
	class PropertiesController extends Al_ControllerAction 
	{
		public $language;
		public $property;
		public function beforeAction() 
		{	
			$this->language = Al_Language::get_instance();
			$this->prices = new Bl_Data_Prices();
			$this->bedBathPark = new Bl_Data_BedBathPark();
			$this->propertyTypes = new Bl_Record_PropertyType();
		}
		
//		public function testEmailAction()
//		{
//			$subject = 'this is a test';
//			$html = 'testing a dummy message <BR> testin second line';
//			$html = '&#1585;&#1605;&#1586; &#1593;&#1576;&#1608;&#1585; &#1601;&#1593;&#1604;&#1740;';
//			
//			Al_Utilities::initialiseMail();
//			$mail = new Zend_Mail();
//			$mail->setSubject($subject);
//			$mail->setBodyHtml($html);
//			$mail->setFrom("stricak@gmail.com", 'Tim');
//			$mail->addTo("darko@accesslogic.com.au", 'Darko');
//			$mail->send();			
//		}

		public function apiMapAction()
		{
			$api = new Bl_Api_Map();
			$api->process();
		}		
		
		public function indexAction() 
		{
			$this->load_page('index.php');
		}
		
// 		public function buyAction() 
// 		{
// 			$this->listingsCommon(
// 				Bl_Record_Property::BUY,
// 				Bl_Record_Property::BUY_PATH,
// 				Bl_Data_Prices::BUY,
// 				Bl_Menu::BUY,
// 				$this->getStr(Al_Language::HOME_TITLE_FEATURED_PROP_BUY)
// 			);
// 		}
		
		public function buyAction()
		{
			$this->listingsCommon(
					Bl_Record_Property::BUY,
					Bl_Record_Property::BUY_PATH,
					Bl_Data_Prices::BUY,
					Bl_Menu::BUY,
					$this->getStr(Al_Language::HOME_TITLE_FEATURED_PROP_BUY),
					true
			);
		}		
		
// 		public function rentAction() 
// 		{
// 			$this->listingsCommon(
// 				Bl_Record_Property::RENT,
// 				Bl_Record_Property::RENT_PATH,
// 				Bl_Data_Prices::RENT,
// 				Bl_Menu::RENT,
// 				$this->getStr(Al_Language::HOME_TITLE_FEATURED_PROP_RENT)
// 			);
// 		}

		public function rentAction()
		{
			$this->listingsCommon(
					Bl_Record_Property::RENT,
					Bl_Record_Property::RENT_PATH,
					Bl_Data_Prices::RENT,
					Bl_Menu::RENT,
					$this->getStr(Al_Language::HOME_TITLE_FEATURED_PROP_RENT),
					true
			);
		}		
		
// 		public function bookAction() 
// 		{
// 			$this->listingsCommon(
// 				Bl_Record_Property::BOOK,
// 				Bl_Record_Property::BOOK_PATH,
// 				Bl_Data_Prices::BOOK,
// 				Bl_Menu::BOOK,
// 				$this->getStr(Al_Language::HOME_TITLE_FEATURED_PROP_BOOK)
// 			);
// 		}
		
		public function bookAction()
		{
			$this->listingsCommon(
					Bl_Record_Property::BOOK,
					Bl_Record_Property::BOOK_PATH,
					Bl_Data_Prices::BOOK,
					Bl_Menu::BOOK,
					$this->getStr(Al_Language::HOME_TITLE_FEATURED_PROP_BOOK),
					true
			);
		}		
		
		public function postAction() 
		{
			$this->_menu->setActive(Bl_Menu::POST);
			$this->load_page('properties/post.php');
		}		
		
		public function contactCountAction() 
		{
			$this->property = new Bl_Record_Property();
			if($this->property->load(Al_Utilities::get('id'))) {		
				$propertyLog = new Bl_Record_PropertyLog();
				$propertyLog->setCommon();
				$propertyLog->setType(Bl_Record_PropertyLog::PROPERTY_CONTACT);
				$propertyLog->setPropertyId($this->property->getId());
				$propertyLog->save();
			}
		}
		
		private function getPropertyId() {
			$path = trim($this->_frontController->getPathInfo(),'/'); 
		    $pos = strrpos($path,'/');
		    if($pos === false) {
				return false;		    	
		    } else {
		    	return substr($path, $pos + 1);
		    }
		}		
		
		public function listingsCommon($recordPropertyType,$recordPropertyPath,$dataPriceType,$menuType,$featTitle,$map=false) 
		{
			$this->message = new Bl_Message();
			$this->prices->setActiveType($dataPriceType);			
			$this->_menu->setActive($menuType);
			
			$this->transactionPath = $recordPropertyPath;	
			$this->property = new Bl_Record_Property();		
			
			$this->featTitle = $featTitle;
			
			$this->recordPropertyType = $recordPropertyType;
			
			$this->propertySearch = new Bl_PropertySearch();
			$this->propertySearch->setTransactionType($recordPropertyType);
			
			
			if(isset($_GET['advanced-search'])) {
				// advanced search
				$pi = new Bl_Record_PropertyIntroduction();
        		$this->propIntroduction = $pi->retrieveCbGroupValues($this->_settings->language);
        		$noOfRecords = count($this->propIntroduction);
				$this->propIntroduction = array_chunk($this->propIntroduction, ceil($noOfRecords / 4));

        		$pf = new Bl_Record_PropertyFeature();
		        $this->propFeatures = $pf->retrieveCbGroupValues($this->_settings->language);
		        $noOfRecords = count($this->propFeatures);
				$this->propFeatures = array_chunk($this->propFeatures, ceil($noOfRecords / 4));
		        
		        $pt = new Bl_Record_PropertyType();
		        $this->propTypes = $pt->retrieveCbGroupValues($this->_settings->language);
		        $noOfRecords = count($this->propTypes);
				$this->propTypes = array_chunk($this->propTypes, ceil($noOfRecords / 4));
				
				$this->propertySearch->setPropIntros($this->propIntroduction);
				$this->propertySearch->setPropFeatures($this->propFeatures);
				$this->propertySearch->setPropTypes($this->propTypes);
								
				$this->load_page('properties/advanced-search.php');
			} else if($this->propertySearch->requested()) {
				// search
				$qb = $this->propertySearch->getQb();
				$this->propertySearch->save();
				
				$this->_settings->tag_title = Al_Utilities::html($this->propertySearch->getSearchTitle());  
				$this->_settings->tag_description = $this->_settings->tag_title;

				
				$pi = new Bl_Record_PropertyIntroduction();
        		$this->propIntroduction = $pi->retrieveCbGroupValues($this->_settings->language);
        		$noOfRecords = count($this->propIntroduction);
				$this->propIntroduction = array_chunk($this->propIntroduction, ceil($noOfRecords / 2));

        		$pf = new Bl_Record_PropertyFeature();
		        $this->propFeatures = $pf->retrieveCbGroupValues($this->_settings->language);
		        $noOfRecords = count($this->propFeatures);
				$this->propFeatures = array_chunk($this->propFeatures, ceil($noOfRecords / 2));
		        
		        $pt = new Bl_Record_PropertyType();
		        $this->propTypes = $pt->retrieveCbGroupValues($this->_settings->language);
		        $noOfRecords = count($this->propTypes);
				$this->propTypes = array_chunk($this->propTypes, ceil($noOfRecords / 2));
				
				$this->propertySearch->setPropIntros($this->propIntroduction);
				$this->propertySearch->setPropFeatures($this->propFeatures);
				$this->propertySearch->setPropTypes($this->propTypes);
				
				$this->propertySort = new Bl_Data_PropertySort();

				$this->pager = new Al_Pager();
		        $this->pager->setPath($this->pathController.$this->transactionPath.'/');
		        $this->pager->count_query("
		            SELECT count(r.id) ".
		            $qb->from.$qb->join.$qb->where
		        );
		        
				$this->pager->set_url_params($this->propertySearch->getAllParams());
		        $this->pager->set_from_get();
		        $this->pager->run();
		        
		        $qb->limit($this->pager->get_limit());
		        $this->properties = $this->_db->query($qb->get_query());
		        
		        
		        if($map) {
		        	$this->load_page('properties/listings-map.php');
		        } else {
		        	$this->load_page('properties/listings.php');
		        }
			} else if(count($_GET) === 0) {
				// home page
				

				
				if($recordPropertyType == Bl_Record_Property::BOOK) {
					$this->featured = $this->property->retrieveBookFeatured(9,3);	
					$this->_settings->tag_title = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_TITLE_BOOK));
					$this->_settings->tag_keywords = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_KEYWORDS_BOOK));
					$this->_settings->tag_description = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_DESCRIPTION_BOOK)); 
					
				} else if($recordPropertyType == Bl_Record_Property::RENT) {
					$this->featured = $this->property->retrieveRentFeatured(9,3);
					$this->_settings->tag_title = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_TITLE_RENT));		
					$this->_settings->tag_keywords = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_KEYWORDS_RENT));
					$this->_settings->tag_description = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_DESCRIPTION_RENT));								
				} else {
					$this->featured = $this->property->retrieveBuyFeatured(9,3);
					$this->_settings->tag_title = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_TITLE_BUY));
					$this->_settings->tag_keywords = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_KEYWORDS_BUY));
					$this->_settings->tag_description = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_DESCRIPTION_BUY));										
				}

				$cms = new Bl_Record_Cms();
				$this->news = $cms->retrieveByType(Bl_Data_CmsTypes::news,3);
				
			
				$this->city = new Bl_Record_City();
				$this->featuredNationalCities = $this->city->retrieveNatioanlFeatured();
				$noOfRecords = count($this->featuredNationalCities);
				$this->featuredNationalCities = array_chunk($this->featuredNationalCities, ceil($noOfRecords / 3));
				
				$this->featuredInternationalCities = $this->city->retrieveInternatioanlFeatured();
				$noOfRecords = count($this->featuredInternationalCities);
				$this->featuredInternationalCities = array_chunk($this->featuredInternationalCities, ceil($noOfRecords / 3));
				

				
				
//				Al_Utilities::a($this->featuredNationalCities);
				
				$this->propertySearch->clearSearch();
//				$this->load_page('properties/'.$this->transactionPath.'.php');
				$this->load_page('properties/home.php');				
			} else {
				/**
				 * display property listing
				 */
				$pid = $this->getPropertyId();
				
				$this->propertySearch->loadFromSession();
				
				$this->property = new Bl_Record_Property();				
				if($pid && $this->property->loadByPropertyId($pid)) {
					$this->_settings->tag_title = Al_Utilities::html(
						$this->property->getCountry().', '.$this->property->getCity().' - '.
						$this->property->getPropertyType() . ' ' . $this->property->getTransactionType() . ', '.
						$this->property->getAddress(). ' - '.
						$this->property->getPropertyId()
					);
					$this->_settings->tag_description = $this->_settings->tag_title;
					
					
					$propertyImages = new Bl_Record_PropertyImage();
					$this->images = $propertyImages->retrieveRecordsByProperty($this->property->getId());
					if(count($this->images)==0) {
						$this->images = $propertyImages->retrieveEmptyImage();
					}

                    $this->member = new Bl_Record_Member();
                    $this->member->load($this->property->getMemberId());

					$propertyLog = new Bl_Record_PropertyLog();
					$propertyLog->setCommon();
					$propertyLog->setType(Bl_Record_PropertyLog::PROPERTY_VIEW);
					$propertyLog->setPropertyId($this->property->getId());
					$propertyLog->save();
					
					if(isset($_POST['contact'])) {
						$this->processContactForm();
					} else if(isset($_POST['tofriend'])) {
						$this->processToFriendFrom();
					}
					
					$this->load_page('properties/listing.php');
					
				} else {
					$this->load_page('404.php');
				}
			}			
		}		
		
		public function processContactForm() 
		{
			$this->form = new Bl_Form_PropertyContact();
	        $this->form->setMode(Al_Form::INSERT);
        
	        if($this->form->posted()) {
	            if($this->form->valid()) {
	                
	                $email = new Bl_Record_Email();
	                $email->load(Bl_Data_EmailTypes::property_contact_agent);
	                $email->addTag('member_name',$this->property->getMemberName());
	                $email->addTag('phone',$this->form->getContactPhone());
	                $email->addTag('description',$this->form->getContactDescription());
	                $email->addTag('property_url',$this->property->getUrl());
	                $email->addTag('subject',$this->form->getContactSubject());
	                
					Al_Utilities::initialiseMail();
					$mail = new Zend_Mail();
					$mail->setSubject($email->getSubject());
					$mail->setBodyHtml($email->getContent());
					$mail->setFrom($this->form->getContactEmail(), $this->form->getContactName());
				    $mail->addTo($this->property->getMemberEmail(), $this->property->getMemberName());
				    $mail->send();

				    $language = Al_Language::get_instance();
				    $this->message->add($language->getValue(Al_Language::MSG_PROP_CONTACT_EMAIL_SENT))->save();
					
				    Al_Utilities::redirect($this->property->getUrl());
				         	
	        	} else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }	            
	        }		
		}
		
		public function processToFriendFrom() 
		{
			$this->form = new Bl_Form_PropertyToFriend();
	        $this->form->setMode(Al_Form::INSERT);
        
	        if($this->form->posted()) {
	            if($this->form->valid()) {
	            	
 					$email = new Bl_Record_Email();
	                $email->load(Bl_Data_EmailTypes::property_send_to_friend);
	                $email->addTag('subject',$this->form->getFriendFriendsSubject());
	                $email->addTag('friends_name',$this->form->getFriendFriendsName());
	                $email->addTag('name',$this->form->getFriendYourName());
	                $email->addTag('email',$this->form->getFriendYourEmail());
	                $email->addTag('description',$this->form->getFriendDescription());
	                $email->addTag('property_url',$this->property->getUrl());
	                
					Al_Utilities::initialiseMail();
					$mail = new Zend_Mail();
					$mail->setSubject($email->getSubject());
					$mail->setBodyHtml($email->getContent());
					$mail->setFrom($this->form->getFriendYourEmail(), $this->form->getFriendYourName());
				    $mail->addTo($this->form->getFriendFriendsEmail(), $this->form->getFriendFriendsName());
				    $mail->send();

				    $language = Al_Language::get_instance();
				    $this->message->add($language->getValue(Al_Language::MSG_PROP_FRIEND_EMAIL_SENT))->save();
					
				    Al_Utilities::redirect($this->property->getUrl());	            	
	            	
	        	} else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }	            
	        }			
		}

		public function contactCaptchaAction() 
		{
			$captcha = new Al_MathCaptcha('properties-contact');
			$captcha->display();
		}
		
		public function friendCaptchaAction()
		{
			$captcha = new Al_MathCaptcha('properties-friend');
			$captcha->display();
		}
	}