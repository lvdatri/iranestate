<?php
	class ErrorController extends Al_ControllerAction {
		public function indexAction() {
			$this->cms = new Bl_Record_Cms();

	        $requestUri = trim($this->getRequestUriNoLang(),'/');

	        if($requestUri == 'sitemap.xml') {
				$sitemap = new Bl_Sitemap();
				$sitemap->show();
	        } else if($requestUri == 'buy.rss') {
				$this->displayRssFeed(Bl_Record_Property::BUY);   	 
	        } else if($requestUri == 'rent.rss') {
	        	$this->displayRssFeed(Bl_Record_Property::RENT);
	        } else if($requestUri == 'book.rss') {
	        	$this->displayRssFeed(Bl_Record_Property::BOOK);
	       	} else if($requestUri == 'all.rss') {
        		$this->displayRssFeed();
	        } else if($this->cms->loadByPath($requestUri)) {
				$this->displayCmsPage();
			} else {
				$this->load_page('404.php');
			}
		}
		
		private function displayRssFeed($type='') 
		{
			$rss = new Bl_Rss($this->_settings->language);
			$rss->display($type);
		}
		
		private function displayCmsPage() 
		{

			$this->_settings->tag_title = $this->cms->getTitle();
			$this->_settings->tag_keywords = $this->cms->getKeywords();
			$this->_settings->tag_description = $this->cms->getDescription();
	        
	        $page = '';
	        switch ($this->cms->getCmsType()) {
	        	case Bl_Data_CmsTypes::article :
	        		$page = 'articles';
	        		break;
	        	case Bl_Data_CmsTypes::services :
	        		$page = 'services';
	        		break;
	        	case Bl_Data_CmsTypes::news :
	        		$page = 'news';
	        		break;
	        	case Bl_Data_CmsTypes::page :
	        		$page = 'page';
	        		break;
	        }
	        
	        $this->cmsPath = $this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/'.$page;
	        
	        $this->load_page($page.'/page.php');
	        
		}
	}