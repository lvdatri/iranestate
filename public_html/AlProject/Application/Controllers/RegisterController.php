<?php
	class RegisterController extends Al_ControllerAction {
		public function beforeAction() 
		{
			$this->message = new Bl_Message();
		}		
		
		public function afterAction() 
		{
//			Al_Utilities::a($_SESSION);
		}
		
		public function indexAction() 
		{
			
			$this->form = new Bl_Form_Register();
	        $this->form->setMode(Al_Form::INSERT);
        
	        if($this->form->posted()) {
        	
	            if($this->form->valid()) {

	                $member = new Bl_Record_Member();
	                $member->setFromArray($this->form->getDbDataArray());
	                $member->setConfirmed('1');
	                $member->setDateConfirmed('now()',true);
	                $member->setLanguage($this->_settings->language);
	                $member->setDateRegistered('now()',true);
                    $member->setShowProfile('1');
	                $member->setConfirmationKeys();
	                $member->save();

	                $settings = new Bl_Record_Setting();
	                $settings->load();
	                
	                $email = new Bl_Record_Email();
	                $email->load(Bl_Data_EmailTypes::registration_confirmation);
	                $email->addTag('name',$member->getName());
	                $email->addTag('confirmation_link',$member->getConfirmationUrl());
	                $email->addTag('email',$member->getEmail());
	                $email->addTag('password',$member->getPassword());
	                
					Al_Utilities::initialiseMail();
					$mail = new Zend_Mail();
					$mail->setSubject($email->getSubject());
					$mail->setBodyHtml($email->getContent());
					$mail->setFrom($settings->getEmailFrom(), $settings->getEmailFromName());
				    $mail->addTo($member->getEmail(), $member->getName());
				    $mail->send();

	                // auto login user
	                $login = new Bl_UserLogin();
	                $login->check($member->getEmail(),$member->getPassword());
	                
	                Al_Utilities::redirect($this->pathModule.'account/summary');
	            } else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }
	        }
	        
		    $this->language = Al_Language::get_instance();
			$this->_settings->tag_title = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_TITLE_REGISTER));		
			$this->_settings->tag_keywords = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_KEYWORDS_REGISTER));
			$this->_settings->tag_description = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_DESCRIPTION_REGISTER));								
	        
			$captcha = new Al_MathCaptcha('register');
			$captcha->generateCode();	        
			
			$this->load_page('login-register.php');
		}
		
		public function confirmAction() {
			$member = new Bl_Record_Member();
			$language = Al_Language::get_instance();
			if($member->loadFromKeys(Al_Utilities::get('u'),Al_Utilities::get('p'))) {
				$member->setConfirmed('1');
				$member->setDateConfirmed('now()',true);
				$member->save();
				
				$login = new Bl_UserLogin();
				$login->check($member->getEmail(),$member->getPassword());
				
				$this->message->add($language->getValue(Al_Language::MSG_REGISTRATION_CONFIRMATION_SUCCESS))->save();				
				Al_Utilities::redirect($this->pathModule.'account');			    	
			} else {
				$this->message->add($language->getValue(Al_Language::MSG_REGISTRATION_CONFIRMATION_ERROR),Al_Message::ERROR)->save();
				Al_Utilities::redirect($this->pathModule.'register');	
			}
		}
		
		public function captchaAction() {
			$captcha = new Al_MathCaptcha('register');
			$captcha->display(false);
		}
		
		public function captchaPopupAction() {
			$captcha = new Al_MathCaptcha('register-popup');
			$captcha->setBgColour(211,211,211);
			$captcha->display();
		}

		public function successAction() {
			$this->load_page('register/success.php');
		}
		
	}