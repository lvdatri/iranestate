<?php
class PropertyImagesController extends Al_ControllerAction
{

	public function beforeAction()
    {
        $this->message = new Al_Message();
    }

    public function afterAction()
    {
    }    
    
    public function indexAction()
    {
        $this->callAction('browse');
    }

    public function browseAction()
    {
        $this->_user->level('1');
        
        $pageState = new Al_PageState();
        $pageState->set($this->pathAction);
        
        $this->property = new Bl_Record_Property();

		if(!$this->property->load(Al_Utilities::get('pid'))) {
            $this->message->add('Error loading property.')->save();
        	Al_Utilities::redirect($this->pathModule.'properties/browse');
		}        
        
        $this->params = new Al_UrlParams();
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathModule.'properties/browse');
        $this->nav->add('View');

        $this->tab_nav = new Al_Navigation();
        $this->tab_nav->set_template('tab');
        $this->tab_nav->add('Property Images');
        
	    $this->tabs = new Bl_Admin_PropertyTabs();
    	$this->tabs->setActiveTab('images');            

        $qb = new Al_QueryBuilder();
        $qb->select("
            i.id, i.property_id, i.image_name, i.main_image
        ");
        $qb->from("property_images i");
        $qb->where("i.property_id = '".$this->property->getId()."'");
        $this->searchTriggered = $this->setSearch($qb);
        
        $this->sorter = new Al_Sorter();
        $this->sorter->setPath($this->pathController.'browse/');
        $this->sorter->add_column('image_name','Image','i.image_name',true);
        $this->sorter->add_column('main_image','Main Image','i.main_image');
        $this->sorter->set_from_get();
        $this->sorter->set_url_params($this->params->get(array(Al_UrlParams::SEARCH)));
        $this->sorter->run($qb);
        $this->params->set(Al_UrlParams::SORTER,$this->sorter->get_url_params());
        
        $this->pager = new Al_Pager();
        $this->pager->setPath($this->pathController.'browse/');
        $this->pager->count_query("
            SELECT count(id) ".
            $qb->from.
            $qb->where
        );
        
        $this->pager->set_url_params($this->params->get(array( Al_UrlParams::SORTER, Al_UrlParams::SEARCH)));
        $this->pager->set_from_get();
        $this->pager->run();
        $this->params->set(Al_UrlParams::PAGER,$this->pager->get_url_params());
        
        $qb->limit($this->pager->get_limit());
        $this->result = $this->_db->query($qb->get_query());
        $this->result->set_callback_class($this,'onBrowseCallback');
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->_template->al_set_grid_hover('dataGrid');
        $this->load_page('property-images/browse.php');
    }

    public function insertMultipleAction() 
    {
        $this->_user->level('1');

        $this->property = new Bl_Record_Property();

		if(!$this->property->load(Al_Utilities::get('pid'))) {
            $this->message->add('Error loading property.')->save();
        	Al_Utilities::redirect($this->pathController.'browse');
		}        
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathModule.'properties/browse');
        $this->nav->add('View');

        $this->tab_nav = new Al_Navigation();
        $this->tab_nav->set_template('tab');
        $this->tab_nav->add('Property Images',$this->pathController.'browse/pid/'.$this->property->getId());
        $this->tab_nav->add('Insert');
        
	    $this->tabs = new Bl_Admin_PropertyTabs();
    	$this->tabs->setActiveTab('images');            

        $this->form = new Bl_Form_Admin_PropertyImages();
        $this->form->setMode(Al_Form::INSERT);
        $this->form->setAction($this->pathController.'insert-multiple/pid/'.$this->property->getId());
        $this->form->setCancelAction($this->pathController.'browse/pid/'.$this->property->getId());

        if($this->form->posted()) {
            if($this->form->valid()) {
            	
            	foreach($this->form->_fields as $field) {
            		if($field->getValue() != '') {
		                $record = new Bl_Record_PropertyImage();
		                $record->setImageName($field->getValue());
		                $record->setPropertyId($this->property->getId());
		                $record->save();            			
            			$this->form->setId($record->getId());
            			$field->save();
            		}
            	}
            	
            	Bl_Record_PropertyImage::setDefaultMainImage($record->getPropertyId());

                $this->message->add('Property images created successfully.')->save();
                Al_Utilities::redirect($this->pathController.'browse/pid/'.$this->property->getId());
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        }
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('property-images/form-multiple.php');    	
    }
    
    public function deleteAction()
    {
        $this->_user->level('1');
        

        $this->property = new Bl_Record_Property();

		if(!$this->property->load(Al_Utilities::get('pid'))) {
            $this->message->add('Error loading property.')->save();
        	Al_Utilities::redirect($this->pathController.'browse');
		}        
        
        $record = new Bl_Record_PropertyImage();
        if($record->load(Al_Utilities::get('id'))) {
        	$record->delete();
        	Bl_Record_PropertyImage::setDefaultMainImage($record->getPropertyId());
        }
        
        $this->message->add('Property Image deleted successfully.')->save();
        Al_Utilities::redirect($this->pathController.'browse/pid/'.$this->property->getId());
    }
    
    public function setAsMainAction() 
    {
    	$this->_user->level('1');
        $record = new Bl_Record_PropertyImage();
        if($record->load(Al_Utilities::get('id'))) {
        	$record->setAsMainImage();
	        $this->message->add('Main image set successfully.')->save();
        } else {
			$this->message->add('Error setting main image.')->save();
        }
        
		Al_Utilities::redirect($this->pathController.'browse');
    }

    public function setSearch(&$qb)
    {
        $searchTriggered = false;
        
        return $searchTriggered;
    }

    public function onBrowseCallback(&$row)
    {
    	if($row['main_image'] == '0') {
    		$row['main_image_frmt'] = 'No';
    	} else {
    		$row['main_image_frmt'] = 'Yes';
    	}
    	
    	$row['image_path'] = $this->_settings->path_web_ssl.'images/properties/'.Al_Utilities::getPathFromNo($row['id']).'th-'.$row['id'].'.jpg';
    	
    }
    
}
