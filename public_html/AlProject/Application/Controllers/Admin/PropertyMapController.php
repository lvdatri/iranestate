<?php
class PropertyMapController extends Al_ControllerAction
{

	public function beforeAction()
    {
        $this->message = new Al_Message();
    }

    public function afterAction()
    {
    }    
    
    public function indexAction()
    {
        $this->callAction('browse');
    }

    public function browseAction()
    {
        $this->_user->level('1');
        
        $pageState = new Al_PageState();
        $pageState->set($this->pathAction);
        
        $this->property = new Bl_Record_Property();
		if(!$this->property->load(Al_Utilities::get('pid'))) {
            $this->message->add('Error loading property.')->save();
        	Al_Utilities::redirect($this->pathModule.'properties/browse');
		}        
		
		$this->settings = new Bl_Record_Setting();
		$this->settings->load();
		
		if($this->property->getLat()=='') {
			$this->lat = $this->settings->getMapLat();
			$this->lon = $this->settings->getMapLong();
			$this->depth = $this->settings->getMapDepth();			
		} else {
			$this->lat = $this->property->getLat();
			$this->lon = $this->property->getLon();
			$this->depth = $this->property->getDepth();
		}
		
        
        $this->params = new Al_UrlParams();
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathModule.'properties/browse');
        $this->nav->add('View');

        $this->tab_nav = new Al_Navigation();
        $this->tab_nav->set_template('tab');
        $this->tab_nav->add('Property Map');
        
	    $this->tabs = new Bl_Admin_PropertyTabs();
    	$this->tabs->setActiveTab('map');            

    	$this->_template->jquery_set_core();
		$this->_template->jquery_set_gmaps();
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');

        $this->load_page('property-map/view.php');
    }

 	public function updateAction()
    {
        $this->_user->level('1');
        
        $this->property = new Bl_Record_Property();
		if(!$this->property->load(Al_Utilities::get('pid'))) {
            $this->message->add('Error loading property.')->save();
        	Al_Utilities::redirect($this->pathModule.'properties/browse');
		}       
        
    	$this->settings = new Bl_Record_Setting();
		$this->settings->load();		
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathModule.'properties/browse');
        $this->nav->add('View');
        
        $this->form = new Bl_Form_Admin_PropertyMap();
        $this->form->setMode(Al_Form::UPDATE);
        $this->form->setId(Al_Utilities::get('pid'));
        $this->form->setAction($this->pathController.'update/pid/'.$this->property->getId());
        $this->form->setCancelAction($this->pathController.'browse/pid/'.$this->property->getId());
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_Property();
                if($record->load($this->form->getId())) {
                    $record->setFromArray($this->form->getDbDataArray());
                    $record->save();
        
                    $this->message->add('Map location updated successfully.')->save();
                    Al_Utilities::redirect($this->pathController.'browse/pid/'.$this->property->getId());
                } else {
                    $this->message->add('Error saving map location.')->save();
                    Al_Utilities::redirect($this->pathController.'browse/pid/'.$this->property->getId());
                }
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_Property();
            if($record->load($this->form->getId())) {
                $this->form->setFromDbArray($record->getDataArray());
				if($this->form->getLat()=='') {
					$this->form->setLat($this->settings->getMapLat());
					$this->form->setLon($this->settings->getMapLong());
					$this->form->setDepth($this->settings->getMapDepth());
				}
            } else {
                $this->message->add('Error loading map location.')->save();
                $this->nav->add('Properties',$this->pathModule.'properties/browse');
            }
        }
        
        $this->tab_nav = new Al_Navigation();
        $this->tab_nav->set_template('tab');
        $this->tab_nav->add('Property Map',$this->pathController.'browse/pid/'.$this->property->getId());
        $this->tab_nav->add('Update');
        
	    $this->tabs = new Bl_Admin_PropertyTabs();
    	$this->tabs->setActiveTab('map');         
        
    	$this->_template->jquery_set_core();
		$this->_template->jquery_set_gmaps();
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('property-map/form.php');
    }    
    
    
    
    public function insertMultipleAction() 
    {
        $this->_user->level('1');

        $this->property = new Bl_Record_Property();

		if(!$this->property->load(Al_Utilities::get('pid'))) {
            $this->message->add('Error loading property.')->save();
        	Al_Utilities::redirect($this->pathController.'browse');
		}        
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathModule.'properties/browse');
        $this->nav->add('View');

        $this->tab_nav = new Al_Navigation();
        $this->tab_nav->set_template('tab');
        $this->tab_nav->add('Property Images',$this->pathController.'browse/pid/'.$this->property->getId());
        $this->tab_nav->add('Insert');
        
	    $this->tabs = new Bl_Admin_PropertyTabs();
    	$this->tabs->setActiveTab('images');            

        $this->form = new Bl_Form_Admin_PropertyImages();
        $this->form->setMode(Al_Form::INSERT);
        $this->form->setAction($this->pathController.'insert-multiple/pid/'.$this->property->getId());
        $this->form->setCancelAction($this->pathController.'browse/pid/'.$this->property->getId());

        if($this->form->posted()) {
            if($this->form->valid()) {
            	
            	foreach($this->form->_fields as $field) {
            		if($field->getValue() != '') {
		                $record = new Bl_Record_PropertyImage();
		                $record->setImageName($field->getValue());
		                $record->setPropertyId($this->property->getId());
		                $record->save();            			
            			$this->form->setId($record->getId());
            			$field->save();
            		}
            	}
            	
            	Bl_Record_PropertyImage::setDefaultMainImage($record->getPropertyId());

                $this->message->add('Property images created successfully.')->save();
                Al_Utilities::redirect($this->pathController.'browse/pid/'.$this->property->getId());
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        }
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('property-images/form-multiple.php');    	
    }
    
}
