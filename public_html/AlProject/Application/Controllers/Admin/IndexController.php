<?php
	class IndexController extends Al_ControllerAction {
		public function beforeAction() 
		{
			$this->message = new Al_Message();
		}		
		
		public function indexAction() 
		{
			$this->_user->level('1');
			
			$res = $this->_db->query("
				SELECT property_transaction_id, COUNT(id) prop_count
				FROM properties p
				WHERE p.temp = 0
				GROUP BY p.property_transaction_id			
			");
			
			$this->data = array();
			
			while($row = $res->fetch_row()) {
				$label = '';
				$url = '';
				switch ($row['property_transaction_id']) {
					case Bl_Data_Transactions::FOR_BOOKING :
						$label = 'properties for booking:';
						$url = $this->pathController.'properties/browse/trans/3/';
						break;
					case Bl_Data_Transactions::FOR_RENT :
						$label = 'properties for rent:';
						$url = $this->pathController.'properties/browse/trans/2/';						
						break;
					case Bl_Data_Transactions::FOR_SALE :
						$label = 'properties for sale:';
						$url = $this->pathController.'properties/browse/trans/1/';						
						break;
				}
				
				if($label != '') {
					$this->data[] = array(
						'label' => $label,
						'count' => $row['prop_count'],
						'url' => $url,
					);
				}
			}
			
			
			$res = $this->_db->query("
				SELECT count(id) memb_count 
				FROM members
			");
			
			$row = $res->fetch_row();
			$this->data[] = array(
				'label' => 'users:',
				'count' => $row['memb_count'],
				'url' => $this->pathController.'members/browse',
			);
			
			$this->load_page('index.php');
		}
		
		public function loginAction() 
		{
			if(Al_User::logged_in()) {
				Al_Utilities::redirect($this->pathController);
		    }
		
		    $this->data = array(
		    	'username' => Al_Utilities::tpost('username'),
		    	'password' => Al_Utilities::tpost('password'),
		    );
		    
		    if(isset($_POST['username'])) {
		        $login = new Bl_Admin_UserLogin();
		        $login->setDefaultPath($this->pathController);

				if($login->check($this->data['username'],$this->data['password'])) {
			    	$login->redirect();
				} else {
			    	$this->message->add('Invalid login details entered.');
			    }
		        
		        $this->data['password'] = '';        
		    }
		    
		    $this->data['username'] = Al_Utilities::html($this->data['username']);
		    $this->data['password'] = Al_Utilities::html($this->data['password']);
    
    		$this->_template->extjs_attach_on_ready('Ext.al.pageReady();');			
			$this->load_page('login.php');
		}
		
		public function deniedAction() {
			$this->load_page('denied.php');
		}
		
		public function logoutAction() {
		    $user = Al_User::get_instance();
		    $user->logout();
		    
		    $this->load_page('logout.php');			
		}
		
	}