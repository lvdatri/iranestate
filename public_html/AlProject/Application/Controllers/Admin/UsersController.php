<?php
	class UsersController extends Al_ControllerAction {
		public function beforeAction() 
		{
			$this->message = new Al_Message();
		}
		
		public function indexAction() 
		{
			$this->callAction('browse');
		}
		
		public function browseAction() 
		{
			$this->_user->level('1');
			
			$pageState = new Al_PageState();
			$pageState->set($this->pathAction);			
			
			$this->params = new Al_UrlParams();
			$this->params->set_get(Al_UrlParams::SEARCH,array('first_name','last_name','active'));			
			
			$this->nav = new Al_Navigation();
			$this->nav->add('Users');
			
			$qb = new Al_QueryBuilder();
			$qb->select("
				u.id, u.first_name, u.last_name, u.email, u.username 
			");
			$qb->from("admin_users u");
			
			$this->searchTriggered = $this->setSearch($qb);
			
			$this->sorter = new Al_Sorter();
			$this->sorter->setPath($this->pathController.'browse/');
			$this->sorter->add_column('first_name','First Name','u.first_name',true);
			$this->sorter->add_column('last_name','Last Name','u.last_name');
			$this->sorter->add_column('email','Email','u.email');
			$this->sorter->add_column('username','Username','u.username');
			$this->sorter->set_from_get();
			$this->sorter->set_url_params($this->params->get(array(Al_UrlParams::SEARCH)));
			$this->sorter->run($qb);
			$this->params->set(Al_UrlParams::SORTER,$this->sorter->get_url_params());	

			$this->pager = new Al_Pager();
			$this->pager->setPath($this->pathController.'browse/');
			$this->pager->count_query("
				SELECT count(id) ".
			    $qb->from.
			    $qb->where	
			);			
			
			$this->pager->set_url_params($this->params->get(array( Al_UrlParams::SORTER, Al_UrlParams::SEARCH)));			
			$this->pager->set_from_get();
			$this->pager->run();
			$this->params->set(Al_UrlParams::PAGER,$this->pager->get_url_params());				
		    
		    $qb->limit($this->pager->get_limit());

			$this->result = $this->_db->query($qb->get_query());
			$this->result->set_callback_class($this,'onBrowseCallback');
		    
			$this->_template->extjs_attach_on_ready('Ext.al.pageReady();');		
			$this->_template->al_set_grid_hover('dataGrid');	
			$this->load_page('users/browse.php');
		}
		
		public function insertAction() 
		{
			$this->_user->level('1');
			
			$this->nav = new Al_Navigation();
			$this->nav->add('Users',$this->pathController.'browse');
			$this->nav->add('Add');	
						
			$this->form = new Bl_Form_Admin_AdminUser();
			$this->form->setMode(Al_Form::INSERT);
			$this->form->setAction($this->pathController.'insert');
			$this->form->setCancelAction($this->pathController.'browse');
			
			if($this->form->posted()) {
				if($this->form->valid()) {
					$record = new Bl_Record_AdminUser();
					$record->setFromArray($this->form->getDbDataArray());
					$record->save();
					
					$this->message->add('User created successfully.')->save();
					Al_Utilities::redirect($this->pathController.'browse');
				} else {
					$this->message->addRecordMessages($this->form->getValidationErrors());
				}
			} else {
				$record = new Bl_Record_AdminUser();
				$this->form->setFromDbArray($record->getDataArray());
			}			
			
			$this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
			$this->load_page('users/form.php');			
		}
		
		public function updateAction() 
		{
			$this->_user->level('1');
			
			$this->nav = new Al_Navigation();
			$this->nav->add('Users',$this->pathController.'browse');
			$this->nav->add('Update');		
			
			$this->form = new Bl_Form_Admin_AdminUser();
			$this->form->setMode(Al_Form::UPDATE);
			$this->form->setId(Al_Utilities::get('id'));			
			$this->form->setAction($this->pathController.'update/id/'.$this->form->getId());
			$this->form->setCancelAction($this->pathController.'browse');	

			if($this->form->posted()) {
				if($this->form->valid()) {
					$record = new Bl_Record_AdminUser();
					if($record->load($this->form->getId())) {
						$record->setFromArray($this->form->getDbDataArray());
						$record->save();
						
						$this->message->add('User updated successfully.')->save();
						Al_Utilities::redirect($this->pathController.'browse');
					} else {
						$this->message->add('Error saving user.')->save();
						Al_Utilities::redirect($this->pathController.'browse');
					}					
				} else {
					$this->message->addRecordMessages($this->form->getValidationErrors());
				}
			} else {
				$record = new Bl_Record_AdminUser();
				if($record->load($this->form->getId())) {
					$this->form->setFromDbArray($record->getDataArray());
				} else {
					$this->message->add('Error loading user.')->save();
					Al_Utilities::redirect($this->pathController.'browse');
				}				
			}					

			$this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
			$this->load_page('users/form.php');
		}
		
		public function deleteAction() 
		{
			$this->_user->level('1');
			
			$record = new Bl_Record_AdminUser();
			$record->setId(Al_Utilities::get('id'));
			$record->delete();
			
			$this->message->add('User deleted successfully.')->save();
			Al_Utilities::redirect($this->pathController.'browse');			
		}

		public function setSearch(&$qb) 
		{
		    $searchTriggered = false;
		    $field = Al_Utilities::tget('first_name'); 
		    if($field != '') {
		        $searchTriggered = true;
		        $qb->where("first_name like '%".Al_Db::escape($field)."%'");
		    }
		    
		    $field = Al_Utilities::tget('last_name'); 
		    if($field != '') {
		        $searchTriggered = true;
		        $qb->where("last_name like '%".Al_Db::escape($field)."%'");
		    }
		    
		    return $searchTriggered;			
		}		
		
		function onBrowseCallback(&$row) 
		{
		}						
	}