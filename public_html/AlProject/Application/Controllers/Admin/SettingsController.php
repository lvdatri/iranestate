<?php
class SettingsController extends Al_ControllerAction
{

    public function beforeAction()
    {
        $this->message = new Al_Message();
    }

    public function indexAction()
    {
        $this->callAction('browse');
    }

    public function browseAction()
    {
        $this->_user->level('1');
        
        $pageState = new Al_PageState();
        $pageState->set($this->pathAction);
        
        $this->params = new Al_UrlParams();
        $this->nav = new Al_Navigation();
        $this->nav->add('Settings');
        
        $this->record = new Bl_Record_Setting();
		$this->record->load('1');
		
	    $this->_template->jquery_set_core();
		$this->_template->jquery_set_gmaps();
		
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->_template->al_set_grid_hover('dataGrid');
        
        $this->load_page('settings/browse.php');
    }

    public function updateAction()
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Settings',$this->pathController.'browse');
        $this->nav->add('Update');
        
        $this->form = new Bl_Form_Admin_Setting();
        $this->form->setMode(Al_Form::UPDATE);
        $this->form->setId('1');
        $this->form->setAction($this->pathController.'update/id/'.$this->form->getId());
        $this->form->setCancelAction($this->pathController.'browse');
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_Setting();
                if($record->load($this->form->getId())) {
                    $record->setFromArray($this->form->getDbDataArray());
                    $record->save();
        
                    $this->message->add('Setting updated successfully.')->save();
                    Al_Utilities::redirect($this->pathController.'browse');
                } else {
                    $this->message->add('Error saving setting.')->save();
                    Al_Utilities::redirect($this->pathController.'browse');
                }
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_Setting();
            if($record->load($this->form->getId())) {
                $this->form->setFromDbArray($record->getDataArray());
            } else {
                $this->message->add('Error loading setting.')->save();
                Al_Utilities::redirect($this->pathController.'browse');
            }
        }
        
       	$this->_template->jquery_set_core();
		$this->_template->jquery_set_gmaps();
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('settings/form.php');
    }

    public function setSearch(&$qb)
    {
        $searchTriggered = false;
        
        return $searchTriggered;
    }

    public function onBrowseCallback(&$row)
    {
    }


}
