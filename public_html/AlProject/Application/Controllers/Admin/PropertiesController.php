<?php
class PropertiesController extends Al_ControllerAction
{

    public function beforeAction()
    {
        $this->message = new Al_Message();
    }

    public function indexAction()
    {
        $this->callAction('browse');
    }

    public function browseAction()
    {
        $this->_user->level('1');
        
        $pageState = new Al_PageState();
        $pageState->set($this->pathAction);
        
        $this->params = new Al_UrlParams();
        $this->params->set_get(Al_UrlParams::SEARCH,array('prop','trans','type','email','country','city'));
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties');
        
        $this->property = new Bl_Record_Property();
        $qb = $this->property->getQb();
        $qb->where('temp = 0');
        
        $this->searchTriggered = $this->setSearch($qb);
        
        $this->form = new Bl_Form_Admin_Property();
        
        $this->sorter = new Al_Sorter();
        $this->sorter->setPath($this->pathController.'browse/');
        $this->sorter->add_column('property_transaction_id','Transaction','r.property_transaction_id');
        $this->sorter->add_column('property_type_id','Type','r.property_type_id');
        $this->sorter->add_column('property_status_id','Status','r.property_status_id');
        $this->sorter->add_column('price','Price','r.price');
        $this->sorter->add_column('country_id','Country','r.country_id');
        $this->sorter->add_column('city_id','City','r.city_id');
        $this->sorter->add_column('zone_id','Zone','r.zone_id');
        $this->sorter->add_column('address','Address','r.address');
        $this->sorter->add_column('postal_code','Code','r.postal_code');
        $this->sorter->set_from_get();
        $this->sorter->set_url_params($this->params->get(array(Al_UrlParams::SEARCH)));
        $this->sorter->run($qb);
        $this->params->set(Al_UrlParams::SORTER,$this->sorter->get_url_params());
        
        $this->pager = new Al_Pager();
        $this->pager->setPath($this->pathController.'browse/');
        $this->pager->count_query("
            SELECT count(r.id) ".
            $qb->from.
            $qb->join.
            $qb->where
        );
        
        $this->pager->set_url_params($this->params->get(array( Al_UrlParams::SORTER, Al_UrlParams::SEARCH)));
        $this->pager->set_from_get();
        $this->pager->run();
        $this->params->set(Al_UrlParams::PAGER,$this->pager->get_url_params());
        
        $qb->limit($this->pager->get_limit());
        $this->result = $this->_db->query($qb->get_query());
        $this->result->set_callback_class($this,'onBrowseCallback');
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->_template->al_set_grid_hover('dataGrid');
        $this->load_page('properties/browse.php');
    }

    public function insertAction()
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathController.'browse');
        $this->nav->add('Add');
        
        $this->form = new Bl_Form_Admin_Property();
        $this->form->setMode(Al_Form::INSERT);
        $this->form->setAction($this->pathController.'insert');
        $this->form->setCancelAction($this->pathController.'browse');
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_Property();
                $record->setFromArray($this->form->getDbDataArray());
                $record->setExpiryDate('now() + INTERVAL 6 month',true);
                $record->setCreationDate('now()',true);
                $record->autoGeneratePropertyId();
				$this->form->setRecordValues($record);              
                $record->save();
                
                $this->message->add('Property created successfully.')->save();
                Al_Utilities::redirect($this->pathModule.'property-images/browse/pid/'.$record->getId());
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_Property();
            $this->form->setFromDbArray($record->getDataArray());
        }
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('properties/form.php');
    }

    public function updateAction()
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathController.'browse');
        $this->nav->add('View');
        
        $this->form = new Bl_Form_Admin_Property();
        $this->form->setMode(Al_Form::UPDATE);
        $this->form->setId(Al_Utilities::get('pid'));
        $this->form->setAction($this->pathController.'update/pid/'.$this->form->getId());
        $this->form->setCancelAction($this->pathController.'view/pid/'.$this->form->getId());
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_Property();
                if($record->load($this->form->getId())) {
                    $record->setFromArray($this->form->getDbDataArray());
					$this->form->setRecordValues($record);                  
                    $record->save();
        
                    $this->message->add('Property updated successfully.')->save();
                    Al_Utilities::redirect($this->pathController.'view/pid/'.$this->form->getId());
                } else {
                    $this->message->add('Error saving property.')->save();
                    Al_Utilities::redirect($this->pathController.'view/pid/'.$this->form->getId());
                }
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_Property();
            if($record->load($this->form->getId())) {
                $this->form->setFromDbArray($record->getDataArray());
            } else {
                $this->message->add('Error loading property.')->save();
                Al_Utilities::redirect($this->pathController.'browse');
            }
        }
        
        $this->tab_nav = new Al_Navigation();
        $this->tab_nav->set_template('tab');
        $this->tab_nav->add('Details',$this->pathController.'view/pid/'.$this->form->getId());
        $this->tab_nav->add('Update');
        
	    $this->tabs = new Bl_Admin_PropertyTabs();
    	$this->tabs->setActiveTab('details');         
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('properties/form.php');
    }

    public function viewAction() 
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Properties',$this->pathController.'browse');
        $this->nav->add('View');

        $this->tab_nav = new Al_Navigation();
        $this->tab_nav->set_template('tab');
        $this->tab_nav->add('Details');
        
        
	    $this->tabs = new Bl_Admin_PropertyTabs();
    	$this->tabs->setActiveTab('details');            

        $this->property = new Bl_Record_Property();

		if(!$this->property->load(Al_Utilities::get('pid'))) {
            $this->message->add('Error loading property.')->save();
        	Al_Utilities::redirect($this->pathController.'browse');
		}
		
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('properties/view.php');		
    }
    
    public function deleteAction()
    {
        $this->_user->level('1');
        
        $record = new Bl_Record_Property();
        if($record->load(Al_Utilities::get('id'))) {
        	$record->delete();
        	$this->message->add('Property deleted successfully.')->save();
        } else {
			$this->message->add('Error deleting property.')->save();        	
        }
        
        Al_Utilities::redirect($this->pathController.'browse');
    }
    
    public function getCitiesAction() 
    {
    	$response = new Al_Response();

    	if($this->_user->allow('1')) {
    		$response->access(true);
    		$data = '';
    		
	    	$country = new Bl_Record_Country();
	    	if($country->load(Al_Utilities::get('id'))) {
	    		$dd = new Al_DropDown();
	    		$dd->setFirstOption(true);
	    		$dd->setCallbackClass($this,'onDdCityRow');
	    		$data = $dd->setFromArray($country->retrieveCities());
	    	}    		

    		$response->addSection('data',$data);
    	} else {
    		$response->access(false);
    	}
    	
    	$response->display();
    }
    
    public function onDdCityRow(&$row) 
    {
    	$row['label'] = Al_Language::concat($row['city_eng'],$row['city_per']);
    	$row['value'] = $row['id'];
    }
    
    public function getZonesAction() 
    {
    	$response = new Al_Response();

    	if($this->_user->allow('1')) {
    		$response->access(true);
    		$data = '';
    		
    		$city = new Bl_Record_City();
    		if($city->load(Al_Utilities::get('id'))) {
    			$dd = new Al_DropDown();
    			$dd->setFirstOption(true);
    			$dd->setCallbackClass($this,'onDdZoneRow');
    			$data = $dd->setFromArray($city->retrieveZones(),Al_Utilities::get('zid'));	
    		}
    		$response->addSection('data',$data);
    	} else {
    		$response->access(false);
    	}
    	
    	$response->display();
    }
    
    public function onDdZoneRow(&$row) 
    {
    	$row['label'] = Al_Language::concat($row['zone_eng'],$row['zone_per']);
    	$row['value'] = $row['id'];
    }

    public function setSearch(&$qb)
    {
		// 'prop','trans','type','email','country','city'
    	
    	$searchTriggered = false;
	    $field = Al_Utilities::tget('prop'); 
	    if($field != '') {
	        $searchTriggered = true;
	        $qb->where("r.property_id = '".Al_Db::escape($field)."'");
	    }
	    
    	$field = Al_Utilities::tget('trans'); 
	    if($field != '') {
	        $searchTriggered = true;
	        $qb->where("r.property_transaction_id = '".Al_Db::escape(intval($field))."'");
	    }

		$field = Al_Utilities::tget('type'); 
	    if($field != '') {
	        $searchTriggered = true;
	        $qb->where("r.property_type_id = '".Al_Db::escape(intval($field))."'");
	    }

		$field = Al_Utilities::tget('country'); 
	    if($field != '') {
	        $searchTriggered = true;
	        $qb->where("r.country_id = '".Al_Db::escape(intval($field))."'");
	    }
	    
    	$field = Al_Utilities::tget('city'); 
	    if($field != '') {
	        $searchTriggered = true;
	        $qb->where("ct.city_eng = '".Al_Db::escape($field)."'");
	    }

    	$field = Al_Utilities::tget('email'); 
	    if($field != '') {
	        $searchTriggered = true;
	        $qb->where("m.email = '".Al_Db::escape($field)."'");
	    }	    
	    
	    return $searchTriggered;			
    }

    public function onBrowseCallback(&$row)
    {
    }


}
