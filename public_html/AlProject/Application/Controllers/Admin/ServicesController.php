<?php
class ServicesController extends Al_ControllerAction
{

    public function beforeAction()
    {
        $this->message = new Al_Message();
    }

    public function indexAction()
    {
        $this->callAction('browse');
    }

    public function browseAction()
    {
        $this->_user->level('1');
        
        $pageState = new Al_PageState();
        $pageState->set($this->pathAction);
        
        $this->params = new Al_UrlParams();
        $this->nav = new Al_Navigation();
        $this->nav->add('Services');
        
        $qb = new Al_QueryBuilder();
        $qb->select("
            *,date_format(c.date_created,'%d/%m/%Y') date_created_frmt
        ");
        $qb->where("c.cms_type = '".Al_Db::escape(Bl_Data_CmsTypes::services)."'");
        $qb->from("cms c");
        
        $this->cms = new Bl_Record_Cms();
        
        $this->searchTriggered = $this->setSearch($qb);
        
        $this->sorter = new Al_Sorter();
        $this->sorter->setPath($this->pathController.'browse/');
        $this->sorter->add_column('sort_order','Order','c.sort_order, c.date_created DESC',true);
        $this->sorter->add_column('date_created','Date','c.date_created',false,Al_Sorter::SORTING_DESC);           
        $this->sorter->add_column('title_eng','Title Eng','c.title_eng');
        $this->sorter->add_column('title_per','Title Per','c.title_per');
        $this->sorter->set_from_get();
        $this->sorter->set_url_params($this->params->get(array(Al_UrlParams::SEARCH)));
        $this->sorter->run($qb);
        $this->params->set(Al_UrlParams::SORTER,$this->sorter->get_url_params());
        
        $this->pager = new Al_Pager();
        $this->pager->setPath($this->pathController.'browse/');
        $this->pager->count_query("
            SELECT count(id) ".
            $qb->from.
            $qb->where
        );
        
        $this->pager->set_url_params($this->params->get(array( Al_UrlParams::SORTER, Al_UrlParams::SEARCH)));
        $this->pager->set_from_get();
        $this->pager->run();
        $this->params->set(Al_UrlParams::PAGER,$this->pager->get_url_params());
        
        $qb->limit($this->pager->get_limit());
        $this->result = $this->_db->query($qb->get_query());
        $this->result->set_callback_class($this,'onBrowseCallback');
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->_template->al_set_grid_hover('dataGrid');
        $this->load_page('cms/browse-services.php');
    }

    public function insertAction()
    {
        $this->_user->level('1');
        $this->customPath = false;
        $this->sortOrder = true;
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Services',$this->pathController.'browse');
        $this->nav->add('Add');
        
        $record = new Bl_Record_Cms();
        $record->setDateCreated(Al_Utilities::datetime_now_mysql());
        
        $this->form = new Bl_Form_Admin_Cms(array(
        	'path'=>$record->getCmsTypePath(Bl_Data_CmsTypes::services),
        	'customPath'=>$this->customPath,
        	'sortOrder'=>$this->sortOrder,
        ));
        
        $this->form->setMode(Al_Form::INSERT);
        $this->form->setAction($this->pathController.'insert');
        $this->form->setCancelAction($this->pathController.'browse');

        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record->setFromArray($this->form->getDbDataArray());
                $record->setCmsType(Bl_Data_CmsTypes::services);
                $record->save();
	            $this->form->save($record->getId());
        
                $this->message->add('Service Item created successfully.')->save();
                Al_Utilities::redirect($this->pathController.'browse');
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $this->form->setFromDbArray($record->getDataArray());
        }
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('cms/form.php');
    }

    public function updateAction()
    {
        $this->_user->level('1');
        $this->customPath = false;
        $this->sortOrder = true;
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Services',$this->pathController.'browse');
        $this->nav->add('Update');
        
        $record = new Bl_Record_Cms();
		if(!$record->load(Al_Utilities::get('id'))) {
			$this->message->add('Error loading service item.')->save();
			Al_Utilities::redirect($this->pathController.'browse');			
		}        
        
        $this->form = new Bl_Form_Admin_Cms(array(
        	'path'=>$record->getCmsTypePath(Bl_Data_CmsTypes::services),
        	'customPath'=>$this->customPath,
        	'sortOrder'=>$this->sortOrder,
        ));
        $this->form->setMode(Al_Form::UPDATE);
        $this->form->setId(Al_Utilities::get('id'));
        $this->form->setAction($this->pathController.'update/id/'.$this->form->getId());
        $this->form->setCancelAction($this->pathController.'browse');
        
        if($this->form->posted()) {
            if($this->form->valid()) {
				$record->setFromArray($this->form->getDbDataArray());
				$record->save();
				$this->form->save($record->getId());
				
				$this->message->add('Service Item updated successfully.')->save();
				Al_Utilities::redirect($this->pathController.'browse');
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
			$this->form->setFromDbArray($record->getDataArray());
        }
        
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('cms/form.php');
    }

    public function deleteAction()
    {
        $this->_user->level('1');
        
        $record = new Bl_Record_Cms();
        $record->setId(Al_Utilities::get('id'));
        $record->delete();
        
        $this->message->add('Service Item deleted successfully.')->save();
        Al_Utilities::redirect($this->pathController.'browse');
    }

    public function setSearch(&$qb)
    {
        $searchTriggered = false;
        
        return $searchTriggered;
    }

    public function onBrowseCallback(&$row)
    {
    }


}
