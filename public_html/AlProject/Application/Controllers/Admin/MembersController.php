<?php
class MembersController extends Al_ControllerAction
{

    public function beforeAction()
    {
        $this->message = new Al_Message();
    }

    public function indexAction()
    {
        $this->callAction('browse');
    }

    public function browseAction()
    {
        $this->_user->level('1');
        
        $pageState = new Al_PageState();
        $pageState->set($this->pathAction);
        
        $this->params = new Al_UrlParams();
        $this->params->set_get(Al_UrlParams::SEARCH,array('name', 'email'));
        $this->nav = new Al_Navigation();
        $this->nav->add('Members');
        
        $qb = new Al_QueryBuilder();
        $qb->select("
            m.id, m.name, m.email, m.date_registered, m.date_confirmed, m.confirmed, m.date_last_login, m.login_count
        ");
        $qb->from("members m");
        
        $this->searchTriggered = $this->setSearch($qb);
        
        $this->sorter = new Al_Sorter();
        $this->sorter->setPath($this->pathController.'browse/');
        $this->sorter->add_column('name','Name','m.name',true);
        $this->sorter->add_column('email','Email','m.email');
        $this->sorter->set_from_get();
        $this->sorter->set_url_params($this->params->get(array(Al_UrlParams::SEARCH)));
        $this->sorter->run($qb);
        $this->params->set(Al_UrlParams::SORTER,$this->sorter->get_url_params());
        
        $this->pager = new Al_Pager();
        $this->pager->setPath($this->pathController.'browse/');
        $this->pager->count_query("
            SELECT count(id) ".
            $qb->from.
            $qb->where
        );
        
        $this->pager->set_url_params($this->params->get(array( Al_UrlParams::SORTER, Al_UrlParams::SEARCH)));
        $this->pager->set_from_get();
        $this->pager->run();
        $this->params->set(Al_UrlParams::PAGER,$this->pager->get_url_params());
        
        $qb->limit($this->pager->get_limit());
        $this->result = $this->_db->query($qb->get_query());
        $this->result->set_callback_class($this,'onBrowseCallback');
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->_template->al_set_grid_hover('dataGrid');
        $this->load_page('members/browse.php');
    }

    public function insertAction()
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Members',$this->pathController.'browse');
        $this->nav->add('Add');
        
        $this->form = new Bl_Form_Admin_Member();
        $this->form->setMode(Al_Form::INSERT);
        $this->form->setAction($this->pathController.'insert');
        $this->form->setCancelAction($this->pathController.'browse');
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_Member();
                $record->setFromArray($this->form->getDbDataArray());
                $record->setConfirmed('1');
                $record->setDateConfirmed('now()',true);
                $record->setDateRegistered('now()',true);
                $record->save();
        
                $this->message->add('Member created successfully.')->save();
                Al_Utilities::redirect($this->pathController.'browse');
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_Member();
            $this->form->setFromDbArray($record->getDataArray());
        }
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('members/form.php');
    }

    public function updateAction()
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Members',$this->pathController.'browse');
        $this->nav->add('Update');
        
        $this->form = new Bl_Form_Admin_Member();
        $this->form->setMode(Al_Form::UPDATE);
        $this->form->setId(Al_Utilities::get('id'));
        $this->form->setAction($this->pathController.'update/id/'.$this->form->getId());
        $this->form->setCancelAction($this->pathController.'browse');
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_Member();
                if($record->load($this->form->getId())) {
                    $record->setFromArray($this->form->getDbDataArray());
                    $record->save();
        
                    $this->message->add('Member updated successfully.')->save();
                    Al_Utilities::redirect($this->pathController.'browse');
                } else {
                    $this->message->add('Error saving member.')->save();
                    Al_Utilities::redirect($this->pathController.'browse');
                }
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_Member();
            if($record->load($this->form->getId())) {
                $this->form->setFromDbArray($record->getDataArray());
            } else {
                $this->message->add('Error loading member.')->save();
                Al_Utilities::redirect($this->pathController.'browse');
            }
        }
        
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('members/form.php');
    }

    public function deleteAction()
    {
        $this->_user->level('1');
        
        $record = new Bl_Record_Member();
        $record->setId(Al_Utilities::get('id'));
        $record->delete();
        
        $this->message->add('Member deleted successfully.')->save();
        Al_Utilities::redirect($this->pathController.'browse');
    }

    public function setSearch(&$qb)
    {
        $searchTriggered = false;
        
        $field = Al_Utilities::tget('name'); 
        if($field != '') {
            $searchTriggered = true;
            $qb->where("m.name like '%".Al_Db::escape($field)."%'");
        }
        
        $field = Al_Utilities::tget('email'); 
        if($field != '') {
            $searchTriggered = true;
            $qb->where("m.email like '%".Al_Db::escape($field)."%'");
        }
        
        return $searchTriggered;
    }

    public function onBrowseCallback(&$row)
    {
    }


}
