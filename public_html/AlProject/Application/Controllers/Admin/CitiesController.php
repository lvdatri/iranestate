<?php
class CitiesController extends Al_ControllerAction
{

    public function beforeAction()
    {
        $pageState = new Al_PageState();
        $pageState->set($this->pathAction);
    	
        $this->message = new Al_Message();
        $this->country = new Bl_Record_Country();
        if(!$this->country->load(Al_Utilities::tget('cid'))) {
        	Al_Utilities::redirect($this->_settings->path_admin_web_ssl.'countries/browse');
        }
    }

    public function indexAction()
    {
        $this->callAction('browse');
    }

    public function browseAction()
    {
        $this->_user->level('1');
                
        $this->params = new Al_UrlParams();
        $this->params->set_get(Al_UrlParams::SEARCH,array('city_eng', 'city_per','cid'));
        $this->nav = new Al_Navigation();
        $this->nav->add('Countries',$this->_settings->path_admin_web_ssl.'countries/browse');
        $this->nav->add('Cities '.$this->country->getNameEng());
        
        $qb = new Al_QueryBuilder();
        $qb->select("
            c.id, c.country_id, c.city_eng, c.city_per, c.sort_order, c.featured
        ");
        $qb->from("cities c");
        $qb->where("c.country_id = '".intval($this->country->getId())."'");        
        
        $this->searchTriggered = $this->setSearch($qb);
        
        $this->sorter = new Al_Sorter();
        $this->sorter->setPath($this->pathController.'browse/');
        $this->sorter->add_column('city_eng','City English','c.city_eng',true);
        $this->sorter->add_column('city_per','City Persian','c.city_per');
		$this->sorter->add_column('featured','Featured','c.featured');        
        $this->sorter->add_column('sort_order','Order','c.sort_order');
        $this->sorter->set_from_get();
        $this->sorter->set_url_params($this->params->get(array(Al_UrlParams::SEARCH)));
        $this->sorter->run($qb);
        $this->params->set(Al_UrlParams::SORTER,$this->sorter->get_url_params());
        
        $this->pager = new Al_Pager();
        $this->pager->setPath($this->pathController.'browse/');
        $this->pager->count_query("
            SELECT count(id) ".
            $qb->from.
            $qb->where
        );
        
        $this->pager->set_url_params($this->params->get(array( Al_UrlParams::SORTER, Al_UrlParams::SEARCH)));
        $this->pager->set_from_get();
        $this->pager->run();
        $this->params->set(Al_UrlParams::PAGER,$this->pager->get_url_params());
        
        $qb->limit($this->pager->get_limit());
        $this->result = $this->_db->query($qb->get_query());
        $this->result->set_callback_class($this,'onBrowseCallback');
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->_template->al_set_grid_hover('dataGrid');
        $this->load_page('cities/browse.php');
    }

    public function insertAction()
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
        $this->nav->add('Countries',$this->_settings->path_admin_web_ssl.'countries/browse');
        $this->nav->add('Cities '.$this->country->getNameEng(),$this->pathController.'browse/');
        $this->nav->add('Add');
        
        $this->form = new Bl_Form_Admin_City();
        $this->form->setMode(Al_Form::INSERT);
        $this->form->setAction($this->pathController.'insert/');
        $this->form->setCancelAction($this->pathController.'browse/');
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_City();
                $record->setFromArray($this->form->getDbDataArray());
                $record->setCountryId($this->country->getId());                
                $record->save();
        
                $this->message->add('City created successfully.')->save();
                Al_Utilities::redirect($this->pathController.'browse/');
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_City();
            $this->form->setFromDbArray($record->getDataArray());
        }
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('cities/form.php');
    }

    public function updateAction()
    {
        $this->_user->level('1');
        
        $this->nav = new Al_Navigation();
		$this->nav->add('Countries',$this->_settings->path_admin_web_ssl.'countries/browse/');
        $this->nav->add('Cities '.$this->country->getNameEng(),$this->pathController.'browse/');
        $this->nav->add('Update');
        
        $this->form = new Bl_Form_Admin_City();
        $this->form->setMode(Al_Form::UPDATE);
        $this->form->setId(Al_Utilities::get('id'));
        $this->form->setAction($this->pathController.'update/');
        $this->form->setCancelAction($this->pathController.'browse/');        
        
        if($this->form->posted()) {
            if($this->form->valid()) {
                $record = new Bl_Record_City();
                if($record->load($this->form->getId())) {
                    $record->setFromArray($this->form->getDbDataArray());
					$record->setCountryId($this->country->getId());
                    $record->save();
        
                    $this->message->add('City updated successfully.')->save();
                    Al_Utilities::redirect($this->pathController.'browse/');
                } else {
                    $this->message->add('Error saving city.')->save();
                    Al_Utilities::redirect($this->pathController.'browse/');
                }
            } else {
                $this->message->addRecordMessages($this->form->getValidationErrors());
            }
        } else {
            $record = new Bl_Record_City();
            if($record->load($this->form->getId())) {
                $this->form->setFromDbArray($record->getDataArray());
            } else {
                $this->message->add('Error loading city.')->save();
                Al_Utilities::redirect($this->pathController.'browse/');
            }
        }
        
        
        $this->_template->extjs_attach_on_ready('Ext.al.pageReady();');
        $this->load_page('cities/form.php');
    }

    public function deleteAction()
    {
        $this->_user->level('1');
        
        $record = new Bl_Record_City();
        $record->setId(Al_Utilities::get('id'));
        $record->delete();
        
        $this->message->add('City delete successfully.')->save();
        Al_Utilities::redirect($this->pathController.'browse/');
    }

    public function setSearch(&$qb)
    {
        $searchTriggered = false;
        
        $field = Al_Utilities::tget('city_eng'); 
        if($field != '') {
            $searchTriggered = true;
            $qb->where("c.city_eng like '%".Al_Db::escape($field)."%'");
        }
        
        $field = Al_Utilities::tget('city_per'); 
        if($field != '') {
            $searchTriggered = true;
            $qb->where("c.city_per like '%".Al_Db::escape($field)."%'");
        }
        
        return $searchTriggered;
    }

    public function onBrowseCallback(&$row)
    {
    	if($row['featured']=='1') {
    		$row['featured_frmt'] = 'Yes';
    	} else {
    		$row['featured_frmt'] = 'No';
    	}
    }


}
