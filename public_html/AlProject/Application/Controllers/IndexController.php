<?php
class IndexController extends Al_ControllerAction
{
    public function beforeAction()
    {
        $this->message = new Bl_Message();
    }

    /*		public function setPricesAction()
            {

    // 			$this->_db->query("
    // 				ALTER TABLE  `properties` CHANGE  `price`  `price` DECIMAL( 20, 2 ) NULL DEFAULT NULL ,
    // 				CHANGE  `deposit`  `deposit` DECIMAL( 20, 2 ) NOT NULL DEFAULT  '0.00'
    // 			");

    // 			$this->_db->query("
    // 				update properties SET
    // 				deposit = if(deposit > 0,deposit*1000000,0),
    // 				price = if(price > 0,price*1000000,0)
    // 			");

    // 			$this->_db->query("
    // 				ALTER TABLE  `properties` CHANGE  `price`  `price` DECIMAL( 20, 0 ) NULL DEFAULT NULL ,
    // 				CHANGE  `deposit`  `deposit` DECIMAL( 20, 0 ) NOT NULL DEFAULT  '0'
    // 			");

            } */

    public function indexAction()
    {
        if ($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
            Al_Utilities::redirect($this->_settings->path_web_ssl . Bl_Settings::LANG_ENGLISH_PREFIX . '/' . 'properties/buy');
        } else {
            Al_Utilities::redirect($this->_settings->path_web_ssl . Bl_Settings::LANG_PERSIAN_PREFIX . '/' . 'properties/buy');
        }
        $this->load_page('index.php');
    }

    public function loginAction()
    {
        if (Al_User::logged_in()) {
            Al_Utilities::redirect($this->pathModule . 'account');
        }

        $this->data = array(
            'lemail' => Al_Utilities::tpost('lemail'),
            'password' => Al_Utilities::tpost('password'),
        );

        $this->language = Al_Language::get_instance();
        $this->_settings->tag_title = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_TITLE_LOGIN));
        $this->_settings->tag_keywords = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_KEYWORDS_LOGIN));
        $this->_settings->tag_description = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_DESCRIPTION_LOGIN));


        if (isset($_POST['lemail'])) {
            $login = new Bl_UserLogin();
            $login->setDefaultPath($this->pathModule . 'account/summary');

            if ($login->check($this->data['lemail'], $this->data['password'])) {
                $login->redirect();
            } else {
                $language = Al_Language::get_instance();
                $this->message->add($language->getValue(Al_Language::MSG_INVALID_LOGIN), Bl_Message::ERROR);
            }

            $_POST['password'] = '';
        }

        $captcha = new Al_MathCaptcha('register');
        $captcha->generateCode();

        $this->load_page('login-register.php');
    }

    public function forgotPasswordAction()
    {
        $this->emailSent = false;
        if (isset($_POST['email'])) {
            $member = new Bl_Record_Member();
            if ($member->loadByEmail(Al_Utilities::tpost('email'))) {

                $member->setPassword(Al_Utilities::getRandomString(12));
                $member->save();

                $settings = new Bl_Record_Setting();
                $settings->load();

                $email = new Bl_Record_Email();
                $email->load(Bl_Data_EmailTypes::forgotten_password);
                $email->addTag('email', $member->getEmail());
                $email->addTag('name', $member->getName());
                $email->addTag('password', $member->getPassword());

                Al_Utilities::initialiseMail();
                $mail = new Zend_Mail();
                $mail->setSubject($email->getSubject());
                $mail->setBodyHtml($email->getContent());
                $mail->setFrom($settings->getEmailFrom(), $settings->getEmailFromName());
                $mail->addTo($member->getEmail(), $member->getName());
                $mail->send();


                Al_Utilities::redirect($this->pathModule . 'forgot-password-sent');
            } else {
                $language = Al_Language::get_instance();
                $this->message->add($language->getValue(Al_Language::MSG_FORGOT_PASSWORD_INVALID_EMAIL), Bl_Message::ERROR);
            }
        }

        $this->load_page('forgot-password.php');
    }

    public function forgotPasswordSentAction()
    {
        $this->emailSent = true;
        $this->load_page('forgot-password.php');
    }

    public function logoutAction()
    {
        $user = Al_User::get_instance();
        $user->logout();

        $language = Al_Language::get_instance();
        $this->message->add($language->getValue(Al_Language::MSG_SUCCESSFUL_LOGOUT), Bl_Message::SUCCESS);

        $this->load_page('login-register.php');
    }

    public function autoApproveAction()
    {
        $db = Al_Db::get_instance();
        $res = $db->query(
            "
                            SELECT p.id
                            FROM properties p
                            LEFT JOIN property_images pm ON pm.property_id = p.id AND pm.`main_image` = 1
                            WHERE p.property_status_id = 1

                            AND p.lat <> '35.59478566548724'
                            AND p.lon <> '51.646728515625'
                            AND pm.id IS NOT NULL
                        "
        );
        while ($row = $res->fetch_row()) {
            $db->query(
                "
                                    UPDATE properties SET
                                        property_status_id = 2
                                    WHERE id = '" . intval($row['id']) . "'
				"
            );
            echo $row['id'] . '<BR>';
        }
        echo 'auto approved';

    }

    public function newSetupAction()
    {
        set_time_limit(0);

//			$setup = new Bl_NewSiteSetup();
//			$setup->setImages();
//			$setup->setPropertyDateUpdated();
//			$setup->setIntroductionsKeys();
//			$setup->setFeaturesKeys();
//			$setup->setOldSiteMapProperties();

        $si = new Bl_SearchIndex();
        $si->updateAll();

        /**
         * cron job actions
         *
         * upate search indexes
         * remove any stale records that have been deleted frop properties table
         *
         */
//			$si = new Bl_SearchIndex();
//			$si->update();			
//			$si->cleanUpPropertySearch();	
//			
//			$log = new Bl_Record_Log();
//			$log->setType(Bl_Record_Log::$PROPERTY_SEARCH_MAINTENANCE);
//			$log->save();			

    }

    public function statsAction()
    {
        $data = array();
        $data['registrations'] = array();
        $data['listings'] = array();

        $res = $this->_db->query(
            "
                SELECT
                  DATE_FORMAT(date_registered, '%Y-%m-%d') date_frmt,
                  COUNT(id) ctr
                FROM
                  members
                 GROUP BY date_frmt
                ORDER BY date_registered
                        "
        );

        while ($row = $res->fetch_row()) {
            $data['registrations'][$row['date_frmt']] = intval($row['ctr']);
        }

        $res = $this->_db->query(
            "
                SELECT
                  DATE_FORMAT(creation_date, '%Y-%m-%d') date_frmt,
                  COUNT(id) ctr
                FROM
                  properties
                  WHERE temp = FALSE
                  AND creation_date IS NOT NULL
                 GROUP BY date_frmt
                ORDER BY creation_date
                        "
        );

        while ($row = $res->fetch_row()) {
            $data['listings'][$row['date_frmt']] = intval($row['ctr']);
        }

        print(json_encode($data));
    }

}


function easy_number_format($number, $dec_point, $thousands_sep)
{
    $number = rtrim(sprintf('%f', $number), "0");
    if (fmod($nummer, 1) != 0) {
        $array_int_dec = explode('.', $number);
    } else {
        $array_int_dec = array(strlen($nummer), 0);
    }
    (strlen($array_int_dec[1]) < 2) ? ($decimals = 2) : ($decimals = strlen($array_int_dec[1]));

    return number_format($number, $decimals, $dec_point, $thousands_sep);
}