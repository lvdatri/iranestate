<?php
	class PropertyController extends Al_ControllerAction 
	{
		public $propertyIdSessionName = 'property-insert-id';
		private $propertyId;
		public $property;
		
		public function beforeAction() 
		{
			$this->message = new Bl_Message();
		}		
		
		public function introAction() 
		{
			$this->_user->level('1');
			
			$this->propertyId = $this->getPropertyId();
	        $this->property = new Bl_Record_Property();
	        
			if(isset($_GET['new'])) {
				
	        	$this->setPropertyId('');
	        	$this->property->setTemp('1');
	        	$newType = Al_Utilities::get('new');
	        	switch($newType) {
	        		case Bl_Data_Transactions::FOR_SALE:
	        		case Bl_Data_Transactions::FOR_RENT:
	        		case Bl_Data_Transactions::FOR_BOOKING:
	        			$this->property->setPropertyTransactionId($newType);
	        		break;
	        	}
	        	$this->property->setContactName($this->_user->get('name'));
	        	$this->property->setContactEmail($this->_user->get('email'));
			} else if(!$this->property->loadUserProperty($this->propertyId,$this->_user->get('id'))) {
	        	$this->setPropertyId('');
	        	$this->property->setTemp('1');
			} else {
				$this->setPropertyId($this->property->getId());
			}			
			
			$this->tabs = new Bl_Tabs();
			$this->tabs->setActive(Bl_Tabs::PROP_INTRODUCTION);			
			if($this->property->getTemp()=='1') {
				$this->tabs->setPartialDisplay(true);
			}
			
			$this->setPageHeading();
			

			$this->form = new Bl_Form_Property(array(
				'property' => $this->property
			));
	        $this->form->setMode(Al_Form::INSERT);
	        $this->form->setAction($this->pathAction);

	        if($this->form->posted()) {
	            if($this->form->valid()) {
	                $record = new Bl_Record_Property();
	                if($this->propertyId=='') {
	                	$record->setFromArray($this->form->getDbDataArray());
	                	$record->setExpiryDate('now() + INTERVAL 1 year',true);
	                	$record->setCreationDate('now()',true);
	                	$record->setMemberId($this->_user->get('id'));
	                	$record->setTemp('1');
	                	$record->autoGeneratePropertyId();
	                } else {
	                	$record->load($this->propertyId);
	                	$record->setFromArray($this->form->getDbDataArray());
	                	$record->setExpiryDate('now() + INTERVAL 1 year',true);
	                }
	                $this->form->setRecordValues($record);           
	                $record->save();
	                
	                $this->setPropertyId($record->getId());

	                Al_Utilities::redirect($this->pathController.'images');
	            } else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }
	        } else {
	            $this->form->setFromDbArray($this->property->getDataArray());
	        }
		
	        $this->language = Al_Language::get_instance();
	        $this->_settings->tag_title = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_TITLE_POST));		
			$this->_settings->tag_keywords = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_KEYWORDS_POST));
			$this->_settings->tag_description = Al_Utilities::html($this->language->getValue(Al_Language::PAGE_DESCRIPTION_POST));								
	        
			$this->load_page('property/form-intro.php');			
		}

		public function setPageHeading() 
		{
			$language = Al_Language::get_instance();
			
			if($this->property->getTemp()=='1') {
				$this->pageHeading = $language->getValue(Al_Language::PROP_HEAD_POST_PROPERTY);
			} else {
				$this->pageHeading = $language->getValue(Al_Language::PROP_HEAD_UPDATE_PROPERTY);
			}
		}
		
		public function pageCommon($status) 
		{
			$this->_user->level('1');
			
			$this->propertyId = $this->getPropertyId();
	        $this->property = new Bl_Record_Property();
	        
	        if(!$this->property->loadUserProperty($this->propertyId,$this->_user->get('id'))) {
	        	$this->setPropertyId('');
	        	Al_Utilities::redirect($this->pathController.'intro');
			} else {
				$this->setPropertyId($this->property->getId());
			}			
			
			$this->tabs = new Bl_Tabs();
			$this->tabs->setActive($status);			
			if($this->property->getTemp()=='1') {
				$this->tabs->setPartialDisplay(true);
			}			
			
			$this->setPageHeading();
		}
		
		public function imagesAction()
		{
			$this->pageCommon(Bl_Tabs::PROP_IMAGES);
	        
			
	        $this->form = new Bl_Form_Admin_PropertyImages();
	        $this->form->setMode(Al_Form::INSERT);
	        $this->form->setAction($this->pathAction);
	
	        if($this->form->posted()) {
	            if($this->form->valid()) {
	            	
	            	foreach($this->form->_fields as $field) {
	            		if($field->getValue() != '') {
			                $record = new Bl_Record_PropertyImage();
			                $record->setImageName($field->getValue());
			                $record->setPropertyId($this->property->getId());
			                $record->save();            			
	            			$this->form->setId($record->getId());
	            			$field->save();
	            		}
	            	}
	            	
	            	Bl_Record_PropertyImage::setDefaultMainImage($record->getPropertyId());
					
	            	$language = Al_Language::get_instance();
	                $this->message->add($language->getValue(Al_Language::MSG_PROP_IMAGES_UPLOAD_SUCCESS))->save();
	                Al_Utilities::redirect($this->pathAction);
	            } else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }
	        }
	        
	        $propertyImage = new Bl_Record_PropertyImage();
			$this->images = $propertyImage->retrieveRecordsByProperty($this->property->getId(),4);	    

			$this->load_page('property/form-images.php');	
		}
		
		
		public function setMainImageAction()
		{
			$this->pageCommon(Bl_Tabs::PROP_IMAGES);

	        $language = Al_Language::get_instance();
			
	        $image = new Bl_Record_PropertyImage();
	        if($image->loadByProperty(Al_Utilities::get('iid'),$this->property->getId())) {
	        	$image->setAsMainImage();
				$this->message->add($language->getValue(Al_Language::MSG_PROP_MAIN_IMAGE_SET_SUCCESS))->save();	        	
	        } else {
	        	$this->message->add($language->getValue(Al_Language::MSG_PROP_MAIN_IMAGE_SET_ERROR))->save();
	        }
	        
			Al_Utilities::redirect($this->pathController.'images');				
			
		}
		
		public function deleteImageAction()
		{
			$this->pageCommon(Bl_Tabs::PROP_IMAGES);
			

	        $image = new Bl_Record_PropertyImage();
	        if($image->loadByProperty(Al_Utilities::get('iid'),$this->property->getId())) {
        		$image->delete();
        		Bl_Record_PropertyImage::setDefaultMainImage($image->getPropertyId());
				
        		$language = Al_Language::get_instance();        		
        		$this->message->add($language->getValue(Al_Language::MSG_PROP_IMAGE_DELETED))->save();
	        } 
	        
			Al_Utilities::redirect($this->pathController.'images');
		}
		
		public function deleteAction()
		{
	        $this->_user->level('1');
	        
			$this->property = new Bl_Record_Property();
			if($this->property->loadUserProperty(Al_Utilities::tget('pid'),$this->_user->get('id'))) {
				$this->property->delete();
        		$language = Al_Language::get_instance();        		
        		$this->message->add($language->getValue(Al_Language::MSG_PROP_DELETED))->save();
			}
			
			Al_Utilities::redirect($this->pathModule.'account/summary');
		}		
		
		
		public function mapAction()
		{
			$this->pageCommon(Bl_Tabs::PROP_MAP);
			
		  	$this->form = new Bl_Form_Admin_PropertyMap();
        	$this->form->setMode(Al_Form::UPDATE);
	        $this->form->setId($this->property->getId());
	        $this->form->setAction($this->pathAction);
	        
			$language = Al_Language::get_instance();	        
	        
	        if($this->form->posted()) {
	            if($this->form->valid()) {
	                if($this->property->load($this->form->getId())) {
	                    $this->property->setFromArray($this->form->getDbDataArray());
	                    if($this->property->getTemp()=='1') {
	                    	$this->property->setTemp('0');
	                    	$settings = new Bl_Record_Setting();
	                    	$settings->load();
	                    	if($settings->getRequirePropertyApproval()=='1') {
								$this->property->setPropertyStatusId(Bl_Data_Statuses::PENDING_APPROVAL);			
	                    	} else {
	                    		$this->property->setPropertyStatusId(Bl_Data_Statuses::ACTIVE);
	                    	}	           
        		        		
        					$this->message->add($language->getValue(Al_Language::MSG_PROP_CREATED_SUCCESS))->save();
	                    } else {
	                    	$this->message->add($language->getValue(Al_Language::MSG_PROP_UPDATE_SUCCESS))->save();
	                    }
						$this->property->save();
                    	$this->setPropertyId('');						
	                    Al_Utilities::redirect($this->pathModule.'account/summary');
	                }
	            } else {
	                $this->message->addRecordMessages($this->form->getValidationErrors());
	            }
	        } else {
                $this->form->setFromDbArray($this->property->getDataArray());
				if($this->form->getLat()=='') {
					
					$this->settings = new Bl_Record_Setting();
					$this->settings->load();	
					$this->form->setLat($this->settings->getMapLat());
					$this->form->setLon($this->settings->getMapLong());
					$this->form->setDepth($this->settings->getMapDepth());					
					
					$city = new Bl_Record_City();
					if($city->load($this->property->getCityId())) {
						if($city->getLat() != '' && $city->getLon() != '') {
							$this->form->setLat($city->getLat());
							$this->form->setLon($city->getLon());				
						}
					}
				}
	        }			
			
			$this->load_page('property/form-map.php');	
		}
		
		private function getPropertyId() 
		{
			if(isset($_GET['id'])) {
				return $_GET['id'];
			} else if(isset($_SESSION[$this->_settings->session][$this->propertyIdSessionName])) {
				return $_SESSION[$this->_settings->session][$this->propertyIdSessionName];
			}
			return '';
		}
		
//		private function getPropertyIdFromGet() 
//		{
//			if(count($_GET)=='1') {
//				foreach($_GET as $key=>$value) 
//				{
//					if($value == '') {
//						return $key;
//					}					
//				}
//			}
//			return '';
//		}
		
		private function setPropertyId($id) 
		{
			$_SESSION[$this->_settings->session][$this->propertyIdSessionName] = $id;
		}
		
		public function getCitiesAction() 
		{
	    	$country = new Bl_Record_Country();
	    	if($country->load(Al_Utilities::get('id'))) {
	    		$dd = new Al_DropDown();
	    		$dd->setFirstOption(true);
	    		$dd->setCallbackClass($this,'onDdCityRow');
	    		print $dd->setFromArray($country->retrieveCities(),Al_Utilities::get('cid'));
	    	} 			
		}
		
	    public function onDdCityRow(&$row) 
    	{
	    	$row['label'] = Al_Language::concat($row['city_eng'],$row['city_per']);
    		$row['value'] = $row['id'];
    	}		
		
		public function getZonesAction() 
		{
		   	$city = new Bl_Record_City();
    		if($city->load(Al_Utilities::get('id'))) {
    			$dd = new Al_DropDown();
    			$dd->setFirstOption(true);
    			$dd->setCallbackClass($this,'onDdZoneRow');
    			print $dd->setFromArray($city->retrieveZones(),Al_Utilities::get('zid'));	
    		}			
		}
		
		public function getBuyStatusesAction() 
		{
		   	$statuses = new Bl_Data_Statuses();
   			$dd = new Al_DropDown();
   			$dd->setFirstOption(true);
   			print $dd->setFromArray($statuses->getBuy(),Al_Utilities::get('sid'));
		}
		
		public function getRentStatusesAction() 
		{
			$statuses = new Bl_Data_Statuses();
   			$dd = new Al_DropDown();
   			$dd->setFirstOption(true);
   			print $dd->setFromArray($statuses->getRent(),Al_Utilities::get('sid'));			
		}
		
		public function getBookStatusesAction()
		{
			$statuses = new Bl_Data_Statuses();
   			$dd = new Al_DropDown();
   			$dd->setFirstOption(true);
   			print $dd->setFromArray($statuses->getBook(),Al_Utilities::get('sid'));			
		}
		
	    public function onDdZoneRow(&$row) 
    	{
	    	$row['label'] = Al_Language::concat($row['zone_eng'],$row['zone_per']);
    		$row['value'] = $row['id'];
    	}		

	}