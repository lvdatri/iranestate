<?php
error_reporting(E_ALL);
	class MgrsController extends Al_ControllerAction {
		public function beforeAction() 
		{
			$this->message = new Bl_Message();
		}		
		
		
		public function mapAction()
		{
			
			$this->load_page('map.php');
		}
		
		
		/**
		 * converts a tile to lat long
		 */
		
/*
	to figure out
	1. determine tile zoom level - several levels are merged together to simplify the process
		- figur out what zoom level will be actully showing properties
	2. from tl and br lat lon boundries retrieve tiles or properties
	3. return results to client
	
	
	
	
	
 */		
		
		private function dbDataToTiles()
		{
			$prop = new Bl_Record_Property();
			$props = $prop->retrieveAll();
			$p = new Bl_Record_Property();
			
			foreach($props as $pr) {
				$p->loadFromArray($pr);
			
				for($i=3;$i<=19;$i+=2) {
					$tile = lonLatToTile($i, $p->getLat(), $p->getLon());
						
					// 					$fname=
			
					$fName = 'setLevel'.$i;
					$p->$fName($tile['cord']);
					// Al_Utilities::a($tile);
			
					// 					echo $tile['cord'].'<BR>';
					// 				3,5,7,9,11,13,15
				}
				echo $p->getId().'<BR>';
				$p->save();
			
			
				// 				$mgrs->fromLatLonCoord($p->getLat(), $p->getLon());
					
				// 				$p->setMgrs100m($mgrs->toString('100m'));
				// 				$p->setMgrs1k($mgrs->toString('1k'));
				// 				$p->setMgrs10k($mgrs->toString('10k'));
				// 				$p->setMgrs100k($mgrs->toString('100k'));
				// 				$p->setMgrs6d($mgrs->toString('6d'));
				// 				$p->save();
					
				// 				echo $p->getId().'<BR>';
			}
			echo 'done...<BR>';
				
			
		}
		
		public function tileAction()
		{
			
			$this->dbDataToTiles();
			exit;			
			
			echo $this->_settings->path_images.'map/pin.png';
			exit;
			
			echo $this->pathModule.'properties/api-map/';
			
			exit;


			$zoom = 13;
			
			$map = new Bl_Map();
			
			
// 			$range = $map->tileRange(
// 				$zoom, 
// 				36.06575205170711, 51.88087463378906, 
// 				35.45283961767315, 51.02943420410156
// 			);
			
/*
take zoom out of tiles within db fields - not required at this stage

 */			
			
			$range = $map->tileRange(
					$zoom,
					35.78439882519532, 51.320743560791016,
					35.63107008009774, 51.10788345336914
			);
			
			
			$records = $this->_db->query("
				SELECT 
				  level_13, COUNT(p.id) rec_count, lat, lon
				FROM
				  properties p 
				WHERE p.`level_13` IN (".$range.")
				GROUP BY level_13
					
			")->get_records();
			
			
// 			Al_Utilities::a($records);
			
			foreach($records as $record) {
// 				echo $record['level_13'] . ' - ' . $record['rec_count'].'<BR>';
				
				$latLon = $map->tileCordToLatLon($record['level_13'],true);
				
// 				echo $latLon['lat'] . ', ' . $latLon['lon']. '<BR>';
				
				if($record['rec_count'] == '1') {
					$lat = $record['lat'];
					$lon = $record['lon'];
				} else {
					$lat = $latLon['lat'];
					$lon = $latLon['lon'];
				}
				
				echo "setPoint(map,".$lat.",".$lon.", '".$record['rec_count']." - ".$record['level_13']."');<BR>"; //.$record['rec_count']." - ".$record['level_13']."');<BR>";
				
			}
		
			
			
			

			
// 			echo $range;
			
			
// 			$ne = lonLatToTile($zoom, 36.06575205170711, 51.88087463378906);
// 			$sw = lonLatToTile($zoom, 35.45283961767315, 51.02943420410156);
			
			
// // 			ne - 10:659:401
// // 			sw - 10:657:404
			
// 			for($j=$sw['x'];$j<=$ne['x'];$j++) {
// 				for($i=$ne['y'];$i<=$sw['y'];$i++) {
// 					echo $j.':'.$i.'<BR>';
// 				}
// 				echo '<BR>';
// 			}				
			
			
			
// 			echo 'ne - ' .$ne['cord'] . '<BR>';
// 			echo 'sw - '.$sw['cord'] . '<BR>';
			
				
			
			exit;			
			
			
			$zoom = 1;
			$xtile = 0;
			$ytile = 1;
			
			$x = 0;
			$y = 0;
			
			$res = tileToLatLonBoundries($zoom,0,0);
			$tl = $res['tl'];
			$br = $res['br'];
			$mp = $res['mp'];
			echo $tl['lat'] . ', ' . $tl['lon'] . " : " . $br['lat'] . ', ' . $br['lon'] . ' :::: ' . $mp['lat'] . ', ' . $mp['lon'] .'<BR>';
			
			$res = tileToLatLonBoundries($zoom,0,1);
			$tl = $res['tl'];
			$br = $res['br'];
			$mp = $res['mp'];
			echo $tl['lat'] . ', ' . $tl['lon'] . " : " . $br['lat'] . ', ' . $br['lon'] . ' :::: ' . $mp['lat'] . ', ' . $mp['lon'] .'<BR>';

			$res = tileToLatLonBoundries($zoom,1,0);
			$tl = $res['tl'];
			$br = $res['br'];
			$mp = $res['mp'];
			echo $tl['lat'] . ', ' . $tl['lon'] . " : " . $br['lat'] . ', ' . $br['lon'] . ' :::: ' . $mp['lat'] . ', ' . $mp['lon'] .'<BR>';
			
			$res = tileToLatLonBoundries($zoom,1,1);
			$tl = $res['tl'];
			$br = $res['br'];
			$mp = $res['mp'];
			echo $tl['lat'] . ', ' . $tl['lon'] . " : " . $br['lat'] . ', ' . $br['lon'] . ' :::: ' . $mp['lat'] . ', ' . $mp['lon'] .'<BR>';
						
			for($i=1;$i<15;$i++) {
				$tile = lonLatToTile($i, 90,66.513260443112);
			
				// Al_Utilities::a($tile);
			
				echo $tile['cord'].'<BR>';
// 				3,5,7,9,11,13,15
			}
			
// 			echo tileToLatLon($zoom,$x,$y+1) . ' : ' . tileToLatLon($zoom,$x+1,$y).'<BR>';
// 			echo tileToLatLon($zoom,0,1).'<BR>';
// 			echo tileToLatLon($zoom,1,0).'<BR>';			
// 			echo tileToLatLon($zoom,1,1).'<BR>';
			
			
		}
		
		public function testAction() {
			
			
// 			echo midpoint(32.0000000, 042.0000000, 40.0000000, 048.0000000);
			
			
// 			echo midpoint(24.0000000, -108.0000000, 32.0000000, -102.0000000);
			
			// echo midpoint(00.0000001, -078.0000000, 08.0000000, -066.0000000);
			
// 			echo midpoint(35.6934582, 051.3315823, 35.7832637, 051.4426058);

			$mgrs = new Al_Coord_Mgrs();
			$mgrs->parseString("39SWV35");

			
			
			$t = chr(ord($mgrs->utmZoneChar) - 1);			
			
// 			echo 'org: '.$mgrs->utmZoneChar.'<BR>';
			
// 			$t = $mgrs->utmZoneChar;
// 			$t--;
			echo $t.'<BR><BR>';
			
			echo "utmZoneNumber: ".$mgrs->utmZoneNumber.'<BR>';
			echo "utmZoneChar: ".$mgrs->utmZoneChar.'<BR>';
			echo "eastingID: ".$mgrs->eastingID.'<BR>';
			echo "northingID: ".$mgrs->northingID.'<BR>';
			echo "easting: ".$mgrs->easting.'<BR>';
			echo "northing: ".$mgrs->northing.'<BR>';

			$utm = $mgrs->toUTMRef();
			$ll = $utm->toLatLng();
			echo $ll->toString().'<BR>';			
			
			
			exit;
			
			echo 'here';
			exit;
			$mgrs = new Al_Coord_Mgrs();
			$mgrs->parseString("39SWV35");
			$utm = $mgrs->toUTMRef();
			$ll = $utm->toLatLng();
			echo $ll->toString();
			exit;				
			
			
			$mgrs = new Al_Coord_Mgrs();
			$mgrs->parseString('39S');

// 			$ll = $mgrs->
			
// 			$mgrs->fromLatLonCoord(33.33, 44.44);
// 			echo $mgrs->toString("1k");
			
// 			exit;
// 			$mgrs->parseString("56HLH2538");
			
			

// 			echo latLonToMgrs(33.33,44.4,"1m").'<BR>';
// 			echo latLonToMgrs(33.33,44.4,"10m").'<BR>';
// 			echo latLonToMgrs(33.33,44.4,"100m").'<BR>';
// 			echo latLonToMgrs(33.33,44.4,"1k").'<BR>';
// 			echo latLonToMgrs(33.33,44.4,"10k").'<BR>';
// 			echo latLonToMgrs(33.33,44.4,"100k").'<BR>';
// 			echo latLonToMgrs(33.33,44.4,"6d").'<BR>';			
			
			
			
			
// 			echo $mgrs->precision;
			
// 			exit;

			$prop = new Bl_Record_Property();
			$props = $prop->retrieveAll();
			$p = new Bl_Record_Property();
			
			$mgrs = new Al_Coord_Mgrs();
			foreach($props as $pr) {
				$p->loadFromArray($pr);
				$mgrs->fromLatLonCoord($p->getLat(), $p->getLon());
				
				$p->setMgrs100m($mgrs->toString('100m'));
				$p->setMgrs1k($mgrs->toString('1k'));
				$p->setMgrs10k($mgrs->toString('10k'));
				$p->setMgrs100k($mgrs->toString('100k'));
				$p->setMgrs6d($mgrs->toString('6d'));			
				$p->save();
				
				echo $p->getId().'<BR>';
			}

			exit;
			
			$res = $this->_db->query("
					SELECT * FROM properties
					WHERE lat <> ''
					AND lon <> ''
					LIMIT 10
			");

			$mgrs = new Al_Coord_Mgrs();			
			while($row = $res->fetch_row()) {
				echo $row['id'] . ' - ' . $row['lat'] . ', '.$row['lon'] . ' - ';

				$mgrs->fromLatLonCoord($row['lat'], $row['lon']);
				echo $mgrs->toString("100m") . ', ' . $mgrs->toString("1k") . ', ' . $mgrs->toString("10k") . ', ' . $mgrs->toString("100k") . ', ' . $mgrs->toString("6d").'<BR>';
				
			}
		}
	}
	
	
/**
	http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#tile_numbers_to_lon.2Flat_2 
	- shows conversions of latLon to tiles and vice verser, as well as finding a mid point by adding .5 
 */	
	
	function tileToLatLonBoundries($zoom,$xtile,$ytile)
	{
		$res = array(
			"br" => tileToLatLon($zoom,$xtile,$ytile+1),
			'tl' => tileToLatLon($zoom,$xtile+1,$ytile),
			"mp" => tileToLatLon($zoom,$xtile+0.5,$ytile+0.5),
		);
		
// 		$res['mp'] = midpoint(
// 			$res['tl']['lat'], $res['tl']['lon'],
// 			$res['br']['lat'], $res['br']['lon']
// 		);
		
		return $res;
		
	}
	
	
	function tileToLatLon($zoom,$xtile,$ytile) {
		$ll = array();
		$n = pow(2, $zoom);
		$lon_deg = $xtile / $n * 360.0 - 180.0;
		$lat_deg = rad2deg(atan(sinh(pi() * (1 - 2 * $ytile / $n))));
			
		$ll['lat'] = $lat_deg;
		$ll['lon'] = $lon_deg;
		
		return $ll;
		
	}
	
	
	/*
		http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#PHP	 
	 */	
	
	function lonLatToTile($zoom,$lat,$lon)
	{
		$res = array();
		$xtile = floor((($lon + 180) / 360) * pow(2, $zoom));
		$ytile = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));
		
		return array(
			'x' => $xtile,
			'y' => $ytile,
			'z' => $zoom,
			'cord' => $zoom.':'.$xtile.':'.$ytile,
		);
	}
	
	function midpoint ($lat1, $lng1, $lat2, $lng2) {
		
		
		$lat1 = deg2rad($lat1);
		$lng1 = deg2rad($lng1);
		
		$lat2 = deg2rad($lat2);
		$lng2 = deg2rad($lng2);		
		
		$dlng = $lng2 - $lng1;
		$Bx = cos($lat2) * cos($dlng);
		$By = cos($lat2) * sin($dlng);
		
		$lat3 = atan2(	sin($lat1)+sin($lat2),	sqrt((cos($lat1)+$Bx)*(cos ($lat1)+$Bx) + $By*$By ));
		
		$lng3 = $lng1 + atan2($By, (cos($lat1) + $Bx));
		$pi = pi();
		
		return array(
			'lat' => ($lat3*180)/$pi,
			'lon' => ($lng3*180)/$pi 
		); 
	}	
	
// 	function latLonToMgrs($lat,$lon,$prec) {
// 		$ll = new  Al_Coord_LatLng($lat,$lon);
	
	
// 		$utm = $ll->toUTMRef();
	
// 		$mgrs = new Al_Coord_Mgrs();
	
	
	
// 		$mgrs->fromUTM($utm);
// 		//    return str_replace(' ', '',$mgrs->toString(1) );
	
// 		return $mgrs->toString($prec);
// 	}