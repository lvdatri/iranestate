<?php
	class ServicesController extends Al_ControllerAction {
		public function beforeAction() 
		{
			$this->message = new Bl_Message();
		}
		
		public function indexAction() {
			
			$this->cms = new Bl_Record_Cms();
			$qb = $this->cms->getQb();
			$qb->where("r.cms_type='" . Al_Db::escape(Bl_Data_CmsTypes::services) . "'");
			$qb->order_by("r.date_created desc");			

			$this->pager = new Al_Pager();
	        $this->pager->setPath($this->pathController.'index/');
	        $this->pager->count_query("
	            SELECT count(r.id) ".
	            $qb->from.$qb->join.$qb->where
	        );
	        
	        $this->pager->set_from_get();
	        $this->pager->run();
	        
	        $qb->limit($this->pager->get_limit());
	        $this->articles = $this->_db->query($qb->get_query());
	        
	        $this->cms->getTitle();
	        $this->cms->getContent(true);
	        $this->cms->getImageSml();

	        $this->load_page('services/browse.php');
		}
	}