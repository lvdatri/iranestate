<?php
	class Bl_PropertySearch {

		private $transactionType;
		private $location;
		private $propertyType;
		private $priceMin;
		private $priceMax;
		
		private $depositMin;
		private $depositMax;
		private $bedrooms;
		private $bathrooms;
		private $parking;
		private $introduction;
		private $features;
		private $landMin;
		private $landMax;
		private $buildMin;
		private $buildMax;
		private $language;
		
		
		public $dataIntroduction = array();
		public $dataFeatures = array();
		public $dataPropertyTypes = array();
		
		public $propIntros=array();
		public $propFeatures=array();
		public $propTypes=array();
		
		public $prices;
		
		private $sort;
		
		private $_settings;
		
		public $descPropertyFeatures = null;
		public $descPropertyTypes = null;
		public $descPrice = null;
		public $descDeposit = null;
		public $descPropertyIntro = null; 
		public $descLandArea = null;
		public $descBuildArea = null;
		
		public function __construct()
		{
			$this->_settings = Bl_Settings::get_instance();
			$this->prices = new Bl_Data_Prices();
			$this->language = Al_Language::get_instance();

			$this->setRequestValues();
		}


		public function setPropIntros($data) 
		{
			$this->propIntros = $data;
		}
		public function setPropFeatures($data) 
		{
			$this->propFeatures = $data;
		}
		public function setPropTypes($data)
		{
			$this->propTypes = $data;
		}		
		
		public function getPropertyPrice() 
		{
			if($this->descPrice === null) {
				if($this->transactionType == Bl_Record_Property::BUY) {
					$priceFrom = $this->getArrayVal($this->priceMin, $this->prices->getBuyFromPrices());
					$priceTo = $this->getArrayVal($this->priceMax, $this->prices->getBuyToPrices());
				} else if($this->transactionType == Bl_Record_Property::RENT) {
					$priceFrom = $this->getArrayVal($this->priceMin, $this->prices->getRentFromPrices());
					$priceTo = $this->getArrayVal($this->priceMax, $this->prices->getRentToPrices());					
				} else {
					$priceFrom = $this->getArrayVal($this->priceMin, $this->prices->getBookFromPrices());
					$priceTo = $this->getArrayVal($this->priceMax, $this->prices->getBookToPrices());					
				}
				
				$price = new Bl_Data_Prices();
				$price->getBuyFromPrices();
				$price->getBuyToPrices();
				
				if($this->priceMin > 0 && $this->priceMax>0) {
					$this->descPrice = $priceFrom . ' - ' . $priceTo;
				} else if($this->priceMax>0) {
					$this->descPrice = $this->language->getValue(Al_Language::SEARCH_LBL_LESS_THAN).' '.$priceTo;
				} else if($this->priceMin>0) {
					$this->descPrice = $this->language->getValue(Al_Language::SEARCH_LBL_MORE_THAN).' '.$priceFrom;
				} else {
					$this->descPrice = '';
				}
			}
			return $this->descPrice;
		}
		
		public function getPropertyDeposit() 
		{
			if($this->descDeposit === null) {
				$priceFrom = $this->getArrayVal($this->depositMin, $this->prices->getDepositFromPrices());
				$priceTo = $this->getArrayVal($this->depositMax, $this->prices->getDepositToPrices());
				
				$price = new Bl_Data_Prices();
				$price->getBuyFromPrices();
				$price->getBuyToPrices();
				
				if($this->depositMin > 0 && $this->depositMax > 0) {
					$this->descDeposit = $priceFrom . ' - ' . $priceTo;
				} else if($this->depositMax>0) {
					$this->descDeposit = $this->language->getValue(Al_Language::SEARCH_LBL_LESS_THAN).' '.$priceTo;
				} else if($this->depositMin>0) {
					$this->descDeposit = $this->language->getValue(Al_Language::SEARCH_LBL_MORE_THAN).' '.$priceFrom;
				} else {
					$this->descDeposit = '';
				}
			}
			return $this->descDeposit;
		}		
		
		public function getPropertyBedrooms() 
		{
			$val = '';
			if($this->bedrooms>0) {
				$val = $this->bedrooms . ' '.$this->language->getValue(Al_Language::SEARCH_LBL_OR_MORE);
			}
			return $val;
		}

		public function getPropertyBathrooms() 
		{
			$val = '';
			if($this->bathrooms>0) {
				$val = $this->bathrooms . ' '.$this->language->getValue(Al_Language::SEARCH_LBL_OR_MORE);
			}
			return $val;
		}

		public function getPropertyParking() 
		{
			$val = '';
			if($this->parking >0) {
				$val = $this->parking . ' '.$this->language->getValue(Al_Language::SEARCH_LBL_OR_MORE);
			}
			return $val;
		}
		
		public function getPropertyTypes() 
		{
			if($this->descPropertyTypes === null) {
				$this->descPropertyTypes = '';
				foreach($this->propTypes as $items) {
					foreach($items as $item) {
						if(array_key_exists($item['id'],$this->dataPropertyTypes)) {
							$this->descPropertyTypes .= $item['label'].', ';
						}
					}
				}
				if($this->descPropertyTypes != '') {
					$this->descPropertyTypes = rtrim($this->descPropertyTypes,', ');
				}
			}
			
			return $this->descPropertyTypes;
		}
		
		public function getPropertyIntroduction() 
		{
			if($this->descPropertyIntro === null) {
				$this->descPropertyIntro = '';
				foreach($this->propIntros as $items) {
					foreach($items as $item) {
						if(array_key_exists($item['id'],$this->dataIntroduction)) {
							$this->descPropertyIntro .= $item['label'].', ';
						}
					}
				}
				if($this->descPropertyIntro != '') {
					$this->descPropertyIntro = rtrim($this->descPropertyIntro,', ');
				}
			}
			
			return $this->descPropertyIntro;
		}		
		
		public function getPropertyFeatures() 
		{
			if($this->descPropertyFeatures === null) {
				$this->descPropertyFeatures = '';
				foreach($this->propFeatures as $items) {
					foreach($items as $item) {
						if(array_key_exists($item['id'],$this->dataFeatures)) {
							$this->descPropertyFeatures .= $item['label'].', ';
						}
					}
				}
				if($this->descPropertyFeatures != '') {
					$this->descPropertyFeatures = rtrim($this->descPropertyFeatures,', ');
				}
			} 
			return $this->descPropertyFeatures;			
		}		
		
		public function getPropertyLandArea()
		{
			if($this->descLandArea === null) {
				if($this->landMin > 0 && $this->landMax>0) {
					$this->descLandArea = $this->landMin . 'm<span class="met-sq">2</span> - ' . $this->landMax .'m<span class="met-sq">2</span>';
				} else if($this->landMax>0) {
					$this->descLandArea = $this->language->getValue(Al_Language::SEARCH_LBL_LESS_THAN).' '.$this->landMax.'m<span class="met-sq">2</span>';
				} else if($this->landMin>0) {
					$this->descLandArea = $this->language->getValue(Al_Language::SEARCH_LBL_MORE_THAN).' '.$this->landMin.'m<span class="met-sq">2</span>';
				} else {
					$this->descLandArea = '';
				}
			}
			return $this->descLandArea;
		}
		
		public function getPropertyBuildArea()
		{
			if($this->descBuildArea === null) {
				if($this->buildMin > 0 && $this->buildMax>0) {
					$this->descBuildArea = $this->buildMin . 'm<span class="met-sq">2</span> - ' . $this->buildMax .'m<span class="met-sq">2</span>';
				} else if($this->buildMax>0) {
					$this->descBuildArea = $this->language->getValue(Al_Language::SEARCH_LBL_LESS_THAN).' '.$this->buildMax.'m<span class="met-sq">2</span>';
				} else if($this->buildMin>0) {
					$this->descBuildArea = $this->language->getValue(Al_Language::SEARCH_LBL_MORE_THAN).' '.$this->buildMin.'m<span class="met-sq">2</span>';
				} else {
					$this->descBuildArea = '';
				}
			}
			return $this->descBuildArea;
		}		
		
		public function getArrayVal($key,$data) {
			$key = strval($key);
			if(array_key_exists($key, $data)) {
				return $data[$key];
			}
			return '';
		}
		
		
		public function getVal($field,$clearVal='') 
		{
			$val = '';
			if(property_exists($this,$field)) {
				$val = $this->{$field};
				if($clearVal != '' && $val == $clearVal) {
					$val = '';
				}
			}	
			return $val;
		}
		
		public function getFeatureCbVal($id) 
		{
			if(array_key_exists($id, $this->dataFeatures)) {
				return 'checked="checked"';
			} else {
				return '';
			}
		}

		public function getIntroductionCbVal($id) 
		{
			if(array_key_exists($id, $this->dataIntroduction)) {
				return 'checked="checked"';
			} else {
				return '';
			}
		}
		
		public function getPropertyTypesCbVal($id) 
		{
			if(array_key_exists($id, $this->dataPropertyTypes)) {
				return 'checked="checked"';
			} else {
				return '';
			}
		}		
		
		public function setTransactionType($type) 
		{
			$this->transactionType = $type;
		}

        public function getTransactionType()
        {
            return $this->transactionType;
        }
		
		public function requested() 
		{
			/**
			 * need to update search detection, if search performed display results regardles if search is empty
			 */
//			Al_Utilities::a($_GET);
			
			if(isset($_GET['search'])) {
				return true;
			} else if($this->location != '' || $this->propertyType != 0 || 	
				$this->priceMin > 0 || $this->priceMax>0
			) {
				return true;
			}
			
			return false;
		}
		
		public function getSortId() {
			return $this->sort;
		}
		
		public function setRequestValues() 
		{
			$this->location = substr(Al_Utilities::tget('location'),0,200);
			$this->propertyType = substr(Al_Utilities::tget('type'),0,200);
			$this->dataPropertyTypes = $this->getMultipleValues($this->propertyType);
			
			$this->priceMin = floatval(Al_Utilities::tget('pmin'));
			$this->priceMax = floatval(Al_Utilities::tget('pmax'));
			
			$this->depositMin = intval(Al_Utilities::tget('dmin'));
			$this->depositMax = intval(Al_Utilities::tget('dmax'));
			$this->bedrooms = intval(Al_Utilities::tget('bed'));
			$this->bathrooms = intval(Al_Utilities::tget('bath'));
			$this->parking = intval(Al_Utilities::tget('park'));
			$this->introduction = substr(Al_Utilities::tget('intro'),0,200);
			$this->dataIntroduction = $this->getMultipleValues($this->introduction);
			
			$this->features = substr(Al_Utilities::tget('feat'),0,200);
			$this->dataFeatures = $this->getMultipleValues($this->features);
			
			$this->landMin = intval(Al_Utilities::tget('lmin'));
			$this->landMax = intval(Al_Utilities::tget('lmax'));
			$this->buildMin = intval(Al_Utilities::tget('bmin'));
			$this->buildMax = intval(Al_Utilities::tget('bmax'));	
			
			$sort = Al_Utilities::tget('sort');
			if($sort == '') {
				$sort = $this->getSortFromSession();
			}
			$this->sort = intval($sort);
		}
		
		public function clearSearch($clearSort=false) 
		{
			$this->location = '';
			$this->propertyType = '';
			$this->priceMin = '0';
			$this->priceMax = '0';

			$this->depositMin = '0';
			$this->depositMax = '0';
			$this->bedrooms = '0';
			$this->bathrooms = '0';
			$this->parking = '0';
			$this->introduction = '';
			$this->features = '';
			$this->landMin = '0';
			$this->landMax = '0';
			$this->buildMin = '0';
			$this->buildMax = '0';			
			
			if($clearSort) {
				$this->sort = '';
			}
			$this->save();
		}
		
		public function loadFromSession() {
			if(@isset($_SESSION[$this->_settings->session][$this->_settings->session_property][$this->transactionType])) {			
				$data = $_SESSION[$this->_settings->session][$this->_settings->session_property][$this->transactionType];
				
				$this->location = $this->getDataValue($data, 'location', '');
				$this->propertyType = $this->getDataValue($data, 'type', '');
				$this->priceMin = $this->getDataValue($data, 'pmin', '0');
				$this->priceMax = $this->getDataValue($data, 'pmax', '0');
				
				$this->depositMin = $this->getDataValue($data, 'dmin', '0');
				$this->depositMax = $this->getDataValue($data, 'dmax', '0');
				$this->bedrooms = $this->getDataValue($data, 'bed', '0');
				$this->bathrooms = $this->getDataValue($data, 'bath', '0');
				$this->parking = $this->getDataValue($data, 'park', '0');
				$this->introduction = $this->getDataValue($data, 'intro');
				$this->features = $this->getDataValue($data, 'feat');
				$this->landMin = $this->getDataValue($data, 'lmin', '0');
				$this->landMax = $this->getDataValue($data, 'lmax', '0');
				$this->buildMin = $this->getDataValue($data, 'bmin', '0');
				$this->buildMax = $this->getDataValue($data, 'bmax', '0');				
				
			}
		}
		
		public function getDataValue($data,$field,$default='') 
		{
			if(isset($data[$field])) {
				$value = $data[$field];
			} else {
				$value = '';
			}			
			
			return $value;
		}
		
		
		public function getSortFromSession() 
		{
			if(@isset($_SESSION[$this->_settings->session][$this->_settings->session_property]['sort'])) {
				return $_SESSION[$this->_settings->session][$this->_settings->session_property]['sort'];
			} else {
				return '';
			}
		}
		
		public function save() {
			$data = array();
			
		    if($this->location != '') {
		    	$data['location'] = $this->location;
        	}
        	
        	if($this->propertyType != '') {
        		$data['type'] = $this->propertyType;
        	}
        	
		    if($this->priceMin > 0) {
		    	$data['min'] = $this->priceMin;
        	}
        	
		    if($this->priceMax > 0) {
		    	$data['max'] = $this->priceMax;
        	}
        	
			if($this->depositMin > 0) {
		    	$data['dmin'] = $this->depositMin;
        	}
        	
			if($this->depositMax > 0) {
		    	$data['dmax'] = $this->depositMax;
        	}
        	
			if($this->bedrooms > 0) {
		    	$data['bed'] = $this->bedrooms;
        	}

			if($this->bathrooms > 0) {
		    	$data['bath'] = $this->bathrooms;
        	}
	
			if($this->parking > 0) {
		    	$data['park'] = $this->parking;
        	}
        	
			if($this->introduction != '') {
		    	$data['intro'] = $this->introduction;
        	}
			
			if($this->features != '') {
		    	$data['feat'] = $this->features;
        	}
        	
			if($this->landMin > 0) {
		    	$data['lmin'] = $this->landMin;
        	}
        	
			if($this->landMax > 0) {
		    	$data['lmax'] = $this->landMax;
        	}
			
			if($this->buildMin > 0) {
		    	$data['bmin'] = $this->buildMin;
        	}
			
			if($this->buildMax > 0) {
		    	$data['bmax'] = $this->buildMax;
        	}	        	
        	
			switch($this->sort) {
        		case Bl_Data_PropertySort::latest:
        		case Bl_Data_PropertySort::price_htl:
        		case Bl_Data_PropertySort::price_lth:
        		case Bl_Data_PropertySort::property_size:
        			$sort = $this->sort;
        			break;
        		default:
        			$sort = Bl_Data_PropertySort::latest;
        			break;
			}
			
			$_SESSION[$this->_settings->session][$this->_settings->session_property]['sort'] = $sort;
			$_SESSION[$this->_settings->session][$this->_settings->session_property][$this->transactionType] = $data;
		}
		
		public function getQb($memberId=null)
		{
			$this->property = new Bl_Record_Property();
			$match = '';

        	$qb = $this->property->getQb();
        	
        	$qb->where("r.property_transaction_id = '".intval($this->transactionType)."'");
        	
        	$qb->where("r.temp = 0");
        	
        	$qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_EXPIRED)."'");
        	$qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_PENDING_APPROVAL)."'");


            if($memberId != null) {
                $qb->where("r.member_id = '".intval($memberId)."'");
            }
        	
        	
        	if($this->location != '') {
        		$match = $this->location.' ';
//        		$qb->where("
//        			ct.city_eng like '".Al_Db::escape($this->location)."%' ||
//        			ct.city_per like '".Al_Db::escape($this->location)."%' ||
//				  	r.address like '%".Al_Db::escape($this->location)."%' ||
//					r.property_id = '".trim(Al_Db::escape($this->location))."'        			  
//        		");
        	}
        	
        	if($this->propertyType !='') {
        		$in = $this->getInValues($this->dataPropertyTypes);
        		if($in != '') {
        			$qb->where("r.property_type_id in (".Al_Db::escape($in).")");
        		}
        	}
        	
		    if($this->priceMin > 0) {
        		$qb->where("r.price >= '" . Al_Db::escape($this->priceMin) . "'");
        	}
        	
		    if($this->priceMax > 0) {
        		$qb->where("r.price <= '" . Al_Db::escape($this->priceMax) . "'");
        	}
        	
			if($this->depositMin > 0) {
				$qb->where("r.deposit >= '" . Al_Db::escape($this->depositMin) . "'");
        	}
        	
			if($this->depositMax > 0) {
				$qb->where("r.deposit <= '" . Al_Db::escape($this->depositMax) . "'");
        	}
        	
			if($this->bedrooms > 0) {
				$qb->where("r.bedrooms >= '" . Al_Db::escape($this->bedrooms) . "'");
        	}

			if($this->bathrooms > 0) {
				$qb->where("r.bathrooms >= '" . Al_Db::escape($this->bathrooms) . "'");
        	}
	
			if($this->parking > 0) {
				$qb->where("r.parking >= '" . Al_Db::escape($this->parking) . "'");
        	}
        	
			if($this->introduction != '') {
				$in = $this->getInValues($this->dataIntroduction,'+yyy');
        		if($in != '') {
		    		$match .= $in.' ';
        		}
        	}
			
			if($this->features != '') {
				$in = $this->getInValues($this->dataFeatures,'+zzz');
        		if($in != '') {
		    		$match .= $in.' ';
        		}
        	}
        	
			if($this->landMin > 0) {
				$qb->where("r.land_area >= '" . Al_Db::escape($this->landMin) . "'");
        	}
        	
			if($this->landMax > 0) {
				$qb->where("r.land_area <= '" . Al_Db::escape($this->landMax) . "'");
        	}
			
			if($this->buildMin > 0) {
				$qb->where("r.build_area >= '" . Al_Db::escape($this->buildMin) . "'");
        	}
			
			if($this->buildMax > 0) {
				$qb->where("r.build_area <= '" . Al_Db::escape($this->buildMax) . "'");
        	}	        	
        	
        	if($match != '') {
        		$qb->where("
        			MATCH(prsr.features,prsr.introduction,prsr.property) 
					AGAINST('".Al_Db::escape($match)."' IN BOOLEAN MODE)
				");
        	}
        	
        	
        	if($this->sort>0) {
	        	switch($this->sort) {
	        		case Bl_Data_PropertySort::latest:
	        			$qb->order_by('r.id desc');
	        			break;
	        		case Bl_Data_PropertySort::price_htl:
	        			$qb->order_by('r.price desc');
	        			break;
	        		case Bl_Data_PropertySort::price_lth:
						$qb->order_by('r.price asc');
	        			break;	        			
	        		case Bl_Data_PropertySort::property_size:
	        			$qb->order_by('r.build_area asc');
	        			break;
	        		break;
	        	}
        	} else {
				$qb->order_by('r.id desc');
        	}
        	return $qb;
		}
		
		private function getMultipleValues($string,$sep='.') 
		{
			$s = array();
			$string = trim($string);
			$string = explode($sep, $string);
			foreach($string as $str) {
				if($str != '') {
					$str = intval($str);
					$s[$str] = $str;
				}
			}
			return $s;
		}
		
		public function getInValues($string,$prefix='') 
		{
			$s = '';
			foreach($string as $str) {
				if($str != '') {
					$s .= $prefix.intval($str).',';
				}
			}

			return rtrim($s,',');
		}
		
		public function getSearchParams() {
			$params = '';
		    if($this->location != '') {
		    	$params .= 'location/'.urlencode($this->location).'/';
        	}
        	
        	if($this->propertyType > 0) {
        		$params .= 'type/'.$this->propertyType.'/';
        	}
        	
		    if($this->priceMin > 0) {
		    	$params .= 'pmin/'.$this->priceMin.'/';
        	}
        	
		    if($this->priceMax > 0) {
		    	$params .= 'pmax/'.$this->priceMax.'/';
        	}
        	

			if($this->depositMin > 0) {
				$params .= 'dmin/'.$this->depositMin.'/';
        	}
        	
			if($this->depositMax > 0) {
				$params .= 'dmax/'.$this->depositMax.'/';
        	}
        	
			if($this->bedrooms > 0) {
				$params .= 'bed/'.$this->bedrooms.'/';
        	}

			if($this->bathrooms > 0) {
				$params .= 'bath/'.$this->bathrooms.'/';
        	}
	
			if($this->parking > 0) {
				$params .= 'park/'.$this->parking.'/';
        	}
        	
			if($this->introduction != '') {
				$params .= 'intro/'.$this->introduction.'/';
        	}
			
			if($this->features != '') {
				$params .= 'feat/'.$this->features.'/';
        	}
        	
			if($this->landMin > 0) {
				$params .= 'lmin/'.$this->landMin.'/';
        	}
        	
			if($this->landMax > 0) {
				$params .= 'lmax/'.$this->landMax.'/';
        	}
			
			if($this->buildMin > 0) {
				$params .= 'bmin/'.$this->buildMin.'/';
        	}
			
			if($this->buildMax > 0) {
				$params .= 'bmax/'.$this->buildMax.'/';
        	}	        	        	
        	
//        	if($params == '') {
        		$params .= 'search/1/';
//        	}
			return $params;
		}
		
		public function getSearchTitle() {
			
			$str = $this->language->getValue(Al_Language::DATA_SEARCH_RESULTS);
			
			
			$serch_details = '';
			
			if($this->location != '') {
				$serch_details .= Al_Utilities::html($this->location);
			}
			
			if($this->propertyType != '') {
				$pt = new Bl_Record_PropertyType();
				if($pt->load($this->propertyType)) {
					if($serch_details != '') {
						$serch_details .= ', ';
					}
					$serch_details .= $pt->getType();					
				}
			}
			
			if($serch_details != '') {
				$str .= ' - ' . $serch_details;	
			}
			
			return $str;
		}
		
		public function getSortParams() {
			$params = ''; 
			switch($this->sort) {
        		case Bl_Data_PropertySort::latest:
        		case Bl_Data_PropertySort::price_htl:
        		case Bl_Data_PropertySort::price_lth:
        		case Bl_Data_PropertySort::property_size:
        			$params.='sort/'.$this->sort;
        		break;
        	}
        	
        	return $params;
		}
		
		public function getAllParams() {
			return $this->getSearchParams().$this->getSortParams();
		}
	}