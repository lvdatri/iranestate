<?php
	class Bl_Sitemap 
	{
		public $_db;
		public function __construct() 
		{
			$this->_db = Al_Db::get_instance();
			$this->_settings = Bl_Settings::get_instance();
			$this->activeDate = date('Y-m-d');
		}
		
		public function show() 
		{
			$this->showXmlStart();
			$this->showCustom();
			$this->showPages();
            $this->showAgents();
			$this->showProperties();
			$this->showXmlEnd();
		}


        public function showAgents()
        {
            $member = new Bl_Record_Member();
            $res = $this->_db->query("
                SELECT m.id, m.name
                FROM members m
                WHERE show_profile = 1
            ");

            while($row = $res->fetch_row()) {
                echo $this->getXmlUrl($member->getMemberUrlFromId(Bl_Settings::LANG_ENGLISH_PREFIX, $row['id'],$row['name']),$this->activeDate);
                echo $this->getXmlUrl($member->getMemberUrlFromId(Bl_Settings::LANG_PERSIAN_PREFIX, $row['id'],$row['name']),$this->activeDate);
            }
        }
		
		public function showCustom() 
		{
			$path = $this->_settings->path_web.Bl_Settings::LANG_ENGLISH_PREFIX.'/';
			echo $this->getXmlUrl($path.'properties/buy',$this->activeDate);			
			echo $this->getXmlUrl($path.'properties/rent',$this->activeDate);
			echo $this->getXmlUrl($path.'properties/book',$this->activeDate);
			echo $this->getXmlUrl($path.'register',$this->activeDate);

			$path = $this->_settings->path_web.Bl_Settings::LANG_PERSIAN_PREFIX.'/';
			echo $this->getXmlUrl($path.'properties/buy',$this->activeDate);			
			echo $this->getXmlUrl($path.'properties/rent',$this->activeDate);
			echo $this->getXmlUrl($path.'properties/book',$this->activeDate);
			echo $this->getXmlUrl($path.'register',$this->activeDate);			
			
			$path = $this->_settings->path_web;	
			echo $this->getXmlUrl($path.'properties/buy',$this->activeDate,'monthly','0.4');			
			echo $this->getXmlUrl($path.'properties/rent',$this->activeDate,'monthly','0.4');
			echo $this->getXmlUrl($path.'properties/book',$this->activeDate,'monthly','0.4');
			echo $this->getXmlUrl($path.'register',$this->activeDate,'monthly','0.4');			
		}
		
		public function showXmlStart()
		{
			echo '<?xml version="1.0" encoding="UTF-8"?>'.
				 '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		}
		
		public function getXmlUrl($loc,$lastmod,$changefreq='monthly',$priority='0.8') {
			return  '<url>'.
					'<loc>'.$loc.'</loc>'.
      				'<lastmod>'.$lastmod.'</lastmod>'.
      				'<changefreq>'.$changefreq.'</changefreq>'.
      				'<priority>'.$priority.'</priority>'.
   					'</url>';
		}
		
		public function showXmlEnd()
		{
			echo '</urlset>';
		}
		
		
		public function showProperties()
		{
			$res = $this->_db->query("
				SELECT 
					p.id, p.property_id, p.property_transaction_id, DATE_FORMAT(p.creation_date,'%Y-%m-%d') creation_date_frmt, 
					pt.type_eng, c.city_eng, old_sitemap
				FROM properties p
				LEFT JOIN property_types pt ON pt.id = p.property_type_id
				LEFT JOIN cities c ON c.id = p.city_id
				WHERE p.temp = 0
				ORDER BY p.creation_date desc			
			");
			$web = $this->_settings->path_web;
			while($row = $res->fetch_row()) {
				$path = $this->getPropertyPath($row);
				
				$xml = $this->getXmlUrl($web.Bl_Settings::LANG_ENGLISH_PREFIX.'/'.$path,$row['creation_date_frmt']);
				$xml .= $this->getXmlUrl($web.Bl_Settings::LANG_PERSIAN_PREFIX.'/'.$path,$row['creation_date_frmt']);
				
//				if($row['old_sitemap'] == '1') {
//					$xml .= $this->getXmlUrl($web.$path,$row['creation_date_frmt'],'monthly','0.4');
//				} 
				
				echo $xml;
			}			
		}
		
		public function getPropertyPath(&$row) 
		{
			switch($row['property_transaction_id']) {
	    		case Bl_Record_Property::RENT :
	    			$transaction = Bl_Record_Property::RENT_PATH;
	    			break;
	    		case Bl_Record_Property::BOOK :
	    			$transaction = Bl_Record_Property::BOOK_PATH;
	    			break;
	    		default:
	    			$transaction = Bl_Record_Property::BUY_PATH;
	    			break;
	    	}    			

			$path = 	
					'properties/'.    	
	    			$transaction.'/'.
	    			$row['type_eng'].'/'.
	    			$row['city_eng'].'/'.
	    			$row['property_id'];
	    	
			
			return $path;
		}
		
		
		public function showPages() 
		{
			$res = $this->_db->query("
				SELECT 
					c.path, DATE_FORMAT(c.date_created,'%Y-%m-%d') date_created_frmt
				FROM cms c
			");
			
			while($row = $res->fetch_row()) {
				$xml = $this->getXmlUrl($this->_settings->path_web.Bl_Settings::LANG_ENGLISH_PREFIX.'/'.$row['path'],$row['date_created_frmt']);
				$xml .= $this->getXmlUrl($this->_settings->path_web.Bl_Settings::LANG_PERSIAN_PREFIX.'/'.$row['path'],$row['date_created_frmt']);
				echo $xml;
			}
		}
	}