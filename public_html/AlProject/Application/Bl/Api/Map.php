<?php

class Bl_Api_Map extends Al_Api
{
	private $_db;
	
	
	public function initialise() {
		$this->accessLevels = null;
		$this->_db = Al_Db::get_instance();
		$this->_settings = Bl_Settings::get_instance();
		$this->_language = Al_Language::get_instance();
	}
	
	public function getStr($key) {
		return $this->_language->getValue($key);
	}	
	
	public function getPropertyAction() {
		$pid = $this->requestData['pid'];
		
		
		
//		$this->recordPropertyType = Bl_Record_Property::BUY;
		
		$this->property = new Bl_Record_Property();
		$qb = $this->property->getQbSearchBase();
		$qb->where("r.id = '".intval($pid)."'");
		
//		$qb->where("r.property_transaction_id = '".intval($this->recordPropertyType)."'");
		
		$this->properties = $this->_db->query($qb->get_query());

		if(count($this->properties)>0) {
			$row = $this->properties->fetch_row();
			
			$this->recordPropertyType = $row['property_transaction_id'];
			$this->properties->reset();
			
	 		ob_start();
	 		require ($this->_settings->path_templates.'properties/map-listings-new-window.php');
	 		$content = ob_get_contents();
	 		ob_end_clean();
		} else {
			$content = "no properties found";
		}
		
		$this->serviceHandler->response->setData($content);		
	}
	
	private function setGetFromUriString($uri) 
	{
		$path = explode('/', $uri);
		
		while(count($path)>0) {
			$param = $path[0];
			array_shift($path);
			if(array_key_exists(0,$path)) {
				$value = $path[0];
				array_shift($path);
			} else {
				$value = '';
			}
			$this->_params[$param] = $value;

			$_GET = $this->_params;
		}		
	}
	
	public function getPropertiesAction() {
		$zoom = intval($this->requestData['zoom']);
		$tileCord = $this->requestData['tileCord'];
		$search = $this->requestData['search'];
		$transType = $this->requestData['transType'];
		
		
		$this->recordPropertyType = $transType;
		
// 		$this->recordPropertyType = Bl_Record_Property::BUY;
		
		switch ($zoom) {
			case 3:
			case 5:
			case 7:
			case 9:
			case 11:
			case 13:
			case 15:
			case 17:
			case 19:
				$this->setGetFromUriString($search);

				$this->propertySearch = new Bl_PropertySearch();
				$this->propertySearch->setTransactionType($transType);
					
				$qb = $this->propertySearch->getQb();
				$qb->where("r.level_".$zoom." = '".Al_Db::escape($tileCord)."'");
//				$qb->where("r.property_transaction_id = '".intval($this->recordPropertyType)."'");
				$qb->limit(30);
//				$qb->order_by("r.id desc");				
//				$this->propertySearch->save();
// 				$qb->select("r.level_".$zoom." tile, COUNT(r.id) rec_count"); //, r.lat, r.lon, r.id");
// 				$qb->where("r.`level_".$zoom."` IN (".$range.")");
// 				$qb->group_by("level_".$zoom);				
				
				
				
				$this->property = new Bl_Record_Property();
// 				$qb = $this->property->getQbSearchBase();
// 				$qb->where("r.level_".$zoom." = '".Al_Db::escape($tileCord)."'");
// 				$qb->where("r.property_transaction_id = '".intval($this->recordPropertyType)."'");
// 				$qb->limit(30);
// 				$qb->order_by("r.id desc");
				
				
				
				$this->properties = $this->_db->query($qb->get_query());
				
				if(count($this->properties)>0) {
					ob_start();
					require ($this->_settings->path_templates.'properties/map-listings-new-window.php');
					$content = ob_get_contents();
					ob_end_clean();
				} else {
					$content = "no properties found";
				}				
				
				break;
			default:
				$content = "no properties found";
		}
		
		
		
		$this->serviceHandler->response->setData($content);	
	}	
	
	public function getPointsAction()
	{
		$map = new Bl_Map();
		
		$neLat = $this->requestData['neLat'];
		$neLon = $this->requestData['neLon'];
		$swLat = $this->requestData['swLat'];
		$swLon = $this->requestData['swLon'];
		$zoom = intval($this->requestData['zoom']);
		$search = $this->requestData['search'];
		$transType = $this->requestData['transType'];
		
		$this->setGetFromUriString($search);
		
		$orgZoom = $zoom;
		
		if($zoom < 3) {
			$zoom = 3;
		} else if($zoom < 5) {
			$zoom = 5;
		} else if($zoom < 7) {
			$zoom = 7;
		} else if($zoom < 9) {
			$zoom = 9;
		} else if($zoom < 11) {
			$zoom = 11;
		} else if($zoom < 13) {
			$zoom = 13;
		} else if($zoom < 15) {
			$zoom = 15;
		} else if($zoom < 17) {
			$zoom = 17;
		} else if($zoom < 19) {
			$zoom = 19;
		} else {
			$zoom = 19;
		}
		
		
// 				echo $zoom . ':'. $neLat.', '.$neLon . ':' . $swLat .','. $swLon;		
		
		$range = $map->tileRange(
			$zoom,
			$neLat, $neLon,
			$swLat, $swLon
		);

		

		
		
		
		if($range != '') {
			
			
			$this->propertySearch = new Bl_PropertySearch();
			$this->propertySearch->setTransactionType($transType);			
			
			
			$qb = $this->propertySearch->getQb();
			$this->propertySearch->save();
			$qb->select("r.level_".$zoom." tile, COUNT(r.id) rec_count"); //, r.lat, r.lon, r.id");
			$qb->where("r.`level_".$zoom."` IN (".$range.")");
			$qb->group_by("level_".$zoom);
// 			echo $qb->get_query();
			$records = $this->_db->query($qb->get_query())->get_records();
		} else {
			$records = array();
		}
			
			
		// 			Al_Utilities::a($records);
		
		$items = array();
			
		foreach($records as $record) {
// 			echo $record['tile'] . ' - ' . $record['rec_count'].'<BR>';
		
			$latLon = $map->tileCordToLatLon($record['tile'],true);
		
			// 				echo $latLon['lat'] . ', ' . $latLon['lon']. '<BR>';
			
			$lat = $record['lat'];
			$lon = $record['lon'];			
		
// 			if($record['rec_count'] == '1') {
// 				$lat = $record['lat'];
// 				$lon = $record['lon'];
// 			} else {
// 				$lat = $latLon['lat'];
// 				$lon = $latLon['lon'];
// 			} 


			
			if($record['rec_count'] == '1') {
// 				Al_Utilities::a($record);
				
				if($record['property_status_id'] == '5') {
					$markerPath = $this->_settings->path_images.'map/pin-sold.png';
				} else {
					$markerPath = $this->_settings->path_images.'map/pin.png';
				}
				$type = 'p';
			} else {
				$markerPath = $this->_settings->path_images.'map/cluster-x1.png';
				$type = 'c';
			}			
			
// 			$markerPath = $this->_settings->path_images.'map/pin.png';

// 			$markerPath = $this->_settings->path_images.'map/cluster-x1.png';
			
			
			$items[] = array(
				'lat' => $lat,
				'lon' => $lon,
				'count' => $record['rec_count'],
				'tile' => $orgZoom.'__'.$record['tile'],
				'tileCord' => $record['tile'],
				'zoom' => $zoom,
				'image' => $markerPath,
				'type' => $type,
				'id' => $record['id'],
			);
		
// 			echo "setPoint(map,".$lat.",".$lon.", '".$record['rec_count']." - ".$record['level_13']."');<BR>"; //.$record['rec_count']." - ".$record['level_13']."');<BR>";
		
		}
		
		$this->serviceHandler->response->setData($items);
		
	}
	
    public function retrieveAction()
    {
    	$db = Al_Db::get_instance();
//        Al_Utilities::a($this->requestData);
        $user = Al_User::get_instance();
        if ($user->allow(array(Bl_Permissions::$oss))) {
        	
        	$jid = trim($this->requestData['jid']);
        	$subJob = false;
        	$pos = strpos($jid,'-');
        	
        	if($pos !== false) {
        		$r = explode('-', $jid);
        		$jid = $r[0];
        		$prefix = $r[1];
        		$subJob = true;
        	}
            
            $qb = new Al_QueryBuilder();
            
            $qb->select("
				js.`job_name`,
	            js.`actual_width`,
	            js.`actual_height`,
	            jfl.`quantity`,
	            jfl.`job_id`,
	            jfl.`jobs_specification_id`,
	            jfl.`jobs_finishing_id`,
	            jfl.id jobs_finishing_locations_id,
	            jl.`location_name`,
	            jl.`state`,
            	jl.id jobs_location_id,
	            si.id service_item_id,
	            si.`service_id`,
				jf.install_required,
				jf.install_notes
           	");
            
            $qb->from("jobs_finishing_locations jfl");
            $qb->join("LEFT JOIN `jobs_locations` jl ON jl.`id` = jfl.`jobs_location_id`");
            $qb->join("LEFT JOIN jobs j ON j.id = jfl.job_id");
            $qb->join("LEFT JOIN jobs_finishing jf ON jfl.jobs_finishing_id = jf.id");
            $qb->join("LEFT JOIN jobs_specifications js ON js.id = jf.jobs_specification_id");
            $qb->join("LEFT JOIN services_items si ON si.jobs_finishing_locations_id = jfl.id");
            
            if($subJob) {
            	$qb->where("j.parent_job_id = '".intval($jid) . "' ");
            	$qb->where("j.order_prefix = '".Al_Db::escape($prefix) . "' ");
            } else {
	            $qb->where("j.id ='" . intval($jid) . "'");
    	        $qb->where("j.id <> ''");
            }
            
            $qb->where("si.id IS NULL");
            
            $qb->order_by("jl.state, jl.location_name,js.job_name");
            
            $res = $db->query($qb->get_query());
            $this->serviceHandler->response->setData($res->get_records());
        } else {
            $this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
            $this->serviceHandler->response->setMessage("You don't have sufficient privileges to access items.");
        }
    }

    public function saveAction()
    {
        
    }
    
    public function removeItemAction()
    {
    	$db = Al_Db::get_instance();
    	$user = Al_User::get_instance();
    	if ($user->allow(array(Bl_Permissions::$oss))) {
    	
    		$sid = $this->requestData['sid'];
    		$siid = $this->requestData['siid'];
    	
    		$loaded = true;
    	
    		$service = new Bl_Record_Service();
    		if($service->load($sid)==false) {
    			$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
    			$this->serviceHandler->response->setMessage("Error removing item, service not found.");
    			$loaded = false;
    		}

    		$si = new Bl_Record_ServiceItem();
    		if($si->load($siid) == false) {
    			$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
    			$this->serviceHandler->response->setMessage("Error removing item, service not found.");
    			$loaded = false;
    		}   

    		if($si->getServiceId() != $service->getId()) {
	    		$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
	    		$this->serviceHandler->response->setMessage("Error removing item, service not found.");
	    		$loaded = false;
    		}    		
    	
    		if($loaded) {
   				$sid = $this->requestData['sid'];
   				$siid = $this->requestData['siid'];
   				$this->serviceHandler->response->setData(array(
   					"sid" => $sid,
   					"siid" => $siid,
   					"jflid" => $si->getJobsFinishingLocationsId(), 
   				));
   				
   				$si->delete();
    		}
	    	
    	} else {
    		$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
    		$this->serviceHandler->response->setMessage("You don't have sufficient privileges to access items.");
    	}
    }
    
    public function addItemAction()
    {
    	$db = Al_Db::get_instance();
    	$user = Al_User::get_instance();
    	if ($user->allow(array(Bl_Permissions::$oss))) {

    		$sid = $this->requestData['sid'];
    		$jflid = $this->requestData['jflid'];
    		
    		$loaded = true;
   	
    		$service = new Bl_Record_Service();
    		if($service->load($sid)==false) {
    			$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
    			$this->serviceHandler->response->setMessage("Error adding item, service not found.");
    			$loaded = false;
    		} 
	    		
    		$jfl = new Bl_Record_JobsFinishingLocation();
	    	if($jfl->load($jflid)==false) {
	    		$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
	    		$this->serviceHandler->response->setMessage("Error adding item, item not found.");
	    		$loaded = false;    		
	    	} 
	    	
	    	$jf = new Bl_Record_JobsFinishing();
	    	if($jf->load($jfl->getJobsFinishingId())==false) {
	    		$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
	    		$this->serviceHandler->response->setMessage("Error adding item, finishing item not found.");
	    		$loaded = false;	    		
	    	}
	    	
	    	if($loaded) {
    			$res = $db->query("
    				SELECT id 
    				FROM services_items
    				WHERE jobs_finishing_locations_id = '" . intval($jfl->getId()) . "'
    				AND service_id = '" . intval($service->getId()) . "'
    			");
    			
    			$sid = $service->getId();
    			$jflid = $jfl->getId();
    			if($res->num_rows()>0) {
    				$row = $res->fetch_row();
    				$siid = $row['id'];
    			} else {
	    			$si = new Bl_Record_ServiceItem();
	    			$si->setJobId($jfl->getJobId());
	    			$si->setJobsFinishingId($jfl->getJobsFinishingId());
	    			$si->setJobsFinishingLocationsId($jfl->getId());
	    			$si->setJobsSpecificationsId($jfl->getJobsSpecificationId());
	    			$si->setNotes($jf->getInstallNotes());
	    			$si->setQuantity($jfl->getQuantity());
	    			$si->setServiceId($service->getId());
	    			$si->save();
	    			$siid = $si->getId();
    			}
    			
    			$this->serviceHandler->response->setData(array(
    					"sid" => $sid,
    					"siid" => $siid,
    					"jflid" => $jflid,
    			));	    			
    		}

    	
    	} else {
    		$this->serviceHandler->response->setStatus(Al_JsonResponse::$CUSTOM_ERROR);
    		$this->serviceHandler->response->setMessage("You don't have sufficient privileges to access items.");
    	}    	
    	
    	
    }    

}

