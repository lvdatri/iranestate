<?php
class Bl_Record_Log extends Al_Record
{
	public static $PROPERTY_SEARCH_MAINTENANCE = '1';
	public static $EXPIRED_PROPERTY_SET_TO_SOLD = '2';
	
    public function initialiseRecord()
    {
        $this->_db_table = 'logs';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('type');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('message');
        $field->setMaxLength(255);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('date_created');
        $this->_add_field($field);
    }

    public function before_insert()
    {
		$this->setDateCreated('now()',true);    	
    }
    
    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setType($value, $escaped = false)
    {
        $this->_set_field_value('type',$value,$escaped);
    }

    public function setMessage($value, $escaped = false)
    {
        $this->_set_field_value('message',$value,$escaped);
    }

    public function setDateCreated($value, $escaped = false)
    {
        $this->_set_field_value('date_created',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getType()
    {
        return $this->_get_field_value('type');
    }

    public function getMessage()
    {
        return $this->_get_field_value('message');
    }

    public function getDateCreated()
    {
        return $this->_get_field_value('date_created');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }

}
