<?php
class Bl_Record_PropertyIntroduction extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'property_introduction';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('option_eng');
        $field->setMaxLength(65);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('option_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('100',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('skey');
        $field->setMaxLength(6);
        $this->_add_field($field);        
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setOptionEng($value, $escaped = false)
    {
        $this->_set_field_value('option_eng',$value,$escaped);
    }

    public function setOptionPer($value, $escaped = false)
    {
        $this->_set_field_value('option_per',$value,$escaped);
    }

    public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }
    
	public function setSkey($value='', $escaped = false)
    {
    	if($value == '') {
    		$value = 'yyy'.$this->getId();
    	}
        $this->_set_field_value('skey',$value,$escaped);
    }    

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getOptionEng()
    {
        return $this->_get_field_value('option_eng');
    }

    public function getOptionPer()
    {
        return $this->_get_field_value('option_per');
    }

    public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }
    
    public function getSkey()
    {
        return $this->_get_field_value('skey');
    }
    

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }

    public function retrieveCbGroupValues($language='',$noOfChunks=0) {
    	
    	if($language!=Bl_Settings::LANG_PERSIAN) {
    		$language = Bl_Settings::LANG_ENGLISH;
    		$labelKey = 'option_eng';	
    	} else {
    		$labelKey = 'option_per';
    	}
    	
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->order_by('r.sort_order, r.option_eng');
		$res = $this->_retrieveRecords();
		
		$data = array();
		
		while($item = $res->fetch_row()) {
			$data[$item['id']] = array(
				'id' => $item['id'],
				'label' => $item[$labelKey],
				'label_eng' => $item['option_eng'],
				'label_per' => $item['option_per'],
			);
		}
		
		
		if($noOfChunks > 1) {
			$data = array_chunk($data,$noOfChunks);
			$last = end($data);

			foreach($data as &$items) {
				$i = 0;
				foreach($items as &$item) {
					$i++;
				}
				while($i < $noOfChunks) {
					$items[$i]['id'] = '';
					$items[$i]['label'] = '';
					$i++;
				}
			}
		}

		return $data;
    	
    }

}