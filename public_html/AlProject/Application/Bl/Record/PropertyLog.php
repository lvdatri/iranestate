<?php
class Bl_Record_PropertyLog extends Al_Record
{
	const PROPERTY_VIEW = '1';
	const PROPERTY_CONTACT = '2';
	
    public function initialiseRecord()
    {
        $this->_db_table = 'property_logs';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('property_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('type');
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('member_id');
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('user_ip');
        $field->setMaxLength(15);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('log_date');
        $field->setValue('null',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setPropertyId($value, $escaped = false)
    {
        $this->_set_field_value('property_id',$value,$escaped);
    }

    public function setType($value, $escaped = false)
    {
        $this->_set_field_value('type',$value,$escaped);
    }

    public function setMemberId($value, $escaped = false)
    {
        $this->_set_field_value('member_id',$value,$escaped);
    }

    public function setUserIp($value, $escaped = false)
    {
        $this->_set_field_value('user_ip',$value,$escaped);
    }

    public function setLogDate($value, $escaped = false)
    {
        $this->_set_field_value('log_date',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getPropertyId()
    {
        return $this->_get_field_value('property_id');
    }

    public function getType()
    {
        return $this->_get_field_value('type');
    }

    public function getMemberId()
    {
        return $this->_get_field_value('member_id');
    }

    public function getUserIp()
    {
        return $this->_get_field_value('user_ip');
    }

    public function getLogDate()
    {
        return $this->_get_field_value('log_date');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
    public function setCommon()
    {
    	$this->setLogDate(Al_Utilities::datetime_now_mysql());
    	$this->setUserIp($_SERVER['REMOTE_ADDR']);
    	$user = Al_User::get_instance();
    	if($user->logged_in()) {
    		$this->setMemberId($user->get('id'));
    	}
    }
    
    public function save()
    {
    	$settings = Bl_Settings::get_instance();
    	$this->_qb = new Al_QueryBuilder();
    	
    	if($this->getType() == self::PROPERTY_CONTACT) {
    		if(!isset($_SESSION[$settings->session][$settings->session_property_count_contact][$this->getPropertyId()])) {
    			$this->before_insert();
            	$this->_insert();
            	$property = new Bl_Record_Property();
            	if($property->load($this->getPropertyId())) {
            		$property->setContactCount($property->getContactCount() + 1,true);
            		$property->save();
            	}
            	$_SESSION[$settings->session][$settings->session_property_count_contact][$this->getPropertyId()] = '1';
    		}
    	} else {
    	    if(!isset($_SESSION[$settings->session][$settings->session_property_count_view][$this->getPropertyId()])) {
    			$this->before_insert();
            	$this->_insert();
    	        $property = new Bl_Record_Property();
            	if($property->load($this->getPropertyId())) {
            		$property->setViewCount($property->getViewCount()+1,true);
            		$property->save();
            	}            	
            	$_SESSION[$settings->session][$settings->session_property_count_view][$this->getPropertyId()] = '1';
    		}    	
    	}
    }    

}
