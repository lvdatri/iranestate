<?php
class Bl_Record_Email extends Al_Record
{
	public $emailTags;

    public function initialiseRecord()
    {
        $this->_db_table = 'emails';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('title');
        $field->setMaxLength(200);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('description');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('subject_eng');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('content_eng');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('subject_per');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('content_per');
        $field->setValue('null',true);
        $this->_add_field($field);
        
		$field = new Al_Record_FieldString();
        $field->setName('tags');
        $field->setMaxLength(255);
        $this->_add_field($field);     

        $this->emailTags = array();
		$this->_settings = Bl_Settings::get_instance();        
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setTitle($value, $escaped = false)
    {
        $this->_set_field_value('title',$value,$escaped);
    }

    public function setDescription($value, $escaped = false)
    {
        $this->_set_field_value('description',$value,$escaped);
    }

    public function setSubjectEng($value, $escaped = false)
    {
        $this->_set_field_value('subject_eng',$value,$escaped);
    }

    public function setContentEng($value, $escaped = false)
    {
        $this->_set_field_value('content_eng',$value,$escaped);
    }

    public function setSubjectPer($value, $escaped = false)
    {
        $this->_set_field_value('subject_per',$value,$escaped);
    }

    public function setContentPer($value, $escaped = false)
    {
        $this->_set_field_value('content_per',$value,$escaped);
    }
    
	public function setTags($value, $escaped = false)
    {
        $this->_set_field_value('tags',$value,$escaped);
    }    

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getTitle()
    {
        return $this->_get_field_value('title');
    }

    public function getDescription()
    {
        return $this->_get_field_value('description');
    }

    public function getSubjectEng()
    {
        return $this->_get_field_value('subject_eng');
    }

    public function getContentEng()
    {
        return $this->_get_field_value('content_eng');
    }

    public function getSubjectPer()
    {
        return $this->_get_field_value('subject_per');
    }

    public function getContentPer()
    {
        return $this->_get_field_value('content_per');
    }
    
	public function getTags()
    {
        return $this->_get_field_value('tags');
    }    
    
	public function getTagsArray()
    {
    	$tags = explode(',',$this->_get_field_value('tags'));
    	foreach($tags as &$tag) {
    		$tag = '['.trim($tag).']';
    	}
        return $tags;
    }    
    

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
    public function getSubject($raw=false)
    {
    	$content = '';
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	$content = $this->_getDataValue('subject_eng');
		} else {
			if($this->_getDataValue('subject_per')=='') {
				$content = $this->_getDataValue('subject_eng');					
			} else {
				$content = $this->_getDataValue('subject_per');
			}
		}
		
		if($raw) {
			return $content;
		} else {
			return $this->getStringTags($content);
		}
    }    
    
    public function getContent($raw=false)
    {
    	$content = '';
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	$content = $this->_getDataValue('content_eng');
		} else {
			if($this->_getDataValue('content_per')=='') {
				$content = $this->_getDataValue('content_eng');					
			} else {
				$content = $this->_getDataValue('content_per');
			}
		}

		if($raw) {
			return $content;
		} else {
			return $this->getStringTags($content);
		}
    }
    
    public function addTag($tag,$value) 
    {
    	$this->emailTags[$tag] = $value;
    }
    
	public function getStringTags($content) {
		foreach($this->emailTags as $key=>$value) {
        	$content = str_ireplace('['.$key.']',$value,$content);
		}
		return nl2br($content);
	}    

}