<?php
class Bl_Record_PropertyType extends Al_Record
{

    public function initialiseRecord()
    {
    	$this->_settings = Bl_Settings::get_instance();
        $this->_db_table = 'property_types';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('type_eng');
        $field->setMaxLength(65);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('type_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('null',true);
        $field->setValue('100',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setTypeEng($value, $escaped = false)
    {
        $this->_set_field_value('type_eng',$value,$escaped);
    }

    public function setTypePer($value, $escaped = false)
    {
        $this->_set_field_value('type_per',$value,$escaped);
    }

    public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getTypeEng()
    {
        return $this->_get_field_value('type_eng');
    }

    public function getTypePer()
    {
        return $this->_get_field_value('type_per');
    }
    
    public function getType() 
    {
    	
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->getTypeEng();
		} else {
			return $this->getTypePer();
		}
    }

    public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
    public function retrieveRecords() {
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->order_by('r.sort_order, r.type_eng');
		$res = $this->_retrieveRecords();

		$settings = Bl_Settings::get_instance();
		
		$data = array();
		while($row = $res->fetch_row()) {
			if($settings->language == Bl_Settings::LANG_ENGLISH) {
				$row['type'] = $row['type_eng'];
			} else {
				$row['type'] = $row['type_per'];
			}
			$data[] = $row;			
		}
		return $data;
    }
    
    public function ddData($id,$emptyVal='') {
        $dd = new Al_DropDown();

        $dd->setFirstOption(true,$emptyVal);
        return $dd->setFromArray($this->retrieveRecords(),$id,'type','id');    	
    }

    public function retrieveCbGroupValues($language='',$noOfChunks=0) {
    	
    	if($language!=Bl_Settings::LANG_PERSIAN) {
    		$language = Bl_Settings::LANG_ENGLISH;
    		$labelKey = 'type_eng';	
    	} else {
    		$labelKey = 'type_per';
    	}
    	
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->order_by('r.sort_order, r.type_eng');
		$res = $this->_retrieveRecords();
		
		$data = array();
		
		while($item = $res->fetch_row()) {
			$data[$item['id']] = array(
				'id' => $item['id'],
				'label' => $item[$labelKey],
				'label_eng' => $item['type_eng'],
				'label_per' => $item['type_per'],
			);
		}
		
		
    	if($noOfChunks > 1) {
			$data = array_chunk($data,$noOfChunks);
			$last = end($data);

			foreach($data as &$items) {
				$i = 0;
				foreach($items as &$item) {
					$i++;
				}
				while($i < $noOfChunks) {
					$items[$i]['id'] = '';
					$items[$i]['label'] = '';
					$i++;
				}
			}
		}

		return $data;
    	
    }    

}
