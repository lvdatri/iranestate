<?php
class Bl_Record_PropertyFeature extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'property_features';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('feature_eng');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('feature_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('null',true);
        $field->setValue('100',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('skey');
        $field->setMaxLength(6);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setFeatureEng($value, $escaped = false)
    {
        $this->_set_field_value('feature_eng',$value,$escaped);
    }

    public function setFeaturePer($value, $escaped = false)
    {
        $this->_set_field_value('feature_per',$value,$escaped);
    }

    public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }
    
    public function setSkey($value='', $escaped = false)
    {
    	if($value == '') {
    		$value = 'zzz'.$this->getId();
    	}
        $this->_set_field_value('skey',$value,$escaped);
    }    

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getFeatureEng()
    {
        return $this->_get_field_value('feature_eng');
    }

    public function getFeaturePer()
    {
        return $this->_get_field_value('feature_per');
    }

    public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }
    
    public function getSkey()
    {
        return $this->_get_field_value('skey');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
    public function retrieveCbGroupValues($language='',$noOfChunks=0) {
    	
    	if($language!=Bl_Settings::LANG_PERSIAN) {
    		$language = Bl_Settings::LANG_ENGLISH;
    		$labelKey = 'feature_eng';	
    	} else {
    		$labelKey = 'feature_per';
    	}
    	
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->order_by('r.sort_order, r.feature_eng');
		$res = $this->_retrieveRecords();
		
		$data = array();
		
		while($item = $res->fetch_row()) {
			$data[$item['id']] = array(
				'id' => $item['id'],
				'label' => $item[$labelKey],
				'label_eng' => $item['feature_eng'],
				'label_per' => $item['feature_per'],
			);
		}
		
		
    	if($noOfChunks > 1) {
			$data = array_chunk($data,$noOfChunks);
			$last = end($data);

			foreach($data as &$items) {
				$i = 0;
				foreach($items as &$item) {
					$i++;
				}
				while($i < $noOfChunks) {
					$items[$i]['id'] = '';
					$items[$i]['label'] = '';
					$i++;
				}
			}
		}

		return $data;
    	
    }


}
