<?php 
class Bl_Record_Client extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'clients';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('client_name');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('client_description');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('client_code');
        $field->setMaxLength(3);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('deleted');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('courier_local_id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('courier_interstate_id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('modification_date');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('cost');
        $field->setDecimalPoints(2);
        $field->setValue('null',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setClientName($value, $escaped = false)
    {
        $this->_set_field_value('client_name',$value,$escaped);
    }

    public function setClientDescription($value, $escaped = false)
    {
        $this->_set_field_value('client_description',$value,$escaped);
    }

    public function setClientCode($value, $escaped = false)
    {
        $this->_set_field_value('client_code',$value,$escaped);
    }

    public function setDeleted($value, $escaped = false)
    {
        $this->_set_field_value('deleted',$value,$escaped);
    }

    public function setCourierLocalId($value, $escaped = false)
    {
        $this->_set_field_value('courier_local_id',$value,$escaped);
    }

    public function setCourierInterstateId($value, $escaped = false)
    {
        $this->_set_field_value('courier_interstate_id',$value,$escaped);
    }

    public function setModificationDate($value, $escaped = false)
    {
        $this->_set_field_value('modification_date',$value,$escaped);
    }

    public function setCost($value, $escaped = false)
    {
        $this->_set_field_value('cost',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getClientName()
    {
        return $this->_get_field_value('client_name');
    }

    public function getClientDescription()
    {
        return $this->_get_field_value('client_description');
    }

    public function getClientCode()
    {
        return $this->_get_field_value('client_code');
    }

    public function getDeleted()
    {
        return $this->_get_field_value('deleted');
    }

    public function getCourierLocalId()
    {
        return $this->_get_field_value('courier_local_id');
    }

    public function getCourierInterstateId()
    {
        return $this->_get_field_value('courier_interstate_id');
    }

    public function getModificationDate()
    {
        return $this->_get_field_value('modification_date');
    }

    public function getCost()
    {
        return $this->_get_field_value('cost');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
}