<?php
class Bl_Record_Zone extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'zones';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('country_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('city_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('zone_eng');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('zone_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('null',true);
        $field->setValue('100',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setCountryId($value, $escaped = false)
    {
        $this->_set_field_value('country_id',$value,$escaped);
    }

    public function setCityId($value, $escaped = false)
    {
        $this->_set_field_value('city_id',$value,$escaped);
    }

    public function setZoneEng($value, $escaped = false)
    {
        $this->_set_field_value('zone_eng',$value,$escaped);
    }

    public function setZonePer($value, $escaped = false)
    {
        $this->_set_field_value('zone_per',$value,$escaped);
    }

    public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getCountryId()
    {
        return $this->_get_field_value('country_id');
    }

    public function getCityId()
    {
        return $this->_get_field_value('city_id');
    }

    public function getZoneEng()
    {
        return $this->_get_field_value('zone_eng');
    }

    public function getZonePer()
    {
        return $this->_get_field_value('zone_per');
    }

    public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }

    public function retrieveRecordsByCity($cityId) {
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->where("r.city_id ='".intval($cityId)."'");
		return $this->_retrieveRecords(true);    	
    }    

}
