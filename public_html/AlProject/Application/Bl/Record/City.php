<?php
class Bl_Record_City extends Al_Record
{
	public static $NATIONAL = 1;
	public static $INTERNATIONAL = 2;
	public static $ALL = 3;

    public function initialiseRecord()
    {
        $this->_db_table = 'cities';
        $this->_settings = Bl_Settings::get_instance();
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('country_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('city_eng');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('city_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('featured');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('null',true);
        $field->setValue('100',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('lat');
        $field->setMaxLength(32);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('lon');
        $field->setMaxLength(32);
        $field->setValue('null',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setCountryId($value, $escaped = false)
    {
        $this->_set_field_value('country_id',$value,$escaped);
    }

    public function setCityEng($value, $escaped = false)
    {
        $this->_set_field_value('city_eng',$value,$escaped);
    }

    public function setCityPer($value, $escaped = false)
    {
        $this->_set_field_value('city_per',$value,$escaped);
    }

    public function setFeatured($value, $escaped = false)
    {
        $this->_set_field_value('featured',$value,$escaped);
    }

    public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }
    
    public function setLat($value, $escaped = false)
    {
        $this->_set_field_value('lat',$value,$escaped);
    }

    public function setLon($value, $escaped = false)
    {
        $this->_set_field_value('lon',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getCountryId()
    {
        return $this->_get_field_value('country_id');
    }

    public function getCityEng()
    {
        return $this->_get_field_value('city_eng');
    }

    public function getCityPer()
    {
        return $this->_get_field_value('city_per');
    }

    public function getCity() {
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->_getDataValue('city_eng');
		} else {
			if($this->_getDataValue('city_per')=='') {
				return $this->_getDataValue('city_eng');					
			} else {
				return $this->_getDataValue('city_per');
			}
		}
    }
    
    public function getFeatured()
    {
        return $this->_get_field_value('featured');
    }

    public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }
    
    public function getLat()
    {
        return $this->_get_field_value('lat');
    }

    public function getLon()
    {
        return $this->_get_field_value('lon');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
    public function retrieveRecordsByCountry($countryId) {
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->where("r.country_id ='".intval($countryId)."'");
		return $this->_retrieveRecords(true);    	
    }
    
    public function retrieveZones() {
    	$zone = new Bl_Record_Zone();
    	return $zone->retrieveRecordsByCity($this->getId());
    }
    
    public function retrieveNatioanlFeatured($noOfChunks=0) 
    {
    	return $this->retrieveFeatured(self::$NATIONAL,$noOfChunks);	
    }
    
	public function retrieveInternatioanlFeatured($noOfChunks=0) 
	{
    	return $this->retrieveFeatured(self::$INTERNATIONAL,$noOfChunks);	
    }    
    
    public function retrieveFeatured($type=null,$noOfChunks=0) {
    	if($type==null) {
    		$type = self::$ALL;
    	}
    	
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->where("r.featured = 1");
		$this->_qb->join("LEFT JOIN countries c ON c.id = r.country_id");
		$this->_qb->order_by("c.sort_order, c.name_eng, r.sort_order,r.city_eng");
		if($type==self::$INTERNATIONAL) {
			$this->_qb->where("c.id <> 2");
		} else if($type==self::$NATIONAL) {
			$this->_qb->where("c.id = 2");	
		}
		$data = $this->_retrieveRecords(true);

		if($noOfChunks>1) {
		    if($noOfChunks > 1) {
				$data = array_chunk($data,$noOfChunks);
				$last = end($data);
	
				foreach($data as &$items) {
					$i = 0;
					foreach($items as &$item) {
						$i++;
					}
					while($i < $noOfChunks) {
						$items[$i]['id'] = '';
						$i++;
					}
				}
			}			
		}
		return $data;
			
    }    
    
    public function getPropertySearchPath($transactionPath) {
    	$city = strtolower(str_replace(' ','-',$this->getCityEng()));
		return $this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/properties/'.$transactionPath.'/location/'.$city;    	    	
    }

}
