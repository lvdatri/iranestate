<?php
class Bl_Record_Property extends Al_Record
{
	const BUY = '1';
	const RENT = '2';
	const BOOK = '3';
	
	const BUY_PATH = 'buy';
	const RENT_PATH = 'rent';
	const BOOK_PATH = 'book';	

    public function initialiseRecord()
    {
        $this->_db_table = 'properties';
        $this->_settings = Bl_Settings::get_instance();
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('member_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('features');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('features_eng');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('features_per');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('introduction');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('introduction_eng');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('introduction_per');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('property_transaction_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('property_type_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('property_price_type_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('property_status_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('price');
        $field->setDecimalPoints(2);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('swimming_pool');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('country_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('city_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('zone_id');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('address');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('postal_code');
        $field->setMaxLength(32);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('zone');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('comments_eng');
        $field->setValue('null',true);
        $this->_add_field($field);
        
		$field = new Al_Record_FieldText();
        $field->setName('comments_per');
        $field->setValue('null',true);
        $this->_add_field($field);        
        
        $field = new Al_Record_FieldInt();
        $field->setName('land_area');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('build_area');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('bedrooms');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('bathrooms');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('parking');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('contact_name');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('contact_phone');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('contact_email');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('lat');
        $field->setMaxLength(32);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('lon');
        $field->setMaxLength(32);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('depth');
        $field->setValue('0');
        $this->_add_field($field);        
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('expiry_date');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('creation_date');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('featured');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('property_id');
        $field->setMaxLength(32);
        $this->_add_field($field);   

        $field = new Al_Record_FieldInt();
        $field->setName('view_count');
        $field->setValue('0',true);
        $this->_add_field($field);

        $field = new Al_Record_FieldInt();
        $field->setName('contact_count');
        $field->setValue('0',true);
        $this->_add_field($field);        
        
		$field = new Al_Record_FieldInt();
        $field->setName('temp');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('terrace_m2');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('land_width');
        $field->setDecimalPoints(2);
        $field->setValue('0.00',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('land_height');
        $field->setDecimalPoints(2);
        $field->setValue('0.00',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('average_ceiling_height');
        $field->setDecimalPoints(2);
        $field->setValue('0.00',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('located_on_level');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('number_of_levels');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('ceiling_height');
        $field->setDecimalPoints(2);
        $field->setValue('0.00',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('deposit');
        $field->setDecimalPoints(2);
        $field->setValue('0.00',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('date_updated');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('main_image_id');
        $field->setValue('0',true);
        $this->_add_field($field);        
    }
    
    public function before_insert()
    {
    	$this->setDateUpdated('now()',true);
    }
    
    public function before_update()
    {
    	$this->setDateUpdated('now()',true);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }
    
	public function setMemberId($value, $escaped = false)
    {
        $this->_set_field_value('member_id',$value,$escaped);
    }    

    public function setFeatures($value, $escaped = false)
    {
        $this->_set_field_value('features',$value,$escaped);
    }

    public function setFeaturesEng($value, $escaped = false)
    {
        $this->_set_field_value('features_eng',$value,$escaped);
    }

    public function setFeaturesPer($value, $escaped = false)
    {
        $this->_set_field_value('features_per',$value,$escaped);
    }

    public function setIntroduction($value, $escaped = false)
    {
        $this->_set_field_value('introduction',$value,$escaped);
    }

    public function setIntroductionEng($value, $escaped = false)
    {
        $this->_set_field_value('introduction_eng',$value,$escaped);
    }

    public function setIntroductionPer($value, $escaped = false)
    {
        $this->_set_field_value('introduction_per',$value,$escaped);
    }

    public function setPropertyTransactionId($value, $escaped = false)
    {
        $this->_set_field_value('property_transaction_id',$value,$escaped);
    }

    public function setPropertyTypeId($value, $escaped = false)
    {
        $this->_set_field_value('property_type_id',$value,$escaped);
    }

    public function setPropertyPriceTypeId($value, $escaped = false)
    {
        $this->_set_field_value('property_price_type_id',$value,$escaped);
    }

    public function setPropertyStatusId($value, $escaped = false)
    {
        $this->_set_field_value('property_status_id',$value,$escaped);
    }

    public function setPrice($value, $escaped = false)
    {
        $this->_set_field_value('price',$value,$escaped);
    }

    public function setCountryId($value, $escaped = false)
    {
        $this->_set_field_value('country_id',$value,$escaped);
    }

    public function setCityId($value, $escaped = false)
    {
        $this->_set_field_value('city_id',$value,$escaped);
    }

    public function setZoneId($value, $escaped = false)
    {
        $this->_set_field_value('zone_id',$value,$escaped);
    }

    public function setAddress($value, $escaped = false)
    {
        $this->_set_field_value('address',$value,$escaped);
    }

    public function setPostalCode($value, $escaped = false)
    {
        $this->_set_field_value('postal_code',$value,$escaped);
    }

    public function setZone($value, $escaped = false)
    {
        $this->_set_field_value('zone',$value,$escaped);
    }

    public function setCommentsEng($value, $escaped = false)
    {
        $this->_set_field_value('comments_eng',$value,$escaped);
    }
    
	public function setCommentsPer($value, $escaped = false)
    {
        $this->_set_field_value('comments_per',$value,$escaped);
    }    

    public function setLandArea($value, $escaped = false)
    {
        $this->_set_field_value('land_area',$value,$escaped);
    }

    public function setBuildArea($value, $escaped = false)
    {
        $this->_set_field_value('build_area',$value,$escaped);
    }

    public function setBedrooms($value, $escaped = false)
    {
        $this->_set_field_value('bedrooms',$value,$escaped);
    }

    public function setBathrooms($value, $escaped = false)
    {
        $this->_set_field_value('bathrooms',$value,$escaped);
    }

    public function setParking($value, $escaped = false)
    {
        $this->_set_field_value('parking',$value,$escaped);
    }

    public function setContactName($value, $escaped = false)
    {
        $this->_set_field_value('contact_name',$value,$escaped);
    }

    public function setContactPhone($value, $escaped = false)
    {
        $this->_set_field_value('contact_phone',$value,$escaped);
    }

    public function setContactEmail($value, $escaped = false)
    {
        $this->_set_field_value('contact_email',$value,$escaped);
    }

    public function setLat($value, $escaped = false)
    {
        $this->_set_field_value('lat',$value,$escaped);
    }

    public function setLon($value, $escaped = false)
    {
        $this->_set_field_value('lon',$value,$escaped);
    }
    
	public function setDepth($value, $escaped = false)
    {
        $this->_set_field_value('depth',$value,$escaped);
    }    

    public function setExpiryDate($value, $escaped = false)
    {
        $this->_set_field_value('expiry_date',$value,$escaped);
    }

    public function setCreationDate($value, $escaped = false)
    {
        $this->_set_field_value('creation_date',$value,$escaped);
    }

    public function setFeatured($value, $escaped = false)
    {
        $this->_set_field_value('featured',$value,$escaped);
    }

    public function setPropertyId($value, $escaped = false)
    {
        $this->_set_field_value('property_id',$value,$escaped);
    }    
    
    public function setViewCount($value, $escaped = false)
    {
        $this->_set_field_value('view_count',$value,$escaped);
    }   

    public function setContactCount($value, $escaped = false)
    {
        $this->_set_field_value('contact_count',$value,$escaped);
    }    
    
    public function setTemp($value, $escaped = false)
    {
        $this->_set_field_value('temp',$value,$escaped);
    }    
    
    public function setTerraceM2($value, $escaped = false)
    {
        $this->_set_field_value('terrace_m2',$value,$escaped);
    }

    public function setLandWidth($value, $escaped = false)
    {
        $this->_set_field_value('land_width',$value,$escaped);
    }

    public function setLandHeight($value, $escaped = false)
    {
        $this->_set_field_value('land_height',$value,$escaped);
    }

    public function setAverageCeilingHeight($value, $escaped = false)
    {
        $this->_set_field_value('average_ceiling_height',$value,$escaped);
    }

    public function setLocatedOnLevel($value, $escaped = false)
    {
        $this->_set_field_value('located_on_level',$value,$escaped);
    }

    public function setNumberOfLevels($value, $escaped = false)
    {
        $this->_set_field_value('number_of_levels',$value,$escaped);
    }

    public function setCeilingHeight($value, $escaped = false)
    {
        $this->_set_field_value('ceiling_height',$value,$escaped);
    }

    public function setDeposit($value, $escaped = false)
    {
        $this->_set_field_value('deposit',$value,$escaped);
    }
        
    public function setDateUpdated($value, $escaped = false)
    {
        $this->_set_field_value('date_updated',$value,$escaped);
    }

    public function setMainImageId($value, $escaped = false)
    {
        $this->_set_field_value('main_image_id',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }
    
	public function getMemberId()
    {
        return $this->_get_field_value('member_id');
    }    

    public function getFeatures($formated=false)
    {
        if($formated) {
			if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
	        	return $this->getFeaturesEng();
			} else {
				return $this->getFeaturesPer();
			}
    	} else {
			return $this->_get_field_value('features');
    	}    	
    }
    
    public function getFeaturesArray() 
    {
    	$tmp = explode(',',trim($this->_get_field_value('features')));
    	$data = array();
    	foreach($tmp as $value) {
    		$value = trim($value);
    		if($value != '') {
    			$data[] = $value;
    		}
    	}
    	return $data;
    }
    
    public function getFeaturesSearchKeys($features,$sep=' ')
    {
    	$keys = '';
    	$ids = $this->getFeaturesArray();

    	foreach($ids as $id) {
			$keys .= $features[$id].' ';
		}
		return $keys;    	
    }

    public function getFeaturesEng()
    {
        return $this->_get_field_value('features_eng');
    }

    public function getFeaturesPer()
    {
        return $this->_get_field_value('features_per');
    }

    public function getIntroduction($formated=false,$noOfItems=null)
    {
    	if($formated) {
			if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
	        	$value = $this->getIntroductionEng();
			} else {
				$value = $this->getIntroductionPer();
			}
    	} else {
        	$value = $this->_get_field_value('introduction');
    	}

    	if($noOfItems !== null) {
    		$value = Al_Utilities::substr($value, ',', $noOfItems);
    	}
    	return $value;
    }
    
    public function getIntroductionArray() 
    {
    	$tmp = explode(',',trim($this->_get_field_value('introduction')));
    	$data = array();
    	foreach($tmp as $value) {
    		$value = trim($value);
    		if($value != '') {
    			$data[] = $value;
    		}
    	}
    	return $data;
    }
    
    public function getIntroductionSearchKeys($introduction,$sep=' ')
    {
    	$keys = '';
    	$ids = $this->getIntroductionArray();

    	foreach($ids as $id) {
			$keys .= $introduction[$id].' ';
		}
		return $keys;    	
    }    
    

    public function getIntroductionEng()
    {
        return $this->_get_field_value('introduction_eng');
    }

    public function getIntroductionPer()
    {
        return $this->_get_field_value('introduction_per');
    }

    public function getPropertyTransactionId()
    {
        return $this->_get_field_value('property_transaction_id');
    }

    public function getPropertyTypeId()
    {
        return $this->_get_field_value('property_type_id');
    }

    public function getPropertyPriceTypeId()
    {
        return $this->_get_field_value('property_price_type_id');
    }

    public function getPropertyStatusId()
    {
        return $this->_get_field_value('property_status_id');
    }

    public function getPrice($formated=false)
    {
    	if($formated) {
    		if($this->_get_field_value('price')>0) {
    			return number_format($this->_get_field_value('price'),2,'.','.').'M';
    		} else {
    			$language = Al_Language::get_instance();
				return $language->getValue(Al_Language::DATA_UNDER_REQUEST);
    		}
    	} else {
    		return $this->_get_field_value('price');
    	}
    }

    public function getCountryId()
    {
        return $this->_get_field_value('country_id');
    }

    public function getCityId()
    {
        return $this->_get_field_value('city_id');
    }

    public function getZoneId()
    {
        return $this->_get_field_value('zone_id');
    }

    public function getAddress()
    {
        return $this->_get_field_value('address');
    }

    public function getPostalCode()
    {
        return $this->_get_field_value('postal_code');
    }

	public function getCommentsEng()
    {
        return $this->_get_field_value('comments_eng');
    }    
    
	public function getCommentsPer()
    {
        return $this->_get_field_value('comments_per');
    }    
    
    public function getComments()
    {
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
			if($this->_getDataValue('comments_eng')=='') {
				return $this->_getDataValue('comments_per');					
			} else {
				return $this->_getDataValue('comments_eng');
			}			
		} else {
			if($this->_getDataValue('comments_per')=='') {
				return $this->_getDataValue('comments_eng');					
			} else {
				return $this->_getDataValue('comments_per');
			}
		}
    }

    public function getLandArea()
    {
        return $this->_get_field_value('land_area');
    }

    public function getBuildArea()
    {
        return $this->_get_field_value('build_area');
    }

    public function getBedrooms()
    {
        return $this->_get_field_value('bedrooms');
    }

    public function getBathrooms()
    {
        return $this->_get_field_value('bathrooms');
    }

    public function getParking()
    {
        return $this->_get_field_value('parking');
    }

    public function getContactName()
    {
        return $this->_get_field_value('contact_name');
    }

    public function getContactPhone()
    {
        return $this->_get_field_value('contact_phone');
    }

    public function getContactEmail()
    {
        return $this->_get_field_value('contact_email');
    }

    public function getLat()
    {
        return $this->_get_field_value('lat');
    }

    public function getLon()
    {
        return $this->_get_field_value('lon');
    }
    
	public function getDepth()
    {
        return $this->_get_field_value('depth');
    }    

    public function getExpiryDate($formated=false)
    {
    	$value = $this->_get_field_value('expiry_date');
    	if($formated) {
    		$value = Al_Utilities::formatDbDate($value);
    	} 
    	return $value;
    }

    public function getCreationDate($formated=false)
    {
    	$value = $this->_get_field_value('creation_date');
    	if($formated) {
    		$value = Al_Utilities::formatDbDate($value,'dd/MM/YYYY');// hh:mm:ss a');
    	} 
    	return $value;        
    }

	public function getPropertyType()
    {
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->_getDataValue('property_type_eng');
		} else {
			if($this->_getDataValue('property_type_per')=='') {
				return $this->_getDataValue('property_type_eng');					
			} else {
				return $this->_getDataValue('property_type_per');
			}
		}
    }

    
	public function getMemberName()
    {
		return $this->_getDataValue('member_name');
    }

	public function getMemberEmail()
    {
		return $this->_getDataValue('member_email');
    }        
    
	public function getTransactionType()
    {
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->_getDataValue('transaction_type_eng');
		} else {
			if($this->_getDataValue('transaction_type_per')=='') {
				return $this->_getDataValue('transaction_type_eng');					
			} else {
				return $this->_getDataValue('transaction_type_per');
			}
		}
    }    
    
	public function getPriceType()
    {
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->_getDataValue('price_type_eng');
		} else {
			if($this->_getDataValue('price_type_per')=='') {
				return $this->_getDataValue('price_type_eng');					
			} else {
				return $this->_getDataValue('price_type_per');
			}
		}
    }    
    
    public function getFeatured($label=false)
    {
    	$value = $this->_get_field_value('featured');
    	if($label) {
    		if($value == '1') {
    			$value = 'Yes';
    		} else {
    			$value = 'No';
    		}
    	} 
    	
    	return $value;
    }

    public function getPropertyId()
    {
        return $this->_get_field_value('property_id');
    }    
    
    public function getStatus()
    {
    	$statuses = new Bl_Data_Statuses();
    	return $statuses->get_label($this->_get_field_value('property_status_id'));
    }
    
    public function getCountry()
    {
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->_getDataValue('country_eng');
		} else {
			if($this->_getDataValue('country_per')=='') {
				return $this->_getDataValue('country_eng');					
			} else {
				return $this->_getDataValue('country_per');
			}
		}
    }    
    
    public function getCity() 
    {
    	if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->_getDataValue('city_eng');
		} else {
			if($this->_getDataValue('city_per')=='') {
				return $this->_getDataValue('city_eng');					
			} else {
				return $this->_getDataValue('city_per');
			}
		}
    }
    
    public function getZone() 
    {
    	$zone = $this->_get_field_value('zone');
    	if($zone != '') {
    		return $zone;
    	}
    	
    	if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->_getDataValue('zone_eng');
		} else {
			if($this->_getDataValue('zone_per')=='') {
				return $this->_getDataValue('zone_eng');					
			} else {
				return $this->_getDataValue('zone_per');
			}
		}
    }    
    
   	public function getDateUpdated()
    {
        return $this->_get_field_value('date_updated');
    }

    public function getMainImageId()
    {
        return $this->_get_field_value('main_image_id');
    }    
    
    public function getTemp()
    {
		return $this->_get_field_value('temp');
    }
    
    public function getTerraceM2()
    {
        return $this->_get_field_value('terrace_m2');
    }

    public function getLandWidth()
    {
        return $this->_get_field_value('land_width');
    }

    public function getLandHeight()
    {
        return $this->_get_field_value('land_height');
    }

    public function getAverageCeilingHeight()
    {
        return $this->_get_field_value('average_ceiling_height');
    }

    public function getLocatedOnLevel()
    {
        return $this->_get_field_value('located_on_level');
    }

    public function getNumberOfLevels()
    {
        return $this->_get_field_value('number_of_levels');
    }

    public function getCeilingHeight()
    {
        return $this->_get_field_value('ceiling_height');
    }

    public function getDeposit($formated=false)
    {
        if($formated) {
   			return number_format($this->_get_field_value('deposit'),2,'.','.').'M';
    	} else {
    		return $this->_get_field_value('deposit');
    	}    	
    }    
    
    public function setSwimmingPool($value, $escaped = false)
    {
        $this->_set_field_value('swimming_pool',$value,$escaped);
    }    
    
    public function getSwimmingPool($formated=false)
    {
    	$value = $this->_get_field_value('swimming_pool');
    	if($formated) {
    		$poolTypes = new Bl_Data_PoolTypes();
    		$value = $poolTypes->get_label($value); 
    	}
    	 
    	return $value; 
    }
    
    public function getViewCount()
    {
        return $this->_get_field_value('view_count');
    }    
    
    public function getContactCount()
    {
        return $this->_get_field_value('contact_count');
    }    
    
    public function delete() {
    	$this->deleteImages();
    	$this->_delete();
    }
    
    public function deleteImages() {
    	$images = new Bl_Record_PropertyImage();
    	$res = $images->retrieveRecordsByProperty($this->getId());
    	foreach($res as $item) {
    		$images->deleteImages($item['id']);
    	}
    }
    
    public function autoGeneratePropertyId() 
    {
    	$this->setPropertyId(strtoupper(uniqid("")));
    }
    
    
    public function load($id)
    {
 		$this->_qb = $this->getQb(false);
        $this->_qb->where("r.id = '".intval($id)."'"); 		
       	return $this->_load_record();
    }
    
    public function loadUserProperty($id,$memberId) 
    {
		$this->_qb = $this->getQb(false);
        $this->_qb->where("r.id = '".intval($id)."'");
		$this->_qb->where("r.member_id = '".intval($memberId)."'");         		
       	return $this->_load_record();    	
    }
    
    public function loadByPropertyId($propertyId) {
    	$propertyId = trim(substr($propertyId,0,32));
		$this->_qb = $this->getQb(false);
        $this->_qb->where("r.property_id = '".Al_Db::escape($propertyId)."'");
        $this->_qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_EXPIRED)."'");
        $this->_qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_PENDING_APPROVAL)."'");         		
       	return $this->_load_record(); 	
    }
    
    public function getQb($full=true) {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->select("
			pt.type_eng property_type_eng,
        	pt.type_per property_type_per,        
        	ptr.type_eng transaction_type_eng,
        	ptr.type_per transaction_type_per,
			ppt.type_eng price_type_eng,
        	ppt.type_per price_type_per,
        	ps.status_eng,
        	ps.status_per,
        	c.name_eng country_eng,
        	c.name_per country_per,
        	ct.city_eng,
        	ct.city_per,
        	z.zone_eng,
        	z.zone_per,
        	m.name member_name,
        	m.email member_email,
        	r.main_image_id image_id
        ");

        $this->_qb->join("LEFT JOIN property_search prsr ON r.id = prsr.id");
        $this->_qb->join("LEFT JOIN members m ON m.id = r.member_id"); 
		$this->_qb->join("LEFT JOIN property_types pt ON pt.id = r.property_type_id");        
        $this->_qb->join("LEFT JOIN property_transactions ptr ON ptr.id = r.property_transaction_id");
        $this->_qb->join("LEFT JOIN property_price_types ppt ON ppt.id = r.property_price_type_id");        
        $this->_qb->join("LEFT JOIN property_statuses ps ON ps.id = r.property_status_id");
        $this->_qb->join("LEFT JOIN countries c ON c.id = r.country_id");
        $this->_qb->join("LEFT JOIN cities ct ON ct.id = r.city_id");
        $this->_qb->join("LEFT JOIN zones z ON z.id = r.zone_id");           		
        
        if($full) {
			$this->_set_qb_select();
            $this->_set_qb_from(); 
    	} 
    	return $this->_qb;
    }
    
    public function retrieveMembersProperties($memberId,$grouped=false)
    {
		$this->_qb = $this->getQb(false);
        $this->_qb->where("r.member_id = '".intval($memberId)."'");
        $this->_qb->where('temp = 0');
        $this->_qb->order_by('r.property_transaction_id, r.creation_date');        
        $data = $this->_retrieveRecords(true);       	
    	
        if($grouped) {
        	$res = array();
        	foreach($data as $item) {
        		$tid = $item['property_transaction_id'];
        		if(array_key_exists($tid,$res)==false) {
        			$res[$tid] = array();
        		}
        		$res[$tid][] = $item;
        	}

			if(array_key_exists(Bl_Data_Transactions::FOR_SALE,$res)==false) {
        		$res[Bl_Data_Transactions::FOR_SALE] = array();
        	}

			if(array_key_exists(Bl_Data_Transactions::FOR_RENT,$res)==false) {
        		$res[Bl_Data_Transactions::FOR_RENT] = array();
        	}
        	
        	if(array_key_exists(Bl_Data_Transactions::FOR_BOOKING,$res)==false) {
        		$res[Bl_Data_Transactions::FOR_BOOKING] = array();
        	}
        	
        	return $res;
        } else {
        	return $data;
        }
    }
    
    public function retrieveAll() 
    {
   		$this->_qb = $this->getQb(false);
//        $this->_qb->where("r.member_id = '".intval($memberId)."'");
        $this->_qb->where('temp = 0');
//        $this->_qb->order_by('r.property_transaction_id, r.creation_date');        
        $data = $this->_retrieveRecords(true);       	
    	
       	return $data;
    }
    
    public function retrieveBuyFeatured($noOfRecords=3,$noOfChunks=1) {
    	return $this->retrieveFeatured(self::BUY,$noOfRecords,$noOfChunks);
    }

    public function retrieveRentFeatured($noOfRecords=3,$noOfChunks=1) {
    	return $this->retrieveFeatured(self::RENT,$noOfRecords,$noOfChunks);
    }    
    
    public function retrieveBookFeatured($noOfRecords=3,$noOfChunks=1) {
    	return $this->retrieveFeatured(self::BOOK,$noOfRecords,$noOfChunks);
    }
    
    
    
    public function retrieveFeatured($type,$noOfRecords=3,$noOfChunks=1) 
    {
		$this->_qb = $this->getQb(false);
        $this->_qb->where("property_transaction_id = '".intval($type)."'"); 		
        $this->_qb->where('temp = 0');
        $this->_qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_EXPIRED)."'");
        $this->_qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_PENDING_APPROVAL)."'");        
        
        $this->_qb->limit(intval($noOfRecords));
        $this->_qb->order_by('RAND()');
        
        $data = $this->_retrieveRecords(true);

    	if($noOfChunks>1) {
		    if($noOfChunks > 1) {
				$data = array_chunk($data,$noOfChunks);
				$last = end($data);
	
				foreach($data as &$items) {
					$i = 0;
					foreach($items as &$item) {
						$i++;
					}
					while($i < $noOfChunks) {
						$items[$i]['id'] = '';
						$i++;
					}
				}
			}			
		}
		
        return $data;
    }
    
    public function getImageThumb() 
    {
    	return $this->getImage('th-');
    }
    
    public function getImageSml() {
    	return $this->getImage('sml-');
    }
    
	public function getImageMed() {
    	return $this->getImage('med-');
    }

	public function getImageLrg() {
    	return $this->getImage('lrg-');
    }    
    
    private function getImage($prefix='') 
    {
    	$image_id = intval($this->_getDataValue('image_id'));
		return $this->_settings->path_web_ssl.'images/properties/'.Al_Utilities::getPathFromNo($image_id).$prefix.$image_id.'.jpg';    	
    }
    
    private function getTransactionPath() {
        switch($this->getPropertyTransactionId()) {
    		case self::RENT :
    			$transaction = self::RENT_PATH;
    			break;
    		case self::BOOK :
    			$transaction = self::BOOK_PATH;
    			break;
    		default:
    			$transaction = self::BUY_PATH;
    			break;
    	}    	
    	return $transaction;
    }
    
    public function getUrl($lang='') {
    	if($lang=='') {
    		$lang = $this->_settings->languagePrefix;
    	}
    	$pid = strtolower($this->getPropertyId());
    	$type = strtolower(str_replace(' ','-',$this->_getDataValue('property_type_eng')));
    	$city = strtolower(str_replace(' ','-',$this->_getDataValue('city_eng')));
    	$transaction = $this->getTransactionPath();
		return $this->_settings->path_web_ssl.$lang.'/properties/'.$transaction.'/'.$type.'/'.$city.'/'.$pid;    	
    }
    
    public function getPriceUsd($formated=false) {
    	$value = ($this->getPrice()/$this->_settings->general->getUsdRate())*10000000;
    	if($formated) {
    		return number_format($value,0,'.',',');
    	} else {
    		return number_format($value,2,'.','');
    	}
    }
    
    public function getPriceGbp($formated=false) {
    	$value = ($this->getPrice()/$this->_settings->general->getGbpRate())*10000000;
    	if($formated) {
    		return number_format($value,0,'.',',');
    	} else {
    		return number_format($value,2,'.','');
    	}
    }    
    
    public function getPriceAud($formated=false) {
    	$value = ($this->getPrice()/$this->_settings->general->getAudRate())*10000000;
    	if($formated) {
    		return number_format($value,0,'.',',');
    	} else {
    		return number_format($value,2,'.','');
    	}
    }    

    public function getDepositUsd($formated=false) {
    	$value = ($this->getDeposit()/$this->_settings->general->getUsdRate())*10000000;
    	if($formated) {
    		return number_format($value,0,'.',',');
    	} else {
    		return number_format($value,2,'.','');
    	}
    }
    
    public function getDepositGbp($formated=false) {
    	$value = ($this->getDeposit()/$this->_settings->general->getGbpRate())*10000000;
    	if($formated) {
    		return number_format($value,0,'.',',');
    	} else {
    		return number_format($value,2,'.','');
    	}
    }    
    
    public function getDepositAud($formated=false) {
    	$value = ($this->getDeposit()/$this->_settings->general->getAudRate())*10000000;
    	if($formated) {
    		return number_format($value,0,'.',',');
    	} else {
    		return number_format($value,2,'.','');
    	}
    }    
    
	public function retrieveRssData($type='',$noOfRecords=50) 
	{
		$this->_qb = $this->getQb(false);
		if($type != '') {
        	$this->_qb->where("property_transaction_id = '".intval($type)."'");
		} 		
        $this->_qb->where('temp = 0');
        $this->_qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_EXPIRED)."'");
        $this->_qb->where("r.property_status_id <> '".intval(Bl_Settings::PROP_PENDING_APPROVAL)."'");        
        
        $this->_qb->limit(intval($noOfRecords));
        $this->_qb->order_by('r.creation_date desc');
        
        return $this->_retrieveRecords(false);
	}    
    
    
}