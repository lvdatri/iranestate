<?php 
class Bl_Record_Country extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'countries';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('name_eng');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('name_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('100',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setNameEng($value, $escaped = false)
    {
        $this->_set_field_value('name_eng',$value,$escaped);
    }

    public function setNamePer($value, $escaped = false)
    {
        $this->_set_field_value('name_per',$value,$escaped);
    }

    public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getNameEng()
    {
        return $this->_get_field_value('name_eng');
    }

    public function getNamePer()
    {
        return $this->_get_field_value('name_per');
    }

    public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }

    public function retrieveCities() {
    	$city = new Bl_Record_City();
    	return $city->retrieveRecordsByCountry($this->getId());
    }

}