<?php
class Bl_Record_Customer extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'customers';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('type');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('first_name');
        $field->setMaxLength(65);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('last_name');
        $field->setMaxLength(65);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('email');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('address');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('discount');
        $field->setValue('20',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('active');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('login_date');
        $field->setValue('null',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setType($value, $escaped = false)
    {
        $this->_set_field_value('type',$value,$escaped);
    }

    public function setFirstName($value, $escaped = false)
    {
        $this->_set_field_value('first_name',$value,$escaped);
    }

    public function setLastName($value, $escaped = false)
    {
        $this->_set_field_value('last_name',$value,$escaped);
    }

    public function setEmail($value, $escaped = false)
    {
        $this->_set_field_value('email',$value,$escaped);
    }

    public function setAddress($value, $escaped = false)
    {
        $this->_set_field_value('address',$value,$escaped);
    }

    public function setDiscount($value, $escaped = false)
    {
        $this->_set_field_value('discount',$value,$escaped);
    }

    public function setActive($value, $escaped = false)
    {
        $this->_set_field_value('active',$value,$escaped);
    }

    public function setLoginDate($value, $escaped = false)
    {
        $this->_set_field_value('login_date',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getType()
    {
        return $this->_get_field_value('type');
    }

    public function getFirstName()
    {
        return $this->_get_field_value('first_name');
    }

    public function getLastName()
    {
        return $this->_get_field_value('last_name');
    }

    public function getEmail()
    {
        return $this->_get_field_value('email');
    }

    public function getAddress()
    {
        return $this->_get_field_value('address');
    }

    public function getDiscount()
    {
        return $this->_get_field_value('discount');
    }

    public function getActive()
    {
        return $this->_get_field_value('active');
    }

    public function getLoginDate()
    {
        return $this->_get_field_value('login_date');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }


}