<?php
class Bl_Record_Member extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'members';
        $this->_settings = Bl_Settings::get_instance();
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('name');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('email');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('password');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('date_registered');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('date_confirmed');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('confirmed');
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('confirmation_key_1');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('confirmation_key_2');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('date_last_login');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('login_count');
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('language');
        $field->setMaxLength(32);
        $field->setValue('',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('address');
        $field->setMaxLength(255);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('city');
        $field->setMaxLength(255);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('postcode');
        $field->setMaxLength(65);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('phone');
        $field->setMaxLength(65);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('newsletter_subscription');
        $field->setValue('0',true);
        $this->_add_field($field);

        $field = new Al_Record_FieldString();
        $field->setName('profile_image');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);

        $field = new Al_Record_FieldString();
        $field->setName('company_name');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);

        $field = new Al_Record_FieldText();
        $field->setName('profile_description_eng');
        $field->setValue('null',true);
        $this->_add_field($field);

        $field = new Al_Record_FieldText();
        $field->setName('profile_description_per');
        $field->setValue('null',true);
        $this->_add_field($field);

        $field = new Al_Record_FieldString();
        $field->setName('website');
        $field->setMaxLength(255);
        $this->_add_field($field);

        $field = new Al_Record_FieldInt();
        $field->setName('show_profile');
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);

        $field = new Al_Record_FieldString();
        $field->setName('profile_email');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);

        
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setName($value, $escaped = false)
    {
        $this->_set_field_value('name',$value,$escaped);
    }

    public function setEmail($value, $escaped = false)
    {
        $this->_set_field_value('email',$value,$escaped);
    }

    public function setPassword($value, $escaped = false)
    {
    	$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
    	$value = $crypt->encrypt($value,$settings->key,true);
        $this->_set_field_value('password',$value,$escaped);
    }

    public function setDateRegistered($value, $escaped = false)
    {
        $this->_set_field_value('date_registered',$value,$escaped);
    }

    public function setDateConfirmed($value, $escaped = false)
    {
        $this->_set_field_value('date_confirmed',$value,$escaped);
    }

    public function setConfirmed($value, $escaped = false)
    {
        $this->_set_field_value('confirmed',$value,$escaped);
    }

    public function setConfirmationKey1($value, $escaped = false)
    {
        $this->_set_field_value('confirmation_key_1',$value,$escaped);
    }

    public function setConfirmationKey2($value, $escaped = false)
    {
        $this->_set_field_value('confirmation_key_2',$value,$escaped);
    }

    public function setDateLastLogin($value, $escaped = false)
    {
        $this->_set_field_value('date_last_login',$value,$escaped);
    }

    public function setLoginCount($value, $escaped = false)
    {
        $this->_set_field_value('login_count',$value,$escaped);
    }

    public function setLanguage($value, $escaped = false)
    {
        $this->_set_field_value('language',$value,$escaped);
    }
    
    public function setAddress($value, $escaped = false)
    {
        $this->_set_field_value('address',$value,$escaped);
    }

    public function setCity($value, $escaped = false)
    {
        $this->_set_field_value('city',$value,$escaped);
    }

    public function setPostcode($value, $escaped = false)
    {
        $this->_set_field_value('postcode',$value,$escaped);
    }

    public function setPhone($value, $escaped = false)
    {
        $this->_set_field_value('phone',$value,$escaped);
    }
    
    public function setNewsletterSubscription($value, $escaped = false)
    {
        $this->_set_field_value('newsletter_subscription',$value,$escaped);
    }

    public function setProfileImage($value, $escaped = false)
    {
        $this->_set_field_value('profile_image',$value,$escaped);
    }

    public function setCompanyName($value, $escaped = false)
    {
        $this->_set_field_value('company_name',$value,$escaped);
    }

    public function setProfileDescriptionEng($value, $escaped = false)
    {
        $this->_set_field_value('profile_description_eng',$value,$escaped);
    }

    public function setProfileDescriptionPer($value, $escaped = false)
    {
        $this->_set_field_value('profile_description_per',$value,$escaped);
    }


    public function setWebsite($value, $escaped = false)
    {
        $this->_set_field_value('website',$value,$escaped);
    }

    public function setShowProfile($value, $escaped = false)
    {
        $this->_set_field_value('show_profile',$value,$escaped);
    }

    public function setProfileEmail($value, $escaped = false)
    {
        $this->_set_field_value('profile_email',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getName()
    {
        return $this->_get_field_value('name');
    }

    public function getEmail()
    {
        return $this->_get_field_value('email');
    }

    public function getPassword()
    {
		$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
    	return $crypt->decrypt($this->_get_field_value('password'),$settings->key,true);        
    }

    public function getDateRegistered()
    {
        return $this->_get_field_value('date_registered');
    }

    public function getDateConfirmed()
    {
        return $this->_get_field_value('date_confirmed');
    }

    public function getConfirmed()
    {
        return $this->_get_field_value('confirmed');
    }

    public function getConfirmationKey1()
    {
		$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
    	return $crypt->decrypt($this->_get_field_value('confirmation_key_1'),$settings->key,true);         	
    }

    public function getConfirmationKey2()
    {
		$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
    	return $crypt->decrypt($this->_get_field_value('confirmation_key_2'),$settings->key,true);         	
    }
    
    public function getConfirmationUrl() {
    	$settings = Bl_Settings::get_instance();
        return $settings->path_web_ssl . 'register/confirm/u/'.urlencode($this->getConfirmationKey1()).'/p/'.urlencode($this->getConfirmationKey2());
    }

    public function getDateLastLogin()
    {
        return $this->_get_field_value('date_last_login');
    }

    public function getLoginCount()
    {
        return $this->_get_field_value('login_count');
    }

    public function getLanguage()
    {
        return $this->_get_field_value('language');
    }
    
    public function getAddress()
    {
        return $this->_get_field_value('address');
    }

    public function getCity()
    {
        return $this->_get_field_value('city');
    }

    public function getPostcode()
    {
        return $this->_get_field_value('postcode');
    }

    public function getPhone()
    {
        return $this->_get_field_value('phone');
    }
    
    public function getNewsletterSubscription($formated=false)
    {
    	if($formated) {
    		if($this->_get_field_value('newsletter_subscription') == '1') {
    			return 'Yes';
    		} else {
    			return 'No';
    		}
    	} else {
        	return $this->_get_field_value('newsletter_subscription');
    	}
    }

    public function getProfileImage()
    {
        return $this->_get_field_value('profile_image');
    }

    public function getCompanyName()
    {
        return $this->_get_field_value('company_name');
    }

    public function getProfileDescriptionEng()
    {
        return $this->_get_field_value('profile_description_eng');
    }

    public function getProfileDescriptionPer()
    {
        return $this->_get_field_value('profile_description_per');
    }

    public function getProfileDescription()
    {
        if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
            if($this->_getDataValue('profile_description_eng')=='') {
                return $this->_getDataValue('profile_description_per');
            } else {
                return $this->_getDataValue('profile_description_eng');
            }
        } else {
            if($this->_getDataValue('profile_description_per')=='') {
                return $this->_getDataValue('profile_description_eng');
            } else {
                return $this->_getDataValue('profile_description_per');
            }
        }
    }

    public function getWebsite()
    {
        return $this->_get_field_value('website');
    }

    public function getShowProfile($format=false)
    {
        $value = $this->_get_field_value('show_profile');
        if($format) {
            if($value == '1') {
                $value = 'Yes';
            } else {
                $value = 'No';
            }
        }
        return $value;
    }

    public function getProfileEmail()
    {
        return $this->_get_field_value('profile_email');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
	public function loadByEmail($email) 
	{
		$this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.email = '".Al_Db::escape($email)."'");
        return $this->_load_record();		
	}    
    
    public function loadFromKeys($key1,$key2) {
    	$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
    	
    	$key1 = $crypt->encrypt($key1,$settings->key,true);
    	$key2 = $crypt->encrypt($key2,$settings->key,true);
    	
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.confirmation_key_1 = '".Al_Db::escape($key1)."'");
        $this->_qb->where("r.confirmation_key_2 = '".Al_Db::escape($key2)."'");
        return $this->_load_record();    	
    } 
    
    
    public function setConfirmationKeys() 
    {
    	$this->setConfirmationKey1($this->getUniqueKey(16,'confirmation_key_1'));
    	$this->setConfirmationKey2($this->getUniqueKey(32,'confirmation_key_2'));
    }
    
    private function getUniqueKey($key_length,$field) 
    {
		$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
        do {
        	$key = Al_Utilities::getRandomString($key_length);
            $key = $crypt->encrypt($key,$settings->key,true);

            $unique = $this->isUniqueKey($key,$field);
        } while($unique == false);
        return $key;
    }
    
    private function isUniqueKey($key,$field) {
        $res = $this->_db->query("
            SELECT id
            FROM members
            WHERE $field = '" . Al_Db::escape($key) ."'
            LIMIT 1
        ");
        if($res->num_rows()>0) {
            return false;
        }
        return true;
    }

    public function getMemberUrl($lang='')
    {
        return $this->getMemberUrlFromId($lang,$this->getId(),$this->getName());
    }


    public function getMemberUrlFromId($lang=null, $memberId,$memberName)
    {
        if($lang=='') {
            $lang = $this->_settings->languagePrefix;
        }
        return $this->_settings->path_web_ssl.$lang.'/agent/view/id/'.$memberId.'/name/'. urlencode(str_replace('/',' ',$memberName));
    }

}
