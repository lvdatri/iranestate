<?php
class Bl_Record_PropertyStatus extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'property_statuses';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('status_eng');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('status_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('null',true);
        $field->setValue('100',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setStatusEng($value, $escaped = false)
    {
        $this->_set_field_value('status_eng',$value,$escaped);
    }

    public function setStatusPer($value, $escaped = false)
    {
        $this->_set_field_value('status_per',$value,$escaped);
    }

    public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getStatusEng()
    {
        return $this->_get_field_value('status_eng');
    }

    public function getStatusPer()
    {
        return $this->_get_field_value('status_per');
    }

    public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }


}
