<?php
class Bl_Record_AdminUser extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'admin_users';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('first_name');
        $field->setMaxLength(65);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('last_name');
        $field->setMaxLength(65);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('email');
        $field->setMaxLength(165);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('username');
        $field->setMaxLength(65);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('password');
        $field->setMaxLength(255);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('permissions');
        $field->setValue('1');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('last_login');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('login_count');
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setFirstName($value, $escaped = false)
    {
        $this->_set_field_value('first_name',$value,$escaped);
    }

    public function setLastName($value, $escaped = false)
    {
        $this->_set_field_value('last_name',$value,$escaped);
    }

    public function setEmail($value, $escaped = false)
    {
        $this->_set_field_value('email',$value,$escaped);
    }

    public function setUsername($value, $escaped = false)
    {
    	
        $this->_set_field_value('username',$value,$escaped);
    }

    public function setPassword($value, $escaped = false)
    {
    	$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
    	$value = $crypt->encrypt($value,$settings->key,true);
        $this->_set_field_value('password',$value,$escaped);
    }

    public function setPermissions($value, $escaped = false)
    {
        $this->_set_field_value('permissions',$value,$escaped);
    }

    public function setLastLogin($value, $escaped = false)
    {
        $this->_set_field_value('last_login',$value,$escaped);
    }

    public function setLoginCount($value, $escaped = false)
    {
        $this->_set_field_value('login_count',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getFirstName()
    {
        return $this->_get_field_value('first_name');
    }

    public function getLastName()
    {
        return $this->_get_field_value('last_name');
    }

    public function getEmail()
    {
        return $this->_get_field_value('email');
    }

    public function getUsername()
    {
        return $this->_get_field_value('username');
    }

    public function getPassword()
    {
    	$crypt = new Al_Crypt();
    	$settings = Bl_Settings::get_instance();
    	return $crypt->decrypt($this->_get_field_value('password'),$settings->key,true);
    }

    public function getPermissions()
    {
        return $this->_get_field_value('permissions');
    }

    public function getLastLogin()
    {
        return $this->_get_field_value('last_login');
    }

    public function getLoginCount()
    {
        return $this->_get_field_value('login_count');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }


}
