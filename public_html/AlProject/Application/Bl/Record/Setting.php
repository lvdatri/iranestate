<?php
class Bl_Record_Setting extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'settings';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('email_from');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('email_from_name');
        $field->setMaxLength(65);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('usd_rate');
        $field->setDecimalPoints(2);
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('gbp_rate');
        $field->setDecimalPoints(2);
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldFloat();
        $field->setName('aud_rate');
        $field->setDecimalPoints(2);
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('require_property_approval');
        $field->setValue('0',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('map_lat');
        $field->setMaxLength(32);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('map_long');
        $field->setMaxLength(32);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('map_depth');
        $field->setMaxLength(32);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('search_index_update_date');
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setEmailFrom($value, $escaped = false)
    {
        $this->_set_field_value('email_from',$value,$escaped);
    }

    public function setEmailFromName($value, $escaped = false)
    {
        $this->_set_field_value('email_from_name',$value,$escaped);
    }

    public function setUsdRate($value, $escaped = false)
    {
        $this->_set_field_value('usd_rate',$value,$escaped);
    }

    public function setGbpRate($value, $escaped = false)
    {
        $this->_set_field_value('gbp_rate',$value,$escaped);
    }

    public function setAudRate($value, $escaped = false)
    {
        $this->_set_field_value('aud_rate',$value,$escaped);
    }
    
    public function setRequirePropertyApproval($value, $escaped = false)
    {
        $this->_set_field_value('require_property_approval',$value,$escaped);
    }

    public function setMapLat($value, $escaped = false)
    {
        $this->_set_field_value('map_lat',$value,$escaped);
    }

    public function setMapLong($value, $escaped = false)
    {
        $this->_set_field_value('map_long',$value,$escaped);
    }

    public function setMapDepth($value, $escaped = false)
    {
        $this->_set_field_value('map_depth',$value,$escaped);
    }
    
	public function setSearchIndexUpdateDate($value, $escaped = false)
    {
        $this->_set_field_value('search_index_update_date',$value,$escaped);
    }    

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getEmailFrom()
    {
        return $this->_get_field_value('email_from');
    }

    public function getEmailFromName()
    {
        return $this->_get_field_value('email_from_name');
    }

    public function getUsdRate($formated=false)
    {
    	if($formated) {
    		return number_format($this->_get_field_value('usd_rate'),2);	
    	} else {
        	return $this->_get_field_value('usd_rate');
    	}
    }

    public function getGbpRate($formated=false)
    {
    	if($formated) {
    		return number_format($this->_get_field_value('gbp_rate'),2);	
    	} else {
        	return $this->_get_field_value('gbp_rate');
    	}
    }

    public function getAudRate($formated=false)
    {
    	if($formated) {
    		return number_format($this->_get_field_value('aud_rate'),2);	
    	} else {
        	return $this->_get_field_value('aud_rate');
    	}
    }

    public function getRequirePropertyApproval()
    {
        return $this->_get_field_value('require_property_approval');
    }

    public function getMapLat()
    {
        return $this->_get_field_value('map_lat');
    }

    public function getMapLong()
    {
        return $this->_get_field_value('map_long');
    }

    public function getMapDepth()
    {
        return $this->_get_field_value('map_depth');
    }
    
	public function getSearchIndexUpdateDate()
    {
        return $this->_get_field_value('search_index_update_date');
    }
    
	public function getNewSearchIndexUpdateDate()
    {
        return $this->_data['new_search_index_update_date'];
    }    
    
    public function load($id=1)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->select('now() new_search_index_update_date');
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }


}
