<?php
class Bl_Record_Cms extends Al_Record
{
	private $_settings;
    public function initialiseRecord()
    {
        $this->_db_table = 'cms';
		$this->_settings = Bl_Settings::get_instance();        
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('cms_type');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('path');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('keywords_eng');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('keywords_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('description_eng');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('description_per');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('title_eng');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('title_per');
        $field->setMaxLength(255);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('content_eng');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldText();
        $field->setName('content_per');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('image');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldDateTime();
        $field->setName('date_created');
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('view_count');
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);
        
       	$field = new Al_Record_FieldInt();
        $field->setName('sort_order');
        $field->setValue('100',true);
        $this->_add_field($field);        
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setCmsType($value, $escaped = false)
    {
        $this->_set_field_value('cms_type',$value,$escaped);
    }

    public function setPath($value, $escaped = false)
    {
        $this->_set_field_value('path',$value,$escaped);
    }

    public function setKeywordsEng($value, $escaped = false)
    {
        $this->_set_field_value('keywords_eng',$value,$escaped);
    }

    public function setKeywordsPer($value, $escaped = false)
    {
        $this->_set_field_value('keywords_per',$value,$escaped);
    }

    public function setDescriptionEng($value, $escaped = false)
    {
        $this->_set_field_value('description_eng',$value,$escaped);
    }

    public function setDescriptionPer($value, $escaped = false)
    {
        $this->_set_field_value('description_per',$value,$escaped);
    }

    public function setTitleEng($value, $escaped = false)
    {
        $this->_set_field_value('title_eng',$value,$escaped);
    }

    public function setTitlePer($value, $escaped = false)
    {
        $this->_set_field_value('title_per',$value,$escaped);
    }

    public function setContentEng($value, $escaped = false)
    {
        $this->_set_field_value('content_eng',$value,$escaped);
    }

    public function setContentPer($value, $escaped = false)
    {
        $this->_set_field_value('content_per',$value,$escaped);
    }

    public function setImage($value, $escaped = false)
    {
        $this->_set_field_value('image',$value,$escaped);
    }

    public function setDateCreated($value, $escaped = false)
    {
        $this->_set_field_value('date_created',$value,$escaped);
    }

    public function setViewCount($value, $escaped = false)
    {
        $this->_set_field_value('view_count',$value,$escaped);
    }
    
	public function setSortOrder($value, $escaped = false)
    {
        $this->_set_field_value('sort_order',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getCmsType()
    {
        return $this->_get_field_value('cms_type');
    }

    public function getPath($formated=false)
    {
    	if($formated) {
    		$value = $this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/'.$this->_get_field_value('path');
    	} else {
    		$value = $this->_get_field_value('path');
    	}
    	
        return $value;
    }

    public function getKeywordsEng()
    {
        return $this->_get_field_value('keywords_eng');
    }

    public function getKeywordsPer()
    {
        return $this->_get_field_value('keywords_per');
    }

    public function getDescriptionEng()
    {
        return $this->_get_field_value('description_eng');
    }

    public function getDescriptionPer()
    {
        return $this->_get_field_value('description_per');
    }

    public function getTitleEng()
    {
        return $this->_get_field_value('title_eng');
    }

    public function getTitlePer()
    {
        return $this->_get_field_value('title_per');
    }

    public function getContentEng()
    {
        return $this->_get_field_value('content_eng');
    }

    public function getContentPer()
    {
        return $this->_get_field_value('content_per');
    }

    public function getImage()
    {
        return $this->_get_field_value('image');
    }

    public function getDateCreated($formated=false,$format='')
    {
    	if($formated) {
    		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
				$value =  Al_Utilities::formatDbDate($this->_get_field_value('date_created'),$format);
    		} else {
    			$value = Al_Utilities::persianDateFormat($this->_get_field_value('date_created'));
    		}    
    	} else {
        	$value = $this->_get_field_value('date_created');
    	}
    	return $value;
    }
    
    public function getTitle() 
    {
    	if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->getTitleEng();
		} else {
			if($this->getTitlePer()=='') {
				return $this->getTitleEng();					
			} else {
				return $this->getTitlePer();
			}
		}
    }
    
    public function getContent($short=false,$length=100) 
    {
    	$value = '';
        if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	$value = $this->getContentEng();
		} else {
			if($this->getTitlePer()=='') {
				$value = $this->getContentEng();					
			} else {
				$value = $this->getContentPer();
			}
		}    	
		
		if($short) {
			$value = mb_substr(strip_tags($value),0,$length);
		} 
		
		return $value;
    }
    
	public function getDescription() 
	{
	    if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->getDescriptionEng();
		} else {
			if($this->getDescriptionPer()=='') {
				return $this->getDescriptionEng();					
			} else {
				return $this->getDescriptionPer();
			}
		}		
	}
	
	public function getKeywords() 
	{
		if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
        	return $this->getKeywordsEng();
		} else {
			if($this->getKeywordsPer()=='') {
				return $this->getKeywordsEng();					
			} else {
				return $this->getKeywordsPer();
			}
		}
	}	

    public function getViewCount()
    {
        return $this->_get_field_value('view_count');
    }
    
	public function getSortOrder()
    {
        return $this->_get_field_value('sort_order');
    }    

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
    public function getQb($full=true) {
        $this->_qb = new Al_QueryBuilder();
		$this->_set_qb_select();
		$this->_set_qb_from(); 
    	return $this->_qb;
    }
    
    public function loadByPath($path) 
    {
		$this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.path = '".Al_Db::escape($path)."'");
        return $this->_load_record();    	
    }

    public function before_insert() 
    {
		$this->setDateCreated(Al_Utilities::datetime_now_mysql());
		$this->setPath($this->generateCmsPath());
    }
    
    public function before_update() {
		$this->setPath($this->generateCmsPath());
    }
    
    public function getCmsTypePath($type='') 
    {
    	if($type == '') {
    		$type = $this->getCmsType();
    	}
		$cmsTypes = new Bl_Data_CmsTypes();

    	return 	$cmsTypes->getPathFromType($type).
				$this->getDateCreated(true,'YYYY/MM/dd/');
    }
    
    private function generateCmsPath()
    {
    	if($this->getCmsType()==Bl_Data_CmsTypes::page) {
    		return 	strtolower(trim($this->getPath(),' /'));
    	} else {
	    	return  $this->getCmsTypePath().
	    			$this->convertToUrlValue($this->getTitleEng());
    	}		
    }
    
	private function convertToUrlValue($value) 
	{
		$name = trim(strtolower($value));
	    $name = preg_replace('/[^a-z0-9 ]/', '', $name);
		$name = preg_replace('/ {2,}/', ' ', $name);		    	
	    $name = str_replace(' ', '-', $name);
	    return $name;
	}    
	
	public function delete() {
		$this->deleteImages($this->getId());
		$this->_delete();
	}    

	public function deleteImages($id) {
		$settings = Bl_Settings::get_instance();
		$path = Al_Utilities::getPathFromNo($id,$settings->path_public.'images/cms/');
		
		Al_File::delete($path.'lrg-'.$id.'.jpg');		
		Al_File::delete($path.'sml-'.$id.'.jpg');			
		Al_File::delete($path.'th-'.$id.'.jpg');
	}	
	
    public function getImageTh() 
    {
    	return $this->getImagePath('th-');
    }
    
    public function getImageSml() {
    	return $this->getImagePath('sml-');
    }

	public function getImageLrg() {
    	return $this->getImagePath('lrg-');
    }    
    
    private function getImagePath($prefix='') 
    {
    	$image_id = intval($this->getId());
		return $this->_settings->path_web_ssl.'images/cms/'.Al_Utilities::getPathFromNo($image_id).$prefix.$image_id.'.jpg';    		
    }
    	
    public function retrieveByType($type,$limit=5,$sortByDate=true) 
    {
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->where("r.cms_type = '".intval($type) . "'");
		if($sortByDate) {
			$this->_qb->order_by('r.date_created desc');
		} else {
			$this->_qb->order_by('r.sort_order');
		}
		$this->_qb->limit($limit);
		
		$res = $this->_retrieveRecords();
		$data = array();
		
		while($row = $res->fetch_row()) {
			$c = new Bl_Record_Cms();
			$c->setFromArray($row);
			$data[] = $c;	
		}
		return $data;
    }

    
}