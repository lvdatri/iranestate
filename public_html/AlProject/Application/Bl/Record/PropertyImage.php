<?php
class Bl_Record_PropertyImage extends Al_Record
{

    public function initialiseRecord()
    {
        $this->_db_table = 'property_images';
        
        $field = new Al_Record_FieldId();
        $field->setName('id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('property_id');
        $this->_add_field($field);
        
        $field = new Al_Record_FieldString();
        $field->setName('image_name');
        $field->setMaxLength(165);
        $field->setValue('null',true);
        $this->_add_field($field);
        
        $field = new Al_Record_FieldInt();
        $field->setName('main_image');
        $field->setValue('null',true);
        $field->setValue('0',true);
        $this->_add_field($field);
    }

    public function setId($value, $escaped = false)
    {
        $this->_set_field_value('id',$value,$escaped);
    }

    public function setPropertyId($value, $escaped = false)
    {
        $this->_set_field_value('property_id',$value,$escaped);
    }

    public function setImageName($value, $escaped = false)
    {
        $this->_set_field_value('image_name',$value,$escaped);
    }

    public function setMainImage($value, $escaped = false)
    {
        $this->_set_field_value('main_image',$value,$escaped);
    }

    public function getId()
    {
        return $this->_get_field_value('id');
    }

    public function getPropertyId()
    {
        return $this->_get_field_value('property_id');
    }

    public function getImageName()
    {
        return $this->_get_field_value('image_name');
    }

    public function getMainImage()
    {
        return $this->_get_field_value('main_image');
    }

    public function load($id)
    {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        return $this->_load_record();
    }
    
    public function loadByProperty($id,$propertyId) 
    {
		$this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.id = '".intval($id)."'");
        $this->_qb->where("r.property_id = '".intval($propertyId)."'");
        return $this->_load_record();    	
    }
    
    public function loadSinglePropertyImage($propertyId) {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.property_id = '".intval($propertyId)."'");
        $this->_qb->order_by("r.id ASC");
        $this->_qb->limit('1');
        
        return $this->_load_record();    	
    }
    
    
    
	public function delete() {
		
		
		$this->deleteImages($this->getId());
		$this->_delete();
	}    
	
	public function loadMainImage($propertyId) {
        $this->_qb = new Al_QueryBuilder();
        $this->_qb->where("r.property_id = '".intval($propertyId)."'");
        $this->_qb->where("r.main_image = '1'");
        return $this->_load_record();
	}
	
	public function setAsMainImage() {
		$mainImage = new Bl_Record_PropertyImage();
		if($mainImage->loadMainImage($this->getPropertyId())) {
			$mainImage->setMainImage('0');
			$mainImage->save();
		}
		
		$this->setMainImage('1');
		$this->save();
		
		$this->setPropertyMainImage($this->getId(), $this->getPropertyId());
				
	}
	
	private function setPropertyMainImage($imageId,$propertyId) {
		$property = new Bl_Record_Property();
		if($property->load($propertyId)) {
			$property->setMainImageId($imageId);
			$property->save();
		}
	}
	
	public static function setDefaultMainImage($propertyId) {
		$mainImage = new Bl_Record_PropertyImage();
		if(!$mainImage->loadMainImage($propertyId)) {
			$image = new Bl_Record_PropertyImage();
			if($image->loadSinglePropertyImage($propertyId)) {
				$image->setMainImage('1');
				$image->save();
				
				self::setPropertyMainImage($image->getId(), $propertyId);
			} else {
				self::setPropertyMainImage('0', $propertyId);	
			}
		}
	}
	
    public function retrieveRecordsByProperty($propertyId,$chunks='') {
		$this->_qb = new Al_QueryBuilder();
		$this->_qb->where("r.property_id ='".intval($propertyId)."'");
		$data = $this->_retrieveRecords(true);

		$settings = Bl_Settings::get_instance();

		foreach($data as &$image) {
			$path = Al_Utilities::getPathFromNo($image['id'],$settings->path_web_ssl.'images/properties/');
			$image['path_th'] = $path.'th-'.$image['id'].'.jpg';
			$image['path_sml'] = $path.'sml-'.$image['id'].'.jpg';
			$image['path_med'] = $path.'med-'.$image['id'].'.jpg';
			$image['path_lrg'] = $path.'lrg-'.$image['id'].'.jpg';
		}
		
		if($chunks != '') {
			$data = array_chunk($data,$chunks);
			$last = end($data);
			
			foreach($data as &$items) {
				$i = 0;
				foreach($items as &$item) {
					$i++;
				}
				while($i < $chunks) {
					$items[$i]['id'] = '';
					$i++;
				}
			}
			return $data;			
		}
		
		return $data;
    }
    
	public function retrieveEmptyImage() 
	{
		$settings = Bl_Settings::get_instance();
		$image = array();
		$image['id'] = '0';
		$path = Al_Utilities::getPathFromNo($image['id'],$settings->path_web_ssl.'images/properties/');
		$image['path_th'] = $path.'th-'.$image['id'].'.jpg';
		$image['path_sml'] = $path.'sml-'.$image['id'].'.jpg';
		$image['path_med'] = $path.'med-'.$image['id'].'.jpg';
		$image['path_lrg'] = $path.'lrg-'.$image['id'].'.jpg';	
		$data = array();
		$data[] = $image;	
		return $data;
	}    
	
	public function deleteImages($id) {
		$settings = Bl_Settings::get_instance();
		$path = Al_Utilities::getPathFromNo($id,$settings->path_public.'images/properties/');
		
		Al_File::delete($path.'lrg-'.$id.'.jpg');		
		Al_File::delete($path.'med-'.$id.'.jpg');
		Al_File::delete($path.'sml-'.$id.'.jpg');			
		Al_File::delete($path.'th-'.$id.'.jpg');
	}

}
