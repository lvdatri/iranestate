<?php
	class Bl_UserLogin {
        private $_db;
		private $default_path;        
        
        public function __construct() {
            $this->_db = Al_Db::get_instance();
        }
        
        public function setDefaultPath($default_path) {
            $this->default_path = $default_path;
        }
        
        public function check($email,$password) {

        	$settings = Bl_Settings::get_instance();
	        $crypt = new Al_Crypt();
        	$password = $crypt->encrypt($password,$settings->key,true);

            $res = $this->_db->query("
            	SELECT 
            		id, name, email, date_last_login, language
            	FROM members
            	WHERE email = '" . Al_Db::escape($email) . "'
            	AND password = '" . Al_Db::escape($password) . "'
            	AND confirmed = '1'
            ");
            
            if($res->num_rows()>0) {
                $data = $res->fetch_row();
                $data['permissions'] = '1';
                $user = Al_User::get_instance();
                $user->set_data($data);
                $this->set_login_stats($data);
                return true;
            }
            return false;
        }
        
        public function set_login_stats($data) {
        	$member = new Bl_Record_Member();
        	$member->load($data['id']);
        	$member->setLoginCount('login_count+1',true);
        	$member->setDateLastLogin('now()',true);
        	$member->save();
        }
        
    	/**
		 * redirects user to requested or default URL.
		 *
		 */
		function redirect() {
			$path = Al_Utilities::get_return_path();
			if($path=='') 
				Al_Utilities::redirect($this->default_path);
			else 
				Al_Utilities::redirect($path);
		}        
    }