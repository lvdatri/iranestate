<?php
	class Bl_Tabs {

		const ACT_ACCOUNT = '1';
		const ACT_NOTIFICATIONS = '2';
		const ACT_SUMMARY = '3';
		
		const PROP_INTRODUCTION = '4';
		const PROP_IMAGES = '5';
		const PROP_MAP = '6';
		
		private $tabs = array();
		private $activeTab;
		
		private $_settings;
		private $language;
		
		private $partialDisplay=false;
		
		public function __construct() 
		{
			$this->_settings = Bl_Settings::get_instance();	
			$this->language = Al_Language::get_instance();
		}
		
		public function setActive($activeTab) 
		{
			$this->activeTab = $activeTab;
		}
		
		public function setPartialDisplay($value) 
		{
			$this->partialDisplay = $value;	
		}
		
		public function displayAccount() 
		{
			$this->add(self::ACT_ACCOUNT,$this->language->getValue(Al_Language::TAB_ACT_MY_ACCOUNT),$this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/account/');	
//			$this->add(self::ACT_NOTIFICATIONS,$this->language->getValue(Al_Language::TAB_ACT_NOTIFICATIONS),$this->_settings->path_web_ssl.'account/notifications');
			$this->add(self::ACT_SUMMARY,$this->language->getValue(Al_Language::TAB_ACT_SUMMARY),$this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/account/summary');
			$this->display();
		}

		public function displayProperty() {
			$pathIntro = $this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/property/intro';
			$pathImages = $this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/property/images';
			$pathMap = $this->_settings->path_web_ssl.$this->_settings->languagePrefix.'/property/map';
			
			if($this->partialDisplay==true) {
				if($this->activeTab >= self::PROP_INTRODUCTION) {
					$this->add(self::PROP_INTRODUCTION,$this->language->getValue(Al_Language::TAB_PROP_INTRODUCTION),$pathIntro);
				}
				if($this->activeTab >= self::PROP_IMAGES) {
					$this->add(self::PROP_IMAGES,$this->language->getValue(Al_Language::TAB_PROP_IMAGES),$pathImages);
				}
				if($this->activeTab >= self::PROP_MAP) {
					$this->add(self::PROP_MAP,$this->language->getValue(Al_Language::TAB_PROP_MAP),$pathMap);
				}
			} else {
				$this->add(self::PROP_INTRODUCTION,$this->language->getValue(Al_Language::TAB_PROP_INTRODUCTION),$pathIntro);
				$this->add(self::PROP_IMAGES,$this->language->getValue(Al_Language::TAB_PROP_IMAGES),$pathImages);
				$this->add(self::PROP_MAP,$this->language->getValue(Al_Language::TAB_PROP_MAP),$pathMap);
			}

			$this->display();
		}		
		
		private function add($id,$label,$link) 
		{
			$this->tabs[] = array(
				'id' => $id,
				'label' => $label,
				'link' => $link,
			);
		}
		
		private function display() 
		{
			$content = '<div style="width:932px; margin:0px;">';
			$content .= '<ul class="tabs">';
			foreach($this->tabs as $tab) {
				if($this->activeTab == $tab['id']) {
        			$content .= '<li class="active"><a href="'.$tab['link'].'">'.$tab['label'].'</a></li>';
				} else {
					$content .= '<li class=""><a href="'.$tab['link'].'">'.$tab['label'].'</a></li>';
				}
			} 
	    	$content .= '</ul>'; 
			$content .= '<div class="tab_container">'; 
    	    $content .= '<div class="tab_content">'; 
	    	print($content);
		}
		
		public function displayEnd() 
		{
			echo '</div></div></div>';	
		}
	}