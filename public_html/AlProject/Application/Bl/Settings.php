<?php
	class Bl_Settings {
		
		const PROP_PENDING_APPROVAL = '1';
		const PROP_EXPIRED = '3';
		
		public $environment;
		
		public $path_public;
		public $path_app;
		public $path_project;
	    public $path_web;
	    public $path_ssl;
	    public $path_web_ssl;
	    public $db_connections=null;
	    
		public $path_admin_web;
	    public $path_admin_ssl;
	    public $path_admin_web_ssl;	    
	    
	    public $path_css;
	    public $path_images;
	    public $path_js;
	    
		public $key;
		public $dir_templates;
		public $dir_controllers;		
		public $dir_js;
		
		public $path_templates;
		public $path_controllers;
		
		public $tag_title;
		public $tag_keywords;
		public $tag_description;
		
		public $from_email;
		public $from_name;
		
		public $session_start;
		public $session;
		public $session_user;
		public $session_history;
    	public $session_pages;
    	public $session_forms;
    	public $session_language;
    	
		public $record_insert;
		public $record_update;
		public $record_delete;
		
		public $records_per_page;
		public $user_login_path;
		public $user_denied_path;	
		
		public $date_format='dd/MM/YYYY';
		public $date_format_db='YYYY-MM-dd HH:mm:ss';
		
		public $jsCssMerged=false;

		public $general;
		
		public $language;
		public $languagePrefix;

		public $frontend;
		
		public $gmap_key;
		
		public $gmail_user='';
		public $gmail_pass='';
		
		public $controllerBaseDir='';
		
		private $path_settings;

	    const ENV_PRODUCTION='1';
	    const ENV_DEVELOPMENT='2';
	    
	    const LANG_ENGLISH='eng';
	    const LANG_PERSIAN='per';
	    
	    const LANG_ENGLISH_PREFIX='en';
	    const LANG_PERSIAN_PREFIX='fa';	    
	    
	    
	    const ENGLISH_DIR = 'eng/';
	    const PERSIAN_DIR = 'per/';
		
		private static $instance;
		private function __construct() {
			$this->path_settings = dirname(__FILE__).'/';
		}
		
	    public static function get_instance() {
	        if (!isset(self::$instance)) {
	            $c = __CLASS__;
	            self::$instance = new $c;
	        }
	        return self::$instance;
	    }
		
	    private function add_db_connection($key,$connection) {
	    	if($this->db_connections == null) {
	    		$this->db_connections = array();
	    	}
	    	$this->db_connections[$key] = $connection;
	    }
	    
	    public function get_db_connection($key) {
	    	if(isset($this->db_connections[$key])) {
	    		return $this->db_connections[$key];
	    	} else {
	    		return false;
	    	}
	    }
	    
	    public function initialise($environment) {
	    	if($environment == self::ENV_DEVELOPMENT) {
	    		$this->initialise_dev();
	    	} else {
	    		$this->initialise_prod();
	    	}
			$this->initialise_common();
	    }

	    /**
	     * set development environment
	     */
	    public function initialise_dev() {
			require($this->path_settings.'../../../../dev-config.php');
	    }

	    /**
	     * set production environment
	     */
	    
	    
		public function initialise_prod() {
//	    	$this->path_web = 'http://173.255.209.121/';
//	    	$this->path_ssl = 'http://173.255.209.121/';
			$this->path_web = 'http://iranestate.com/';
	    	$this->path_ssl = 'https://iranestate.com/';

			// http://iranesta.arvixevps.com/~iranesta
//			$this->path_web = 'http://iranesta.arvixevps.com/~iranesta/';
//	    	$this->path_ssl = 'http://iranesta.arvixevps.com/~iranesta/';			
			
			
	    	
	    	$this->controllerBaseDir = '~iranesta';
	    	
//			$this->path_web = 'http://iranestate.com/staging/';
//	    	$this->path_ssl = 'http://iranestate.com/staging/';
//	    	$this->controllerBaseDir = 'staging';	    	
	    	
	    	$this->environment = self::ENV_PRODUCTION;
	    	$this->add_db_connection('0',array(
				'host' => 'localhost',
    			'database' => 'iranestate',
    			'user' => 'mysql_iranestate',
    			'password' => 'pink153'
	    	));			
	    	
//	    	$this->gmail_user='no-reply@iranestate.com';
//			$this->gmail_pass='1ranreale5tate';

			$this->gmail_user='noreply@iranestate.com';
			$this->gmail_pass='1ranr3al35tat3';	    	
			
	    	$this->gmap_key = 'ABQIAAAAiDGvCAXixPd-ppPsmH5ojhQscYpvJPTuzgYkgpOv5CYEhLJJXxSGoCe_uK2rFe9YbQ3o1Yc-Q-XoBA';
	    	
	    	$this->jsCssMerged = true;
	    }	    
	    
		public function initialise_prod_active() {
	    	$this->path_web = 'http://iranestate.com/';
	    	$this->path_ssl = 'https://iranestate.com/';

			// http://iranesta.arvixevps.com/~iranesta
//			$this->path_web = 'http://iranesta.arvixevps.com/~iranesta/';
//	    	$this->path_ssl = 'http://iranesta.arvixevps.com/~iranesta/';			
			
			
	    	$this->controllerBaseDir = '~iranesta';
	    	
//			$this->path_web = 'http://iranestate.com/staging/';
//	    	$this->path_ssl = 'http://iranestate.com/staging/';
//	    	$this->controllerBaseDir = 'staging';	    	
	    	
	    	$this->environment = self::ENV_PRODUCTION;
	    	$this->add_db_connection('0',array(
//    			'host' => 'localhost:/tmp/mysql5.sock',
				'host' => 'localhost',
    			'database' => 'iranesta_realestate',
    			'user' => 'iranesta_realest',
    			'password' => 'f09j32Ds#fs32'
	    	));			
	    	
	    	$this->gmap_key = 'ABQIAAAAiDGvCAXixPd-ppPsmH5ojhQscYpvJPTuzgYkgpOv5CYEhLJJXxSGoCe_uK2rFe9YbQ3o1Yc-Q-XoBA';
	    }
	    
	    public function initialise_admin($language) {
	    	if($language==self::LANG_PERSIAN) {
	    		$language = self::LANG_PERSIAN;
	    		$dir = self::PERSIAN_DIR;
	    	} else {
	    		$dir = self::ENGLISH_DIR;
	    		$language = self::LANG_ENGLISH;
	    	}	    	
	    	
	    	$this->dir_admin = 'Admin/';
	    	$this->dir_templates = 'Templates/'.$dir.$this->dir_admin;
			$this->session = 'iranestate_admin';

			$this->language = $language;
			$this->path_templates = $this->path_project.$this->dir_templates;			
			$this->path_css = $this->path_web_ssl.'css/'.$dir.$this->dir_admin; 
	    	$this->path_images = $this->path_web_ssl.'images/Templates/'.$this->dir_admin;
	    	
			$this->path_admin_web = $this->path_web.'admin/';
		    $this->path_admin_ssl = $this->path_ssl.'admin/';
		    $this->path_admin_web_ssl = $this->path_web_ssl.'admin/';	    	
		    $this->frontend = false;
		    
	    	$this->user_login_path = $this->path_admin_web_ssl.'login';
			$this->user_denied_path = $this->path_admin_web_ssl.'denied';			    
	    }
	    
	    public function set_template($language) 
	    {
	    	if($language==self::LANG_PERSIAN) {
	    		$language = self::LANG_PERSIAN;
	    		$dir = self::PERSIAN_DIR;
	    	} else {
	    		$dir = self::ENGLISH_DIR;
	    		$language = self::LANG_ENGLISH;
	    	}
	    	
	    	$this->language = $language;
			
//	    	$this->path_templates = $this->path_project.$this->dir_templates.$dir;
	    	$this->path_templates = $this->path_project.$this->dir_templates.self::ENGLISH_DIR; // try to make it all work from one template
			
			$this->path_css = $this->path_web_ssl.'css/'.$dir; 
//	    	$this->path_images = $this->path_web_ssl.'images/'.$dir;
			$this->path_images = $this->path_web_ssl.'images/Templates/';
		
	    }
	
	    
		public function initialise_common() 
		{
			$this->frontend = true;
	    	$this->path_public=$this->path_settings.'../../../';
	    	$this->path_app=$this->path_settings.'../';
	    	$this->path_project=$this->path_settings.'../../';		
			 
	    	
			
			$this->key = 'fd6pzw0r2u23jfaw053718as';
			$this->dir_templates = 'Templates/';
			$this->dir_controllers = 'Controllers/';
			$this->dir_js = 'js/';
			
			
				
			
			$this->language = self::LANG_ENGLISH;
			
			$this->path_controllers = $this->path_app.$this->dir_controllers;
			
			$this->path_temp = $this->path_project.'Temp/';
			
			$this->tag_title = 'title';
			$this->tag_keywords = 'keywords';
			$this->tag_description = 'description';
			
			$this->from_email='';
			$this->from_name='';
			
			$this->session_start = true;
			$this->session = 'iranestate';
			$this->session_user = 'user';
			$this->session_history = 'history';
			$this->session_messages = 'messages';
    		$this->session_pages = 'pages';
    		$this->session_forms = 'forms';
    		$this->session_language = 'language';		
    		$this->session_property = 'property';
    		$this->session_property_count_view = 'property-count-view';
    		$this->session_property_count_contact = 'property-count-contact';
    		
			$this->record_insert = '1';
			$this->record_update = '2';
			$this->record_delete = '3';
			
			$this->records_per_page = '20';
			
			/**
			 * ssl detection
			 */
			if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=='on') {
				$this->path_web_ssl = $this->path_ssl;
			} else {
				$this->path_web_ssl = $this->path_web;
			}			
			
			$this->path_js = $this->path_web_ssl.'js/';
			
			$this->user_login_path = $this->path_web_ssl.'login';
			$this->user_denied_path = $this->path_web_ssl.'denied';
			
			$this->general = new Bl_Record_Setting();
			$this->general->load();
			
		}	
	}
