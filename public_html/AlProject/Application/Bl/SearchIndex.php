<?php
	class Bl_SearchIndex {
		private $db; 
		private $settings;
		public function __construct()
		{
			$this->db = Al_Db::get_instance();
			$this->settings = Bl_Settings::get_instance();
		}
		
		public function update() 
		{
//			echo 'new - ' . $this->settings->general->getNewSearchIndexUpdateDate();
//			echo '<BR>'.$this->settings->general->getSearchIndexUpdateDate();
			$this->updateSearchIndex($this->settings->general->getSearchIndexUpdateDate());
			$this->settings->general->setSearchIndexUpdateDate($this->settings->general->getNewSearchIndexUpdateDate());
			$this->settings->general->save();
		}
		
		public function updateAll()
		{
			$this->updateSearchIndex();
		}
		
		private function getFeatures() 
		{
			$res = $this->db->query("
				SELECT id, skey from property_features
			");
			$data = array();
			while($row = $res->fetch_row()) {
				$data[$row['id']] = $row['skey'];
			}
			return $data;
		}		

		private function getIntroduction() 
		{
			$res = $this->db->query("
				SELECT id, skey from property_introduction
			");
			$data = array();
			while($row = $res->fetch_row()) {
				$data[$row['id']] = $row['skey'];
			}
			return $data;
		}		
		
		
		private function updateSearchIndex($startDdate='')
		{
			$features = $this->getFeatures();
			$introduction = $this->getIntroduction();
			
			$product = new Bl_Record_Property();
			$qb = $product->getQb();
			
			$qb->join("LEFT JOIN property_search psrch ON psrch.id = r.id ");
			$qb->select("psrch.id ps_id");
			
			if($startDdate != '') {
				$qb->where("date_updated >= '".Al_Db::escape($startDdate)."'");
			}
			
			
			$res = $this->db->query($qb->get_query());

			while($data = $res->fetch_row()) {
				$product->loadFromArray($data);
				
				$keywords = $data['property_id'] . 
							" " . $data['city_eng'] . 
							" " . $data['city_per'] .
							" " . $data['zone_eng'] . 
							" " . $data['zone_per'] .
							" " . $data['country_eng'] .
							" " . $data['country_per'] .  
							" " . $product->getAddress();
//							" " . $product->getFeaturesEng() .  
//							" " . $product->getFeaturesPer() .
//							" " . $product->getIntroductionEng() .  				
//							" " . $product->getIntroductionPer();
				
				if($data['ps_id'] == '') {
					$query = " INSERT INTO property_search SET ";
					$queryWhere = '';			
				} else {
					$query = " UPDATE property_search SET ";
					$queryWhere = 'WHERE id = '.Al_Db::escape($data['ps_id']);						
				}
				
				$this->db->query($query . "
					id = '" . $product->getId() . "',
					features = '" . Al_Db::escape($product->getFeaturesSearchKeys($features)) . "',
					introduction = '" . Al_Db::escape($product->getIntroductionSearchKeys($introduction)) . "',
					property = '" . Al_Db::escape($keywords) . "' ".
					$queryWhere
				);
			}			
		}
		
		
		/**
		 * remove any stale records that have been deleted frop properties table 
		 */
		public function cleanUpPropertySearch() 
		{
			$this->db->query("
				DELETE ps FROM `property_search` ps
				LEFT JOIN `properties` p ON p.id = ps.`id`
				WHERE p.id IS NULL			
			");
		}
			
	}