<?php
	class Bl_Rss {
		public $_language;
		public $_settings;
		
		public function __construct()
		{
			$this->_settings = Bl_Settings::get_instance();
			$this->_language = Al_Language::get_instance();
		}
		
		public function showBuy() 
		{
			$this->display(Bl_Record_Property::BUY);
		}
		
		public function showRent()
		{
			$this->display(Bl_Record_Property::RENT);
		}
		
		public function showBook()
		{
			$this->display(Bl_Record_Property::BOOK);
		}
		
		public function showAll() 
		{
			$this->display('');
		}
		
		public function display($type='')
		{
			$property = new Bl_Record_Property();
			$res = $property->retrieveRssData($type,50);
			
			$latest = $res->fetch_row();			
			$this->displayRssStartingBlock($type,$latest);
			


//			$DateOfRequest = date("Y-m-d H:i:s", strtotime($_REQUEST["DateOfRequest"]));

			
//			echo $latest['creation_date'].'<BR>';			
			
//			echo $date->getDate();
			
//			Al_Utilities::a($latest);
			
//			echo $latest['creation_date'];
			
//			exit;
			
			$res->reset();
			while($row = $res->fetch_row()) {
				$property->loadFromArray($row);
				$this->displayPropertyItem($property);
			}
			$this->displayRssEndingBlock();
		}
		
		private function displayPropertyItem(Bl_Record_Property $property)
		{
/*
        <item>
                <title>Example entry</title>
                <description>Here is some text containing an interesting description.</description>
                <link>http://www.wikipedia.org/</link>
                <guid>unique string per item</guid>
                <pubDate>Mon, 06 Sep 2009 16:45:00 +0000 </pubDate>
        </item>
 */			
			$date = new DateTime($property->getCreationDate());
			$date = $date->format('D, d M Y H:i:s'). ' +0000';
			
			$item = '<item>';
			$item .= $this->getXmlTag(
				'title', 
				$property->getCity().' ' .$property->getPropertyType().' '.$property->getTransactionType(). ' - ' . $property->getPrice(true)
			);
			$item .= $this->getXmlTag(
				'description',
                '<p>'.$property->getFeatures(true).'</p>'.
                '<p>'.$property->getAddress() .', ' . $property->getZone() . ' '. $property->getCountry() . ' '.$property->getPostalCode().'</p>'			
			);			
			$item .= $this->getXmlTag('link',$property->getUrl());
			$item .= $this->getXmlTag('guid',$property->getPropertyId());
			$item .= $this->getXmlTag('pubDate',$date);
			$item .= '</item>';

			echo $item;
		}
		
		private function displayRssStartingBlock($type,$latest)
		{
			$date = new DateTime($latest['creation_date']);
			$date = $date->format('D, d M Y H:i:s'). ' +0000';
			
			
			echo 
				'<?xml version="1.0" encoding="UTF-8" ?>'.
				'<rss version="2.0">'.
				'<channel>'.
        		$this->getRssTitle($type).
        		$this->getRssDescription($type).
        		$this->getLanguage().
        		$this->getLink($type).
        		$this->getXmlTag('lastBuildDate', $date),
        		$this->getXmlTag('pubDate', $date);
		}
		
		private function getLink($type) 
		{
			if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
				$lang = Bl_Settings::LANG_ENGLISH_PREFIX;
			} else {
				$lang = Bl_Settings::LANG_PERSIAN_PREFIX;
			} 
			
			switch($type) {
				case Bl_Record_Property::BUY:
					$type = 'buy';
					break;
				case Bl_Record_Property::RENT:
					$type = 'rent';
					break;
				case Bl_Record_Property::BOOK:
					$type = 'book';
					break;
				default:
					$type = 'buy';
					break;
			} 			
			
			return $this->getXmlTag('link', 
				$this->_settings->path_web.$lang.'/'.$type.'/search/1/'
			);
		}
		
		private function getLanguage() 
		{
			if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
				$value = Bl_Settings::LANG_ENGLISH_PREFIX;
			} else {
				$value = Bl_Settings::LANG_PERSIAN_PREFIX;
			}
			
			return $this->getXmlTag('language', $value);
		}
		
		private function getRssTitle($type) 
		{
			switch($type) {
				case Bl_Record_Property::BUY:
					$value = $this->_language->getValue(Al_Language::RSS_BUY_TITLE);
					break;
				case Bl_Record_Property::RENT:
					$value = $this->_language->getValue(Al_Language::RSS_RENT_TITLE);
					break;
				case Bl_Record_Property::BOOK:
					$value = $this->_language->getValue(Al_Language::RSS_BOOK_TITLE);
					break;
				default:
					$value = $this->_language->getValue(Al_Language::RSS_ALL_TITLE);
					break;
			} 
			return $this->getXmlTag('title', $value);
		}
		
		private function getRssDescription($type) 
		{
			switch($type) {
				case Bl_Record_Property::BUY:
					$value = $this->_language->getValue(Al_Language::RSS_BUY_DESCRIPTION);
					break;
				case Bl_Record_Property::RENT:
					$value = $this->_language->getValue(Al_Language::RSS_RENT_DESCRIPTION);
					break;
				case Bl_Record_Property::BOOK:
					$value = $this->_language->getValue(Al_Language::RSS_BOOK_DESCRIPTION);
					break;
				default:
					$value = $this->_language->getValue(Al_Language::RSS_ALL_DESCRIPTION);
					break;
			}
			
			return $this->getXmlTag('description', $value);
		}	

		private function getRssLink($type)
		{
			
		}
		
		private function getXmlTag($tag,$value,$escape=true)
		{
			if($escape) {
				$value = $this->xmlspecialchars($value);
			}
			return '<'.$tag.'>'.$value.'</'.$tag.'>';
		}
		
		
		
		private function displayRssEndingBlock()
		{
			echo 
				'</channel>'.
				'</rss>';
		}
		
		private function xmlspecialchars($text) 
		{
   			return str_replace('&#039;', '&apos;', htmlspecialchars($text, ENT_QUOTES));
		}
		
	}