<?php
	class Bl_NewSiteSetup {
		public $db;
		
		public $debug = true;
		public $debugFull = true;
		
		public function __construct() 
		{
			$this->db = Al_Db::get_instance();
		}
		
		
		/**
		 * 
		 * sets main_image_id field for all property records.
		 * 0 is used for properties without images
		 */
		public function setImages()
		{
			$this->debug('start - property main image setup.');
			$res = $this->db->query("
				SELECT p.id, i.id image_id
				FROM properties p
				LEFT JOIN `property_images` i ON i.property_id = p.id
				WHERE i.`main_image` = 1
				GROUP BY p.id			
			");			
			while($row = $res->fetch_row()) {
				$this->debugFull('property id - '.$row['id'] . ' image id - ' . $row['image_id']);
				$this->db->query("
					UPDATE properties SET
						main_image_id = '" . Al_Db::escape($row['image_id']) . "'
					where id = '" . Al_Db::escape($row['id']) . "'
				");
			}
			$this->debug('end - property main image setup.');
		}
		
		private function debug($message) {
			if($this->debug) {
				echo '<strong>'.$message.'</strong><BR>';
			}			
		}
		
		private function debugFull($message) {
			if($this->debug) {
				echo $message.'<BR>';
			}			
		}

		/**
		 * 
		 * updates property date_updated field to now
		 */
		public function setPropertyDateUpdated() 
		{
			$this->debug('start - update property dates');
			$this->db->query("
				update properties set 
				date_updated = now()
			");
			$this->debug('end - update property dates');
		}

		public function setIntroductionsKeys() 
		{
			$res = $this->db->query("
				SELECT * from property_introduction
			");
			
			$item = new Bl_Record_PropertyIntroduction();
			while($row = $res->fetch_row()) {
				$item->loadFromArray($row);
				$item->setSkey();
				$item->save();
			}			
		}
		
		public function setFeaturesKeys()
		{
			$res = $this->db->query("
				SELECT * from property_features 
			");
			
			$item = new Bl_Record_PropertyFeature();
			while($row = $res->fetch_row()) {
				$item->loadFromArray($row);
				$item->setSkey();
				$item->save();
			}
		}		
		
		/**
		 * marks existing properties to be displayed within old url
		 * - property with old_sitemap set to 1 will have english URL's displayed without en prefix
		 * 
		 */
		public function setOldSiteMapProperties()
		{
			$res = $this->db->query("
				UPDATE properties 
					SET old_sitemap = 1
			");
		}
		
		
	}