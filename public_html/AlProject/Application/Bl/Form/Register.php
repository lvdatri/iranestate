<?php
class Bl_Form_Register extends Al_Form
{
	public function afterDbData() {
		if($this->getMode()==Al_Form::UPDATE) {
			$this->_getField('password_confirm')->setValue($this->_getField('password')->getValue());
		}
	}
	
	public function beforeValidation() {
		$this->_getField('email')->getRule('dbUnique')->setRecordId($this->getId());
	}	
	
    public function initialiseForm()
    {
    	$language = Al_Language::get_instance();
    	
        $field = new Al_Form_Field($this);
        $field->setName('name');
        $field->setLabel($language->getValue(Al_Language::FLD_REG_NAME));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('email');
        $field->setLabel($language->getValue(Al_Language::FLD_REG_EMAIL));
        $field->addRule('required');
        $field->addRule('email');
        $field->addRule('dbUnique',array(
            'table' => 'members',
        ));
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password');
		$field->setLabel($language->getValue(Al_Language::FLD_REG_PASSWORD));        
        $field->addRule('required');
        $field->addRule('length',array(
            'min' => '8',
            'max' => '20',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password_confirm');
		$field->setLabel($language->getValue(Al_Language::FLD_REG_REPEAT_PASSWORD));        
        $field->addRule('required');
        $field->addRule('equalTo',array(
            'toField' => 'password',
        ));
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('captcha');
        $field->setLabel($language->getValue(Al_Language::FLD_REG_QUESTION));
        $field->addRule('required');
        if(isset($_POST['popup'])) {
	        $field->addRule('mathCaptcha',array(
	        	'type'=>'register-popup',
	        ));
        } else {
	        $field->addRule('mathCaptcha',array(
	        	'type'=>'register',
	        ));
        }
        $this->_addField($field);        
        
    }

    public function getName($escaped = true)
    {
        return $this->_getField('name')->getValue($escaped);
    }

    public function getEmail($escaped = true)
    {
        return $this->_getField('email')->getValue($escaped);
    }

    public function getPassword($escaped = true)
    {
        return $this->_getField('password')->getValue($escaped);
    }
    
	public function getPasswordConfirm($escaped = true)
    {
        return $this->_getField('password_confirm')->getValue($escaped);
    }

	public function getCaptcha($escaped = true)
    {
        return $this->_getField('captcha')->getValue($escaped);
    }        

    public function setName($value)
    {
        $this->_setField('name')->setValue($escaped);
    }

    public function setEmail($value)
    {
        $this->_setField('email')->setValue($escaped);
    }

    public function setPassword($value)
    {
        $this->_setField('password')->setValue($escaped);
    }

	public function setPasswordConfirm($value)
    {
        $this->_getField('password_confirm')->setValue($value);
    }

	public function setCaptcha($value)
    {
        $this->_getField('captcha')->setValue($value);
    }        

}
