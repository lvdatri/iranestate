<?php
class Bl_Form_Admin_Email extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('title');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '200',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('description');
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('subject_eng');
        $field->setLabel('Subject');
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('content_eng');
        $field->setLabel('Content');
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('subject_per');
        $field->setLabel('Subject');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('content_per');
        $field->setLabel('Content');
        $this->_addField($field);
    }

    public function getTitle($escaped = true)
    {
        return $this->_getField('title')->getValue($escaped);
    }

    public function getDescription($escaped = true)
    {
        return $this->_getField('description')->getValue($escaped);
    }

    public function getSubjectEng($escaped = true)
    {
        return $this->_getField('subject_eng')->getValue($escaped);
    }

    public function getContentEng($escaped = true)
    {
        return $this->_getField('content_eng')->getValue($escaped);
    }

    public function getSubjectPer($escaped = true)
    {
        return $this->_getField('subject_per')->getValue($escaped);
    }

    public function getContentPer($escaped = true)
    {
        return $this->_getField('content_per')->getValue($escaped);
    }

    public function setTitle($value)
    {
        $this->_setField('title')->setValue($escaped);
    }

    public function setDescription($value)
    {
        $this->_setField('description')->setValue($escaped);
    }

    public function setSubjectEng($value)
    {
        $this->_setField('subject_eng')->setValue($escaped);
    }

    public function setContentEng($value)
    {
        $this->_setField('content_eng')->setValue($escaped);
    }

    public function setSubjectPer($value)
    {
        $this->_setField('subject_per')->setValue($escaped);
    }

    public function setContentPer($value)
    {
        $this->_setField('content_per')->setValue($escaped);
    }


}
