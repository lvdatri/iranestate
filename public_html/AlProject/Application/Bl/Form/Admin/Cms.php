<?php
class Bl_Form_Admin_Cms extends Al_Form
{
	
	public function beforeValidation() 
	{
		if($this->_customSettings['customPath']==false) {
			$this->_getField('title_eng')->getRule('dbUnique')->setRecordId($this->getId());
		} else {
			$this->_getField('path')->getRule('dbUnique')->setRecordId($this->getId());
		}
	}

    public function initialiseForm()
    {
		if($this->_customSettings['customPath']==true) {
	    	$field = new Al_Form_Field($this);
	        $field->setName('path');
	        $field->setLabel('Path');
	        $field->addRule('required');
	        $field->addRule('length',array(
	            'max' => '255',
	        ));
	        $this->_addField($field);    	
	        $field->addRule('dbUnique',array(
	        	'table' => 'cms',
	        ));			
		}
    	
    	$field = new Al_Form_Field($this);
        $field->setName('keywords_eng');
        $field->setLabel('Keywords English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);    	
    	
    	
        $field = new Al_Form_Field($this);
        $field->setName('keywords_eng');
        $field->setLabel('Keywords English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('keywords_per');
        $field->setLabel('Keywords Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('description_eng');
        $field->setLabel('Description English');
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('description_per');
        $field->setLabel('Description Persian');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('title_eng');
        $field->setLabel('Title English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        if($this->_customSettings['customPath']==false) {        
	        $field->addRule('dbUnique',array(
	        	'table' => 'cms',
	        	'dbField' => 'path',
	        	'urlValue' => true,
	        	'prefix' => $this->_customSettings['path'], 
	        ));
        }
        
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('title_per');
        $field->setLabel('Title Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('content_eng');
        $field->setLabel('Content English');
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('content_per');
        $field->setLabel('Content Persian');
        $this->_addField($field);
        
        $field = new Al_Form_FieldFile($this);
        $field->setName('image');
        $field->setLabel('Image');
        $field->setPath('images/cms/');
        $field->setExtension(true);
        $field->setImages(array(
        	array(
        		"width" => "100",
        		"prefix" => "th-",
			),
			array(
        		"width" => "150",
        		"prefix" => "sml-",
			),
			array(
				"width" => "850",
        		"prefix" => "lrg-",
			),
        ));
//		$field->addRule('fileRequired');
        $field->addRule('file',array(
            'validExtensions' => 'jpg,jpeg,gif,png',
        	'maxFileSize' => '1048576',
        	'validImage' => true, 
        ));
        $this->_addField($field);     

        if($this->_customSettings['sortOrder']==true) {
	        $field = new Al_Form_Field($this);
	        $field->setName('sort_order');
	        $field->addRule('required');
	        $field->addRule('int');
	        $this->_addField($field);
        }        

    }

    public function getPath($escaped = true)
    {
        return $this->_getField('path')->getValue($escaped);
    }
    
    public function getKeywordsEng($escaped = true)
    {
        return $this->_getField('keywords_eng')->getValue($escaped);
    }

    public function getKeywordsPer($escaped = true)
    {
        return $this->_getField('keywords_per')->getValue($escaped);
    }

    public function getDescriptionEng($escaped = true)
    {
        return $this->_getField('description_eng')->getValue($escaped);
    }

    public function getDescriptionPer($escaped = true)
    {
        return $this->_getField('description_per')->getValue($escaped);
    }

    public function getTitleEng($escaped = true)
    {
        return $this->_getField('title_eng')->getValue($escaped);
    }

    public function getTitlePer($escaped = true)
    {
        return $this->_getField('title_per')->getValue($escaped);
    }

    public function getContentEng($escaped = true)
    {
        return $this->_getField('content_eng')->getValue($escaped);
    }

    public function getContentPer($escaped = true)
    {
        return $this->_getField('content_per')->getValue($escaped);
    }

    public function getImage($escaped = true)
    {
        return $this->_getField('image')->getValue($escaped);
    }
    
    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }    

    public function setKeywordsEng($value)
    {
        $this->_setField('keywords_eng')->setValue($escaped);
    }

    public function setKeywordsPer($value)
    {
        $this->_setField('keywords_per')->setValue($escaped);
    }

    public function setDescriptionEng($value)
    {
        $this->_setField('description_eng')->setValue($escaped);
    }

    public function setDescriptionPer($value)
    {
        $this->_setField('description_per')->setValue($escaped);
    }

    public function setTitleEng($value)
    {
        $this->_setField('title_eng')->setValue($escaped);
    }

    public function setTitlePer($value)
    {
        $this->_setField('title_per')->setValue($escaped);
    }

    public function setContentEng($value)
    {
        $this->_setField('content_eng')->setValue($escaped);
    }

    public function setContentPer($value)
    {
        $this->_setField('content_per')->setValue($escaped);
    }
    
    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }    

    public function setImage($value)
    {
        $this->_setField('image')->setValue($escaped);
    }


}