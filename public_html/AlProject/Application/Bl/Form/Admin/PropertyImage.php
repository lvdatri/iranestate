<?php
class Bl_Form_Admin_PropertyImage extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_FieldFile($this);
        $field->setName('image_name');
        $field->setLabel('Image');
        $field->setPath('images/properties/');
        $field->setExtension(true);
        $field->setImages(array(
        	array(
        		"width" => "180",
        		"prefix" => "th-",
			),
			array(
        		"width" => "250",
        		"prefix" => "sml-",
			),			
			array(
				"width" => "500",
        		"prefix" => "med-",
			),
			array(
				"width" => "900",
        		"prefix" => "lrg-",
			),		
			
        ));
//        $field->addRule('fileRequired');        
        $field->addRule('file',array(
            'validExtensions' => 'jpg,jpeg,gif,png',
        	'maxFileSize' => '1048576',
        	'validImage' => true, 
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('main_image');
        $field->addRule('required');
        $this->_addField($field);
    }

    public function getImageName($escaped = true)
    {
        return $this->_getField('image_name')->getValue($escaped);
    }

    public function getMainImage($escaped = true)
    {
        return $this->_getField('main_image')->getValue($escaped);
    }

    public function setImageName($value)
    {
        $this->_setField('image_name')->setValue($escaped);
    }

    public function setMainImage($value)
    {
        $this->_setField('main_image')->setValue($escaped);
    }

    public function cbMainImage()
    {
        return $this->_cbValue($this->getMainImage());
    }


}