<?php
class Bl_Form_Admin_PropertyImages extends Al_Form
{

    public function initialiseForm()
    {
    	$this->settings = Bl_Settings::get_instance();
    	
    	$imageSettings = array(
        	array(
        		"width" => "180",
        		"prefix" => "th-",
				"watermarkPath" => $this->settings->path_public.'images/watermarks/th.png',
				"watermarkVerticalAlign" => 'b', //t,m,b
				"watermarkHorizontalAlign"=>'r', //l,c,r     	
			),
			array(
        		"width" => "250",
        		"prefix" => "sml-",
				"watermarkPath" => $this->settings->path_public.'images/watermarks/sml.png',
				"watermarkVerticalAlign" => 'b', //t,m,b
				"watermarkHorizontalAlign"=>'r', //l,c,r     				
			),			
			array(
				"width" => "500",
        		"prefix" => "med-",
				"watermarkPath" => $this->settings->path_public.'images/watermarks/med.png',
				"watermarkVerticalAlign" => 'b', //t,m,b
				"watermarkHorizontalAlign"=>'r', //l,c,r     				
			),
			array(
				"width" => "900",
        		"prefix" => "lrg-",
				"watermarkPath" => $this->settings->path_public.'images/watermarks/lrg.png',
				"watermarkVerticalAlign" => 'b', //t,m,b
				"watermarkHorizontalAlign"=>'r', //l,c,r     				
			),		
        );
        $fileSettings = array(
            'validExtensions' => 'jpg,jpeg,gif,png',
        	'maxFileSize' => '1048576',
        	'validImage' => true, 
        );
    	
        $field = new Al_Form_FieldFile($this);
        $field->setName('image_1');
        $field->setLabel('Image 1');
        $field->setPath('images/properties/');
        $field->setExtension(true);
        $field->setImages($imageSettings);
        $field->addRule('fileRequired');        
        $field->addRule('file',$fileSettings);
        $this->_addField($field);
        
        $field = new Al_Form_FieldFile($this);
        $field->setName('image_2');
        $field->setLabel('Image 2');
        $field->setPath('images/properties/');
        $field->setExtension(true);
        $field->setImages($imageSettings);
        $field->addRule('file',$fileSettings);
        $this->_addField($field);       

        $field = new Al_Form_FieldFile($this);
        $field->setName('image_3');
        $field->setLabel('Image 3');
        $field->setPath('images/properties/');
        $field->setExtension(true);
        $field->setImages($imageSettings);
        $field->addRule('file',$fileSettings);
        $this->_addField($field);

        $field = new Al_Form_FieldFile($this);
        $field->setName('image_4');
        $field->setLabel('Image 4');
        $field->setPath('images/properties/');
        $field->setExtension(true);
        $field->setImages($imageSettings);
        $field->addRule('file',$fileSettings);
        $this->_addField($field);   

        $field = new Al_Form_FieldFile($this);
        $field->setName('image_5');
        $field->setLabel('Image 5');
        $field->setPath('images/properties/');
        $field->setExtension(true);
        $field->setImages($imageSettings);
        $field->addRule('file',$fileSettings);
        $this->_addField($field);                
        
    }

    public function getImage1($escaped = true)
    {
        return $this->_getField('image_1')->getValue($escaped);
    }
    
    public function getImage2($escaped = true)
    {
        return $this->_getField('image_2')->getValue($escaped);
    }

    public function getImage3($escaped = true)
    {
        return $this->_getField('image_3')->getValue($escaped);
    }

    public function getImage4($escaped = true)
    {
        return $this->_getField('image_4')->getValue($escaped);
    }

    public function getImage5($escaped = true)
    {
        return $this->_getField('image_5')->getValue($escaped);
    }    

    public function setImage1($value)
    {
        $this->_setField('image_1')->setValue($escaped);
    }

    public function setImage2($value)
    {
        $this->_setField('image_2')->setValue($escaped);
    }

    public function setImage3($value)
    {
        $this->_setField('image_3')->setValue($escaped);
    }
    
    public function setImage4($value)
    {
        $this->_setField('image_4')->setValue($escaped);
    }

    public function setImage5($value)
    {
        $this->_setField('image_5')->setValue($escaped);
    }    


}