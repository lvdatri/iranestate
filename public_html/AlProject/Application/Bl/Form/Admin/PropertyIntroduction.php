<?php
class Bl_Form_Admin_PropertyIntroduction extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('option_eng');
        $field->setLabel('Option English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '65',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('option_per');
        $field->setLabel('Option Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('sort_order');
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
    }

    public function getOptionEng($escaped = true)
    {
        return $this->_getField('option_eng')->getValue($escaped);
    }

    public function getOptionPer($escaped = true)
    {
        return $this->_getField('option_per')->getValue($escaped);
    }

    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }

    public function setOptionEng($value)
    {
        $this->_setField('option_eng')->setValue($escaped);
    }

    public function setOptionPer($value)
    {
        $this->_setField('option_per')->setValue($escaped);
    }

    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }


}
