<?php
class Bl_Form_Admin_PropertyStatus extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('status_eng');
        $field->setLabel('Status English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('status_per');
        $field->setLabel('Status Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('sort_order');
        $field->setLabel('Order');
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
    }

    public function getStatusEng($escaped = true)
    {
        return $this->_getField('status_eng')->getValue($escaped);
    }

    public function getStatusPer($escaped = true)
    {
        return $this->_getField('status_per')->getValue($escaped);
    }

    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }

    public function setStatusEng($value)
    {
        $this->_setField('status_eng')->setValue($escaped);
    }

    public function setStatusPer($value)
    {
        $this->_setField('status_per')->setValue($escaped);
    }

    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }


}
