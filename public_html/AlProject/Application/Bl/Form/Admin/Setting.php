<?php
class Bl_Form_Admin_Setting extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('email_from');
        $field->addRule('required');
        $field->addRule('email');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('email_from_name');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '65',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('usd_rate');
        $field->setLabel('1 USD =');
        $field->addRule('number');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('gbp_rate');
        $field->setLabel('1 GBP =');
        $field->addRule('number');
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('aud_rate');
        $field->setLabel('1 AUD =');
        $field->addRule('number');
        $this->_addField($field);        
        
        $field = new Al_Form_Field($this);
        $field->setName('require_property_approval');
        $this->_addField($field);
        
 		$field = new Al_Form_Field($this);
        $field->setName('map_lat');
        $field->setLabel('Latitute');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('map_long');
        $field->setLabel('Longitude');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('map_depth');
        $field->setLabel('Depth');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);        
    }

    public function getEmailFrom($escaped = true)
    {
        return $this->_getField('email_from')->getValue($escaped);
    }

    public function getEmailFromName($escaped = true)
    {
        return $this->_getField('email_from_name')->getValue($escaped);
    }

    public function getUsdRate($escaped = true)
    {
        return $this->_getField('usd_rate')->getValue($escaped);
    }
    
	public function getGbpRate($escaped = true)
    {
        return $this->_getField('gbp_rate')->getValue($escaped);
    }
	
    public function getAudRate($escaped = true)
    {
        return $this->_getField('aud_rate')->getValue($escaped);
    }        

    public function getRequirePropertyApproval($escaped = true)
    {
        return $this->_getField('require_property_approval')->getValue($escaped);
    }
    
  	public function getMapLat($escaped = true)
    {
        return $this->_getField('map_lat')->getValue($escaped);
    }

    public function getMapLong($escaped = true)
    {
        return $this->_getField('map_long')->getValue($escaped);
    }

    public function getMapDepth($escaped = true)
    {
        return $this->_getField('map_depth')->getValue($escaped);
    }    

    public function setEmailFrom($value)
    {
        $this->_setField('email_from')->setValue($escaped);
    }

    public function setEmailFromName($value)
    {
        $this->_setField('email_from_name')->setValue($escaped);
    }

    public function setUsdRate($value)
    {
        $this->_getField('usd_rate')->getValue($value);
    }
    
	public function setGbpRate($value)
    {
        $this->_getField('gbp_rate')->getValue($value);
    }

	public function setAudRate($value)
    {
        $this->_getField('aud_rate')->getValue($value);
    }    

    public function setRequirePropertyApproval($value)
    {
        $this->_setField('require_property_approval')->setValue($escaped);
    }

    public function cbRequirePropertyApproval()
    {
        return $this->_cbValue($this->getRequirePropertyApproval());
    }

    public function setMapLat($value)
    {
        $this->_setField('map_lat')->setValue($escaped);
    }

    public function setMapLong($value)
    {
        $this->_setField('map_long')->setValue($escaped);
    }

    public function setMapDepth($value)
    {
        $this->_setField('map_depth')->setValue($escaped);
    }
    
}
