<?php
class Bl_Form_Admin_PropertyFeature extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('feature_eng');
        $field->setLabel('Feature English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('feature_per');
        $field->setLabel('Feature Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('sort_order');
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
    }

    public function getFeatureEng($escaped = true)
    {
        return $this->_getField('feature_eng')->getValue($escaped);
    }

    public function getFeaturePer($escaped = true)
    {
        return $this->_getField('feature_per')->getValue($escaped);
    }

    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }

    public function setFeatureEng($value)
    {
        $this->_setField('feature_eng')->setValue($escaped);
    }

    public function setFeaturePer($value)
    {
        $this->_setField('feature_per')->setValue($escaped);
    }

    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }


}
