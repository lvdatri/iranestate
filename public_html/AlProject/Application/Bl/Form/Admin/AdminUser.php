<?php
class Bl_Form_Admin_AdminUser extends Al_Form
{

	public function afterDbData() {
		if($this->getMode()==Al_Form::UPDATE) {
			$this->_getField('password_confirm')->setValue($this->_getField('password')->getValue());
		}
	}
	
	public function beforeValidation() {
		$this->_getField('username')->getRule('dbUnique')->setRecordId($this->getId());
	}
	
    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('first_name');
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('last_name');
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('email');
        $field->addRule('required');
        $field->addRule('email');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('username');
        $field->addRule('required');
        $field->addRule('dbUnique',array(
            'table' => 'admin_users',
        ));
        $field->addRule('length',array(
            'min' => '6',
            'max' => '20',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password');
        $field->addRule('required');
        $field->addRule('length',array(
            'min' => '6',
            'max' => '20',
        ));
        $field->addRule('notEqualTo',array(
            'toField' => 'username',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password_confirm');
        $field->addRule('required');
        $field->addRule('equalTo',array(
            'toField' => 'password',
        ));
        $this->_addField($field);
    }	
	
//    public function initialiseForm()
//    {
//        $field = new Al_Form_Field($this);
//        $field->setName('first_name');
//        $field->addRule('required');
//        $this->_addField($field);
//        
//        $field = new Al_Form_Field($this);
//        $field->setName('last_name');
//        $field->addRule('required');
//        $this->_addField($field);
//        
//        $field = new Al_Form_Field($this);
//        $field->setName('email');
//        $field->addRule('required');
//        $field->addRule('email');
//        $this->_addField($field);
//        
//        $field = new Al_Form_Field($this);
//        $field->setName('username');
//        $field->addRule('required');
//        $field->addRule('length',array(
//        	'min' => '6',
//        	'max' => '20',
//        ));
//        $field->addRule('dbUnique',array(
//        	'table' => 'admin_users',
//        ));
//        $this->_addField($field);
//        
//        $field = new Al_Form_Field($this);
//        $field->setName('password');
//        $field->addRule('required');
//		$field->addRule('notEqualTo',array(
//        	'toField' => 'username',
//        ));        
//		$field->addRule('length',array(
//        	'min' => '6',
//        	'max' => '20',
//        ));        
//        $this->_addField($field);
//        
//		$field = new Al_Form_Field($this);
//        $field->setName('password_confirm');
//		$field->addRule('required');        
//		$field->addRule('equalTo',array(
//        	'toField' => 'password',
//        ));
//        $this->_addField($field);        
//    }
    
    

    public function getFirstName($escaped = true)
    {
        return $this->_getField('first_name')->getValue($escaped);
    }

    public function getLastName($escaped = true)
    {
        return $this->_getField('last_name')->getValue($escaped);
    }

    public function getEmail($escaped = true)
    {
        return $this->_getField('email')->getValue($escaped);
    }

    public function getUsername($escaped = true)
    {
        return $this->_getField('username')->getValue($escaped);
    }

    public function getPassword($escaped = true)
    {
        return $this->_getField('password')->getValue($escaped);
    }
    
	public function getPasswordConfirm($escaped = true)
    {
        return $this->_getField('password_confirm')->getValue($escaped);
    }    

    public function setFirstName($value)
    {
        $this->_setField('first_name')->setValue($escaped);
    }

    public function setLastName($value)
    {
        $this->_setField('last_name')->setValue($escaped);
    }

    public function setEmail($value)
    {
        $this->_setField('email')->setValue($escaped);
    }

    public function setUsername($value)
    {
        $this->_setField('username')->setValue($escaped);
    }

    public function setPassword($value)
    {
        $this->_setField('password')->setValue($escaped);
    }
    
	public function setPasswordConfirm($value)
    {
        $this->_getField('password_confirm')->setValue($value);
    }    


}