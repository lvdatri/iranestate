<?php
class Bl_Form_Admin_City extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('city_eng');
        $field->setLabel('City English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('city_per');
        $field->setLabel('City Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('featured');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('sort_order');
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('lat');
        $field->setLabel('Latitude');
//        $field->addRule('required');
//        $field->addRule('number');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('lon');
        $field->setLabel('Longitude');
//        $field->addRule('required');
//        $field->addRule('number');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);             
    }

    public function getCityEng($escaped = true)
    {
        return $this->_getField('city_eng')->getValue($escaped);
    }

    public function getCityPer($escaped = true)
    {
        return $this->_getField('city_per')->getValue($escaped);
    }

    public function getFeatured($escaped = true)
    {
        return $this->_getField('featured')->getValue($escaped);
    }

    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }

	public function getLat($escaped = true)
    {
        return $this->_getField('lat')->getValue($escaped);
    }

	public function getLon($escaped = true)
    {
        return $this->_getField('lon')->getValue($escaped);
    }    
    
    public function setCityEng($value)
    {
        $this->_setField('city_eng')->setValue($escaped);
    }

    public function setCityPer($value)
    {
        $this->_setField('city_per')->setValue($escaped);
    }

    public function setFeatured($value)
    {
        $this->_setField('featured')->setValue($escaped);
    }

    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }

    public function cbFeatured()
    {
        return $this->_cbValue($this->getFeatured());
    }
    
    public function setLat($value)
    {
        $this->_getField('lat')->setValue($value);
    }

	public function setLon($value)
    {
        $this->_getField('lon')->setValue($value);
    }    


}
