<?php
class Bl_Form_Admin_PropertyType extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('type_eng');
        $field->setLabel('Type English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '65',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('type_per');
        $field->setLabel('Type Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('sort_order');
        $field->addRule('int');
        $this->_addField($field);
    }

    public function getTypeEng($escaped = true)
    {
        return $this->_getField('type_eng')->getValue($escaped);
    }

    public function getTypePer($escaped = true)
    {
        return $this->_getField('type_per')->getValue($escaped);
    }

    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }

    public function setTypeEng($value)
    {
        $this->_setField('type_eng')->setValue($escaped);
    }

    public function setTypePer($value)
    {
        $this->_setField('type_per')->setValue($escaped);
    }

    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }


}
