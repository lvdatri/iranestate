<?php
class Bl_Form_Admin_Zone extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('zone_eng');
        $field->setLabel('Zone English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('zone_per');
        $field->setLabel('Zone Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('sort_order');
        $this->_addField($field);
    }

    public function getZoneEng($escaped = true)
    {
        return $this->_getField('zone_eng')->getValue($escaped);
    }

    public function getZonePer($escaped = true)
    {
        return $this->_getField('zone_per')->getValue($escaped);
    }

    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }

    public function setZoneEng($value)
    {
        $this->_setField('zone_eng')->setValue($escaped);
    }

    public function setZonePer($value)
    {
        $this->_setField('zone_per')->setValue($escaped);
    }

    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }


}
