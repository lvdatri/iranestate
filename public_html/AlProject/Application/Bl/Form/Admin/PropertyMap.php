<?php
class Bl_Form_Admin_PropertyMap extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('lat');
        $field->setLabel('Latitude');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('lon');
        $field->setLabel('Longitude');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);        
        
        $field = new Al_Form_Field($this);
        $field->setName('depth');
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
    }

	public function getLat($escaped = true)
    {
        return $this->_getField('lat')->getValue($escaped);
    }

	public function getLon($escaped = true)
    {
        return $this->_getField('lon')->getValue($escaped);
    }

	public function getDepth($escaped = true)
    {
        return $this->_getField('depth')->getValue($escaped);
    }

    public function setLat($value)
    {
        $this->_getField('lat')->setValue($value);
    }

	public function setLon($value)
    {
        $this->_getField('lon')->setValue($value);
    }

	public function setDepth($value)
    {
        $this->_getField('depth')->setValue($value);
    }    

}
