<?php
class Bl_Form_Admin_Member extends Al_Form
{

	public function afterDbData() {
		if($this->getMode()==Al_Form::UPDATE) {
			$this->_getField('password_confirm')->setValue($this->_getField('password')->getValue());
		}
	}
	
	public function beforeValidation() {
		$this->_getField('email')->getRule('dbUnique')->setRecordId($this->getId());
	}	
	
    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('name');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('email');
        $field->addRule('required');
        $field->addRule('email');
        $field->addRule('dbUnique',array(
            'table' => 'members',
        ));
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password');
        $field->addRule('required');
        $field->addRule('length',array(
            'min' => '8',
            'max' => '20',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password_confirm');
        $field->addRule('required');
        $field->addRule('equalTo',array(
            'toField' => 'password',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('language');
        $field->addRule('required');
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('address');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('city');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('postcode');
        $field->addRule('length',array(
            'max' => '65',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('phone');
        $field->addRule('length',array(
            'max' => '65',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('newsletter_subscription');
        $this->_addField($field);        
    }

    public function getName($escaped = true)
    {
        return $this->_getField('name')->getValue($escaped);
    }

    public function getEmail($escaped = true)
    {
        return $this->_getField('email')->getValue($escaped);
    }

    public function getPassword($escaped = true)
    {
        return $this->_getField('password')->getValue($escaped);
    }
    
	public function getPasswordConfirm($escaped = true)
    {
        return $this->_getField('password_confirm')->getValue($escaped);
    }    

    public function getLanguage($escaped = true)
    {
        return $this->_getField('language')->getValue($escaped);
    }
    
    public function getAddress($escaped = true)
    {
        return $this->_getField('address')->getValue($escaped);
    }

    public function getCity($escaped = true)
    {
        return $this->_getField('city')->getValue($escaped);
    }

    public function getPostcode($escaped = true)
    {
        return $this->_getField('postcode')->getValue($escaped);
    }

    public function getPhone($escaped = true)
    {
        return $this->_getField('phone')->getValue($escaped);
    }

    public function getNewsletterSubscription($escaped = true)
    {
        return $this->_getField('newsletter_subscription')->getValue($escaped);
    }    

    public function setName($value)
    {
        $this->_setField('name')->setValue($escaped);
    }

    public function setEmail($value)
    {
        $this->_setField('email')->setValue($escaped);
    }

    public function setPassword($value)
    {
        $this->_setField('password')->setValue($escaped);
    }

	public function setPasswordConfirm($value)
    {
        $this->_getField('password_confirm')->setValue($value);
    }    
        
    public function setLanguage($value)
    {
        $this->_setField('language')->setValue($escaped);
    }
    
    public function setAddress($value)
    {
        $this->_setField('address')->setValue($escaped);
    }

    public function setCity($value)
    {
        $this->_setField('city')->setValue($escaped);
    }

    public function setPostcode($value)
    {
        $this->_setField('postcode')->setValue($escaped);
    }

    public function setPhone($value)
    {
        $this->_setField('phone')->setValue($escaped);
    }

    public function setNewsletterSubscription($value)
    {
        $this->_setField('newsletter_subscription')->setValue($escaped);
    }

    public function cbNewsletterSubscription()
    {
        return $this->_cbValue($this->getNewsletterSubscription());
    }    

    public function ddLanguage()
    {
        $data = array(
        	Bl_Settings::LANG_ENGLISH => 'English',
        	Bl_Settings::LANG_PERSIAN => 'Persian',
        );
        $dd = new Al_DropDown();
        $dd->setFirstOption(true);
        return $dd->setFromArray($data,$this->getLanguage(false));

    }


}
