<?php 
class Bl_Form_Admin_Country extends Al_Form
{

    public function initialiseForm()
    {
        $field = new Al_Form_Field($this);
        $field->setName('name_eng');
        $field->setLabel('Name English');
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('name_per');
        $field->setLabel('Name Persian');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('sort_order');
        $field->setLabel('Order');
        $field->addRule('int');
        $this->_addField($field);
    }

    public function getNameEng($escaped = true)
    {
        return $this->_getField('name_eng')->getValue($escaped);
    }

    public function getNamePer($escaped = true)
    {
        return $this->_getField('name_per')->getValue($escaped);
    }

    public function getSortOrder($escaped = true)
    {
        return $this->_getField('sort_order')->getValue($escaped);
    }

    public function setNameEng($value)
    {
        $this->_setField('name_eng')->setValue($escaped);
    }

    public function setNamePer($value)
    {
        $this->_setField('name_per')->setValue($escaped);
    }

    public function setSortOrder($value)
    {
        $this->_setField('sort_order')->setValue($escaped);
    }


}
