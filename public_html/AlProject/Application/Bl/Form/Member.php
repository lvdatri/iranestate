<?php
class Bl_Form_Member extends Al_Form
{
	public function beforeValidation() {
		$this->_getField('email')->getRule('dbUnique')->setRecordId($this->getId());
	}
	
    public function initialiseForm()
    {
    	$language = Al_Language::get_instance();
    	
        $field = new Al_Form_Field($this);
        $field->setName('name');
        $field->setLabel($language->getValue(Al_Language::FLD_ACCOUNT_NAME));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('email');
        $field->setLabel($language->getValue(Al_Language::FLD_ACCOUNT_EMAIL));
        $field->addRule('required');
        $field->addRule('email');
        $field->addRule('dbUnique',array(
            'table' => 'members',
        ));
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('address');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('city');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('postcode');
        $field->addRule('length',array(
            'max' => '65',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('phone');
        $field->addRule('length',array(
            'max' => '65',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('newsletter_subscription');
        $field->setLabel('Receive Emails');
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('profile_image');
        $field->addRule('length',array(
                'max' => '165',
            ));
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('company_name');
        $field->addRule('length',array(
                'max' => '255',
            ));
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('profile_description_eng');
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('profile_description_per');
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('website');
        $field->addRule('length',array(
                'max' => '255',
            ));
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('show_profile');
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('profile_email');
        $field->addRule('email');
        $field->addRule('length',array(
                'max' => '255',
            ));
        $this->_addField($field);



    }

    public function getName($escaped = true)
    {
        return $this->_getField('name')->getValue($escaped);
    }

    public function getEmail($escaped = true)
    {
        return $this->_getField('email')->getValue($escaped);
    }

    public function getAddress($escaped = true)
    {
        return $this->_getField('address')->getValue($escaped);
    }

    public function getCity($escaped = true)
    {
        return $this->_getField('city')->getValue($escaped);
    }

    public function getPostcode($escaped = true)
    {
        return $this->_getField('postcode')->getValue($escaped);
    }

    public function getPhone($escaped = true)
    {
        return $this->_getField('phone')->getValue($escaped);
    }

    public function getNewsletterSubscription($escaped = true)
    {
        return $this->_getField('newsletter_subscription')->getValue($escaped);
    }

    public function getProfileImage($escaped = true)
    {
        return $this->_getField('profile_image')->getValue($escaped);
    }

    public function getCompanyName($escaped = true)
    {
        return $this->_getField('company_name')->getValue($escaped);
    }

    public function getProfileDescriptionEng($escaped = true)
    {
        return $this->_getField('profile_description_eng')->getValue($escaped);
    }

    public function getProfileDescriptionPer($escaped = true)
    {
        return $this->_getField('profile_description_per')->getValue($escaped);
    }

    public function getWebsite($escaped = true)
    {
        return $this->_getField('website')->getValue($escaped);
    }

    public function getShowProfile($escaped = true)
    {
        return $this->_getField('show_profile')->getValue($escaped);
    }

    public function getProfileEmail($escaped = true)
    {
        return $this->_getField('profile_email')->getValue($escaped);
    }

    public function setName($value)
    {
        $this->_setField('name')->setValue($value);
    }

    public function setEmail($value)
    {
        $this->_setField('email')->setValue($value);
    }

    public function setAddress($value)
    {
        $this->_setField('address')->setValue($value);
    }

    public function setCity($value)
    {
        $this->_setField('city')->setValue($value);
    }

    public function setPostcode($value)
    {
        $this->_setField('postcode')->setValue($value);
    }

    public function setPhone($value)
    {
        $this->_setField('phone')->setValue($value);
    }

    public function setNewsletterSubscription($value)
    {
        $this->_setField('newsletter_subscription')->setValue($value);
    }

    public function setProfileImage($value)
    {
        $this->_setField('profile_image')->setValue($value);
    }

    public function setCompanyName($value)
    {
        $this->_setField('company_name')->setValue($value);
    }

    public function setProfileDescriptionEng($value)
    {
        $this->_setField('profile_description_eng')->setValue($value);
    }

    public function setProfileDescriptionPer($value)
    {
        $this->_setField('profile_description_per')->setValue($value);
    }

    public function setWebsite($value)
    {
        $this->_setField('website')->setValue($value);
    }

    public function setShowProfile($value)
    {
        $this->_setField('show_profile')->setValue($value);
    }

    public function setProfileEmail($value)
    {
        $this->_setField('profile_email')->setValue($value);
    }

    public function cbShowProfile()
    {
        return $this->_cbValue($this->getShowProfile());
    }

    public function cbNewsletterSubscription()
    {
        return $this->_cbValue($this->getNewsletterSubscription());
    }


}
