<?php
class Bl_Form_PropertyToFriend extends Al_Form
{
    public function initialiseForm()
    {
    	$language = Al_Language::get_instance();
    	
        $field = new Al_Form_Field($this);
        $field->setName('friend_your_name');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FRD_YOUR_NAME));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('friend_your_email');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FRD_YOUR_EMAIL));
        $field->addRule('required');        
        $field->addRule('email');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('friend_friends_name');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FRD_FRIENDS_NAME));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('friend_friends_email');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FRD_FRIENDS_EMAIL));
        $field->addRule('required');        
        $field->addRule('email');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);

        $field = new Al_Form_Field($this);
        $field->setName('friend_friends_subject');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FRD_SUBJECT));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);   
     
        $field = new Al_Form_Field($this);
        $field->setName('friend_description');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FRD_DESCRIPTION));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '300',
        ));
        $this->_addField($field);           
        
		$field = new Al_Form_Field($this);
        $field->setName('friend_captcha');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FRD_QUESTION));
        $field->addRule('required');
        $field->addRule('mathCaptcha',array(
        	'type'=>'properties-friend',
        ));
        $this->_addField($field);        

    }
 
    public function getFriendYourName($escaped = true)
    {
        return $this->_getField('friend_your_name')->getValue($escaped);
    }

    public function getFriendYourEmail($escaped = true)
    {
        return $this->_getField('friend_your_email')->getValue($escaped);
    }

    public function getFriendFriendsName($escaped = true)
    {
        return $this->_getField('friend_friends_name')->getValue($escaped);
    }

    public function getFriendFriendsEmail($escaped = true)
    {
        return $this->_getField('friend_friends_email')->getValue($escaped);
    }

    public function getFriendFriendsSubject($escaped = true)
    {
        return $this->_getField('friend_friends_subject')->getValue($escaped);
    }
 
    public function getFriendDescription($escaped = true)
    {
        return $this->_getField('friend_description')->getValue($escaped);
    }

    public function getCaptcha($escaped = true)
    {
        return $this->_getField('captcha')->getValue($escaped);
    }    
    

}