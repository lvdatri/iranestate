<?php
class Bl_Form_Property extends Al_Form
{
	
    public function initialiseForm()
    {
        $language = Al_Language::get_instance();
        $this->settings = Bl_Settings::get_instance();
    	
        $field = new Al_Form_Field($this);
        $field->setName('property_transaction_id');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_TRANSACTION));
        $field->addRule('required');
        $this->_addField($field);
        
        
        
        if($this->_customSettings['property']->getTemp()=='0' && 
			$this->_customSettings['property']->getPropertyStatusId() != Bl_Data_Statuses::PENDING_APPROVAL && 
			$this->_customSettings['property']->getPropertyStatusId() != Bl_Data_Statuses::EXPIRED        
        ) {
	        $field = new Al_Form_Field($this);
	        $field->setName('property_status_id');
	        $field->setLabel($language->getValue(Al_Language::FLD_PROP_STATUS));
	        $field->addRule('required');
	        $field->addRule('allowedValues',array(
	        	'allowedValues' => '2,4,5,6,7,8,9',
	        ));
	        $this->_addField($field);
        }
        
        $field = new Al_Form_Field($this);
        $field->setName('property_type_id');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_TYPE));
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('price');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_PRICE));
//        $field->addRule('required');
        $field->addRule('number');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('deposit');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_DEPOSIT));
        $field->addRule('requiredConditional',array(
        	'triggerField' => 'property_transaction_id',
        	'triggerFieldValues' => Bl_Data_Transactions::FOR_RENT,
        ));        
        $field->addRule('number');
        $this->_addField($field);        
        
        $field = new Al_Form_Field($this);
        $field->setName('property_price_type_id');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_PRICE_TYPE));
//        $field->addRule('required');
        $this->_addField($field);        
        
		$field = new Al_Form_Field($this);
        $field->setName('swimming_pool');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_SWIMMING_POOL));
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);  

        $field = new Al_Form_Field($this);
        $field->setName('land_area');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_LAND_AREA));
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('build_area');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_BUILD_AREA));
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('bedrooms');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_BEDROOMS));
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('bathrooms');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_BATHROOMS));
        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('parking');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_PARKING));
//        $field->addRule('required');
        $field->addRule('int');
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('address');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_ADDRESS));
//        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);        
        
        $field = new Al_Form_Field($this);
        $field->setName('country_id');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_COUNTRY));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('city_id');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CITY));
        $field->addRule('required');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('zone_id');
        $field->setLabel('Zone');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('zone');
        $field->setLabel('New Zone');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);        
       
        $field = new Al_Form_Field($this);
        $field->setName('postal_code');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_POSTAL_CODE));
//        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '32',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('comments_eng');
        $this->_addField($field);

		$field = new Al_Form_Field($this);
        $field->setName('comments_per');
        $this->_addField($field);           
        
        $propertyIntroduction = new Bl_Record_PropertyIntroduction();
        $proertyFeatures = new Bl_Record_PropertyFeature();
		$field = new Al_Form_FieldCheckboxGroup($this);
        $field->setName('introduction');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_INTRODUCTION));
        $field->addRule('required');
        $field->setOptions($propertyIntroduction->retrieveCbGroupValues($this->settings->language));
        $this->_addField($field);        
        
        $proertyFeatures = new Bl_Record_PropertyFeature();
		$field = new Al_Form_FieldCheckboxGroup($this);
        $field->setName('features');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_FEATURES));
        $field->addRule('required');
        $field->setOptions($proertyFeatures->retrieveCbGroupValues($this->settings->language));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('contact_name');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONTACT_NAME));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('contact_phone');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONTACT_PHONE));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('contact_email');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONTACT_EMAIL));
        $field->addRule('required');
        $field->addRule('email');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('terrace_m2');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_TERRACE));
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('land_width');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_LAND_WIDTH));
        $field->addRule('number');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('land_height');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_LAND_HEIGHT));
        $field->addRule('number');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('average_ceiling_height');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_AVERAGE_CEILING_HEIGHT));
        $field->addRule('number');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('located_on_level');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_LOCATED_ON_LEVEL));
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('number_of_levels');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_NUM_OF_LEVELS));
        $field->addRule('int');
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('ceiling_height');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CEILING_HEIGHT));
        $field->addRule('number');
        $this->_addField($field);

    }

	public function getExpiryDate($escaped = true)
    {
        return $this->_getField('expiry_date')->getValue($escaped);
    }

	public function getMemberId($escaped = true)
    {
        return $this->_getField('member_id')->getValue($escaped);
    }    
    
    public function getPropertyTransactionId($escaped = true)
    {
        return $this->_getField('property_transaction_id')->getValue($escaped);
    }

    public function getPropertyTypeId($escaped = true)
    {
        return $this->_getField('property_type_id')->getValue($escaped);
    }

    public function getPropertyPriceTypeId($escaped = true)
    {
        return $this->_getField('property_price_type_id')->getValue($escaped);
    }

    public function getPropertyStatusId($escaped = true)
    {
        $f = $this->_getField('property_status_id');
        if($f) {
        	return $f->getValue($escaped);
        } else {
        	return '';
        }
    }

    public function getPrice($escaped = true)
    {
        return $this->_getField('price')->getValue($escaped);
    }
    
    public function getSwimmingPool()
    {
    	return $this->_getField('swimming_pool')->getValue();
    }

    public function getCountryId($escaped = true)
    {
        return $this->_getField('country_id')->getValue($escaped);
    }

    public function getCityId($escaped = true)
    {
        return $this->_getField('city_id')->getValue($escaped);
    }

    public function getZoneId($escaped = true)
    {
        return $this->_getField('zone_id')->getValue($escaped);
    }

    public function getAddress($escaped = true)
    {
        return $this->_getField('address')->getValue($escaped);
    }

    public function getPostalCode($escaped = true)
    {
        return $this->_getField('postal_code')->getValue($escaped);
    }

    public function getZone($escaped = true)
    {
        return $this->_getField('zone')->getValue($escaped);
    }

    public function getCommentsEng($escaped = true)
    {
        return $this->_getField('comments_eng')->getValue($escaped);
    }
    
	public function getCommentsPer($escaped = true)
    {
        return $this->_getField('comments_per')->getValue($escaped);
    }    

    public function getLandArea($escaped = true)
    {
        return $this->_getField('land_area')->getValue($escaped);
    }

    public function getBuildArea($escaped = true)
    {
        return $this->_getField('build_area')->getValue($escaped);
    }

    public function getBedrooms($escaped = true)
    {
        return $this->_getField('bedrooms')->getValue($escaped);
    }

    public function getBathrooms($escaped = true)
    {
        return $this->_getField('bathrooms')->getValue($escaped);
    }

    public function getParking($escaped = true)
    {
        return $this->_getField('parking')->getValue($escaped);
    }

    public function getContactName($escaped = true)
    {
        return $this->_getField('contact_name')->getValue($escaped);
    }

    public function getContactPhone($escaped = true)
    {
        return $this->_getField('contact_phone')->getValue($escaped);
    }

    public function getContactEmail($escaped = true)
    {
        return $this->_getField('contact_email')->getValue($escaped);
    }

    public function getFeatured($escaped = true)
    {
        return $this->_getField('featured')->getValue($escaped);
    }    
    
    public function getTerraceM2($escaped = true)
    {
        return $this->_getField('terrace_m2')->getValue($escaped);
    }

    public function getLandWidth($escaped = true)
    {
        return $this->_getField('land_width')->getValue($escaped);
    }

    public function getLandHeight($escaped = true)
    {
        return $this->_getField('land_height')->getValue($escaped);
    }

    public function getAverageCeilingHeight($escaped = true)
    {
        return $this->_getField('average_ceiling_height')->getValue($escaped);
    }

    public function getLocatedOnLevel($escaped = true)
    {
        return $this->_getField('located_on_level')->getValue($escaped);
    }

    public function getNumberOfLevels($escaped = true)
    {
        return $this->_getField('number_of_levels')->getValue($escaped);
    }

    public function getCeilingHeight($escaped = true)
    {
        return $this->_getField('ceiling_height')->getValue($escaped);
    }

    public function getDeposit($escaped = true)
    {
        return $this->_getField('deposit')->getValue($escaped);
    }    
    
	public function setExpiryDate($value)
    {
        $this->_getField('expiry_date')->setValue($value);
    }

    public function setMemberId($value)
    {
        $this->_getField('member_id')->setValue($value);
    }    
    
    public function setPropertyTransactionId($value)
    {
        $this->_getField('property_transaction_id')->setValue($value);
    }

    public function setPropertyTypeId($value)
    {
        $this->_getField('property_type_id')->setValue($value);
    }

    public function setPropertyPriceTypeId($value)
    {
        $this->_getField('property_price_type_id')->setValue($value);
    }

    public function setPropertyStatusId($value)
    {
        $this->_getField('property_status_id')->setValue($value);
    }

    public function setPrice($value)
    {
        $this->_getField('price')->setValue($value);
    }
    
    public function setSwimmingPool($value)
    {
    	$this->_getField('swimming_pool')->setValue($value);
    }    

    public function setCountryId($value)
    {
        $this->_getField('country_id')->setValue($value);
    }

    public function setCityId($value)
    {
        $this->_getField('city_id')->setValue($value);
    }

    public function setZoneId($value)
    {
        $this->_getField('zone_id')->setValue($value);
    }

    public function setAddress($value)
    {
        $this->_getField('address')->setValue($value);
    }

    public function setPostalCode($value)
    {
        $this->_getField('postal_code')->setValue($value);
    }

    public function setZone($value)
    {
        $this->_getField('zone')->setValue($value);
    }

    public function setCommentsEng($value)
    {
        $this->_getField('comments_eng')->setValue($value);
    }
    
	public function setCommentsPer($value)
    {
        $this->_getField('comments_per')->setValue($value);
    }    

    public function setLandArea($value)
    {
        $this->_getField('land_area')->setValue($value);
    }

    public function setBuildArea($value)
    {
        $this->_getField('build_area')->setValue($value);
    }

    public function setBedrooms($value)
    {
        $this->_getField('bedrooms')->setValue($value);
    }

    public function setBathrooms($value)
    {
        $this->_getField('bathrooms')->setValue($value);
    }

    public function setParking($value)
    {
        $this->_getField('parking')->setValue($value);
    }

    public function setContactName($value)
    {
        $this->_getField('contact_name')->setValue($value);
    }

    public function setContactPhone($value)
    {
        $this->_getField('contact_phone')->setValue($value);
    }

    public function setContactEmail($value)
    {
        $this->_getField('contact_email')->setValue($value);
    }
    
    public function setFeatured($value)
    {
        $this->_setField('featured')->setValue($value);
    }    

    public function setTerraceM2($value)
    {
        $this->_setField('terrace_m2')->setValue($escaped);
    }

    public function setLandWidth($value)
    {
        $this->_setField('land_width')->setValue($escaped);
    }

    public function setLandHeight($value)
    {
        $this->_setField('land_height')->setValue($escaped);
    }

    public function setAverageCeilingHeight($value)
    {
        $this->_setField('average_ceiling_height')->setValue($escaped);
    }

    public function setLocatedOnLevel($value)
    {
        $this->_setField('located_on_level')->setValue($escaped);
    }

    public function setNumberOfLevels($value)
    {
        $this->_setField('number_of_levels')->setValue($escaped);
    }

    public function setCeilingHeight($value)
    {
        $this->_setField('ceiling_height')->setValue($escaped);
    }

    public function setDeposit($value)
    {
        $this->_setField('deposit')->setValue($escaped);
    }    
    
	public function ddMemberId()
    {
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromDbQuery("
            SELECT id, concat(name,', ', email) label
            FROM members
            ORDER BY name,email
        ",$this->getMemberId(false));
    }    
    
    public function ddPropertyTransactionId()
    {
    	$transactions = new Bl_Data_Transactions();
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromArray($transactions->data,$this->getPropertyTransactionId(false));
    }

    public function ddPropertyTypeId()
    {
    	$settings = Bl_Settings::get_instance();
    	if($settings->language == Bl_Settings::LANG_ENGLISH) {
    		$field = "type_eng";
    	} else {
    		$field = "type_per";
    	}
    	
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromDbQuery("
            SELECT id, ".$field."
            FROM property_types
            ORDER BY sort_order, type_eng
        ",$this->getPropertyTypeId(false));
    }

    public function ddPropertyPriceTypeId()
    {
    	$settings = Bl_Settings::get_instance();
    	if($settings->language == Bl_Settings::LANG_ENGLISH) {
    		$field = "type_eng";
    	} else {
    		$field = "type_per";
    	}
    	
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromDbQuery("
            SELECT id, ".$field."
            FROM property_price_types
            ORDER BY sort_order, type_eng
        ",$this->getPropertyPriceTypeId(false));
    }

    public function ddPropertyStatusId()
    {
		$s = new Bl_Data_Statuses();
		$data = $s->getByType($this->_customSettings['property']->getPropertyTransactionId());
		
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromArray($data,$this->getPropertyStatusId(false));
    }

    public function ddCountryId()
    {
        $settings = Bl_Settings::get_instance();
    	if($settings->language == Bl_Settings::LANG_ENGLISH) {
    		$field = "name_eng";
    	} else {
    		$field = "name_per";
    	}    	
    	
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromDbQuery("
            SELECT id, ".$field."
            FROM countries
            ORDER BY sort_order, name_eng
        ",$this->getCountryId(false));
    }

    public function ddCityId()
    {
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromDbQuery("
            SELECT id, city_eng
            FROM cities
            WHERE country_id = '" . intval($this->getCountryId()) . "'
            ORDER BY sort_order, city_eng
        ",$this->getCityId(false));
    }

    public function ddZoneId()
    {
        $dd = new Al_DropDown();
        $dd->setFirstOption(true,'','');
        return $dd->setFromDbQuery("
            SELECT id, zone_eng
            FROM zones
            WHERE city_id = '" . intval($this->getCityId()) . "'
            ORDER BY sort_order, zone_eng
        ",$this->getZoneId(false));
    }
    
    public function itemCityStyle() {
    	if($this->getZoneId(false)=='' || $this->getZoneId(false)=='') {
    		return 'display:none;';
    	}
    	return '';
    }

    public function cbGroupFeature($id) {
    	if($this->_getField('features')->getOptionValue($id) == '1') {
    		return ' checked ';
    	}
    	return ''; 
    }
    
    public function cbGroupOptionsFeature() {
    	return $this->_getField('features')->retrieveFormOptions();
    }
    
    public function cbGroupIntroduction($id) {
    	if($this->_getField('introduction')->getOptionValue($id) == '1') {
    		return ' checked ';
    	}
    	return ''; 
    }
    
    public function cbGroupOptionsIntroduction() {
    	return $this->_getField('introduction')->retrieveFormOptions();
    }   

    public function setRecordValues(&$record) {
    	$valueEng = '';
   		$valuePer = '';
   		
    	if($this->getSwimmingPool() != Bl_Data_PoolTypes::NO) {
	   		$language = Al_Language::get_instance();
	   		$settings = Bl_Settings::get_instance();

	   		$language->setLanguage(Bl_Settings::LANG_ENGLISH);
    		$poolTypes = new Bl_Data_PoolTypes();	   		
    		$valueEng = $poolTypes->get_label($this->getSwimmingPool()). ', ';
    		
    		$language->setLanguage(Bl_Settings::LANG_PERSIAN);
    		$poolTypes = new Bl_Data_PoolTypes();    		
    		$valuePer .= $poolTypes->get_label($this->getSwimmingPool()) . ', ';
    		
    		$language->setLanguage($settings->language);
    	}
		
		$record->setFeaturesEng($valueEng.$this->_getField('features')->getSelectedLabels('label_eng'));
        $record->setFeaturesPer($valuePer.$this->_getField('features')->getSelectedLabels('label_per'));
		$record->setIntroductionEng($this->_getField('introduction')->getSelectedLabels('label_eng'));
        $record->setIntroductionPer($this->_getField('introduction')->getSelectedLabels('label_per'));         
    }
    
    public function cbFeatured()
    {
        return $this->_cbValue($this->getFeatured());
    }    
    
    public function rbSwimmingPool($option) 
    {
    	return $this->_rbValue($this->getSwimmingPool(),$option);
    }

}
