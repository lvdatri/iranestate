<?php
class Bl_Form_PropertyContact extends Al_Form
{
    public function initialiseForm()
    {
    	$language = Al_Language::get_instance();
    	
        $field = new Al_Form_Field($this);
        $field->setName('contact_name');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONT_YOUR_NAME));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('contact_email');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONT_YOUR_EMAIL));
        $field->addRule('required');
        $field->addRule('email');
        $field->addRule('length',array(
            'max' => '255',
        ));
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('contact_phone');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONT_YOUR_PHONE));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('contact_subject');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONT_SUBJECT));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '165',
        ));
        $this->_addField($field);
        
		$field = new Al_Form_Field($this);
        $field->setName('contact_description');
        $field->setLabel('Description');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONT_DESCRIPTION));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '300',
        ));
        $this->_addField($field);        
        
		$field = new Al_Form_Field($this);
        $field->setName('contact_captcha');
        $field->setLabel($language->getValue(Al_Language::FLD_PROP_CONT_QUESTION));
        $field->addRule('required');
        $field->addRule('mathCaptcha',array(
        	'type'=>'properties-contact',
        ));

        $this->_addField($field);        
        
    }

    public function getContactName($escaped = true)
    {
        return $this->_getField('contact_name')->getValue($escaped);
    }
    
	public function getContactEmail($escaped = true)
    {
        return $this->_getField('contact_email')->getValue($escaped);
    }

	public function getContactPhone($escaped = true)
    {
        return $this->_getField('contact_phone')->getValue($escaped);
    }

	public function getContactSubject($escaped = true)
    {
        return $this->_getField('contact_subject')->getValue($escaped);
    }

    public function getContactDescription($escaped = true)
    {
        return $this->_getField('contact_description')->getValue($escaped);
    }

    public function getCaptcha($escaped = true)
    {
        return $this->_getField('captcha')->getValue($escaped);
    }


}