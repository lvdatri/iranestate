<?php
class Bl_Form_MemberPassword extends Al_Form
{

	public function afterDbData() {
		if($this->getMode()==Al_Form::UPDATE) {
			$this->_getField('password_confirm')->setValue($this->_getField('password')->getValue());
		}
	}	
	
	public function beforeValidation() {
		$this->_getField('current_password')->getRule('DbMatch')->setRecordId($this->getId());
	}		
	
    public function initialiseForm()
    {
		$language = Al_Language::get_instance();
    	
        $field = new Al_Form_Field($this);
        $field->setName('current_password');
        $field->setLabel($language->getValue(Al_Language::FLD_ACCOUNT_CURRENT_PASSWORD));
        $field->addRule('required');
        $field->addRule('length',array(
            'max' => '20',
        ));
		$field->addRule('DbMatch',array(
            'table' => 'members',
			'dbField' => 'password',
			'encrypted' => true,
			'message' => $language->getValue(Al_Language::MSG_CURRENT_PASSWORD_NO_MATCH),
        ));        
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password');
        $field->setLabel($language->getValue(Al_Language::FLD_ACCOUNT_NEW_PASSWORD));
        $field->addRule('required');
        $field->addRule('length',array(
            'min' => '8',
            'max' => '20',
        ));
        $this->_addField($field);
        
        $field = new Al_Form_Field($this);
        $field->setName('password_confirm');
        $field->setLabel($language->getValue(Al_Language::FLD_ACCOUNT_NEW_PASSWORD_CONFIRM));
        $field->addRule('required');
        $field->addRule('equalTo',array(
            'toField' => 'password',
        ));
        $this->_addField($field);
    }

    public function getCurrentPassword($escaped = true)
    {
        return $this->_getField('current_password')->getValue($escaped);
    }

    public function getPassword($escaped = true)
    {
        return $this->_getField('password')->getValue($escaped);
    }

    public function setCurrentPassword($value)
    {
        $this->_getField('current_password')->setValue($value);
    }

    public function setPassword($value)
    {
        $this->_setField('password')->setValue($escaped);
    }

}
