<?php
	class Bl_Message {
		
		private $_settings;	
		private $messages = array();
		private $type;		

		const ERROR = 'error';
		const SUCCESS = 'success';
		
		public function __construct() {
			$this->_settings = Bl_Settings::get_instance();
			$this->load();
			$this->type = self::SUCCESS;
		}
		
		public function addRecordMessages($messages) {
			
			if($this->_settings->language == Bl_Settings::LANG_ENGLISH) {
				foreach($messages as $message) {
					$this->add('<B>'.$message['label'].'</B>: '.$message['message'],self::ERROR);					
				}				
			}  else {
				foreach($messages as $message) {
					$this->add($message['message'].' :'.$message['label'],self::ERROR);
				}
			}
		}
		
		public function add($message,$type='') {
			if($type == '') {
				$type = $this->type;
			}
			if(array_key_exists($type,$this->messages)==false) {
				$this->messages[$type] = array();
			}
	 		$this->messages[$type][] = $message;
	 			
	 		return $this;	
		}
		
		public function show() {
			if(count($this->messages)>0) {
				
				$msg = '<script type="text/javascript"> $(document).ready(function() { $(".notify_box span").click(function() {	$(this).parent().fadeOut("slow"); }); }); </script>';
				$msg .= '<div class="errorsCol">';
				foreach ($this->messages as $type=>$messages) {
					if($type==self::SUCCESS) {
						$class = 'notify_confirmation';
					} else {
						$class = 'notify_error';
					}
					$msg .= '<div class="notify_box '.$class.'"><span></span>';														
					foreach ($messages as $message) {
						$msg .= '<div>'.$message.'</div>';
					}
					$msg .= '</div>';
				}
				$msg .= '</div>';
				$this->clear();				
				print $msg;
			}
		}
		
		public function save() {
			$_SESSION[$this->_settings->session][$this->_settings->session_messages] = $this->messages;
		}
		
		public function clear() {
			$this->messages = array();
			$this->save();
		}
		
		private function load() {
			if(isset($_SESSION[$this->_settings->session][$this->_settings->session_messages])) {
				$this->messages = $_SESSION[$this->_settings->session][$this->_settings->session_messages];
			}
			else {
				$this->messages = array();
			}
		}
	}