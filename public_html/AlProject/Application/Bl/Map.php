<?php
class Bl_Map {
	public function lonLatToTile($zoom,$lat,$lon)
	{
		$res = array();
		$xtile = floor((($lon + 180) / 360) * pow(2, $zoom));
		$ytile = floor((1 - log(tan(deg2rad($lat)) + 1 / cos(deg2rad($lat))) / pi()) /2 * pow(2, $zoom));
		
		return array(
			'x' => $xtile,
			'y' => $ytile,
			'z' => $zoom,
			'cord' => $zoom.':'.$xtile.':'.$ytile,
		);
	}

	public function tileToLatLon($zoom,$xtile,$ytile) {
		$ll = array();
		$n = pow(2, $zoom);
		$lon_deg = $xtile / $n * 360.0 - 180.0;
		$lat_deg = rad2deg(atan(sinh(pi() * (1 - 2 * $ytile / $n))));
			
		$ll['lat'] = $lat_deg;
		$ll['lon'] = $lon_deg;
		
		return $ll;
	}

	/**
	 * @param string $cord zoom:x:y
	 */
	public function tileCordToLatLon($cord,$center=false)
	{
// 		echo $cord;
		$cord = explode(":", $cord);
		
		
		if($center==true) {
			$shift = 0.5;
		} else {
			$shift = 0;
		}
		
		return $this->tileToLatLon($cord[0], $cord[1]+$shift, $cord[2]+$shift);
	}
	
	
	/**
	 http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#tile_numbers_to_lon.2Flat_2
	 - shows conversions of latLon to tiles and vice verser, as well as finding a mid point by adding .5
	 */
	
	public function tileToLatLonBoundries($zoom,$xtile,$ytile)
	{
		$res = array(
			"br" => $this->tileToLatLon($zoom,$xtile,$ytile+1),
			'tl' => $this->tileToLatLon($zoom,$xtile+1,$ytile),
			"mp" => $this->tileToLatLon($zoom,$xtile+0.5,$ytile+0.5),
		);

		return $res;
	}

	/**
	 * 
	 * @param int $zoom
	 * @param $neLat north east (top right)
	 * @param $neLon 
	 * @param $swLat south west (bottom left)
	 * @param $swLon
	 * @return string comma delimted tile range
	 */
	public function tileRange($zoom, $neLat,$neLon,$swLat,$swLon)
	{
		$ne = $this->lonLatToTile($zoom, $neLat, $neLon);
		$sw = $this->lonLatToTile($zoom, $swLat, $swLon);
			
			
		// 			ne - 10:659:401
		// 			sw - 10:657:404
		$tiles = '';
		for($j=$sw['x'];$j<=$ne['x'];$j++) {
			for($i=$ne['y'];$i<=$sw['y'];$i++) {
				$tiles .= '"'.$zoom.':'.$j.':'.$i.'",';
// 				echo $j.':'.$i.'<BR>';
			}
// 			echo '<BR>';
		}		
		
		return rtrim($tiles,',');
	}
	

	
	
	
}