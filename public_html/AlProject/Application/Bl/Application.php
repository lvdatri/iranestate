<?php
	error_reporting(E_ALL);
	class Bl_Application {
		public $session_start = true;
		public $set_template = true;
		
		private $settings;
		private $template;
		
		public function initialise($environment=null) {
			$file_path = dirname(__FILE__).'/../../';
			
			set_include_path (
				$file_path .'Library/' . PATH_SEPARATOR .
				$file_path.'Application/' .PATH_SEPARATOR .
				get_include_path()
			);
			
			self::disableMagicQuotes();
			
			require_once 'Zend/Loader/Autoloader.php';
			$autoloader = Zend_Loader_Autoloader::getInstance();
			$autoloader->registerNamespace('Al_');
			$autoloader->registerNamespace('Bl_');
		
			/*@var $settings Bl_Settings */
			$this->settings = Bl_Settings::get_instance();
			
			
			
			$locale = new Zend_Locale('en_GB');
			Zend_Registry::set('Zend_Locale', $locale);			
			
			if($environment==null) {
				$environment = getenv("APPLICATION_ENV");	
			}
			
			
			if($environment == 'development') {
				$this->settings->initialise(Bl_Settings::ENV_DEVELOPMENT);
			} else {
				$this->settings->initialise(Bl_Settings::ENV_PRODUCTION);
			}
			
			/* set session */
			if($this->session_start) {
				$session = new Al_Session();
				session_start();	
			}
			
			if($this->set_template) {
				$this->template = Al_Template::get_instance();
			}			
		}
		
		public function set_admin_settings() {
			$this->settings->initialise_admin(Bl_Settings::LANG_ENGLISH);
			$language = Al_Language::get_instance();
			$language->setLanguage(Bl_Settings::LANG_ENGLISH);			
			if($this->set_template) {
			    $this->template->js_include('admin.js');
				$this->template->css_core_include('style.css');
				$this->template->extjs_set_core();
				$this->template->extjs_attach_on_ready("Ext.al.menus();"); 
				
			}			
		}
		
		public function set_frontend_settings() 
		{
			
			$this->settings->set_template($this->settings->language);

			
//			$this->settings->set_template($this->getLanguage());
			$language = Al_Language::get_instance();
			$language->setLanguage($this->settings->language);
			
			
			
			$this->settings->user_login_path = $this->settings->path_web_ssl.$this->settings->languagePrefix.'/login';
			$this->settings->user_denied_path = $this->settings->path_web_ssl.$this->settings->languagePrefix.'/denied';			
			
			$this->settings->tag_title = Al_Utilities::html($language->getValue(Al_Language::PAGE_TITLE_DEFAULT));
			$this->tag_keywords = Al_Utilities::html($language->getValue(Al_Language::PAGE_KEYWORDS_DEFAULT));
			$this->tag_description = Al_Utilities::html($language->getValue(Al_Language::PAGE_DESCRIPTION_DEFAULT));	 			
			
			if($this->set_template) {
		    	$this->template->jquery_set_core();
		    	$this->template->js_core_include('https://maps.google.com/maps/api/js?sensor=false',true);
		    	
		    	if($this->settings->jsCssMerged) { 
		    		$this->template->js_include('all-min.js');	
		    		$this->template->css_core_include('all-min.css');
		    	} else {
					$this->template->js_include('front.js');
					$this->template->js_include('gmap.js');		

//				$this->template->jquery_set_gmaps();				
				    $this->template->js_include('jquery/java.js');
				    $this->template->js_include('jquery/simplemodal.js');
				    $this->template->js_include('jquery/basic.js');
				    $this->template->js_include('jquery/tablesorter.js');
				    $this->template->js_include('jquery/jquery.bxslider.min.js');
				    
				    
				    
				    $this->template->js_include('jquery/jquery.corner.js');
				    $this->template->js_include('jquery/jquery.jqtransform.js');
				    $this->template->js_include('jquery/jquery.selectBox.min.js');					
				    
					$this->template->css_core_include('style.css');
					$this->template->css_include('jqtransform.css');
		    	}
			}	
		}
		
		
//		public function getLanguage() 
//		{
//			$lang = '';
//			if(isset($_SESSION[$this->settings->session][$this->settings->session_language])) {
//				$lang = $_SESSION[$this->settings->session][$this->settings->session_language];
//			} else {
//				$lang = $this->getLanguageFromIp();
//				$_SESSION[$this->settings->session][$this->settings->session_language] = $lang;
//			}
//			if($lang != Bl_Settings::LANG_PERSIAN) {
//				$lang = Bl_Settings::LANG_ENGLISH;
//			}
//			
//			return $lang;
//		}
//		
//		private function getLanguageFromIp() {
//			$db = Al_Db::get_instance();
//			$user_ip = $_SERVER['REMOTE_ADDR'];
//			$res = $db->query("
//				SELECT b.id
//				FROM ip_blocks b
//				WHERE INET_ATON('".Al_Db::escape($user_ip)."') BETWEEN b.ip_start AND b.ip_end
//				LIMIT 1
//			");
//			
//			if($res->num_rows()>0) {
//				return Bl_Settings::LANG_PERSIAN;
//			} else {
//				return Bl_Settings::LANG_ENGLISH;
//			}
//		}
		
		public function controllerBeforePathProcess(&$path)
		{ 
//			$lang = Bl_Settings::LANG_ENGLISH;
//			$langPre = Bl_Settings::LANG_ENGLISH_PREFIX;
			
			$lang = Bl_Settings::LANG_PERSIAN;
			$langPre = Bl_Settings::LANG_PERSIAN_PREFIX;			
			
			if(is_array($path) && count($path)>0) {
				switch(strtolower($path['0'])) {
					case  Bl_Settings::LANG_ENGLISH_PREFIX :
						$lang = Bl_Settings::LANG_ENGLISH;
						$langPre = Bl_Settings::LANG_ENGLISH_PREFIX;
						array_shift($path);
						break;
					case Bl_Settings::LANG_PERSIAN_PREFIX :
						$lang = Bl_Settings::LANG_PERSIAN;
						$langPre = Bl_Settings::LANG_PERSIAN_PREFIX;
						array_shift($path); 
						break;					
				}
			}
//			Al_Utilities::a($path);
			
			$this->settings->language = $lang;
			$this->settings->languagePrefix = $langPre;
		}
		
		public function dispatch() {
			$front_controller = new Al_ControllerFront;
			$front_controller->setBeforePathProcess($this,'controllerBeforePathProcess');
			$front_controller->add_module('admin',$this->settings->path_controllers.'Admin/');	
			$front_controller->initialise();
			$module = $front_controller->get_module_name();
			if($module == 'admin') {
				$this->set_admin_settings();
			} else {
				$this->set_frontend_settings();
				$front_controller->beforeActionCall($this,'beforeActionCall');
			}
			$front_controller->dispatch();			
		}
		
		public function beforeActionCall(&$controller) {
			$cms = new Bl_Record_Cms();
			
//			$controller->_articles = array();
//			$controller->_services = array();
//			$controller->_pages = array();			
			
//			$cms->retrieveByType(Bl_Data_CmsTypes::article);
			
			$controller->_articles = $cms->retrieveByType(Bl_Data_CmsTypes::article);
			$controller->_services = $cms->retrieveByType(Bl_Data_CmsTypes::services,5,false);
			$controller->_pages = $cms->retrieveByType(Bl_Data_CmsTypes::page,20,false);
		}
		
		public function set_session_start($value) {
			if($value==true) {
				$this->session_start = true;
			} else {
				$this->session_start = false;
			}
		}

		public function set_template($value) {
			if($value==true) {
				$this->set_template = true;
			} else {
				$this->set_template = false;
			}
		}

	    protected static function disableMagicQuotes()
	    {
	        if (get_magic_quotes_gpc()) {
	            $_GET = self::stripSlashes($_GET);
	            $_POST = self::stripSlashes($_POST);
	            $_REQUEST = self::stripSlashes($_REQUEST);
	            $_COOKIE = self::stripSlashes($_COOKIE);
	        }
	    }
	
	    protected static function stripSlashes($value)
	    {
	        if (is_array($value)) {
	            $value = array_map(array('Bl_Application', 'stripSlashes'), $value);
	        } else {
	            $value = stripslashes($value);
	        }
	        return $value;
	    }		
	}