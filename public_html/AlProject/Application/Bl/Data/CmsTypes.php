<?php
	class Bl_Data_CmsTypes {
        const news = '1';
        const article = '2'; 
        const services = '3';
        const page = '4';
        
        const path_news = 'news/';
        const path_articles = 'articles/';
        const path_services = 'services/';
        
        public $data;

        public function __construct() {
            self::
            $this->data = array(
				self::news => 'news',
				self::article => 'article',
				self::services => 'service',
				self::page => 'page',
            );
        }
        
        public function get_label($id) {
            if(isset($this->data[$id])) {
                return $this->data[$id];
            } else {
                return '';
            }
        }
        
        
        public function getPathFromType($type) 
        {
	    	$path = '';
	    	switch($type) {
	    		case self::news:
	    			$path = self::path_news;
	    			break;
	    		case self::article:
	    			$path = self::path_articles;
	    			break;	    			
	    		case self::services:
	    			$path = self::path_services;
	    			break;	    			
	    	}
	    	
	    	return $path;
        }

	
	}