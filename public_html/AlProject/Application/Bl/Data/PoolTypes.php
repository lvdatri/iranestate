<?php
	class Bl_Data_PoolTypes {
        const NO = '0';
        const PRIVATE_POOL = '1';
        const PUBLIC_POOL = '2';
        
        public $data;

        public function __construct() 
        {
			$language = Al_Language::get_instance();
        	
            $this->data = array(
				self::NO => $language->getValue(Al_Language::DATA_POOL_NO),
				self::PRIVATE_POOL => $language->getValue(Al_Language::DATA_POOL_PRIVATE_POOL),
				self::PUBLIC_POOL => $language->getValue(Al_Language::DATA_POOL_PUBLIC_POOL),
            );
        }
        
        public function get_label($id) 
        {
            if(isset($this->data[$id])) {
                return $this->data[$id];
            }
			return '';
        }
    }