<?php
	class Bl_Data_Prices {
		const BUY = '1';
		const RENT = '2';
		const BOOK = '3';
		
		private $settings;
		private $activeType='';
		private $language;
		
		private $prices = array(
			'10000000' => '10M',
			'20000000' => '20M',
			'40000000' => '40M',
			'60000000' => '60M',
			'80000000' => '80M',
			'100000000' => '100M',
			'200000000' => '200M',
			'300000000' => '300M',
			'400000000' => '400M',
			'500000000' => '500M',
			'600000000' => '600M',
			'700000000' => '700M',
			'800000000' => '800M',
			'900000000' => '900M',
			'1000000000' => '1.000M',
			'1500000000' => '1.500M',
			'2000000000' => '2.000M',
			'2500000000' => '2.500M',
			'3000000000' => '3.000M',
			'3500000000' => '3.500M',
			'4000000000' => '4.000M',
			'4500000000' => '4.500M',
			'5000000000' => '5.000M',
			'5500000000' => '5.500M',
			'6000000000' => '6.000M',
			'6500000000' => '6.500M',
			'7000000000' => '7.000M',
			'8000000000' => '8.000M',
			'9000000000' => '9.500M',
			'10000000000' => '+10.000M',		
		);
		
		private $rentPrices = array(
			'50000' => '50k',
			'100000' => '100k',
			'250000' => '250k',
			'500000' => '500k',
			'800000' => '800k',
			'1000000' => '1M',
			'2000000' => '2M',
			'5000000' => '5M',
			'8000000' => '8M',
			'10000000' => '10M',
			'15000000' => '15M',
		);
		
		private $depositPrices = array(
			'5000000' => '5M',
			'10000000' => '10M',
			'20000000' => '20M',
			'30000000' => '30M',
			'40000000' => '40M',
			'50000000' => '50M',
			'60000000' => '60M',
			'70000000' => '70M',
			'80000000' => '80M',
			'100000000' => '+100M',
		); 
		
		
		public function setActiveType($type) 
		{
			$this->activeType = $type;	
		}
		
		public function __construct() 
		{
			$this->settings = Bl_Settings::get_instance();
			$this->language = Al_Language::get_instance();
		}
		
		public function getBuyFromPrices() 
		{
			if($this->settings->language == Bl_Settings::LANG_ENGLISH) {
				return $this->prices;		
			} else {
				return $this->prices;
			}
		}
		
		public function getBuyToPrices() 
		{
			if($this->settings->language == Bl_Settings::LANG_ENGLISH) {
				return $this->prices;		
			} else {
				return $this->prices;
			}			
		}

		
		
		
		public function ddFromPrices($id) 
		{
			if($this->activeType==self::BOOK) {
				return $this->ddBookFromPrices($id);
			} else if($this->activeType==self::RENT) {
				return $this->ddRentFromPrices($id);
			} else {
				return $this->ddBuyFromPrices($id);
			}
		}
		
		public function ddToPrices($id) 
		{
			if($this->activeType==self::BOOK) {
				return $this->ddBookToPrices($id);
			} else if($this->activeType==self::RENT) {
				return $this->ddRentToPrices($id);
			} else {
				return $this->ddBuyToPrices($id);
			}			
		}
		
		public function ddBuyFromPrices($id) 
		{
	        $dd = new Al_DropDown();
	        
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getBuyFromPrices(),$id);
		}		
		
		public function ddBuyToPrices($id) 
		{
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getBuyToPrices(),$id);
		}

		
		public function getRentFromPrices() 
		{
			if($this->settings->language == Bl_Settings::LANG_ENGLISH) {
				return $this->rentPrices;		
			} else {
				return $this->rentPrices;
			}
		}
		
		public function getRentToPrices() 
		{
			if($this->settings->language == Bl_Settings::LANG_ENGLISH) {
				return $this->rentPrices;		
			} else {
				return $this->rentPrices;
			}			
		}
		
		public function ddRentFromPrices($id) 
		{
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getRentFromPrices(),$id);
		}		
		
		public function ddRentToPrices($id) 
		{
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getRentToPrices(),$id);
		}
		
			public function getBookFromPrices() 
		{
			if($this->settings->language == Bl_Settings::LANG_ENGLISH) {
				return $this->rentPrices;		
			} else {
				return $this->rentPrices;
			}
		}
		
		public function getBookToPrices() 
		{
			if($this->settings->language == Bl_Settings::LANG_ENGLISH) {
				return $this->rentPrices;		
			} else {
				return $this->rentPrices;
			}			
		}
		
		public function ddBookFromPrices($id) 
		{
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getBookFromPrices(),$id);
		}		
		
		public function ddBookToPrices($id) 
		{
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getBookToPrices(),$id);
		}		
		
		public function getDepositFromPrices() {
			return $this->depositPrices;
		}
		
		public function getDepositToPrices() {
			return $this->depositPrices;
		}		
		
		public function ddDepositFromPrices($id) 
		{
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getDepositFromPrices(),$id);
		}		
		
		public function ddDepositToPrices($id) 
		{
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->getDepositToPrices(),$id);
		}			
		
	}