<?php
	class Bl_Data_Transactions {
        const FOR_SALE = '1';
        const FOR_RENT = '2';
        const FOR_BOOKING = '3'; 
        
		public $data;

        public function __construct() 
        {
        	$language = Al_Language::get_instance();
        	
            $this->data = array(
				self::FOR_SALE => $language->getValue(Al_Language::DATA_TRANSACTION_FOR_SALE),
				self::FOR_RENT => $language->getValue(Al_Language::DATA_TRANSACTION_FOR_RENT),
				self::FOR_BOOKING => $language->getValue(Al_Language::DATA_TRANSACTION_FOR_BOOKING),
            );
        }
        
		public function get_label($id) 
		{
            if(isset($this->data[$id])) {
                return $this->data[$id];
            }
			return '';
        }

        public function ddData($id)
        {
            $dd = new Al_DropDown();
            $dd->setFirstOption(false);
            return $dd->setFromArray($this->data,$id);
        }

        public function getValidType($value,$defaultValue)
        {
            if(array_key_exists($value,$this->data)) {
                return $value;
            } else {
                return $defaultValue;
            }
        }

    }