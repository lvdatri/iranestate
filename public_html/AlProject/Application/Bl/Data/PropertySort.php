<?php
	class Bl_Data_PropertySort {
	    const latest = '1';
        const price_lth = '2';
        const price_htl = '3'; 
        const property_size = '4';
        
        public $data;
        
        public function __construct() 
        {
        	
			$language = Al_Language::get_instance();
			
            $this->data = array(
				self::latest => $language->getValue(Al_Language::DATA_SORT_LATEST),
				self::price_lth => $language->getValue(Al_Language::DATA_SORT_LOW_TO_HIGH),
				self::price_htl => $language->getValue(Al_Language::DATA_SORT_HIGH_TO_LOW),
				self::property_size => $language->getValue(Al_Language::DATA_SORT_PROPERTY_SIZE),
            );
        }
        
	    public function get_label($id) {
            if(isset($this->data[$id])) {
                return $this->data[$id];
            }
			return '';
        }
        
        public function ddData($id) 
        {
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(false);
        	return $dd->setFromArray($this->data,$id);
        }
    }