<?php
	class Bl_Data_EmailTypes {
        const registration_confirmation = '1';
        const property_contact_agent = '2';
        const property_send_to_friend = '3';
        const forgotten_password = '4'; 
        
        public $data;

        public function __construct() {
            self::
            $this->data = array(
				self::registration_confirmation => 'Registration Confirmation',
				self::property_contact_agent => 'Property Contact Agent',
				self::property_send_to_friend => 'Property Send to friend',
				self::forgotten_password => 'Forgotten Password',
            );
        }
        
        public function get_label($id) {
            if(isset($this->data[$id])) {
                return $this->data[$id];
            } else {
                return '';
            }
        }
    }