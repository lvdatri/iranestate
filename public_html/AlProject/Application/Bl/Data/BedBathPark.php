<?php
	class Bl_Data_BedBathPark {
		public $data;

        public function __construct() 
        {
        	$this->language = Al_Language::get_instance();
        	
            $this->data = array(
				'1' => '1+',
				'2' => '2+',
				'3' => '3+',
				'4' => '4+',
				'5' => '5+',				
            );
        }
        
		public function get_label($id) 
		{
            if(isset($this->data[$id])) {
                return $this->data[$id];
            }
			return '';
        }
        
        public function ddData($id) {
	        $dd = new Al_DropDown();
	        $dd->setFirstOption(true,$this->language->getValue(Al_Language::SEARCH_ANY),'');
	        return $dd->setFromArray($this->data,$id);
        }        
    }