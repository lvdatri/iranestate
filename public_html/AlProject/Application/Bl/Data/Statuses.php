<?php
	class Bl_Data_Statuses {
        const PENDING_APPROVAL = '1';
        const ACTIVE = '2';
        const EXPIRED = '3'; 
        
        const BUY_UNDER_CONTRACT = '4';
        const BUY_SOLD = '5';
        const RENT_UNDER_CONTRACT = '4';
        const RENT_RENTED = '5';
        const BOOK_UNDER_CONTRACT = '4';
        const BOOK_BOOKED = '5';
        
        public $data;
        public $data_buy;
        public $data_rent;
        public $data_book;

        public function __construct() {
        	$language = Al_Language::get_instance();
        	
        	$this->data = array(
        		self::PENDING_APPROVAL => $language->getValue(Al_Language::DATA_STATUS_PENDING_APPROVAL),
        		self::EXPIRED => $language->getValue(Al_Language::DATA_STATUS_EXPIRED),
        	);
        	
            $this->data_rent = array(
				self::ACTIVE => $language->getValue(Al_Language::DATA_STATUS_ACTIVE),
// 				self::RENT_UNDER_CONTRACT => $language->getValue(Al_Language::DATA_STATUS_RENT_UNDER_CONTRACT),
				self::RENT_RENTED => $language->getValue(Al_Language::DATA_STATUS_RENT_RENTED),
            	self::EXPIRED => $language->getValue(Al_Language::DATA_STATUS_EXPIRED),            		
            );
            
            $this->data_buy = array(
				self::ACTIVE => $language->getValue(Al_Language::DATA_STATUS_ACTIVE),
// 				self::BUY_UNDER_CONTRACT => $language->getValue(Al_Language::DATA_STATUS_BUY_UNDER_CONTRACT),
				self::BUY_SOLD => $language->getValue(Al_Language::DATA_STATUS_BUY_SOLD),
            	self::EXPIRED => $language->getValue(Al_Language::DATA_STATUS_EXPIRED),
            );
            
			$this->data_book = array(
				self::ACTIVE => $language->getValue(Al_Language::DATA_STATUS_ACTIVE),
// 				self::BOOK_UNDER_CONTRACT => $language->getValue(Al_Language::DATA_STATUS_BOOK_UNDER_CONTRACT),
// 				self::BOOK_BOOKED => $language->getValue(Al_Language::DATA_STATUS_BOOK_BOOKED),
				self::EXPIRED => $language->getValue(Al_Language::DATA_STATUS_EXPIRED),					
            );      

        	$this->data_all = array(
        		self::PENDING_APPROVAL => $language->getValue(Al_Language::DATA_STATUS_PENDING_APPROVAL),
        		self::EXPIRED => $language->getValue(Al_Language::DATA_STATUS_EXPIRED),
				self::ACTIVE => $language->getValue(Al_Language::DATA_STATUS_ACTIVE),
				self::RENT_UNDER_CONTRACT => $language->getValue(Al_Language::DATA_STATUS_RENT_UNDER_CONTRACT),
				self::RENT_RENTED => $language->getValue(Al_Language::DATA_STATUS_RENT_RENTED),
				self::BUY_UNDER_CONTRACT => $language->getValue(Al_Language::DATA_STATUS_BUY_UNDER_CONTRACT),
				self::BUY_SOLD => $language->getValue(Al_Language::DATA_STATUS_BUY_SOLD),
				self::BOOK_UNDER_CONTRACT => $language->getValue(Al_Language::DATA_STATUS_BOOK_UNDER_CONTRACT),
				self::BOOK_BOOKED => $language->getValue(Al_Language::DATA_STATUS_BOOK_BOOKED),
            );              
            
        }
        
        public function getBuy() 
        {
        	return $this->data_buy;
        }
        
        public function getRent()
        {
        	return $this->data_rent;
        }
        
        public function getBook()
        {
        	return $this->data_book;
        }
        
        public function getAll()
        {
        	return $this->data_all;
        }
        
        public function getByType($type) 
        {
        	$data = array();
        	switch($type) {
        		case Bl_Data_Transactions::FOR_SALE:
        			$data = $this->getBuy();
        			break;
        		case Bl_Data_Transactions::FOR_RENT:
        			$data = $this->getRent();
        			break;
        		case Bl_Data_Transactions::FOR_BOOKING:
        			$data = $this->getBook();
        			break;
        	}   
        	return $data;     	
        }
        
	    public function get_label($id) 
        {
            if(isset($this->data_all[$id])) {
                return $this->data_all[$id];
            }
			return '';
        }
        
        public function getLabelByType($type,$id)
        {
        	$data = $this->getByType($type);
        	$value = '';
        	if(isset($data[$id])) {
        		$value = $data[$id];
        	}
        	return $value;
        }
        
    }