<?php
	class Bl_Menu {
		const BUY = '1';
		const RENT = '2';
		const BOOK = '3';
		const POST = '4';
		
		private $activeMenu = '';
		
		private $activeClassName = 'selected-menu';
		private $inactiveClassName = '';

		public function setActive($item) {
			$this->activeMenu = $item;
		}
		
		public function getBuyClass() {
			return $this->getClassName(self::BUY);
		}
		
		public function getRentClass() {
			return $this->getClassName(self::RENT);
		}
		
		public function getBookClass() {
			return $this->getClassName(self::BOOK);
		}
		
		public function getPostClass() {
			return $this->getClassName(self::POST);
		}
		
		private function getClassName($item) {
			if($this->activeMenu == $item) {
				return $this->activeClassName;
			} else {
				return $this->inactiveClassName;
			}
		}
	}