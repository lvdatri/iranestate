<?php 
    class Bl_Admin_Menu {
        private $buttons;
        private $js;
        private $js_buttons;
        private $lb = " ";//"\n";
        private $tab = " ";//\t";
        
        public function __construct() {
            $this->buttons = array();
            $this->js = '';
            $this->js_buttons = '';
        }
        
        public function display() {
            $this->setup_menus();
            if(count($this->buttons)>0) {
                foreach($this->buttons as $button) {
                    $this->set_button($button,$this->set_menu($button));
                }                
                
                $this->js .= '<script type="text/javascript">'.$this->lb;
				$this->js .= "Ext.al.menus = function(){".$this->lb;
				$this->js .= $this->tab."var tb = new Ext.Toolbar();".$this->lb;
				$this->js .= $this->tab."tb.render('tmpToolBar');".$this->lb;
				$this->js .= $this->tab."tb.add(".$this->lb;                
                
				$this->js .= rtrim($this->js_buttons,",".$this->lb).$this->lb;
                
				$this->js .= ");".$this->lb;
				$this->js .= "tb.doLayout();".$this->lb;
				$this->js .= "};";
                $this->js .= "</script>";
                echo $this->js;
//                exit;
            }
        }
        
        public function set_menu($button) {
            $menu = '';
            if($button['menu']=='') {
                return $menu; 
            }
            
            $menu_items = '';
            foreach($button['menu'] as $item) {
                $menu_items .= "{";
                if($item['url'] != '') {
                    $menu_items .= $this->tab."href:'".$item['url']."',".$this->lb;
                }
                $menu_items .= $this->tab."text: '".$item['text']."'".$this->lb;
                $menu_items .= "},".$this->lb;
            }       
            $menu_items = rtrim($menu_items,",".$this->lb);
            
            $menu .= "{".$this->lb;
            $menu .= $this->tab."xtype: 'menu',".$this->lb;
            $menu .= $this->tab."plain: true,".$this->lb;
            $menu .= $this->tab."items: [ ".$this->lb;
            $menu .= $menu_items.$this->lb;
            $menu .= $this->tab."]".$this->lb;
            $menu .= "},".$this->lb;
            
            return $menu;
        }
        
        public function set_button($button,$menu='') {
            $this->js_buttons .= "{";
            if($button['width']!='') {
                $this->js_buttons.= $this->tab."width:".$button['width'].",".$this->lb; 
            }
            if($menu!='') {
                $this->js_buttons.= $this->tab."menu:".$menu.$this->lb;
            }    
            
            if($button['url']!='') {
            	$this->js_buttons.= $this->tab."handler: function() {".$this->lb;
				$this->js_buttons.= $this->tab.$this->tab."window.location='".$button['url']."'".$this->lb;
				$this->js_buttons.= $this->tab."},".$this->lb;          
            }
            
            $this->js_buttons .= 	$this->tab."text:'".$button['text']."'".$this->lb;
            $this->js_buttons .= "},".$this->lb;
            $this->js_buttons .= "'-',".$this->lb;
        }
        
        private function setup_menus() {
        	$settings = Bl_Settings::get_instance();
        	
            $this->add_button('Home',$settings->path_admin_web_ssl,'70');
            
            $this->add_button('Properties',$settings->path_admin_web_ssl.'properties','70');
            $this->add_button('Members',$settings->path_admin_web_ssl.'members/browse','70');            
            
            
            $menu = array();
            $menu[] = $this->get_menu_item('News',$settings->path_admin_web_ssl.'news/browse');
			$menu[] = $this->get_menu_item('Articles',$settings->path_admin_web_ssl.'articles/browse');            
            $menu[] = $this->get_menu_item('Services',$settings->path_admin_web_ssl.'services/browse');
            $menu[] = $this->get_menu_item('Pages',$settings->path_admin_web_ssl.'pages/browse');            
            $this->add_button('CMS','','70',$menu);
            
//            $this->add_button('Pages',$settings->path_admin_web_ssl.'pages/browse','70');
//            $this->add_button('Articles',$settings->path_admin_web_ssl.'articles/browse','70');
//            $this->add_button('News',$settings->path_admin_web_ssl.'news/browse','70');
            
            $menu = array();
            $menu[] = $this->get_menu_item('Property Types',$settings->path_admin_web_ssl.'property-types/browse');
            $menu[] = $this->get_menu_item('Property Introduction',$settings->path_admin_web_ssl.'property-introductions/browse');
            $menu[] = $this->get_menu_item('Property Features',$settings->path_admin_web_ssl.'property-features/browse');
//            $menu[] = $this->get_menu_item('Property Transactions',$settings->path_admin_web_ssl.'property-transactions/browse');
			$menu[] = $this->get_menu_item('Property Price Types',$settings->path_admin_web_ssl.'property-price-types/browse');            
//            $menu[] = $this->get_menu_item('Property Statuses',$settings->path_admin_web_ssl.'property-statuses/browse');
            $menu[] = $this->get_menu_item('Settings',$settings->path_admin_web_ssl.'settings/browse');
            $menu[] = $this->get_menu_item('Emails',$settings->path_admin_web_ssl.'emails/browse');
            $menu[] = $this->get_menu_item('Countries',$settings->path_admin_web_ssl.'countries/browse');
            $this->add_button('Settings','','70',$menu);
            
            $this->add_button('Users',$settings->path_admin_web_ssl.'users/browse','70');            
            $this->add_button('Logout',$settings->path_admin_web_ssl.'logout','70');            
        }
        
        private function get_menu_item($text,$url='') {
            return array(
                'text' => $text,
                'url' => $url,
            );
        }
        
        private function add_button($text,$url='',$width='',$menu='') {
            $this->buttons[] = array(
                'text' => $text,
                'url' => $url,
                'width' => $width,
                'menu' => $menu,
            );
        }
        
    }