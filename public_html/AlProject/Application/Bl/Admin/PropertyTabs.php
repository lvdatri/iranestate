<?php
    class Bl_Admin_PropertyTabs {
        private $activeTab;
        private $tabs;
        private $lb = "\n";
        
        public function __construct() {
            $this->activ_tab = '';
            $this->tabs = '';
			$template = Al_Template::get_instance();
			$template->css_core_include('tab.css');            
        }
        
        public function setActiveTab($id) {
            $this->activeTab = $id;
        }
        
        private function add($id,$label,$path) {
            $selected = '';
            if($this->activeTab == $id) {
                $selected = ' class="selected" '; 
            }
            $this->tabs .= '<li '.$selected.'><a href="'.$path.'"><em>'.$label.'</em></a></li>'.$this->lb;
        }
        
        public function display($propertyId) {
        	$settings = Bl_Settings::get_instance();

			$this->add('details','Details',$settings->path_admin_web_ssl.'properties/view/pid/'.$propertyId);
			$this->add('images','Images',$settings->path_admin_web_ssl.'property-images/browse/pid/'.$propertyId);
			$this->add('map','Map',$settings->path_admin_web_ssl.'property-map/browse/pid/'.$propertyId);
            $this->displayTabs();        	
        }
        
        private function displayTabs() {
			echo '<div id="tabs" class="yui-skin-sam yui-navset yui-navset-top">'.$this->lb; 
			echo '<ul class="yui-nav">'.$this->lb;             
			echo $this->tabs;
			echo '</ul>'.$this->lb;
            echo '<div class="yui-content"> </div>'.$this->lb;
            echo '</div>'.$this->lb;
        }
    }