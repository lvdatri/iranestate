<?php
	class Bl_Admin_UserLogin {
        private $_db;
		private $default_path;        
        
        public function __construct() {
            $this->_db = Al_Db::get_instance();
//            $this->default_path = _PTH_WEB_SSL.'index.php';
        }
        
        public function setDefaultPath($default_path) {
            $this->default_path = $default_path;
        }
        
        public function check($username,$password) {
        	$settings = Bl_Settings::get_instance();
	        $crypt = new Al_Crypt();
        	$password = $crypt->encrypt($password,$settings->key,true);
            
            $res = $this->_db->query("
            	SELECT 
            		id, first_name, last_name, email, permissions, last_login
            	FROM admin_users
            	WHERE username = '" . Al_Db::escape($username) . "'
            	AND password = '" . Al_Db::escape($password) . "'
            ");
            
            if($res->num_rows()>0) {
                $data = $res->fetch_row();
                $user = Al_User::get_instance();
                $user->set_data($data);
                $this->set_login_stats($data);
                return true;
            }
            return false;
        }
        
        public function set_login_stats($data) {
            $this->_db->query("
            	UPDATE admin_users SET
            		login_count = login_count + 1,
            		last_login = now()
            	WHERE id = '" . Al_Db::escape($data['id']) . "'
            ");
        }
        
    	/**
		 * redirects user to requested or default URL.
		 *
		 */
		function redirect() {
			$path = Al_Utilities::get_return_path();
			if($path=='') 
				Al_Utilities::redirect($this->default_path);
			else 
				Al_Utilities::redirect($path);
		}        
    }		