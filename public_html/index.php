<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL|E_STRICT);
	$file_path = dirname(__FILE__).'/';
	require_once $file_path.'AlProject/Application/Bl/Application.php';
	
	$application = new Bl_Application();
	$application->initialise();
	$application->dispatch();