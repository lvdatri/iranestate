Ext.namespace('Ext.al');

Ext.al.menus = function(){};

Ext.al.gridHover = function (tableId,rowOverClass) {
	if(rowOverClass==undefined) {
		rowOverClass = 'gridRowOver';
	}
	var el = Ext.select("#"+tableId+" tr");	
	el.on('mouseover',function(e,t,o){
		var me = Ext.get(this);
		me.dom.setAttribute('orgCl',me.dom.className);
		me.dom.className = 'gridRowOver';
	});
	el.on('mouseout',function(e,t,o){
		var me = Ext.get(this);
		me.dom.className = me.dom.getAttribute('orgCl');
	}); 
}

Ext.al.confirm = function(message,path) {
	var res = Ext.Msg.confirm('Confirm', message, function(btn, text,a,b,c){
		if (btn == 'yes'){
			location=path;
		}
	});	
}

Ext.al.btn = {
	floatLeft:{
		marginRight: '5px',
		float: 'left'
	},
	floatRight:{
		marginRight: '5px',
		float: 'right'
	}
}

Ext.al.hideElement = function(el) {
	var e = Ext.get(el);
	e.setVisibilityMode(Ext.Element.DISPLAY);
	e.hide();
}

Ext.al.showElement = function(el) {
	var e = Ext.get(el);
	e.setVisibilityMode(Ext.Element.DISPLAY);
	e.show();	
}

Ext.al.getLoadingAnimationSml = function() {
	return '<table style="background-color:#FFFFFF" width="100%" height="100%" border="0" cellspacing="0" cellpadding="0"><tr><td style="padding:0px" align="center" valign="middle"><img src="'+Ext.al.path+'images/loading-sml.gif" border="0" /></td></tr></table>';
}
Ext.al.getLoadingAnimationMed = function() {
	return '<table style="background-color:#FFFFFF" width="100%" height="100%" border="0" cellspacing="0" cellpadding="0"><tr><td  style="padding:0px" align="center" valign="middle"><img src="'+Ext.al.path+'images/loading.gif" border="0" /></td></tr></table>';
}


Ext.al.getCenterWindowPos = function(width,height) {
		var _x = 0;
		var _y = 0;
		var offsetX = 0;
		var offsetY = 0;
		//IE
		if(!window.pageYOffset) {
			//strict mode
			if(!(document.documentElement.scrollTop == 0)) {
				offsetY = document.documentElement.scrollTop;
				offsetX = document.documentElement.scrollLeft;
			} else {
				offsetY = document.body.scrollTop;
				offsetX = document.body.scrollLeft;
			}
		}
		else {
			offsetX = window.pageXOffset;
			offsetY = window.pageYOffset;
		}
		var b = Ext.getBody();
		
		if(width>b.getViewSize().width) {
			_x = 0;
		} else {
			_x = ((b.getViewSize().width-width)/2)+offsetX;			
		}
		
		if(height>b.getViewSize().height) {
			_y = offsetY; //((b.getHeight()-height)/2)+offsetY;
		} else {
			_y = ((b.getViewSize().height - height)/2) + offsetY;
		}
		
		return{x:_x,y:_y};
}

Ext.al.ltrim = function(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
Ext.al.rtrim = function(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

Ext.al.sendFormUri = function(formName) {
	var f = Ext.getDom(formName);
	var a = Ext.util.Format.trim(f.action);
	a = Ext.al.rtrim(a,'/');
	var path = a+'/'+Ext.al.getFormUri(formName);
	location=path;
}

Ext.al.getFormUri = function(formName) {
	var path = '';
	var e = Ext.getDom(formName).elements;
	for(var i = 0; i < e.length; i++) { 
		switch(e[i].type) {
			case 'hidden':
			case 'text':
			case 'textarea':
			case 'select-one':
				var value = e[i].value;
				value = value.trim();
				if(value != '') {
					path += encodeURIComponent(e[i].name)+'/'+encodeURIComponent(value)+'/';
				}
				break;
			case 'checkbox':
				var value = e[i].value;
				value = value.trim();
				if(e[i].checked == true) {
					path += encodeURIComponent(e[i].name)+'/'+encodeURIComponent(value)+'/';
				}			
				break;
		}
	}
	return path;
}

jQuery.namespace = function() {
    var a=arguments, o=null, i, j, d;
    for (i=0; i<a.length; i=i+1) {
        d=a[i].split(".");
        o=window;
        for (j=0; j<d.length; j=j+1) {
            o[d[j]]=o[d[j]] || {};
            o=o[d[j]];
        }
    }
    return o;
};

jQuery.namespace( 'jQuery.al' );