Ext.namespace('Ext.ux.plugins');
 
/**
 * Ext.ux.plugins.MultiLineHeader plugin for Ext.grid.GridView
 *
 * @author  Clayton L. Donahue
 * @date    October 7, 2008
 *
 * @class Ext.ux.plugins.MultiLineHeader
 * @extends Ext.util.Observable
 */
Ext.ux.plugins.MultiLineHeader = function(config) {
    Ext.apply(this, config);
};
 
// plugin code
Ext.extend(Ext.ux.plugins.MultiLineHeader, Ext.util.Observable, {
    
    /**
     *    init
     */
    init : function(grid) {
        var view = grid.getView();
        view.beforeMethod('initTemplates', this.initTemplates);
        // When the view UI is refreshed, set the header heights.
        view.on('refresh',     this.setHeaderHeight, grid );
    },
    
    /**
     *    initTemplates
     * 
     *    Sets up the GridView template for the header cell. Adds a DIV with class "ux-grid3-hd-wrap"
     *  to handle vertically aligning the header text.
     */
    initTemplates : function(){
        var ts = this.templates || {};
        
        if(!ts.hcell){
            ts.hcell = new Ext.Template(
                    '<td class="x-grid3-hd x-grid3-cell x-grid3-td-{id}" style="{style}"><div {attr} class="x-grid3-hd-inner x-grid3-hd-{id}" unselectable="on" style="{istyle} "><div {tooltip} class="ux-grid3-hd-wrap">', this.grid.enableHdMenu ? '<a class="x-grid3-hd-btn" href="#"></a>' : '',
                    '{value}<img class="x-grid3-sort-icon" src="', Ext.BLANK_IMAGE_URL, '" />',
                    "</div></div></td>"
                    );
        }
        
        this.templates = ts;
    },
    
    /**
     *    setHeaderHeight
     * 
     *    Adjusts the height of the header DIV to match that of the header TD.
     *    Also adjusts the height of the wrap DIV to that of the header DIV
     */
    setHeaderHeight : function() {
        var columns  = this.getColumnModel().config.length;
        var xView      = this.getView();
        var height      = xView.mainHd.getComputedHeight();
        
        for(var c=0; c<columns; c++) {
            
            // Check that this is a valid column (not a "numberer" or "checker"). Otherwise, the checkbox will
            // be improperly aligned to the top of the header.
            var validcol = this.getColumnModel().config[c].id; 
            
            if (validcol==='numberer' || validcol==='checker') { 
                // skip this column
            } else {
                var headerCellObj = Ext.get( xView.getHeaderCell([c]).firstChild );
                var headerWrapObj = Ext.get( headerCellObj.child('.ux-grid3-hd-wrap') );
                var headerBtnObj  = Ext.get( headerWrapObj.child('.x-grid3-hd-btn') );
                
                headerCellObj.setHeight(height - xView.mainHd.getPadding('tb'));
                headerWrapObj.setHeight(height - headerCellObj.getPadding('tb'));    // Account for inner padding.
    
                // If the height of the header is > 50, we need to replace the button image (Ext provides a 28 x 50 pixel image).
                // The image referenced by "ux-grid3-hd-btn-large" is 28 x 80 pixel image. Larger than that and you are on your own.
                if(height > 50) {
                    headerBtnObj.addClass('ux-grid3-hd-btn-large');
                } else {
                    headerBtnObj.removeClass('ux-grid3-hd-btn-large');
                }
            }
        }
    }
}); // end of extend