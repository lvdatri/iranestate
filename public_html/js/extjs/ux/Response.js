Ext.namespace('Ext.ux.al');
Ext.ux.al.Response = function() {
	var data;
	
	this.setResponse = function(response) {
		try	{
			this.data = Ext.util.JSON.decode(response.responseText);
		}
		catch(err) {
			Ext.MessageBox.alert('Server Error', response.responseText);
			return false;
		}
		
		var access = this.getDataSection('access');
		if(access != null) {
			if(access.status == '1') {
				var e = this.getDataSection('error');
				if(e != null && e.status == '1') {
					Ext.MessageBox.alert('Server Error', e.message);				
					return false;
				}
				return true;
			}
		}
		window.location = Ext.al.login_path;
	}

	this.getData = function() {
		return data;
	}
	
	this.getDataSection = function(section) {
		if(this.data[section] != undefined) {
			return this.data[section];
		}
		return null;
	}
	
	this.getDataSectionRecords = function(section) {
		var s = this.getDataSection(section);
		if(s != null) {
			var rs = new Array();			
			for ( var i in s ) {
				rs[i] = (new Ext.data.Record(s[i]));
			}
			return rs;
		}
		return null;
	}
	
	this.isValid = function() {
		var validation = this.getDataSection('validation');
		if(validation != null) {
			if(validation.length>0) {
				return false;
			}
		}
		return true;
	}
	
	this.displayValidationErrors = function() {
		var validation = this.getDataSection('validation');
		if(validation != null) {
			if(validation.length>0) {
				Ext.Msg.show({
				   title:'Validation error',
				   buttons: Ext.Msg.OK,
				   msg: validation[0],
				   icon: Ext.MessageBox.WARNING
				});		
			}
		}
	}
	
	

};