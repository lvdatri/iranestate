<script type="text/javascript">
Ext.al.pageReady = function() {
	var b = new Ext.Button({
		renderTo:'pageActions',
		text:'Add Email',
		minWidth:'80',
		style: Ext.al.btn.floatRight,
		handler: function(e) {
			this.disable();
			location='index.php?action=insert';
		}
	});
}
</script>
<div class="page_header page_header_a"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="71%"><h1 class="page_title"><?php echo $_nav->show(); ?></h1> </td>
    <td width="29%" align="right" id="pageActions" >
      </td>
  </tr>
</table>
</div>
<div class="page">
<?php $_message->show(); ?>
<table class="grid" id="dataGrid" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th width="1%">id</th>
      <th><?php $sorter->show_column('title'); ?></th>
      <th width="30%" ><?php $sorter->show_column('subject'); ?></th>
      <th width="1%" nowrap="nowrap" ><?php $sorter->show_column('send_to'); ?></th>
      <th width="1%">&nbsp;</th>
    </tr>
    <?php while($row = $result->fetch_row()) { ?>
        <tr>
          <td valign="top"><?php echo $row['id'];?></td>
          <td valign="top"><?php echo $row['title'];?>
          <?php if($row['description'] != '') { ?>
          	<div class="smlTxt1" style="margin-top:3px;">
            <?php echo $row['description']; ?>
            </div>
          <?php } ?>
          </td>
          <td align="left" valign="top"><?php echo $row['subject']; ?></td>
          <td align="left" valign="top" nowrap="nowrap"><?php echo $row['send_to_frmt']; ?></td>
          <td valign="top" nowrap="nowrap"><input onclick="location='index.php?action=update&id=<?php echo $row['id']; ?>'" class="btnSml" type="button" name="btnUpdate" id="btnUpdate" value="Update" />
          <?php if($row['locked'] != '1') { ?>
            <input onclick="Ext.al.confirm('Delete Item?','index.php?action=delete&id=<?php echo $row['id']; ?>');" class="btnSml" type="button" name="btnDelete" id="btnDelete" value="Delete" />
          <?php } ?>
		</td>
        </tr>
    <?php } ?>    
    <?php if($pager->total_num_of_records < 1) { ?>
    <tr>
      <td colspan="5">No records found.</td>
    </tr>
    <?php } ?>
  </table>
  <?php if($pager->total_num_of_records > 0) { ?>
<div class="gridFoot">
      <div class="gridNav">
      <?php $pager->display_nav(); ?>
    </div>
      <div class="gridFootTxt">
	  <?php echo $pager->starting_record_num; ?> to <?php echo $pager->ending_record_num; ?> of <?php echo $pager->total_num_of_records; ?> records
      </div>
  </div> 
  <?php } ?>
 
</div>
