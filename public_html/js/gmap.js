jQuery.al.gmap_set = function(mapId,lat,long,depth) {
	var latlng = new google.maps.LatLng(lat, long);
	var myOptions = {
		zoom: depth,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP	
	};
	var map = new google.maps.Map(document.getElementById(mapId), myOptions );
	
	return map;
}
	
jQuery.al.gmap_setMarker = function(map,lat,long,pthMarker,pthShadow,dragable) {
	if(dragable == undefined) {
		dragable = false;
	}
	var latlng = new google.maps.LatLng(lat, long);
	
	var image = new google.maps.MarkerImage(
		pthMarker,
		new google.maps.Size(32, 32)
	);
	  
	var shadow = new google.maps.MarkerImage(
		pthShadow, 
		new google.maps.Size(40, 37)
	);		
		
	var marker = new google.maps.Marker({
		position: latlng,
		map: map,
		shadow: shadow,
		icon: image,
		draggable: dragable
	});		
	
	return marker;
}