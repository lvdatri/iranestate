jQuery.namespace = function() {
    var a=arguments, o=null, i, j, d;
    for (i=0; i<a.length; i=i+1) {
        d=a[i].split(".");
        o=window;
        for (j=0; j<d.length; j=j+1) {
            o[d[j]]=o[d[j]] || {};
            o=o[d[j]];
        }
    }
    return o;
};

jQuery.namespace( 'jQuery.al' );


jQuery.al.ltrim = function(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
jQuery.al.rtrim = function(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}


jQuery.al.confirm = function($message,$path) {
	if(confirm($message))  	{
		location=$path;
	}
}
	
jQuery.al.sendFormUri = function(formName) {
	var f = $('#'+formName)['0'];
	var a = $.trim(f.action);
	a = $.al.rtrim(a,'/');
	var path = a+'/'+$.al.getFromUri(formName); //Ext.al.getFormUri(formName);
	location=path;
}

jQuery.al.getAdvSrchUri = function(formName) {
	
	var path = '';
	var e = $('#'+formName)['0'].elements;	
	
	var intro = '';
	var feat = '';
	var type = '';
	
	for(var i = 0; i < e.length; i++) { 
		switch(e[i].type) {
			case 'hidden':
			case 'text':
			case 'textarea':
			case 'select-one':	
	
				var value = e[i].value;
				value = $.trim(value);
				if(value != '') {
					path += encodeURIComponent(e[i].name)+'/'+encodeURIComponent(value)+'/';
				}
				break;
			case 'checkbox':
				if(e[i].checked == true) {
					var value = e[i].value;
					var name = e[i].name;
	
					if($.al.isCbType(name,"i_")) {
						intro += encodeURIComponent(value)+'.';
					}
					
					if($.al.isCbType(name,"f_")) {
						feat += encodeURIComponent(value)+'.';
					}		
					
					if($.al.isCbType(name,"t_")) {
						type += encodeURIComponent(value)+'.';
					}				
				} 
				break;
		}
	}
	
	intro =  $.al.rtrim(intro,'.');
	feat =  $.al.rtrim(feat,'.');
	type = 	$.al.rtrim(type,'.');
	
	if(intro != '') {
		path += 'intro/'+intro+'/';
	}
	if(feat != '') {
		path += 'feat/'+feat+'/';
	}
	if(type != '') {
		path += 'type/'+type+'/';
	}
	

	return path;	
}

jQuery.al.isCbType = function (name,type) {
	var pos=name.indexOf(type)
	if(pos==0) {
		return true;
	} 
	return false;
}


jQuery.al.getFromUri = function(formName) {
	var path = '';
	var e = $('#'+formName)['0'].elements;	
	for(var i = 0; i < e.length; i++) { 
		switch(e[i].type) {
			case 'hidden':
			case 'text':
			case 'textarea':
			case 'select-one':	
				var value = e[i].value;
				value = $.trim(value);
				if(value != '') {
					path += encodeURIComponent(e[i].name)+'/'+encodeURIComponent(value)+'/';
				}
				break;
			case 'checkbox':
				var value = e[i].value;
				value = $.trim(value);
				if(e[i].checked == true) {
					path += encodeURIComponent(e[i].name)+'/'+encodeURIComponent(value)+'/';
				}			
				break;
		}
	}
	return path;	
}


$.fn.setDefaultValue = function(options)
{
    var options = $.extend({ 
		css : 'txt_default'
	}, options);
    
    return this.each(function()
    {
        var $this = $(this);
        
        var value = $this.attr('title');
        
        $(this).attr('autocomplete','off');
        
        if($this.val().length <= 0 || $this.val() == value)
        {
            $this.addClass(options.css);
            $this.val(value);
        }
        
        $this.click(clear);
        
        $this.blur(fill);
        
        $this.focus(clear).blur();
        
        function clear()
        {
            if($this.hasClass(options.css) || $this.val() == value)
            {
                $this.val('');
                $this.removeClass(options.css);
            }
        }
        
        function fill()
        {
            if($this.val().length <= 0)
            {
                $this.addClass(options.css);
                $this.val(value);
            }
        }
        $(window).unload(clear); // Firefox-Autocomplete
    });  
};






	jQuery.al.refineSearchFields = {
		'price': {'visible':false},
		'deposit': {'visible':false},			
		'propType': {'visible':false},			
		'bed': {'visible':false},			
		'bath': {'visible':false},			
		'park': {'visible':false},			
		'intro': {'visible':false},			
		'feat': {'visible':false},			
		'land': {'visible':false},
		'build': {'visible':false}
	};	
	
	
	jQuery.al.refineSearchCloseAll = function(section) {
		for( var i in jQuery.al.refineSearchFields ) {
			if(jQuery.al.refineSearchFields[i].visible == true) {
				if(section != i) {
					jQuery.al.refineSearchToggle(i);
				}
			}
		}
	}
	
	jQuery.al.refineSearchToggle = function(val,e) {
		$("#"+val+"SearchRes").toggle();
		$("#"+val+"Form").toggle();	
		$("#"+val+"Title h3").toggleClass('right-arrow down-arrow');
		if(e != null) {
			e.preventDefault();											
		}
		if(jQuery.al.refineSearchFields[val].visible ==  true) {
			jQuery.al.refineSearchFields[val].visible = false;
		} else {
			jQuery.al.refineSearchFields[val].visible = true;
		}
	}
	
	jQuery.al.refineSearchInitialise = function() {
		jQuery.al.refineSearchToggle('price');
		
		$("#priceTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('price');
			jQuery.al.refineSearchToggle('price',e);	
		});
		
		$("#depositTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('deposit');
			jQuery.al.refineSearchToggle('deposit',e);	
		});				
		
		$("#propTypeTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('propType');
			jQuery.al.refineSearchToggle('propType',e);	
		});		
		
		$("#bedTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('bed');
			jQuery.al.refineSearchToggle('bed',e);	
		});	
		
		$("#bathTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('bath');
			jQuery.al.refineSearchToggle('bath',e);	
		});				
		
		$("#parkTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('park');
			jQuery.al.refineSearchToggle('park',e);	
		});			
		
		$("#introTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('intro');
			jQuery.al.refineSearchToggle('intro',e);	
		});		
		$("#featTitle").click(function(e) {
			jQuery.al.refineSearchCloseAll('feat');
			jQuery.al.refineSearchToggle('feat',e);	
		});		
		
		$("#landTitle").click(function(e) {
			if(jQuery.al.refineSearchFields['feat'].visible==true) {
				jQuery.al.refineSearchCloseAll('land');
				jQuery.al.refineSearchToggle('land');	
			} else {
				jQuery.al.refineSearchCloseAll('land');				
				jQuery.al.refineSearchToggle('land',e);	
			}
		});		

		$("#buildTitle").click(function(e) {
			if(jQuery.al.refineSearchFields['feat'].visible==true) {
				jQuery.al.refineSearchCloseAll('build');
				jQuery.al.refineSearchToggle('build');	
			} else {
				jQuery.al.refineSearchCloseAll('build');				
				jQuery.al.refineSearchToggle('build',e);	
			}			
		});					
	}